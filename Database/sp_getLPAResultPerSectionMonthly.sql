USE [mhlpa_db_dev]
GO
/****** Object:  StoredProcedure [dbo].[sp_getLPAResultPerSectionMonthly]    Script Date: 1/2/2020 4:41:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER procedure [dbo].[sp_getLPAResultPerSectionMonthly](@p_region_id		INT=null,
											@p_country_id		INT=null,
											@p_location_id		INT=null,
	@p_building_id	INT=NULL,
											@p_line_id			INT=null,
											@p_date				DATE)
AS
BEGIN

	DECLARE @l_region_id		INT;
	DECLARE @l_country_id		INT;
	DECLARE @l_location_id		INT;
	DECLARE @l_line_id		INT;
	DECLARE @i					INT;
	DECLARE @l_count				INT;
	DECLARE @l_date				DATE;
	   DECLARE @l_building_id            INT;

	DECLARE @l_tempTable TABLE
	(
	audit_date		DATE,
	sec1_score		NUMERIC(10,5),
	sec2_score		NUMERIC(10,5),
	sec3_score		NUMERIC(10,5),
	sec4_score		NUMERIC(10,5),
	sec5_score		NUMERIC(10,5),
	sec6_score		NUMERIC(10,5)
	);
	IF @p_building_id IS NULL OR @p_building_id = 0 
              SET @l_building_id = NULL;
       ELSE
              SET @l_building_id = @p_building_id;

	IF @p_region_id IS NULL OR @p_region_id = 0 
		SET @l_region_id = NULL;
	ELSE
		SET @l_region_id = @p_region_id;

	IF @p_country_id IS NULL OR @p_country_id = 0 
		SET @l_country_id = NULL;
	ELSE
		SET @l_country_id = @p_country_id;

	IF @p_location_id IS NULL OR @p_location_id = 0 
		SET @l_location_id = NULL;
	ELSE
		SET @l_location_id = @p_location_id;

	IF @p_line_id IS NULL OR @p_line_id = 0 
		SET @l_line_id = NULL;
	ELSE
		SET @l_line_id = @p_line_id;

	INSERT INTO @l_tempTable
	SELECT -- FORMAT(audit_date,'MMM-yyyy') audit_period,
			CONVERT(DATE, '01-'+FORMAT(sc.audit_date,'MM-yyyy'),105),
			ISNULL(AVG(sc.sec1_score),0.00) 'GP',
			ISNULL(AVG(sc.sec2_score),0.00) 'PD',
			ISNULL(AVG(sc.sec3_score),0.00) 'SOCWA',
			ISNULL(AVG(sc.sec4_score),0.00) 'RPE',
			ISNULL(AVG(sc.sec5_score),0.00) 'SW',
			ISNULL(AVG(sc.sec6_score),0.00) 'RPS'
	FROM    mh_lpa_score_summary sc LEFT JOIN mh_lpa_line_master lm ON sc.line_id=lm.line_id
	WHERE   sc.region_id = ISNULL(@l_region_id, sc.region_id)
	AND     sc.country_id = ISNULL(@l_country_id, sc.country_id)
	AND     sc.location_id = ISNULL(@l_location_id, sc.location_id)
		AND		lm.building_id=ISNULL(@l_building_id,lm.building_id)
	AND     sc.line_id = ISNULL(@l_line_id, sc.line_id)
	AND     sc.audit_date BETWEEN DATEADD(year,-1,@p_date) AND @p_date
	-- 		  AND     FORMAT(audit_date,'MMM-yyyy') = FORMAT(@p_date,'MMM-yyyy')
	GROUP BY CONVERT(DATE, '01-'+FORMAT(sc.audit_date,'MM-yyyy'),105);


	SET @i = 0;
	WHILE @i < 13
	BEGIN
		SET @l_count = 0;
		SET @l_date = CONVERT(DATE, '01-'+FORMAT(DATEADD(month,-@i,@p_date),'MM-yyyy'),105);
		SELECT @l_count = count(*)
		FROM   @l_tempTable
		WHERE  audit_date = @l_date;

		IF @l_count = 0 
			INSERT INTO @l_tempTable VALUES(@l_date,0.0, 0.0, 0.0, 0.0, 0.0, 0.0);

		SET @i = @i+1; 
	END;

	SELECT * FROM (select 
							FORMAT(audit_date,'MMM-yyyy') audit_period,
							audit_date,
							ISNULL(sec1_score,0)		'GT',
/*							sec2_score		'PD',
							sec3_score		'SOCWA',
							sec4_score		'RPE',
							sec5_score		'SW',
							sec6_score		'RPS'
							*/
							ISNULL(sec2_score,0)		'A',
							ISNULL(sec3_score,0)		'B',
							ISNULL(sec4_score,0)		'C',
							ISNULL(sec5_score,0)		'D',
							ISNULL(sec6_score,0)		'E'
					FROM @l_tempTable) t
					ORDER BY audit_date;


	
END


