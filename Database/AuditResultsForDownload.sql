USE [mhlpa_db_dev]
GO
/****** Object:  StoredProcedure [dbo].[AuditResultsForDownload]    Script Date: 1/8/2020 3:40:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[AuditResultsForDownload](@p_region_id   int,
		@p_country_id   int,
		@p_location_id   int,
	@p_building_id	INT=null,
		@p_line_id   int,
        @l_st_date    DATE,
        @l_end_date    DATE
        )
AS
 SET NOCOUNT ON;
 
BEGIN

IF @p_region_id IS NULL OR @p_region_id = 0 
  SET @p_region_id = NULL;
 ELSE
  SET @p_region_id = @p_region_id;
 IF @p_country_id IS NULL OR @p_country_id = 0 
  SET @p_country_id = NULL;
 ELSE
  SET @p_country_id = @p_country_id;
 IF @p_location_id IS NULL OR @p_location_id = 0 
  SET @p_location_id = NULL;
 ELSE
  SET @p_location_id = @p_location_id;

 IF @p_building_id IS NULL OR @p_building_id = 0 
  SET @p_building_id = NULL
 
 IF @p_line_id IS NULL OR @p_line_id = 0 
  SET @p_line_id = NULL;
 ELSE
  SET @p_line_id = @p_line_id;


	SELECT audit_date,
		  region_name,
		  country_name,
		  location_name,
		  bm.building_name,
		  lm.line_name,
		  pm.product_code,
		  CASE [q1_answer]
			WHEN 0 THEN 'YES'
			WHEN 1 THEN 'NO'
			ELSE 'N/A'
		  END Q1,
		  CASE [q2_answer]
			WHEN 0 THEN 'YES'
			WHEN 1 THEN 'NO'
			ELSE 'N/A'
		  END Q2,
		  CASE [q3_answer]
			WHEN 0 THEN 'YES'
			WHEN 1 THEN 'NO'
			ELSE 'N/A'
		  END Q3,
		  CASE [q4_answer]
			WHEN 0 THEN 'YES'
			WHEN 1 THEN 'NO'
			ELSE 'N/A'
		  END Q4,
		  CASE [q5_answer]
			WHEN 0 THEN 'YES'
			WHEN 1 THEN 'NO'
			ELSE 'N/A'
		  END Q5,
		  CASE [q6_answer]
			WHEN 0 THEN 'YES'
			WHEN 1 THEN 'NO'
			ELSE 'N/A'
		  END Q6,
		  CASE [q7_answer]
			WHEN 0 THEN 'YES'
			WHEN 1 THEN 'NO'
			ELSE 'N/A'
		  END Q7,
		  CASE [q8_answer]
			WHEN 0 THEN 'YES'
			WHEN 1 THEN 'NO'
			ELSE 'N/A'
		  END Q8,
		  CASE [q9_answer]
			WHEN 0 THEN 'YES'
			WHEN 1 THEN 'NO'
			ELSE 'N/A'
		  END Q9,
		  CASE [q10_answer]
			WHEN 0 THEN 'YES'
			WHEN 1 THEN 'NO'
			ELSE 'N/A'
		  END Q10,
		  CASE [q11_answer]
			WHEN 0 THEN 'YES'
			WHEN 1 THEN 'NO'
			ELSE 'N/A'
		  END Q11,
		  CASE [q12_answer]
			WHEN 0 THEN 'YES'
			WHEN 1 THEN 'NO'
			ELSE 'N/A'
		  END q12,
		  CASE [q13_answer]
			WHEN 0 THEN 'YES'
			WHEN 1 THEN 'NO'
			ELSE 'N/A'
		  END q13,
		  CASE [q14_answer]
			WHEN 0 THEN 'YES'
			WHEN 1 THEN 'NO'
			ELSE 'N/A'
		  END q14,
		  CASE [q15_answer]
			WHEN 0 THEN 'YES'
			WHEN 1 THEN 'NO'
			ELSE 'N/A'
		  END q15,
		  CASE [q16_answer]
			WHEN 0 THEN 'YES'
			WHEN 1 THEN 'NO'
			ELSE 'N/A'
		  END q16,
		  CASE [q17_answer]
			WHEN 0 THEN 'YES'
			WHEN 1 THEN 'NO'
			ELSE 'N/A'
		  END q17,
		  CASE [q18_answer]
			WHEN 0 THEN 'YES'
			WHEN 1 THEN 'NO'
			ELSE 'N/A'
		  END q18,
		  CASE [q19_answer]
			WHEN 0 THEN 'YES'
			WHEN 1 THEN 'NO'
			ELSE 'N/A'
		  END q19,
		  CASE [q20_answer]
			WHEN 0 THEN 'YES'
			WHEN 1 THEN 'NO'
			ELSE 'N/A'
		  END q20,
		  CASE [q21_answer]
			WHEN 0 THEN 'YES'
			WHEN 1 THEN 'NO'
			ELSE 'N/A'
		  END q21,
		  CASE [q22_answer]
			WHEN 0 THEN 'YES'
			WHEN 1 THEN 'NO'
			ELSE 'N/A'
		  END q22,
		  CASE [q23_answer]
			WHEN 0 THEN 'YES'
			WHEN 1 THEN 'NO'
			ELSE 'N/A'
		  END q23,
		  CASE [q24_answer]
			WHEN 0 THEN 'YES'
			WHEN 1 THEN 'NO'
			ELSE 'N/A'
		  END q24,
		  CASE [q25_answer]
			WHEN 0 THEN 'YES'
			WHEN 1 THEN 'NO'
			ELSE 'N/A'
		  END q25
	  FROM [mhlpa_db].[dbo].[mh_lpa_score_summary] s,
			mh_lpa_region_master rm,
			mh_lpa_country_master cm,
			mh_lpa_location_master loc,
			mh_lpa_line_master lm,
			mh_lpa_product_master pm,
			mh_lpa_building_master bm
	WHERE s.region_id = rm.region_id
	AND   s.country_id = cm.country_id
	AND   s.location_id = loc.location_id
	AND   s.line_id =lm.line_id
	AND   s.part_id = pm.product_id
	AND		lm.building_id=bm.building_id
	AND   s.region_id = ISNULL(@p_region_id, s.region_id)
	AND   s.country_id = ISNULL(@p_country_id, s.country_id)
	AND   s.location_id = ISNULL(@p_location_id, s.location_id)
	AND   s.line_id = ISNULL(@p_line_id, s.line_id)
	AND   lm.building_id=ISNULL(@p_building_id, lm.building_id)
	AND   s.audit_date BETWEEN @l_st_date AND @l_end_date;

END;
