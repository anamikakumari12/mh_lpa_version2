USE [mhlpa_db_dev]
GO
/****** Object:  StoredProcedure [dbo].[sp_getAuditPlanReport]    Script Date: 2/12/2020 11:00:24 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec sp_getSQDCReport 1,1,1,2017,12
ALTER PROCEDURE [dbo].[sp_getSQDCReport]
(
@l_region_id INT=NULL,
@l_country_id INT=NULL,
@l_location_id INT=NULL
)
AS
BEGIN
if(@l_region_id=0)
SET  @l_region_id = null
if(@l_country_id=0)
SET  @l_country_id = null
if(@l_location_id=0)
SET  @l_location_id = null

DECLARE @sqdc TABLE(Name VARCHAR(200), Jan VARCHAR(10), Feb VARCHAR(10), Mar VARCHAR(10), Apr VARCHAR(10), May VARCHAR(10), Jun VARCHAR(10), Jul VARCHAR(10), Aug VARCHAR(10), Sep VARCHAR(10), Oct VARCHAR(10), Nov VARCHAR(10), Dec VARCHAR(10), YTD VARCHAR(10) )

INSERT INTO @sqdc(Name, Jan, Feb, Mar, Apr, May, Jun, Jul, Aug, Sep, Oct, Nov, Dec )
select u.role, COUNT(ans1.audit_id) ,COUNT(ans2.audit_id), 
COUNT(ans3.audit_id),COUNT(ans4.audit_id), COUNT(ans5.audit_id),COUNT(ans6.audit_id), COUNT(ans7.audit_id),COUNT(ans8.audit_id),
 COUNT(ans9.audit_id),COUNT(ans10.audit_id), COUNT(ans11.audit_id),COUNT(ans12.audit_id) from mh_lpa_user_master u 
LEFT JOIN mh_lpa_local_answers ans1 ON u.user_id=ans1.audited_by_user_id AND Month(ans1.audit_date)=1 AND YEAR(ans1.audit_date)=2017
LEFT JOIN mh_lpa_local_answers ans2 ON u.user_id=ans2.audited_by_user_id AND Month(ans2.audit_date)=2 AND YEAR(ans2.audit_date)=2017
LEFT JOIN mh_lpa_local_answers ans3 ON u.user_id=ans3.audited_by_user_id AND Month(ans3.audit_date)=3 AND YEAR(ans3.audit_date)=2017
LEFT JOIN mh_lpa_local_answers ans4 ON u.user_id=ans4.audited_by_user_id AND Month(ans4.audit_date)=4 AND YEAR(ans4.audit_date)=2017
LEFT JOIN mh_lpa_local_answers ans5 ON u.user_id=ans5.audited_by_user_id AND Month(ans5.audit_date)=5 AND YEAR(ans5.audit_date)=2017
LEFT JOIN mh_lpa_local_answers ans6 ON u.user_id=ans6.audited_by_user_id AND Month(ans6.audit_date)=6 AND YEAR(ans6.audit_date)=2017
LEFT JOIN mh_lpa_local_answers ans7 ON u.user_id=ans7.audited_by_user_id AND Month(ans7.audit_date)=7 AND YEAR(ans7.audit_date)=2017
LEFT JOIN mh_lpa_local_answers ans8 ON u.user_id=ans8.audited_by_user_id AND Month(ans8.audit_date)=8 AND YEAR(ans8.audit_date)=2017
LEFT JOIN mh_lpa_local_answers ans9 ON u.user_id=ans9.audited_by_user_id AND Month(ans9.audit_date)=9 AND YEAR(ans9.audit_date)=2017
LEFT JOIN mh_lpa_local_answers ans10 ON u.user_id=ans10.audited_by_user_id AND Month(ans10.audit_date)=10 AND YEAR(ans10.audit_date)=2017
LEFT JOIN mh_lpa_local_answers ans11 ON u.user_id=ans11.audited_by_user_id AND Month(ans11.audit_date)=11 AND YEAR(ans11.audit_date)=2017
LEFT JOIN mh_lpa_local_answers ans12 ON u.user_id=ans12.audited_by_user_id AND Month(ans12.audit_date)=12 AND YEAR(ans12.audit_date)=2017
GROUP BY u.role


--INSERT INTO @sqdc(Name, Jan, Feb, Mar, Apr, May, Jun, Jul, Aug, Sep, Oct, Nov, Dec)
--select l.line_name, COUNT(ans1.audit_id) ,COUNT(ans2.audit_id), 
--COUNT(ans3.audit_id),COUNT(ans4.audit_id), COUNT(ans5.audit_id),COUNT(ans6.audit_id), COUNT(ans7.audit_id),COUNT(ans8.audit_id),
-- COUNT(ans9.audit_id),COUNT(ans10.audit_id), COUNT(ans11.audit_id),COUNT(ans12.audit_id) from mh_lpa_line_master l
--LEFT JOIN (select line_id, count(*) audit_id FROM mh_lpa_local_answers WHERE Month(audit_date)=1 AND YEAR(audit_date)=2018 GROUP BY location_id) ans1 ON l.line_id=ans1.line_id 
--LEFT JOIN (select line_id, count(*) audit_id FROM mh_lpa_local_answers WHERE Month(audit_date)=1 AND YEAR(audit_date)=2018 GROUP BY location_id) ans2 ON l.line_id=ans2.line_id 
--LEFT JOIN (select line_id, count(*) audit_id FROM mh_lpa_local_answers WHERE Month(audit_date)=1 AND YEAR(audit_date)=2018 GROUP BY location_id) ans3 ON l.line_id=ans3.line_id 
--LEFT JOIN (select line_id, count(*) audit_id FROM mh_lpa_local_answers WHERE Month(audit_date)=1 AND YEAR(audit_date)=2018 GROUP BY location_id) ans4 ON l.line_id=ans4.line_id 
--LEFT JOIN (select line_id, count(*) audit_id FROM mh_lpa_local_answers WHERE Month(audit_date)=1 AND YEAR(audit_date)=2018 GROUP BY location_id) ans5 ON l.line_id=ans5.line_id 
--LEFT JOIN (select line_id, count(*) audit_id FROM mh_lpa_local_answers WHERE Month(audit_date)=1 AND YEAR(audit_date)=2018 GROUP BY location_id) ans6 ON l.line_id=ans6.line_id 
--LEFT JOIN (select line_id, count(*) audit_id FROM mh_lpa_local_answers WHERE Month(audit_date)=1 AND YEAR(audit_date)=2018 GROUP BY location_id) ans7 ON l.line_id=ans7.line_id 
--LEFT JOIN (select line_id, count(*) audit_id FROM mh_lpa_local_answers WHERE Month(audit_date)=1 AND YEAR(audit_date)=2018 GROUP BY location_id) ans8 ON l.line_id=ans8.line_id 
--LEFT JOIN (select line_id, count(*) audit_id FROM mh_lpa_local_answers WHERE Month(audit_date)=1 AND YEAR(audit_date)=2018 GROUP BY location_id) ans9 ON l.line_id=ans9.line_id 
--LEFT JOIN (select line_id, count(*) audit_id FROM mh_lpa_local_answers WHERE Month(audit_date)=1 AND YEAR(audit_date)=2018 GROUP BY location_id) ans10 ON l.line_id=ans10.line_id
--LEFT JOIN (select line_id, count(*) audit_id FROM mh_lpa_local_answers WHERE Month(audit_date)=1 AND YEAR(audit_date)=2018 GROUP BY location_id) ans11 ON l.line_id=ans11.line_id
--LEFT JOIN (select line_id, count(*) audit_id FROM mh_lpa_local_answers WHERE Month(audit_date)=1 AND YEAR(audit_date)=2018 GROUP BY location_id) ans12 ON l.line_id=ans12.line_id

----WHERE l.location_id=ISNULL(@l_location_id,l.location_id)
----AND l.region_id=ISNULL(@l_region_id,l.region_id)
----AND l.country_id=ISNULL(@l_country_id,l.country_id)
--GROUP BY l.location_name






END


