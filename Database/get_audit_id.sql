USE [MH_LPA-TEST]
GO
/****** Object:  UserDefinedFunction [dbo].[get_audit_id]    Script Date: 20-04-2020 09:55:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


ALTER FUNCTION [dbo].[get_audit_id](@p_audit_id INT)
RETURNS VARCHAR(1000)
AS
BEGIN
 DECLARE @l_count    INT;
 DECLARE @l_location_id   INT;
 DECLARE @l_ret_val   VARCHAR(1000);
 

 SELECT @l_ret_val = CONCAT(CONVERT(varchar, audit_date,111),'-',line_name,'-',la.audit_id)
 FROM   mh_lpa_local_answers la,
		mh_lpa_line_master lm
 WHERE  la.line_id = lm.line_id
 AND    la.audit_id = @p_audit_id;
 
 RETURN(@l_ret_val);
 
END;

