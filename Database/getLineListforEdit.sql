
--exec getLineListforEdit 17,1
ALTER PROCEDURE [dbo].[getLineListforEdit](@p_user_id		INT, @p_inactive_flag INT)
AS
BEGIN
	DECLARE @l_count 			INT;
	DECLARE @l_location_id 			INT;
	DECLARE @l_global_admin			VARCHAR(1);
	DECLARE @l_local_admin			VARCHAR(1);


	SELECT @l_global_admin = ISNULL(global_admin_flag,'N'),
		@l_local_admin = ISNULL(site_admin_flag,'N'),
		@l_location_id = location_id
	FROM   mh_lpa_user_master
	WHERE  user_id = @p_user_id;
	if(@p_inactive_flag=1)
	BEGIN
	SELECT  
			rm.region_id,
			rm.region_name,
			cm.country_id,
			cm.country_name,
			lm.location_id,
			lm.location_name,
			lne.line_id,
			lne.line_name,
			lne.line_code,
			lne.start_date,
			lne.end_date,
			lne.distribution_list,
			bm.building_name
	FROM   mh_lpa_region_master rm,
			mh_lpa_country_master cm,
			mh_lpa_location_master lm,
			mh_lpa_line_master lne,
			mh_lpa_building_master bm
	WHERE  lne.region_id = rm.region_id
	AND    lne.country_id = cm.country_id
	AND    lne.location_id = lm.location_id
	AND lne.building_id=bm.building_id
	AND		(
			(ISNULL(@l_local_admin,'N') = 'Y' AND lne.location_id = @l_location_id) OR
			(ISNULL(@l_global_admin,'N') = 'Y')
			)
	--AND    ISNULL(lne.end_date, DATEADD(day,1,getdate())) >= getdate()
	ORDER BY rm.region_name,
			cm.country_name,
			lm.location_name,
			lne.line_name;
END
	ELSE
	BEGIN 
	SELECT  
			rm.region_id,
			rm.region_name,
			cm.country_id,
			cm.country_name,
			lm.location_id,
			lm.location_name,
			lne.line_id,
			lne.line_name,
			lne.line_code,
			lne.start_date,
			lne.end_date,
			lne.distribution_list,
			bm.building_name
	FROM   mh_lpa_region_master rm,
			mh_lpa_country_master cm,
			mh_lpa_location_master lm,
			mh_lpa_line_master lne,
			mh_lpa_building_master bm
	WHERE  lne.region_id = rm.region_id
	AND    lne.country_id = cm.country_id
	AND    lne.location_id = lm.location_id
	AND lne.building_id=bm.building_id
	AND		(
			(ISNULL(@l_local_admin,'N') = 'Y' AND lne.location_id = @l_location_id) OR
			(ISNULL(@l_global_admin,'N') = 'Y')
			)
	AND    ISNULL(lne.end_date, DATEADD(day,1,getdate())) >= FORMAT(getdate(),'yyyy-MM-dd')
	ORDER BY rm.region_name,
			cm.country_name,
			lm.location_name,
			lne.line_name;
		END
END;

