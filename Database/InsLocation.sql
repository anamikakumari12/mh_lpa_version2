USE [mhlpa_db]
GO
/****** Object:  StoredProcedure [dbo].[InsLocation]    Script Date: 12/6/2019 1:15:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[InsLocation](@p_location_id			int=null,
								@p_region_name				VARCHAR(100),
								@p_country_name				VARCHAR(100),
								@p_location_code			VARCHAR(10)=null,
								@p_location_name			VARCHAR(100),
								@STARTDATE                  DATE=NULL,
								@ENDDATE                    DATE=NULL,
								@p_timezone_code			VARCHAR(100),
								@p_timezone_desc			VARCHAR(100),
								@p_no_of_shifts				int,
								@p_shift1_st_time			time(0),
								@p_shift1_end_time			time(0),
								@p_shift2_st_time			time(0),
								@p_shift2_end_time			time(0),
								@p_shift3_st_time			time(0),
								@p_shift3_end_time			time(0),
								@l_err_code				int OUTPUT,
								@l_err_message			varchar(100) OUTPUT
								)
AS
	SET NOCOUNT ON;
	
BEGIN
	DECLARE @l_count 			INT;
	DECLARE @l_location_id		INT;
	DECLARE @l_country_id		INT;
	DECLARE @l_region_id		INT;
	DECLARE @l_line_id			INT;
	DECLARE @l_product_id		INT;
	

	EXEC getRegionId  NULL, @p_region_name, @l_region_id = @l_region_id OUTPUT;
	EXEC getCountryId  NULL, @p_country_name, @l_country_id = @l_country_id OUTPUT;

	SET @l_err_code = 0;
	SET @l_err_message = 'Initializing';

	IF @p_location_id IS NOT NULL AND @p_location_id != 0 
	BEGIN
			UPDATE mh_lpa_location_master
			SET 
				region_id = @l_region_id,
				country_id = @l_country_id,
				location_code = @p_location_code,
				location_name = @p_location_name,
				start_date = @STARTDATE,
				end_date=@ENDDATE,
				timezone_code = @p_timezone_code,
				timezone_desc = @p_timezone_desc,
				no_of_shifts = @p_no_of_shifts,
				/*
				shift1_start_time = CAST((FORMAT(@p_shift1_st_time,'hh:mm') + ':00 ' +  @p_timezone_code) as datetimeoffset),
				shift1_end_time = CAST(( FORMAT(@p_shift1_end_time,'hh:mm') + ':00 ' +  @p_timezone_code) as datetimeoffset),
				shift2_start_time = CAST(( FORMAT(@p_shift2_st_time,'hh:mm') + ':00 ' +  @p_timezone_code) as datetimeoffset),
				shift2_end_time = CAST(( FORMAT(@p_shift2_end_time,'hh:mm') + ':00 ' +  @p_timezone_code) as datetimeoffset),
				shift3_start_time = CAST(( FORMAT(@p_shift3_st_time,'hh:mm') + ':00 ' +  @p_timezone_code) as datetimeoffset),
				shift3_end_time = CAST(( FORMAT(@p_shift3_end_time,'hh:mm') + ':00 ' +  @p_timezone_code) as datetimeoffset)
				*/
				shift1_start_time = CAST(@p_shift1_st_time as datetimeoffset),
				shift1_end_time = CAST(@p_shift1_end_time as datetimeoffset),
				shift2_start_time = CAST(@p_shift2_st_time as datetimeoffset),
				shift2_end_time = CAST(@p_shift2_end_time as datetimeoffset),
				shift3_start_time = CAST(@p_shift3_st_time as datetimeoffset),
				shift3_end_time = CAST(@p_shift3_end_time as datetimeoffset)
				WHERE location_id = @p_location_id;

			UPDATE mh_lpa_line_master
				SET    end_date = @ENDDATE
				WHERE  location_id = @p_location_id
					AND    country_id = @l_country_id
					AND    region_id = @l_region_id
					AND    (end_date IS NULL OR	end_date>@ENDDATE)

				  UPDATE mh_lpa_line_product_relationship
					SET    end_date = @ENDDATE
					WHERE   location_id = @p_location_id
					AND    country_id = @l_country_id
					AND    region_id = @l_region_id
					AND    (end_date IS NULL OR	end_date>@ENDDATE)


			SET @l_err_code = 0;
			SET @l_err_message = 'Updated Successfully';
			
		END;
	ELSE
		BEGIN	
			SELECT @l_count = count(*)  
			FROM   mh_lpa_location_master
			WHERE  UPPER(ISNULL(location_code,'*')) = UPPER(ISNULL(@p_location_code,'*'))
			AND    UPPER(location_name) = UPPER(@p_location_name)
			AND    region_id = @l_region_id
			AND    country_id = @l_country_id;
	
		IF @l_count = 0 
			BEGIN
				IF @p_location_name IS NOT NULL AND @p_location_name != '' 
					BEGIN
						INSERT INTO mh_lpa_location_master (
							region_id,
							country_id,
							location_code, 
							location_name,
							start_date,
							end_date,
							timezone_code,
							timezone_desc, 
							no_of_shifts,
							shift1_start_time,
							shift1_end_time,
							shift2_start_time,
							shift2_end_time,
							shift3_start_time,
							shift3_end_time
						) VALUES (
							@l_region_id,
							@l_country_id,
							@p_location_code, 
							@p_location_name,
							@STARTDATE,
							@ENDDATE,
							@p_timezone_code,
							@p_timezone_desc,
							@p_no_of_shifts,
							CAST(@p_shift1_st_time as datetimeoffset),
							CAST(@p_shift1_end_time as datetimeoffset),
							CAST(@p_shift2_st_time as datetimeoffset),
							CAST(@p_shift2_end_time as datetimeoffset),
							CAST(@p_shift3_st_time as datetimeoffset),
							CAST(@p_shift3_end_time as datetimeoffset)
							/*
							CAST(FORMAT(@p_shift1_st_time,'hh:mm') + ':00 ' +  @p_timezone_code as datetimeoffset),
							CAST(FORMAT(@p_shift1_end_time,'hh:mm') + ':00 ' +  @p_timezone_code as datetimeoffset),
							CAST(FORMAT(@p_shift2_st_time,'hh:mm') + ':00 ' +  @p_timezone_code as datetimeoffset),
							CAST(FORMAT(@p_shift2_end_time,'hh:mm') + ':00 ' +  @p_timezone_code as datetimeoffset),
							CAST(FORMAT(@p_shift3_st_time,'hh:mm') + ':00 ' +  @p_timezone_code as datetimeoffset),
							CAST(FORMAT(@p_shift3_end_time,'hh:mm') + ':00 ' +  @p_timezone_code as datetimeoffset)
							*/
						);

						SELECT @l_location_id = location_id
						FROM   mh_lpa_location_master
						WHERE  location_name = @p_location_name;

						insert into mh_lpa_local_questions
						select question_id, @l_location_id, section_id, display_sequence
						from mh_lpa_question_master	

/*	
						DELETE FROM mh_lpa_distribution_list
						WHERE  location_id = @l_location_id;

						INSERT INTO mh_lpa_distribution_list (location_id,list_type) VALUES (@l_location_id, 'At Audit Plan');
						INSERT INTO mh_lpa_distribution_list (location_id,list_type) VALUES (@l_location_id, 'At Audit Completion');
*/
						SET @l_err_code = 0;
			SET @l_err_message = 'Inserted Successfully';
					END;
			END;

		END;

	
	
END

