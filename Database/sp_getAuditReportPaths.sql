USE [mhlpa_db_dev]
GO
/****** Object:  StoredProcedure [dbo].[sp_getAuditReportPaths]    Script Date: 1/7/2020 4:44:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--exec sp_getAuditReportPaths null,null,null,null,'12-10-2017','11-11-2017'
ALTER PROCEDURE [dbo].[sp_getAuditReportPaths](
	@p_region_id  INT=null,
    @p_country_id  INT=null,
    @p_location_id  INT=null,
	@p_building_id	INT=null,
	@p_line_id		INT = null,
	@p_from_date VARCHAR(100),
	@p_to_date VARCHAR(100) 
)
AS 
BEGIN 
 IF @p_region_id IS NULL OR @p_region_id = 0 
  SET @p_region_id = NULL
 IF @p_country_id IS NULL OR @p_country_id = 0 
  SET @p_country_id = NULL
 IF @p_location_id IS NULL OR @p_location_id = 0 
  SET @p_location_id = NULL
 IF @p_building_id IS NULL OR @p_building_id = 0 
  SET @p_building_id = NULL

   IF @p_line_id IS NULL OR @p_line_id = 0 
  SET @p_line_id = NULL
              SELECT DISTINCT  rm.region_name,
                           cm.country_name,
                           lm.location_name,
						   bm.building_name,
                           linem.line_name,
                           CONCAT(la.audit_date,'(Central European Time)') audit_date,
                           la.audit_id
              FROM    mh_lpa_local_answers la,
                           mh_lpa_region_master rm,
                           mh_lpa_country_master cm,
                           mh_lpa_location_master lm,
                           mh_lpa_line_master linem,
						   mh_lpa_building_master bm
              WHERE   la.region_id=rm.region_id
              AND la.country_id=cm.country_id
              AND la.location_id=lm.location_id
              AND la.line_id=linem.line_id
		AND linem.building_id=bm.building_id
			  AND    la.region_id = ISNULL(@p_region_id, la.region_id)
			  AND    la.country_id = ISNULL(@p_country_id, la.country_id)
			AND     la.location_id = ISNULL(@p_location_id, la.location_id)
			AND     la.line_id = ISNULL(@p_line_id, la.line_id)
			AND linem.building_id=ISNULL(@p_building_id, linem.building_id)
    
AND Convert(Date,la.audit_date,103) BETWEEN Convert(Date,Convert(Varchar(100), @p_from_date),103) 
	AND Convert(Date,Convert(Varchar(100), @p_to_date),103)
   ORDER BY audit_id  DESC         
END
 

