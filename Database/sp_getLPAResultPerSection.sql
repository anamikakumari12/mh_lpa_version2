USE [mhlpa_db_dev]
GO
/****** Object:  StoredProcedure [dbo].[sp_getLPAResultPerSection]    Script Date: 1/2/2020 4:38:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec sp_getLPAResultPerSection null,null,null,null,null,'01-01-2017'
ALTER procedure [dbo].[sp_getLPAResultPerSection](@p_region_id		INT=null,
											@p_country_id		INT=null,
											@p_location_id		INT=null,
	@p_building_id	INT=NULL,
											@p_line_id			INT=null,
											@p_date				DATE)
AS
BEGIN

DECLARE @l_region_id		INT;
	DECLARE @l_country_id		INT;
	DECLARE @l_location_id		INT;
	DECLARE @l_line_id		INT;
	   DECLARE @l_building_id            INT;

	   
       IF @p_building_id IS NULL OR @p_building_id = 0 
              SET @l_building_id = NULL;
       ELSE
              SET @l_building_id = @p_building_id;


	IF @p_region_id IS NULL OR @p_region_id = 0 
		SET @l_region_id = NULL;
	ELSE
		SET @l_region_id = @p_region_id;

	IF @p_country_id IS NULL OR @p_country_id = 0 
		SET @l_country_id = NULL;
	ELSE
		SET @l_country_id = @p_country_id;

	IF @p_location_id IS NULL OR @p_location_id = 0 
		SET @l_location_id = NULL;
	ELSE
		SET @l_location_id = @p_location_id;

	IF @p_line_id IS NULL OR @p_line_id = 0 
		SET @l_line_id = NULL;
	ELSE
		SET @l_line_id = @p_line_id;

	SELECT
			ROUND(AVG(ISNULL(sc.sec1_score,0)),2) Section1_score,
			ROUND(AVG(ISNULL(sc.sec2_score,0)),2) Section2_score,
			ROUND(AVG(ISNULL(sc.sec3_score,0)),2) Section3_score,
			ROUND(AVG(ISNULL(sc.sec4_score,0)),2) Section4_score,
			ROUND(AVG(ISNULL(sc.sec5_score,0)),2) Section5_score,
			ROUND(AVG(ISNULL(sc.sec6_score,0)),2) Section6_score
		INTO #TEMP1
	FROM    mh_lpa_score_summary sc LEFT JOIN mh_lpa_line_master lm ON sc.line_id=lm.line_id
	WHERE   sc.region_id = ISNULL(@l_region_id, sc.region_id)
	AND     sc.country_id = ISNULL(@l_country_id, sc.country_id)
	AND     sc.location_id = ISNULL(@l_location_id, sc.location_id)
		AND		lm.building_id=ISNULL(@l_building_id,lm.building_id)
	AND     sc.line_id = ISNULL(@l_line_id, sc.line_id)
	AND     sc.audit_date BETWEEN DATEADD(year,-1,@p_date) AND @p_date;
	--select * from #TEMP1
	SELECT Section_number, 
	CASE 
						WHEN CHARINDEX('A. ',section_name)>0 THEN REPLACE(sm.section_name,'A. ','') 
						WHEN CHARINDEX('B. ',section_name)>0 THEN REPLACE(sm.section_name,'B. ','') 
						WHEN CHARINDEX('C. ',section_name)>0 THEN REPLACE(sm.section_name,'C. ','') 
						WHEN CHARINDEX('D. ',section_name)>0 THEN REPLACE(sm.section_name,'D. ','') 
						WHEN CHARINDEX('E. ',section_name)>0 THEN REPLACE(sm.section_name,'E. ','') 
						ELSE section_name
						END section_name, ISNULL(Score,0) Score, Sec_name_abbr
	FROM (
		SELECT 1 Section_number, Section1_score Score, 'GT' as Sec_name_abbr
		from #TEMP1
		UNION ALL
		SELECT 2 Section_number, Section2_score Score, 'A' as Sec_name_abbr
		from #TEMP1
		UNION ALL
		SELECT 3 Section_number, Section3_score Score, 'B' as Sec_name_abbr
		from #TEMP1
		UNION ALL
		SELECT 4 Section_number, Section4_score Score, 'C' as Sec_name_abbr
		from #TEMP1
		UNION ALL
		SELECT 5 Section_number, Section5_score Score, 'D' as Sec_name_abbr
		from #TEMP1
		UNION ALL
		SELECT 6 Section_number, Section6_score Score, 'E' as Sec_name_abbr
		from #TEMP1
/*		SELECT 2 Section_number, Section2_score Score, 'PD' as Sec_name_abbr
		from #TEMP1
		UNION ALL
		SELECT 3 Section_number, Section3_score Score, 'SOCWA' as Sec_name_abbr
		from #TEMP1
		UNION ALL
		SELECT 4 Section_number, Section4_score Score, 'RPE' as Sec_name_abbr
		from #TEMP1
		UNION ALL
		SELECT 5 Section_number, Section5_score Score, 'SW' as Sec_name_abbr
		from #TEMP1
		UNION ALL
		SELECT 6 Section_number, Section6_score Score, 'RPS' as Sec_name_abbr
		from #TEMP1
*/
	) tmp, mh_lpa_section_master sm
	WHERE tmp.Section_number = sm.section_id
	ORDER BY section_id

END



