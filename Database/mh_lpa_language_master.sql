﻿

SELECT [code]
      ,[english]
      ,[german]
      ,[french]
      ,[spanish]
      ,[czech]
      ,[chinese]
      ,[korean]
      ,[thai]
      ,[bosnian]
      ,[portuguese]
  FROM [dbo].[mh_lpa_language_master]
GO


--delete from [dbo].[mh_lpa_language_master] where code='help1'
INSERT INTO [dbo].[mh_lpa_language_master]([code]
      ,[english]
      ,[german]
      ,[french]
      ,[spanish]
      ,[czech]
      ,[chinese]
      ,[korean]
      ,[thai]
      ,[bosnian]
      ,[portuguese])
VALUES('help1','What should be in place?',N'Was soll vorhanden sein?',N'What should be in place?',N'¿Qué debe existir?',N'Co by mělo být na svém místě?',
N'What should be in place?什么应该在现场',N'What should be in place? (필요한 사항)',N'What should be in place? สิ่งที่มองหา และต้องมี',N'Šta treba postojati?',N'O que deve estar em vigor?')
INSERT INTO [dbo].[mh_lpa_language_master]([code]
      ,[english]
      ,[german]
      ,[french]
      ,[spanish]
      ,[czech]
      ,[chinese]
      ,[korean]
      ,[thai]
      ,[bosnian]
      ,[portuguese])
VALUES('help2','How to check?',N'Wie wird geprüft?',N'How to check?',N'¿Cómo comprobar?',N'Jak kontrolovat?	',
N'How to check?如何检查',N'How to check?(확인 방법)',N'How to check? วิธีการเช็ค',N'Kako se provjerava?',N'Como verificar?')

