Alter TABLE mh_lpa_location_master ADD start_date date
Alter TABLE mh_lpa_location_master ADD end_date date

update mh_lpa_local_answers SET review_closed_status=1, review_user_id=24, review_closed_on=GETDATE(), 
review_comments='All open tasks until April 2020 will be automatically closed with LPA Version 2 update'
  where answer=1 and review_closed_status IS NULL