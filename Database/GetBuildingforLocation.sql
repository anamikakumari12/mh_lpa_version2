
/****** Object:  StoredProcedure [dbo].[GetBuildingforLocation]    Script Date: 2/19/2020 5:03:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[GetBuildingforLocation](
@p_location_id
int)
AS
BEGIN

select building_id, building_name 
	from mh_lpa_building_master 
	where building_id in (Select lm.building_id FROM   mh_lpa_line_master lm WHERE lm.location_id = @p_location_id
AND   ISNULL(lm.end_date, DATEADD(day,1,getdate())) >= FORMAT(getdate(),'yyyy-MM-dd'))
ORDER BY building_name;

END;




