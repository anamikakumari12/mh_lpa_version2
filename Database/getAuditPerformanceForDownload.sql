USE [mhlpa_db_dev]
GO
/****** Object:  StoredProcedure [dbo].[getAuditPerformanceForDownload]    Script Date: 1/8/2020 3:42:43 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[getAuditPerformanceForDownload](@p_region_id   int,
		@p_country_id   int,
		@p_location_id   int,
	@p_building_id	INT=null,
		@p_line_id   int,
        @l_st_date    DATE,
        @l_end_date    DATE
        )
AS
 SET NOCOUNT ON;
 
BEGIN

IF @p_region_id IS NULL OR @p_region_id = 0 
  SET @p_region_id = NULL;
 ELSE
  SET @p_region_id = @p_region_id;
 IF @p_country_id IS NULL OR @p_country_id = 0 
  SET @p_country_id = NULL;
 ELSE
  SET @p_country_id = @p_country_id;
 IF @p_location_id IS NULL OR @p_location_id = 0 
  SET @p_location_id = NULL;
 ELSE
  SET @p_location_id = @p_location_id;
 
 IF @p_building_id IS NULL OR @p_building_id = 0 
  SET @p_building_id = NULL
 
 IF @p_line_id IS NULL OR @p_line_id = 0 
  SET @p_line_id = NULL;
 ELSE
  SET @p_line_id = @p_line_id;


SELECT  region_name,
   country_name,
   location_name,
   building_name,
   line_name,
   dt,
   SUM(cy_audit_planned) no_of_audits_planned,
   SUM(cy_audit_performed) no_of_audits_performed
 FROM (
  SELECT 
     rm.region_name,
     cm.country_name,
     loc.location_name,
	 bm.building_name,
	 l.line_name,
                    count(*) cy_audit_planned,
                    0 cy_audit_performed,
     0 ly_audit_planned,
     0 ly_audit_performed,
                    CONVERT(DATE, '01-'+FORMAT(planned_date,'MM-yyyy'),105) dt
        FROM    mh_lpa_audit_plan p,
    mh_lpa_line_master l,
    mh_lpa_location_master loc,
    mh_lpa_country_master cm,
    mh_lpa_region_master rm,
	mh_lpa_building_master bm
        WHERE   p.line_id = l.line_id
  AND     l.region_id = rm.region_id
        AND     l.country_id = cm.country_id
        AND     l.location_id = loc.location_id
		AND		l.building_id=bm.building_id
        -- AND     planned_date BETWEEN DATEADD(year,-1,getdate()) AND getdate()
		AND   l.region_id = ISNULL(@p_region_id, l.region_id)
	AND   l.country_id = ISNULL(@p_country_id, l.country_id)
	AND   l.location_id = ISNULL(@p_location_id, l.location_id)
	AND   l.line_id = ISNULL(@p_line_id, l.line_id)
	AND   l.building_id=ISNULL(@p_building_id, l.building_id)

	--AND   planned_date BETWEEN @l_st_date AND @l_end_date
	AND   planned_date >= @l_st_date AND planned_date<@l_end_date
	AND (l.end_date IS NULL OR l.end_date>GETDATE())
        GROUP BY  rm.region_name,
     cm.country_name,
     loc.location_name,
	 bm.building_name,
     l.line_name,
     CONVERT(DATE, '01-'+FORMAT(planned_date,'MM-yyyy'),105)
        UNION
        SELECT 
       rm.region_name,
     cm.country_name,
     loc.location_name,
	 bm.building_name,
	 lm.line_name,
                    0 cy_audit_planned,
                    count(*) cy_audit_performed,
     0 ly_audit_planned,
     0 ly_audit_performed,
                    CONVERT(DATE, '01-'+FORMAT(audit_date,'MM-yyyy'),105) dt
        FROM    mh_lpa_score_summary l,
    mh_lpa_line_master lm,
    mh_lpa_location_master loc,
    mh_lpa_country_master cm,
    mh_lpa_region_master rm,
	mh_lpa_building_master bm
        WHERE   l.line_id = lm.line_id
  AND     l.region_id = rm.region_id
        AND     l.country_id = cm.country_id
        AND     l.location_id = loc.location_id
		AND		lm.building_id=bm.building_id
        -- AND     audit_date BETWEEN DATEADD(year,-1,getdate()) AND getdate()
		AND   l.region_id = ISNULL(@p_region_id, l.region_id)
	AND   l.country_id = ISNULL(@p_country_id, l.country_id)
	AND   l.location_id = ISNULL(@p_location_id, l.location_id)
	AND   l.line_id = ISNULL(@p_line_id, l.line_id)
	AND   lm.building_id=ISNULL(@p_building_id, lm.building_id)
	AND   audit_date BETWEEN @l_st_date AND @l_end_date
        GROUP BY   rm.region_name,
     cm.country_name,
     loc.location_name,
	 bm.building_name,
     lm.line_name,CONVERT(DATE, '01-'+FORMAT(audit_date,'MM-yyyy'),105)
  ) tmp
 GROUP BY region_name,
   country_name,
   location_name,
   building_name,
   line_name,
   dt;

END;