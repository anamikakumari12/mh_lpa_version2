USE [MH_LPA-TEST]
GO
/****** Object:  StoredProcedure [dbo].[sp_getAllTasks]    Script Date: 17-06-2020 09:50:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--exec sp_getAllTasks null,null,null,null,null,'2','12-10-2017','11-11-2019'
ALTER PROCEDURE [dbo].[sp_getAllTasks](
	@p_region_id  INT=null,
    @p_country_id  INT=null,
    @p_location_id  INT=null,
	@p_building_id	INT=null,
	@p_line_id		INT = null,
	@p_closed_status VARCHAR(10),
	@p_from_date VARCHAR(100),
	@p_to_date VARCHAR(100) 
) 
 
AS 
BEGIN 
 IF @p_region_id IS NULL OR @p_region_id = 0 
  SET @p_region_id = NULL;
 IF @p_country_id IS NULL OR @p_country_id = 0 
  SET @p_country_id = NULL;
 IF @p_location_id IS NULL OR @p_location_id = 0 
  SET @p_location_id = NULL;
 IF @p_building_id IS NULL OR @p_building_id = 0 
  SET @p_building_id = NULL;
 IF @p_line_id IS NULL OR @p_line_id = 0 
  SET @p_line_id = NULL;

   SELECT  region_name,
                     country_name,
                     location_name,
					 building_name,
                     line_name,
                     audit_date,
                     audit_id,
                     section_id,
                     section_name,
                     section_display_sequence,
                     question_id,
                     question,
                     question_display_sequence,
                     answer,
                     remarks,
                     review_closed_status,
                     review_comments,
					image_file_name,
					review_image_file_name,
					audit_name
       FROM (
              SELECT  rm.region_name,
                           cm.country_name,
                           lm.location_name,
						   bm.building_name,
                           linem.line_name,
                           la.audit_date,
                           la.audit_id,
                           sm.section_id,
                           sm.section_name,
                           sm.display_sequence  section_display_sequence,
                           qm.question_id,
                           qm.question,
                           lq.display_sequence  question_display_sequence,
                           la.answer,
                           la.remarks,
                           la.review_closed_status,
                           la.review_comments,
						   la.image_file_name,
						   la.review_image_file_name,
				dbo.[get_audit_id](la.audit_id) audit_name
              FROM    mh_lpa_local_answers la,
                           mh_lpa_question_master qm,
                           mh_lpa_local_questions lq,
                           mh_lpa_section_master sm,
                           mh_lpa_region_master rm,
                           mh_lpa_country_master cm,
                           mh_lpa_location_master lm,
                           mh_lpa_line_master linem,
						   mh_lpa_building_master bm
              WHERE  la.answer = 1
              AND    la.region_id=rm.region_id
              AND la.country_id=cm.country_id
              AND la.location_id=lm.location_id
              AND la.line_id=linem.line_id
			  AND bm.building_id=linem.building_id
              AND    la.question_id = qm.question_id
              AND    la.question_id < 10000
              AND    la.location_id = lq.location_id
              AND    lq.question_id = qm.question_id
              AND    lq.section_id = sm.section_id
			  AND    la.region_id = ISNULL(@p_region_id, la.region_id)
			  AND    la.country_id = ISNULL(@p_country_id, la.country_id)
			AND     la.location_id = ISNULL(@p_location_id, la.location_id)
			AND     la.line_id = ISNULL(@p_line_id, la.line_id)
			AND   linem.building_id=ISNULL(@p_building_id, linem.building_id)
     AND    (
(ISNULL(@p_closed_status,'3') ='3' AND la.review_closed_status IS NULL) OR
(@p_closed_status = '0' AND la.review_closed_status =0) OR
(@p_closed_status = '1' AND la.review_closed_status =1) OR
(@p_closed_status = '2')
    )
--	AND la.audit_date BETWEEN @p_from_date AND @p_to_date
AND Convert(Date,la.audit_date,103) BETWEEN Convert(Date,Convert(Varchar(100), @p_from_date),103) AND Convert(Date,Convert(Varchar(100), @p_to_date),103)
              UNION
              SELECT  rm.region_name,
                           cm.country_name,
                           lm.location_name,
						   bm.building_name,
                           linem.line_name,
                           la.audit_date,
                           la.audit_id,
                           sm.section_id,
                           sm.section_name,
                           sm.display_sequence  section_display_sequence,
                           qm.local_question_id question_id,
                           qm.local_question question,
                           lq.display_sequence  question_display_sequence,
                           la.answer,
                           la.remarks,
                           la.review_closed_status,
                           la.review_comments,
						   la.image_file_name,
						   la.review_image_file_name,
				dbo.[get_audit_id](la.audit_id) audit_name
              FROM    mh_lpa_local_answers la,
                           mh_lpa_local_question_master qm,
                           mh_lpa_local_questions lq,
                           mh_lpa_section_master sm,
                           mh_lpa_region_master rm,
                           mh_lpa_country_master cm,
                           mh_lpa_location_master lm,
                           mh_lpa_line_master linem,
						   mh_lpa_building_master bm
              WHERE  la.answer = 1
              AND    la.region_id=rm.region_id
              AND la.country_id=cm.country_id
              AND la.location_id=lm.location_id
              AND la.line_id=linem.line_id
			  AND bm.building_id=linem.building_id
              AND    la.question_id = qm.local_question_id
              AND    la.question_id >= 10000
              AND    lq.question_id = qm.local_question_id
              AND    la.location_id = lq.location_id
              AND    lq.section_id = sm.section_id
			  AND    la.region_id = ISNULL(@p_region_id, la.region_id)
			  AND    la.country_id = ISNULL(@p_country_id, la.country_id)
			AND     la.location_id = ISNULL(@p_location_id, la.location_id)
			AND     la.line_id = ISNULL(@p_line_id, la.line_id)
			AND   linem.building_id=ISNULL(@p_building_id, linem.building_id)
     AND    (
(ISNULL(@p_closed_status,'3') ='3' AND la.review_closed_status IS NULL) OR
(@p_closed_status = '0' AND la.review_closed_status =0) OR
(@p_closed_status = '1' AND la.review_closed_status =1) OR
(@p_closed_status = '2')
    )
	--AND la.audit_date BETWEEN @p_from_date AND @p_to_date
AND Convert(Date,la.audit_date,103) BETWEEN Convert(Date,Convert(Varchar(100), @p_from_date),103) AND Convert(Date,Convert(Varchar(100), @p_to_date),103)
--              AND    ISNULL(CONVERT(VARCHAR(10),la.review_closed_status),'0') in (SELECT item from dbo.SplitString(@p_closed_status,','))
              ) tmp
       ORDER BY section_display_sequence, question_display_sequence
   
END
 
