

CREATE TABLE [dbo].[mh_lpa_deleted_audits](
	[loc_answer_id] [int] NOT NULL,
	[audit_id] [int] NULL,
	[audit_plan_id] [int] NULL,
	[audited_by_user_id] [int] NULL,
	[line_product_rel_id] [int] NOT NULL,
	[audit_date] [datetime] NULL,
	[Shift_No] [int] NULL,
	[question_id] [int] NULL,
	[answer] [int] NULL,
	[remarks] [nvarchar](1000) NULL,
	[image_file_name] [varchar](1000) NULL,
	[review_user_id] [int] NULL,
	[review_closed_on] [datetime] NULL,
	[review_closed_status] [int] NULL,
	[review_comments] [nvarchar](1000) NULL,
	[review_image_file_name] [varchar](1000) NULL,
	[notification_sent_flag] [varchar](1) NULL,
	[region_id] [int] NULL,
	[country_id] [int] NULL,
	[location_id] [int] NULL,
	[section_id] [int] NULL,
	[line_id] [int] NULL,
	[Audit_Title] [varchar](200) NULL
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[mh_lpa_deleted_score_summary](
	[audit_id] [int] NULL,
	[audit_date] [date] NULL,
	[region_id] [int] NULL,
	[country_id] [int] NULL,
	[location_id] [int] NULL,
	[line_id] [int] NULL,
	[part_id] [int] NULL,
	[sec1_yes_count] [int] NULL,
	[sec1_tot_count] [int] NULL,
	[sec1_score] [float] NULL,
	[sec2_yes_count] [int] NULL,
	[sec2_tot_count] [int] NULL,
	[sec2_score] [float] NULL,
	[sec3_yes_count] [int] NULL,
	[sec3_tot_count] [int] NULL,
	[sec3_score] [float] NULL,
	[sec4_yes_count] [int] NULL,
	[sec4_tot_count] [int] NULL,
	[sec4_score] [float] NULL,
	[sec5_yes_count] [int] NULL,
	[sec5_tot_count] [int] NULL,
	[sec5_score] [float] NULL,
	[sec6_yes_count] [int] NULL,
	[sec6_tot_count] [int] NULL,
	[sec6_score] [float] NULL,
	[total_yes] [int] NULL,
	[total_no] [int] NULL,
	[total_na] [int] NULL,
	[total_score] [float] NULL,
	[q1_answer] [int] NULL,
	[q2_answer] [int] NULL,
	[q3_answer] [int] NULL,
	[q4_answer] [int] NULL,
	[q5_answer] [int] NULL,
	[q6_answer] [int] NULL,
	[q7_answer] [int] NULL,
	[q8_answer] [int] NULL,
	[q9_answer] [int] NULL,
	[q10_answer] [int] NULL,
	[q11_answer] [int] NULL,
	[q12_answer] [int] NULL,
	[q13_answer] [int] NULL,
	[q14_answer] [int] NULL,
	[q15_answer] [int] NULL,
	[q16_answer] [int] NULL,
	[q17_answer] [int] NULL,
	[q18_answer] [int] NULL,
	[q19_answer] [int] NULL,
	[q20_answer] [int] NULL,
	[q21_answer] [int] NULL,
	[q22_answer] [int] NULL,
	[q23_answer] [int] NULL,
	[q24_answer] [int] NULL,
	[q25_answer] [int] NULL,
	[q26_answer] [int] NULL,
	[q27_answer] [int] NULL,
	[q28_answer] [int] NULL,
	[q29_answer] [int] NULL,
	[q30_answer] [int] NULL,
	[update_complete] [int] NULL
) ON [PRIMARY]

GO

--exec sp_deleteAudit 10357

ALTER PROCEDURE sp_deleteAudit
(
@p_audit_id VARCHAR(200)
)
AS
BEGIN
INSERT INTO [mh_lpa_deleted_audits]
		SELECT 	*, dbo.[get_audit_id](@p_audit_id) FROM mh_lpa_local_answers where audit_id=@p_audit_id
	
	DELETE FROM mh_lpa_local_answers where audit_id=@p_audit_id
	
	INSERT INTO [mh_lpa_deleted_score_summary]
		SELECT *  FROM [mh_lpa_score_summary] where audit_id=@p_audit_id

	DELETE FROM [mh_lpa_score_summary] where audit_id=@p_audit_id
END
GO