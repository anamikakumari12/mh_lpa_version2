USE [mhlpa_db_dev]
GO
/****** Object:  StoredProcedure [dbo].[get_st_end_date]    Script Date: 1/10/2020 1:07:57 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[get_st_end_date](@p_year  VARCHAR(4),
      @p_week_no    INT,
      @l_start_date   DATE OUTPUT,
      @l_end_date    DATE OUTPUT)
AS
BEGIN
 SET @l_start_date = DATEADD(day, 1, DATEADD(wk, DATEDIFF(wk, 6, '1/1/' + @p_year) + (@p_week_no-1), 6));
 SET @l_end_date = DATEADD(day, -1, DATEADD(wk, DATEDIFF(wk, 5, '1/1/' + @p_year) + (@p_week_no-1), 7));
END;
GO

update mh_lpa_audit_plan SET planned_date_end= DateAdd(day,2,planned_date_end)
GO