
/****** Object:  StoredProcedure [dbo].[getLocListforEdit]    Script Date: 12/6/2019 1:02:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec getLocListforEdit 17,1
ALTER PROCEDURE [dbo].[getLocListforEdit](@p_user_id		INT, @p_inactive_flag INT)
AS
BEGIN
	DECLARE @l_count 			INT;
	DECLARE @l_location_id 			INT;
	DECLARE @l_global_admin			VARCHAR(1);
	DECLARE @l_local_admin			VARCHAR(1);


	SELECT @l_global_admin = ISNULL(global_admin_flag,'N'),
		@l_local_admin = ISNULL(site_admin_flag,'N'),
		@l_location_id = location_id
	FROM   mh_lpa_user_master
	WHERE  user_id = @p_user_id;
	
	SELECT  rm.region_id,
			rm.region_name,
			cm.country_id,
			cm.country_name,
			lm.location_id,
			lm.location_name,
			no_of_shifts,
			shift1_start_time,
			shift1_end_time,
			shift2_start_time,
			shift2_end_time,
			shift3_start_time,
			shift3_end_time,
			timezone_desc,
			lm.start_date,
			lm.end_date
	FROM   mh_lpa_region_master rm,
			mh_lpa_country_master cm,
			mh_lpa_location_master lm
	WHERE  lm.region_id = rm.region_id
	AND    lm.country_id = cm.country_id
	AND		(
			(ISNULL(@l_local_admin,'N') = 'Y' AND lm.location_id = @l_location_id) OR
			(ISNULL(@l_global_admin,'N') = 'Y')
			)
			AND(
			(@p_inactive_flag=0 AND ISNULL(lm.end_date, DATEADD(day,1,getdate())) >= FORMAT(getdate(),'yyyy-MM-dd')) OR (@p_inactive_flag>0)
			)
			
	--AND    ISNULL(lm.end_date, DATEADD(day,1,getdate())) >= getdate()
	ORDER BY location_name;


END;

