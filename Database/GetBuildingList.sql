
CREATE FUNCTION [dbo].[GetBuildingList](@p_user_id INT,
								@p_location_id int)
 RETURNS NVARCHAR(MAX)  
AS  
BEGIN  
   RETURN (
   SELECT building_id,
   			building_name
	FROM (
		SELECT distinct lm.building_id,
			bm.building_name
		FROM   mh_lpa_line_master lm,
			mh_lpa_building_master bm
		WHERE lm.building_id = bm.building_id
		AND   lm.location_id = @p_location_id
		
		) tmp

		order by building_name
	FOR JSON AUTO)  
END

