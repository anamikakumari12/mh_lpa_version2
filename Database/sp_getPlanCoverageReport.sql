USE [mhlpa_db_dev]
GO
/****** Object:  StoredProcedure [dbo].[sp_getAuditPlanReport]    Script Date: 2/12/2020 11:00:24 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec sp_getPlanCoverageReport 1,1,1
CREATE PROCEDURE [dbo].[sp_getPlanCoverageReport]
(
@l_region_id INT=NULL,
@l_country_id INT=NULL,
@l_location_id INT=NULL
)
AS
BEGIN
if(@l_region_id=0)
SET  @l_region_id = null
if(@l_country_id=0)
SET  @l_country_id = null
if(@l_location_id=0)
SET  @l_location_id = null

DECLARE @audited_line INT, @total_line INT

Select @audited_line= COUNT(*) FROM (Select distinct line_id from mh_lpa_local_answers 
 WHERE location_id = ISNULL(@l_location_id, location_id)
AND  country_id = ISNULL(@l_country_id, country_id)
AND  region_id = ISNULL(@l_region_id, region_id)) t

select @total_line=COUNT(*) from mh_lpa_line_master WHERE  location_id = ISNULL(@l_location_id, location_id)
AND  country_id = ISNULL(@l_country_id, country_id)
AND  region_id = ISNULL(@l_region_id, region_id)

select @audited_line audited_line, @total_line total_line, CASE WHEN @total_line>0 THEN @audited_line*100/@total_line ELSE 0 END percentage_coverage

select COUNT(ans.audit_id) Audit_count, loc.location_name, lin.line_name
from mh_lpa_local_answers ans 
Right JOIN mh_lpa_location_master loc ON ans.location_id=loc.location_id
JOIN mh_lpa_line_master lin ON lin.line_id=ans.line_id
WHERE
  loc.location_id = ISNULL(@l_location_id, loc.location_id)
AND  loc.country_id = ISNULL(@l_country_id, loc.country_id)
AND  loc.region_id = ISNULL(@l_region_id, loc.region_id)
GROUP BY loc.location_name, lin.line_name
END


--select * from [dbo].[mh_lpa_local_answers]

--select * from [dbo].[mh_lpa_score_summary] where audit_id=10855