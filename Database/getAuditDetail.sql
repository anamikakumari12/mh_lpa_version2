--EXEC getAuditDetail 10350
CREATE PROCEDURE getAuditDetail
(
@p_audit_id VARCHAR(200)
)
AS
BEGIN

SELECT DISTINCT audit_id Audit_ID,
                     FORMAT(Convert(date,audit_date),'dd-MMM-yyyy') conducted_on,
                     lme.line_name,
                     lm.location_name,
                     dbo.getEmployeeName(la.audited_by_user_id) conducted_by,
                     dbo.getAuditScorebyAudit(audit_id) Score,
                     pm.product_code,
                     lme.line_id,
					 (select building_name from mh_lpa_building_master where building_id=lme.building_id) building_name,
      (select username from mh_lpa_user_master where user_id=la.audited_by_user_id) username,
      la.shift_No no_of_shifts
       FROM   mh_lpa_local_answers la,
                     mh_lpa_line_product_relationship rel,
                     mh_lpa_product_master pm,
                     mh_lpa_location_master lm,
                     mh_lpa_line_master lme,
                     mh_lpa_user_master mhu
       WHERE la.audit_id = @p_audit_id
       AND   la.line_product_rel_id = rel.line_product_rel_id
       AND   rel.product_id = pm.product_id
       AND   rel.location_id = lm.location_id
       AND   rel.line_id = lme.line_id
       AND      mhu.user_id=la.audited_by_user_id;


END