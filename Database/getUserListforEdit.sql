
/****** Object:  StoredProcedure [dbo].[getUserListforEdit]    Script Date: 12/3/2019 1:30:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[getUserListforEdit](@p_user_id		INT, @p_inactive_flag INT)
AS
BEGIN
	DECLARE @l_count 			INT;
	DECLARE @l_location_id 			INT;
	DECLARE @l_global_admin			VARCHAR(1);
	DECLARE @l_local_admin			VARCHAR(1);


	SELECT @l_global_admin = ISNULL(global_admin_flag,'N'),
		@l_local_admin = ISNULL(site_admin_flag,'N'),
		@l_location_id = location_id
	FROM   mh_lpa_user_master
	WHERE  user_id = @p_user_id;
	if(@p_inactive_flag=1)
	BEGIN
		SELECT user_id,
			username,
			emp_full_name,
			emp_first_name,
			emp_last_name,
			display_name_flag,
			email_id,
			um.location_id,
			lm.location_name,
			role,
			rm.role_id,
			global_admin_flag,
			site_admin_flag,
			um.start_date,
			um.end_date,
			um.passwd,
			(select case  when global_admin_flag='Y' then 'Global'
			 when site_admin_flag='Y' then 'Local' end ) as 'admin_flag'
	FROM  mh_lpa_user_master um
	join [dbo].[mh_lpa_location_master] lm on um.location_id=lm.location_id
	join [dbo].[mh_lpa_role_master] rm on rm.role_id=um.role_id
	WHERE  (
			(@l_local_admin = 'Y' AND um.location_id = @l_location_id) OR
			(@l_global_admin = 'Y')
			)
			--AND (um.end_date is null OR um.end_date>=GETDATE())
			order by emp_last_name, emp_first_name
	END
	ELSE
	BEGIN 
		SELECT user_id,
			username,
			emp_full_name,
			emp_first_name,
			emp_last_name,
			display_name_flag,
			email_id,
			um.location_id,
			lm.location_name,
			role,
			rm.role_id,
			global_admin_flag,
			site_admin_flag,
			um.start_date,
			um.end_date,
			um.passwd,
			(select case  when global_admin_flag='Y' then 'Global'
			 when site_admin_flag='Y' then 'Local' end ) as 'admin_flag'
			FROM  mh_lpa_user_master um
				join [dbo].[mh_lpa_location_master] lm on um.location_id=lm.location_id
				join [dbo].[mh_lpa_role_master] rm on rm.role_id=um.role_id
			WHERE  (
					(@l_local_admin = 'Y' AND um.location_id = @l_location_id) OR
					(@l_global_admin = 'Y')
				)
				AND (um.end_date is null OR um.end_date>=FORMAT(getdate(),'yyyy-MM-dd'))
			order by emp_last_name, emp_first_name
	END

END;


