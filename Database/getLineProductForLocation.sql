
/****** Object:  UserDefinedFunction [dbo].[getLineProductForLocation]    Script Date: 2/17/2020 4:19:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER FUNCTION [dbo].[getLineProductForLocation](@p_location_id   int, @p_building_id INT,
         @p_user_id    int)  
 RETURNS NVARCHAR(MAX)  
AS  
BEGIN 
   RETURN (
   SELECT 
      line_name, 
   line_id
 FROM mh_lpa_line_master 
 WHERE location_id = @p_location_id 
 AND building_id=@p_building_id
and ISNULL(end_date , dateadd(day,1,getdate())) > FORMAT(getdate(),'yyyy-MM-dd') 
 order by line_name
 FOR JSON AUTO)  
/* 
   RETURN (
   SELECT line_product_rel_id,
      line_name, 
   line_id,
    product_code,
   product_name,
   Shift_no
 FROM (
  SELECT rel.line_product_rel_id,
    -- line_name+'('+product_code+' - '+product_name+')' line_name,
    l.line_id line_id,
    line_name line_name,
    product_code,
    product_name,
    dbo.getShift(rel.location_id) as Shift_no
    *
    CASE 
     WHEN CAST('1900-01-01 ' + FORMAT(getdate(),'HH:mm:ss')+' '+loc.timezone_code as datetimeoffset) 
       between  CAST('1900-01-01 ' + FORMAT(loc.shift1_start_time,'HH:mm')+':00 '+loc.timezone_code as datetimeoffset )
         and  CAST('1900-01-01 ' + FORMAT(loc.shift1_end_time,'HH:mm')+':00 '+loc.timezone_code as datetimeoffset ) THEN 1
     WHEN CAST('1900-01-01 ' + FORMAT(getdate(),'HH:mm:ss')+' '+loc.timezone_code as datetimeoffset) 
       between  CAST('1900-01-01 ' + FORMAT(loc.shift2_start_time,'HH:mm')+':00 '+loc.timezone_code as datetimeoffset )
         and  CAST('1900-01-01 ' + FORMAT(loc.shift2_end_time,'HH:mm')+':00 '+loc.timezone_code as datetimeoffset ) THEN 2
     WHEN CAST('1900-01-01 ' + FORMAT(getdate(),'HH:mm:ss')+' '+loc.timezone_code as datetimeoffset) 
       between  CAST('1900-01-01 ' + FORMAT(loc.shift3_start_time,'HH:mm')+':00 '+loc.timezone_code as datetimeoffset )
         and  CAST('1900-01-01 ' + FORMAT(loc.shift3_end_time,'HH:mm')+':00 '+loc.timezone_code as datetimeoffset ) THEN 3
     ELSE 1
    END as Shift_no
    *
  FROM   mh_lpa_line_product_relationship rel,
   mh_lpa_line_master l,
   mh_lpa_product_master p,
   mh_lpa_location_master loc
  WHERE  rel.line_id = l.line_id
  AND    rel.location_id = loc.location_id
  AND    rel.product_id = p.product_id
  AND    rel.location_id = @p_location_id
  ) tmp
 FOR JSON AUTO)  
*/
END
