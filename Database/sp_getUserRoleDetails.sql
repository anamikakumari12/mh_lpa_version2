USE [mhlpa_db_dev]
GO
/****** Object:  StoredProcedure [dbo].[sp_getAuditPlanReport]    Script Date: 2/12/2020 11:00:24 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec sp_getUserRoleDetails 1,1,1
CREATE PROCEDURE [dbo].[sp_getUserRoleDetails]
(
@l_region_id INT=NULL,
@l_country_id INT=NULL,
@l_location_id INT=NULL
)
AS
BEGIN
if(@l_region_id=0)
SET  @l_region_id = null
if(@l_country_id=0)
SET  @l_country_id = null
if(@l_location_id=0)
SET  @l_location_id = null

select loc.location_name, u.role, u.emp_full_name, u.email_id, u.start_date, u.end_date
FROM mh_lpa_user_master u JOIN mh_lpa_location_master loc ON u.location_id=loc.location_id
WHERE
  loc.location_id = ISNULL(@l_location_id, loc.location_id)
AND  loc.country_id = ISNULL(@l_country_id, loc.country_id)
AND  loc.region_id = ISNULL(@l_region_id, loc.region_id)

END


