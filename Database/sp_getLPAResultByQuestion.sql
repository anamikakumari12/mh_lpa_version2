USE [mhlpa_db_dev]
GO
/****** Object:  StoredProcedure [dbo].[sp_getLPAResultByQuestion]    Script Date: 1/2/2020 4:51:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec sp_getLPAResultByQuestion null,null,null,null,null,'02-01-2020'
ALTER procedure [dbo].[sp_getLPAResultByQuestion](@p_region_id		INT=null,
											@p_country_id		INT=null,
											@p_location_id		INT=null,
											@p_building_id	INT=NULL,
											@p_line_id			INT=null,
											@p_date				DATE)
AS
BEGIN

DECLARE @l_region_id		INT;
	DECLARE @l_country_id		INT;
	DECLARE @l_location_id		INT;
	DECLARE @l_line_id		INT;
	DECLARE @l_count		INT;
	   DECLARE @l_building_id            INT;

	   IF @p_building_id IS NULL OR @p_building_id = 0 
              SET @l_building_id = NULL;
       ELSE
              SET @l_building_id = @p_building_id;

	IF @p_region_id IS NULL OR @p_region_id = 0 
		SET @l_region_id = NULL;
	ELSE
		SET @l_region_id = @p_region_id;

	IF @p_country_id IS NULL OR @p_country_id = 0 
		SET @l_country_id = NULL;
	ELSE
		SET @l_country_id = @p_country_id;

	IF @p_location_id IS NULL OR @p_location_id = 0 
		SET @l_location_id = NULL;
	ELSE
		SET @l_location_id = @p_location_id;

	IF @p_line_id IS NULL OR @p_line_id = 0 
		SET @l_line_id = NULL;
	ELSE
		SET @l_line_id = @p_line_id;

	SELECT @l_count = count(*)
	FROM    mh_lpa_score_summary sc LEFT JOIN mh_lpa_line_master lm ON sc.line_id=lm.line_id
	WHERE  1=1
	AND     sc.region_id = ISNULL(@l_region_id, sc.region_id)
	AND     sc.country_id = ISNULL(@l_country_id, sc.country_id)
	AND     sc.location_id = ISNULL(@l_location_id, sc.location_id)
	AND		lm.building_id=ISNULL(@l_building_id,lm.building_id)
	AND     sc.line_id = ISNULL(@l_line_id, sc.line_id)
	AND     sc.audit_date BETWEEN DATEADD(year,-1,@p_date) AND @p_date;

	IF @l_count = 0
		select qm.question_id, qm.question, 0 score
		FROM   mh_lpa_question_master qm
	ELSE
SELECT unpvt.question_id, qm.question,  ISNULL(score,0) score
FROM (
		SELECT
		CASE 
			WHEN q1_tot_count = 0 THEN 0.0
			ELSE CAST(ROUND (q1_yes_count/CAST(q1_tot_count as numeric(10,5)),2 ) AS numeric(10,5))
		END as q0,
		CASE 
			WHEN q2_tot_count = 0  THEN 0.0
			ELSE CAST((q2_yes_count/CAST(q2_tot_count as numeric(10,5) ) ) AS numeric(10,5))
		END as q1,
		CASE 
			WHEN q3_tot_count = 0  THEN 0.0
			ELSE CAST((q3_yes_count/CAST(q3_tot_count as numeric(10,5) ) ) AS numeric(10,5))
		END as q2,
		CASE 
			WHEN q4_tot_count = 0  THEN 0.0
			ELSE CAST((q4_yes_count/CAST(q4_tot_count as numeric(10,5) ) ) AS numeric(10,5))
		END as q3,
		CASE 
			WHEN q5_tot_count = 0  THEN 0.0
			ELSE CAST((q5_yes_count/CAST(q5_tot_count as numeric(10,5) ) ) AS numeric(10,5))
		END as q4,
		CASE 
			WHEN q6_tot_count = 0  THEN 0.0
			ELSE CAST((q6_yes_count/CAST(q6_tot_count as numeric(10,5) ) ) AS numeric(10,5))
		END as q5,
		CASE 
			WHEN q7_tot_count = 0  THEN 0.0
			ELSE CAST((q7_yes_count/CAST(q7_tot_count as numeric(10,5) ) ) AS numeric(10,5))
		END as q6,
		CASE 
			WHEN q8_tot_count = 0  THEN 0.0
			ELSE CAST((q8_yes_count/CAST(q8_tot_count as numeric(10,5) ) ) AS numeric(10,5))
		END as q7,
		CASE 
			WHEN q9_tot_count = 0  THEN 0.0
			ELSE CAST((q9_yes_count/CAST(q9_tot_count as numeric(10,5) ) ) AS numeric(10,5))
		END as q8,
		CASE 
			WHEN q10_tot_count = 0  THEN 0.0
			ELSE CAST((q10_yes_count/CAST(q10_tot_count as numeric(10,5) ) ) AS numeric(10,5))
		END as q9,
		CASE 
			WHEN q11_tot_count = 0  THEN 0.0
			ELSE CAST((q11_yes_count/CAST(q11_tot_count as numeric(10,5) ) ) AS numeric(10,5))
		END as q10,
		CASE 
			WHEN q12_tot_count = 0  THEN 0.0
			ELSE CAST((q12_yes_count/CAST(q12_tot_count as numeric(10,5) ) ) AS numeric(10,5))
		END as q11,
		CASE 
			WHEN q13_tot_count = 0  THEN 0.0
			ELSE CAST((q13_yes_count/CAST(q13_tot_count as numeric(10,5) ) ) AS numeric(10,5))
		END as q12,
		CASE 
			WHEN q14_tot_count = 0  THEN 0.0
			ELSE CAST((q14_yes_count/CAST(q14_tot_count as numeric(10,5) ) ) AS numeric(10,5))
		END as q13,
		CASE 
			WHEN q15_tot_count = 0  THEN 0.0
			ELSE CAST((q15_yes_count/CAST(q15_tot_count as numeric(10,5) ) ) AS numeric(10,5))
		END as q14,
		CASE 
			WHEN q16_tot_count = 0  THEN 0.0
			ELSE CAST((q16_yes_count/CAST(q16_tot_count as numeric(10,5) ) ) AS numeric(10,5))
		END as q15,
		CASE 
			WHEN q17_tot_count = 0  THEN 0.0
			ELSE CAST((q17_yes_count/CAST(q17_tot_count as numeric(10,5) ) ) AS numeric(10,5))
		END as q16,
		CASE 
			WHEN q18_tot_count = 0  THEN 0.0
			ELSE CAST((q18_yes_count/CAST(q18_tot_count as numeric(10,5) ) ) AS numeric(10,5))
		END as q17,
		CASE 
			WHEN q19_tot_count = 0  THEN 0.0
			ELSE CAST((q19_yes_count/CAST(q19_tot_count as numeric(10,5) ) ) AS numeric(10,5))
		END as q18,
		CASE 
			WHEN q20_tot_count = 0  THEN 0.0
			ELSE CAST((q20_yes_count/CAST(q20_tot_count as numeric(10,5) ) ) AS numeric(10,5))
		END as q19,
		CASE 
			WHEN q21_tot_count = 0  THEN 0.0
			ELSE CAST((q21_yes_count/CAST(q21_tot_count as numeric(10,5) ) ) AS numeric(10,5))
		END as q20,
		CASE 
			WHEN q22_tot_count = 0  THEN 0.0
			ELSE CAST((q22_yes_count/CAST(q22_tot_count as numeric(10,5) ) ) AS numeric(10,5))
		END as q21,
		CASE 
			WHEN q23_tot_count = 0  THEN 0.0
			ELSE CAST((q23_yes_count/CAST(q23_tot_count as numeric(10,5) ) ) AS numeric(10,5))
		END as q22,
		CASE 
			WHEN q24_tot_count = 0  THEN 0.0
			ELSE CAST((q24_yes_count/CAST(q24_tot_count as numeric(10,5) ) ) AS numeric(10,5))
		END as q23,
		CASE 
			WHEN q25_tot_count = 0  THEN 0.0
			ELSE CAST((q25_yes_count/CAST(q25_tot_count as numeric(10,5) ) ) AS numeric(10,5))
		END as q24
		--INTO #TEMP1
	FROM
		(SELECT
				SUM(
					CASE 
						WHEN q1_answer = 0 THEN 1
						ELSE 0
					END) as q1_yes_count,
				SUM(
					CASE 
						WHEN q1_answer IN( 0,1) THEN 1
						ELSE 0
					END) as q1_tot_count,
		--
				SUM(
					CASE 
						WHEN q2_answer = 0 THEN 1
						ELSE 0
					END) as q2_yes_count,
				SUM(
					CASE 
						WHEN q2_answer IN( 0,1) THEN 1
						ELSE 0
					END) as q2_tot_count,
		--
				SUM(
					CASE 
						WHEN q3_answer = 0 THEN 1
						ELSE 0
					END) as q3_yes_count,
				SUM(
					CASE 
						WHEN q3_answer IN( 0,1) THEN 1
						ELSE 0
					END) as q3_tot_count,
		--
				SUM(
					CASE 
						WHEN q4_answer = 0 THEN 1
						ELSE 0
					END) as q4_yes_count,
				SUM(
					CASE 
						WHEN q4_answer IN( 0,1) THEN 1
						ELSE 0
					END) as q4_tot_count,
		--
				SUM(
					CASE 
						WHEN q5_answer = 0 THEN 1
						ELSE 0
					END) as q5_yes_count,
				SUM(
					CASE 
						WHEN q5_answer IN( 0,1) THEN 1
						ELSE 0
					END) as q5_tot_count,
		--
				SUM(
					CASE 
						WHEN q6_answer = 0 THEN 1
						ELSE 0
					END) as q6_yes_count,
				SUM(
					CASE 
						WHEN q6_answer IN( 0,1) THEN 1
						ELSE 0
					END) as q6_tot_count,
		--
				SUM(
					CASE 
						WHEN q7_answer = 0 THEN 1
						ELSE 0
					END) as q7_yes_count,
				SUM(
					CASE 
						WHEN q7_answer IN( 0,1) THEN 1
						ELSE 0
					END) as q7_tot_count,
		--
				SUM(
					CASE 
						WHEN q8_answer = 0 THEN 1
						ELSE 0
					END) as q8_yes_count,
				SUM(
					CASE 
						WHEN q8_answer IN( 0,1) THEN 1
						ELSE 0
					END) as q8_tot_count,
		--
				SUM(
					CASE 
						WHEN q9_answer = 0 THEN 1
						ELSE 0
					END) as q9_yes_count,
				SUM(
					CASE 
						WHEN q9_answer IN( 0,1) THEN 1
						ELSE 0
					END) as q9_tot_count,
		--
				SUM(
					CASE 
						WHEN q10_answer = 0 THEN 1
						ELSE 0
					END) as q10_yes_count,
				SUM(
					CASE 
						WHEN q10_answer IN( 0,1) THEN 1
						ELSE 0
					END) as q10_tot_count,
		--
				SUM(
					CASE 
						WHEN q11_answer = 0 THEN 1
						ELSE 0
					END) as q11_yes_count,
				SUM(
					CASE 
						WHEN q11_answer IN( 0,1) THEN 1
						ELSE 0
					END) as q11_tot_count,
		--
				SUM(
					CASE 
						WHEN q12_answer = 0 THEN 1
						ELSE 0
					END) as q12_yes_count,
				SUM(
					CASE 
						WHEN q12_answer IN( 0,1) THEN 1
						ELSE 0
					END) as q12_tot_count,
		--
				SUM(
					CASE 
						WHEN q13_answer = 0 THEN 1
						ELSE 0
					END) as q13_yes_count,
				SUM(
					CASE 
						WHEN q13_answer IN( 0,1) THEN 1
						ELSE 0
					END) as q13_tot_count,
		--
				SUM(
					CASE 
						WHEN q14_answer = 0 THEN 1
						ELSE 0
					END) as q14_yes_count,
				SUM(
					CASE 
						WHEN q14_answer IN( 0,1) THEN 1
						ELSE 0
					END) as q14_tot_count,
		--
				SUM(
					CASE 
						WHEN q15_answer = 0 THEN 1
						ELSE 0
					END) as q15_yes_count,
				SUM(
					CASE 
						WHEN q15_answer IN( 0,1) THEN 1
						ELSE 0
					END) as q15_tot_count,
		--
				SUM(
					CASE 
						WHEN q16_answer = 0 THEN 1
						ELSE 0
					END) as q16_yes_count,
				SUM(
					CASE 
						WHEN q16_answer IN( 0,1) THEN 1
						ELSE 0
					END) as q16_tot_count,
		--
				SUM(
					CASE 
						WHEN q17_answer = 0 THEN 1
						ELSE 0
					END) as q17_yes_count,
				SUM(
					CASE 
						WHEN q17_answer IN( 0,1) THEN 1
						ELSE 0
					END) as q17_tot_count,
		--
				SUM(
					CASE 
						WHEN q18_answer = 0 THEN 1
						ELSE 0
					END) as q18_yes_count,
				SUM(
					CASE 
						WHEN q18_answer IN( 0,1) THEN 1
						ELSE 0
					END) as q18_tot_count,
		--
				SUM(
					CASE 
						WHEN q19_answer = 0 THEN 1
						ELSE 0
					END) as q19_yes_count,
				SUM(
					CASE 
						WHEN q19_answer IN( 0,1) THEN 1
						ELSE 0
					END) as q19_tot_count,
		--
				SUM(
					CASE 
						WHEN q20_answer = 0 THEN 1
						ELSE 0
					END) as q20_yes_count,
				SUM(
					CASE 
						WHEN q20_answer IN( 0,1) THEN 1
						ELSE 0
					END) as q20_tot_count,
		--
				SUM(
					CASE 
						WHEN q21_answer = 0 THEN 1
						ELSE 0
					END) as q21_yes_count,
				SUM(
					CASE 
						WHEN q21_answer IN( 0,1) THEN 1
						ELSE 0
					END) as q21_tot_count,
		--
				SUM(
					CASE 
						WHEN q22_answer = 0 THEN 1
						ELSE 0
					END) as q22_yes_count,
				SUM(
					CASE 
						WHEN q22_answer IN( 0,1) THEN 1
						ELSE 0
					END) as q22_tot_count,
		--
				SUM(
					CASE 
						WHEN q23_answer = 0 THEN 1
						ELSE 0
					END) as q23_yes_count,
				SUM(
					CASE 
						WHEN q23_answer IN( 0,1) THEN 1
						ELSE 0
					END) as q23_tot_count,
		--
				SUM(
					CASE 
						WHEN q24_answer = 0 THEN 1
						ELSE 0
					END) as q24_yes_count,
				SUM(
					CASE 
						WHEN q24_answer IN( 0,1) THEN 1
						ELSE 0
					END) as q24_tot_count,
		--
				SUM(
					CASE 
						WHEN q25_answer = 0 THEN 1
						ELSE 0
					END) as q25_yes_count,
				SUM(
					CASE 
						WHEN q25_answer IN( 0,1) THEN 1
						ELSE 0
					END) as q25_tot_count
		FROM    mh_lpa_score_summary sc LEFT JOIN mh_lpa_line_master lm ON sc.line_id=lm.line_id
		WHERE  1=1
		AND     sc.region_id = ISNULL(@l_region_id, sc.region_id)
		AND     sc.country_id = ISNULL(@l_country_id, sc.country_id)
		AND     sc.location_id = ISNULL(@l_location_id, sc.location_id)
		AND		lm.building_id=ISNULL(@l_building_id,lm.building_id)
		AND     sc.line_id = ISNULL(@l_line_id, sc.line_id)
		AND     sc.audit_date BETWEEN DATEADD(year,-1,@p_date) AND @p_date 
		)  tmp ) t
	UNPIVOT
	(score FOR question_id IN 
		(
			q0,q1,q2,q3,q4,q5,
			q6,q7,q8,q9,q10,
			q11,q12,q13,q14,q15,
			q16,q17,q18,q19,q20,
			q21,q22,q23,q24
		)
	)AS unpvt,
	mh_lpa_question_master qm
	WHERE CAST(REPLACE(unpvt.question_id,'q','') as INT)+1 = qm.question_id
order by qm.question_id


END



