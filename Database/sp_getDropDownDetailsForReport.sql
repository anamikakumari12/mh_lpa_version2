USE [mhlpa_db_dev]
GO
/****** Object:  StoredProcedure [dbo].[sp_getDropDownDetailsForReport]    Script Date: 1/2/2020 11:31:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec sp_getDropDownDetailsForReport 0,0,0,0,null

ALTER PROCEDURE [dbo].[sp_getDropDownDetailsForReport]
(
@p_region_id INT=null,
@p_country_id INT =null,
@p_location_id INT = NULL,
@p_building_id	INT=NULL,
@p_line_id INT=NULL,
@p_Current_field VARCHAR(100)=NULL
)
AS
BEGIN
 DECLARE @l_region_id INT;
 DECLARE @l_country_id INT;
 DECLARE @l_location_id INT;
 DECLARE @l_line_id INT;
DECLARE @l_building_id INT;

 IF @p_region_id IS NOT NULL AND @p_region_id != 0 
SET @l_region_id = @p_region_id;
 ELSE
SET @l_region_id = NULL;

 IF @p_country_id IS NOT NULL AND @p_country_id != 0 
SET @l_country_id = @p_country_id;

 ELSE
SET @l_country_id = NULL;

 IF @p_location_id IS NOT NULL AND @p_location_id != 0 
SET @l_location_id = @p_location_id;
 ELSE
SET @l_location_id = NULL;

 IF @p_building_id IS NOT NULL AND @p_building_id != 0 
SET @l_building_id = @p_building_id;
 ELSE
SET @l_building_id = NULL;

 IF @p_line_id IS NOT NULL AND @p_line_id != 0 
SET @l_line_id = @p_line_id;
 ELSE
SET @l_line_id = NULL;

SELECT distinct rm.region_id,region_name
FROM   mh_lpa_location_master loc,
mh_lpa_country_master cm,
mh_lpa_region_master rm,
mh_lpa_line_master lm
WHERE  lm.location_id = loc.location_id
AND    lm.country_id = cm.country_id
AND    lm.region_id = rm.region_id
AND    lm.region_id = ISNULL(@l_region_id, lm.region_id)
AND    lm.country_id = ISNULL(@l_country_id, lm.country_id)
AND    lm.location_id = ISNULL(@l_location_id, lm.location_id)
AND    lm.line_id = ISNULL(@l_line_id , lm.line_id)
ORDER BY region_name;


SELECT distinct cm.country_id, country_name
FROM mh_lpa_location_master loc,
mh_lpa_country_master cm,
mh_lpa_region_master rm,
mh_lpa_line_master lm
WHERE  lm.location_id = loc.location_id
AND    lm.country_id = cm.country_id
AND    lm.region_id = rm.region_id
AND    lm.region_id = ISNULL(@l_region_id, lm.region_id)
AND    lm.country_id = ISNULL(@l_country_id, lm.country_id)
AND    lm.location_id = ISNULL(@l_location_id, lm.location_id)
AND    lm.line_id = ISNULL(@l_line_id , lm.line_id)
ORDER BY country_name;

SELECT distinct loc.location_id, location_name
FROM   mh_lpa_location_master loc,
mh_lpa_country_master cm,
mh_lpa_region_master rm,
mh_lpa_line_master lm
WHERE  lm.location_id = loc.location_id
AND    lm.country_id = cm.country_id
AND    lm.region_id = rm.region_id
AND    lm.region_id = ISNULL(@l_region_id, lm.region_id)
AND    lm.country_id = ISNULL(@l_country_id, lm.country_id)
AND    lm.location_id = ISNULL(@l_location_id, lm.location_id)
AND    lm.line_id = ISNULL(@l_line_id , lm.line_id)
ORDER BY location_name;

SELECT distinct lm.line_id, line_name
FROM   mh_lpa_location_master loc,
mh_lpa_country_master cm,
mh_lpa_region_master rm,
mh_lpa_line_master lm
WHERE  lm.location_id = loc.location_id
AND    lm.country_id = cm.country_id
AND    lm.region_id = rm.region_id
AND    lm.region_id = ISNULL(@l_region_id, lm.region_id)
AND    lm.country_id = ISNULL(@l_country_id, lm.country_id)
AND    lm.location_id = ISNULL(@l_location_id, lm.location_id)
AND    lm.line_id = ISNULL(@l_line_id , lm.line_id)
AND		lm.building_id=ISNULL(@l_building_id, lm.building_id)
ORDER BY line_name;

-- IF(UPPER(@p_Current_field)=UPPER('Region'))
-- BEGIN
--SELECT region_name, rm.region_id
--FROM   mh_lpa_location_master loc,
--mh_lpa_country_master cm,
--mh_lpa_region_master rm,
--mh_lpa_line_master lm
--WHERE  lm.location_id = loc.location_id
--AND    lm.country_id = cm.country_id
--AND    lm.region_id = rm.region_id
--AND    lm.region_id = ISNULL(@l_region_id, lm.region_id)
--AND    lm.country_id = ISNULL(@l_country_id, lm.country_id)
--AND    lm.location_id = ISNULL(@l_location_id, lm.location_id)
--AND    lm.line_id = ISNULL(@l_line_id , lm.line_id);
-- END
-- ELSE IF(UPPER(@p_Current_field)=UPPER('Country'))
-- BEGIN
--SELECT country_name, lm.country_id
--FROM   mh_lpa_location_master loc,
--mh_lpa_country_master cm,
--mh_lpa_region_master rm,
--mh_lpa_line_master lm
--WHERE  lm.location_id = loc.location_id
--AND    lm.country_id = cm.country_id
--AND    lm.region_id = rm.region_id
--AND    lm.region_id = ISNULL(@l_region_id, lm.region_id)
--AND    lm.country_id = ISNULL(@l_country_id, lm.country_id)
--AND    lm.location_id = ISNULL(@l_location_id, lm.location_id)
--AND    lm.line_id = ISNULL(@l_line_id , lm.line_id);
-- END
-- ELSE IF(UPPER(@p_Current_field)=UPPER('Location'))
-- BEGIN
--SELECT location_name, loc.location_id
--FROM   mh_lpa_location_master loc,
--mh_lpa_country_master cm,
--mh_lpa_region_master rm,
--mh_lpa_line_master lm
--WHERE  lm.location_id = loc.location_id
--AND    lm.country_id = cm.country_id
--AND    lm.region_id = rm.region_id
--AND    lm.region_id = ISNULL(@l_region_id, lm.region_id)
--AND    lm.country_id = ISNULL(@l_country_id, lm.country_id)
--AND    lm.location_id = ISNULL(@l_location_id, lm.location_id)
--AND    lm.line_id = ISNULL(@l_line_id , lm.line_id);
-- END
-- ELSE IF(UPPER(@p_Current_field)=UPPER('Line'))
-- BEGIN
--SELECT line_name,lm.line_id
--FROM   mh_lpa_location_master loc,
--mh_lpa_country_master cm,
--mh_lpa_region_master rm,
--mh_lpa_line_master lm
--WHERE  lm.location_id = loc.location_id
--AND    lm.country_id = cm.country_id
--AND    lm.region_id = rm.region_id
--AND    lm.region_id = ISNULL(@l_region_id, lm.region_id)
--AND    lm.country_id = ISNULL(@l_country_id, lm.country_id)
--AND    lm.location_id = ISNULL(@l_location_id, lm.location_id)
--AND    lm.line_id = ISNULL(@l_line_id , lm.line_id);
-- END

select distinct  bm.building_id, bm.building_name 
from mh_lpa_building_master bm
LEFT JOIN mh_lpa_line_master lm ON bm.building_id=lm.building_id
AND lm.line_id = ISNULL(@l_line_id , lm.line_id)
AND	lm.building_id=ISNULL(@l_building_id, lm.building_id)
ORDER BY building_name;
END
