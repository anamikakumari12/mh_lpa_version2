
ALTER PROCEDURE [dbo].[InsLine](  @p_line_id    int,
        @p_region_name   VARCHAR(100),
        @p_country_name   VARCHAR(100),
        @p_location_name  VARCHAR(100),
        @p_line_code   varchar(100)= NULL,
        @p_line_name   VARCHAR(MAX),
		@p_building_name VARCHAR(MAX),
        @p_distribution_list   VARCHAR(max),
		@STARTDATE                  DATE=NULL,
        @ENDDATE                    DATE=NULL,
        @l_err_code    int OUTPUT,
        @l_err_message   varchar(100) OUTPUT
        )
AS
 SET NOCOUNT ON;
 
BEGIN
 DECLARE @l_count    INT;
 DECLARE @l_region_id  INT;
 DECLARE @l_country_id  INT;
 DECLARE @l_location_id  INT;
 DECLARE @l_line_id   INT;
 DECLARE @l_building_id   INT;
 
 EXEC getRegionId  NULL, @p_region_name, @l_region_id = @l_region_id OUTPUT;
 EXEC getCountryId  NULL, @p_country_name, @l_country_id = @l_country_id OUTPUT;
 EXEC getLocationId NULL, @p_location_name, @l_location_id = @l_location_id OUTPUT;
 SET @l_err_code = 0;
 SET @l_err_message = 'Initializing';
/*
 SELECT  @l_mail_to_list = email_list
 FROM   mh_lpa_distribution_list
 WHERE location_id = 0
 AND   UPPER(list_type) = UPPER('At New Line Addition');
*/

IF EXISTS (Select * from mh_lpa_building_master WHERE UPPER(building_name) =UPPER(RTRIM(LTRIM(@p_building_name))))
BEGIN
	Select @l_building_id=building_id from mh_lpa_building_master WHERE UPPER(building_name) =UPPER(RTRIM(LTRIM(@p_building_name)))
END
ELSE
BEGIN
	INSERT INTO mh_lpa_building_master(building_name) VALUES(RTRIM(LTRIM(@p_building_name)))
	Select @l_building_id=building_id from mh_lpa_building_master WHERE UPPER(building_name) =UPPER(RTRIM(LTRIM(@p_building_name)))
END

 IF @p_line_id IS NOT NULL AND @p_line_id != 0
 BEGIN
  UPDATE mh_lpa_line_master
  SET    location_id = @l_location_id,
    country_id = @l_country_id,
    region_id = @l_region_id,
    line_code = @p_line_code,
    line_name = @p_line_name,
	start_date = @STARTDATE,
	end_date=@ENDDATE,
    distribution_list = @p_distribution_list,
	building_id=@l_building_id
  WHERE   line_id = @p_line_id;

  UPDATE mh_lpa_line_product_relationship
		SET    end_date = @ENDDATE
		WHERE   line_id = @p_line_id
		AND    (end_date IS NULL OR	end_date>@ENDDATE)

  SET @l_err_code = 0;
   SET @l_err_message = 'Updated Successfully';
   
 END 
 ELSE
  BEGIN
 
   SELECT @l_count = count(*)  
   FROM   mh_lpa_line_master
   WHERE  UPPER(line_name) = UPPER(@p_line_name)
   AND    location_id = @l_location_id
   AND    country_id = @l_country_id
   AND    region_id = @l_region_id;
 
   IF @l_count = 0 AND @p_line_name IS NOT NULL AND @p_line_name != ''
   BEGIN
    INSERT INTO mh_lpa_line_master (
      location_id,
      country_id,
      region_id,
      line_code,
      line_name,
	  start_date,
	  end_date,
      distribution_list,
	  building_id
     ) VALUES (
      @l_location_id,
      @l_country_id,
      @l_region_id,
      @p_line_code,
      @p_line_name,
	  @STARTDATE,
	  @ENDDATE,
      @p_distribution_list,
	  @l_building_id
     );
    SET @l_err_code = 0;
    SET @l_err_message = 'Inserted Successfully';
   END
   ELSE
   BEGIN
    SELECT @l_count = count(*)  
    FROM   mh_lpa_line_master
    WHERE  UPPER(line_name) = UPPER(@p_line_name)
    AND    location_id = @l_location_id
    AND    country_id = @l_country_id
    AND    region_id = @l_region_id
    AND    end_date IS NOT NULL;
    IF @l_count > 0
     UPDATE mh_lpa_line_master
     SET    end_date = @ENDDATE, 
       distribution_list = @p_distribution_list
     WHERE  UPPER(line_name) = UPPER(@p_line_name)
     AND    location_id = @l_location_id
     AND    country_id = @l_country_id
     AND    region_id = @l_region_id
     AND    end_date IS NOT NULL;

   END;
  END;
 
END



