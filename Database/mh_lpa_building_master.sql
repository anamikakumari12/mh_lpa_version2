CREATE TABLE mh_lpa_building_master
(
building_id INT IDENTITY(1,1),
building_name VARCHAR(MAX),
region_id INT,
country_id INT,
location_id INT
)


ALTER TABLE mh_lpa_line_master ADD building_id INT

select * from mh_lpa_line_master

ALTER PROCEDURE sp_getBuildingList
AS
BEGIN
SELECT building_id,building_name FROM mh_lpa_building_master 
END