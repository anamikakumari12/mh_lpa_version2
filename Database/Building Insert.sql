TRUNCATE TABLE mh_lpa_building_master
SET IDENTITY_INSERT [dbo].[mh_lpa_building_master] ON 

GO
INSERT INTO mh_lpa_building_master(building_id,building_name)
VALUES(0,'Default Building')
SET IDENTITY_INSERT [dbo].[mh_lpa_building_master] OFF
GO

INSERT INTO mh_lpa_building_master(region_id, country_id, location_id,building_name)
VALUES(8,11,22,'Halle61')
,(8,11,22,'Werkzeugbau')
,(8,11,22,'Halle 1')
,(8,11,22,'Halle 3 alt')
,(8,11,22,'Halle 12')
,(8,11,22,'Halle 2 /3')
,(8,11,22,'Halle 61')
,(8,11,22,'Halle 3 neu')
,(8,11,22,'Halle 2')
,(10,14,27,'Air')
,(10,14,27,'Components')
,(10,14,27,'Liquid')
,(1,7,10,'AC line')
,(1,7,10,'Air filter Element')
,(1,7,10,'AOS')
,(1,7,10,'Blow molding')
,(1,7,10,'Cabin filter')
,(1,7,10,'CCV')
,(1,7,10,'Converter')
,(1,7,10,'Cylinder Head Cover')
,(1,7,10,'IM Assy line')
,(1,7,10,'injection')
,(1,7,10,'Oil Filter')
,(1,7,10,'Seperator')
,(1,7,10,'Tank')
,(1,7,10,'welding')
,(10,14,42,'East Campus')
,(10,14,42,'North Campus')
,(10,14,42,'South Campus')
,(8,11,19,'Halle 16')
,(8,11,19,'Halle 6')
,(8,11,19,'Halle 8 ')
,(8,11,19,'Halle 17')
,(8,11,19,'Halle 10-11')
,(8,11,19,'Stufe 4')
,(8,11,19,'Halle 12')
,(8,11,19,'Stufe 2')
,(8,11,19,'Hallo 6')
,(8,11,19,'Halle 5')
,(8,11,19,'Halle 4')
,(8,11,19,'Halle 3')
,(8,11,19,'Halle 3-5 Anbau')
,(8,11,19,'Stufe 3')
,(8,11,19,'Stufe 1')
,(8,11,19,'Geschoßbau EG')
,(8,11,19,'Stufe 1-2')
,(8,11,19,'Halle 7')
,(8,11,19,'Halle 8')
,(8,11,19,'Halle 14')
,(8,11,19,'Halle 23')
,(8,11,19,'Halle 6/Aussenbereich')
,(8,11,19,'Halle 6/14/12/8/2/15')
,(8,11,19,'Halle 9/14')
,(8,11,19,'Halle 9/14/Aussenbereich')
,(8,11,19,'Halle 33')
(1,7,11,'ADB')
,(1,7,11,'ASSEMBLY 1')
,(1,7,11,'Center tube')
,(1,7,11,'Centrifuge')
,(1,7,11,'EP')
,(1,7,11,'HASOL Gluing')
,(1,7,11,'HU')
,(1,7,11,'IMM1')
,(1,7,11,'IMM2')
,(1,7,11,'IMM3')
,(1,7,11,'IR welding')
,(1,7,11,'NLG')
,(1,7,11,'Oil Moudle')
,(1,7,11,'Painting')
,(1,7,11,'Pleating 1')  
,(1,7,11,'Pleating 2') 
,(1,7,11,'Preline')
,(1,7,11,'PUR1')
,(1,7,11,'PUR2')
,(1,7,11,'PUR3')
,(1,7,11,'Resin Feeding')
,(1,7,11,'Spin on')
,(1,7,11,'Stamping')
,(1,7,11,'Washing1')
,(1,7,11,'Washing2')
,(1,7,11,'ASSEMBLY 2')
,(1,7,11,'ASSEMBLY 3')
,(8,16,31,'APU 1')
,(8,16,31,'APU 2')
,(8,16,31,'APU 3')
,(8,16,31,'APU 4')
,(8,16,31,'APU 5')
,(8,16,31,'X')
GO
update mh_lpa_line_master set building_id=1 where line_id=2413
update mh_lpa_line_master set building_id=2 where line_id=2414
update mh_lpa_line_master set building_id=0 where line_id=2415
update mh_lpa_line_master set building_id=0 where line_id=2462
update mh_lpa_line_master set building_id=0 where line_id=2463
update mh_lpa_line_master set building_id=0 where line_id=2464
update mh_lpa_line_master set building_id=0 where line_id=2465
update mh_lpa_line_master set building_id=0 where line_id=2466
update mh_lpa_line_master set building_id=0 where line_id=2467
update mh_lpa_line_master set building_id=3 where line_id=2659
update mh_lpa_line_master set building_id=3 where line_id=2660
update mh_lpa_line_master set building_id=3 where line_id=2661
update mh_lpa_line_master set building_id=4 where line_id=2662
update mh_lpa_line_master set building_id=5 where line_id=2663
update mh_lpa_line_master set building_id=5 where line_id=2664
update mh_lpa_line_master set building_id=5 where line_id=2665
update mh_lpa_line_master set building_id=3 where line_id=2666
update mh_lpa_line_master set building_id=6 where line_id=1825
update mh_lpa_line_master set building_id=3 where line_id=1826
update mh_lpa_line_master set building_id=1 where line_id=1827
update mh_lpa_line_master set building_id=4 where line_id=1828
update mh_lpa_line_master set building_id=8 where line_id=1829
update mh_lpa_line_master set building_id=8 where line_id=1830
update mh_lpa_line_master set building_id=9 where line_id=1831
update mh_lpa_line_master set building_id=9 where line_id=1832
update mh_lpa_line_master set building_id=9 where line_id=1833
update mh_lpa_line_master set building_id=0 where line_id=1834
update mh_lpa_line_master set building_id=5 where line_id=1835
update mh_lpa_line_master set building_id=5 where line_id=1836
update mh_lpa_line_master set building_id=1 where line_id=1837
update mh_lpa_line_master set building_id=0 where line_id=2658
update mh_lpa_line_master set building_id=4 where line_id=2969
update mh_lpa_line_master set building_id=8 where line_id=2970
GO
update mh_lpa_line_master set building_id=10 where line_id=2726
update mh_lpa_line_master set building_id=10 where line_id=2727
update mh_lpa_line_master set building_id=10 where line_id=2728
update mh_lpa_line_master set building_id=10 where line_id=2729
update mh_lpa_line_master set building_id=10 where line_id=2733
update mh_lpa_line_master set building_id=10 where line_id=2732
update mh_lpa_line_master set building_id=10 where line_id=2735
update mh_lpa_line_master set building_id=10 where line_id=2734
update mh_lpa_line_master set building_id=10 where line_id=2731
update mh_lpa_line_master set building_id=10 where line_id=2730
update mh_lpa_line_master set building_id=10 where line_id=2737
update mh_lpa_line_master set building_id=10 where line_id=2736
update mh_lpa_line_master set building_id=10 where line_id=1474
update mh_lpa_line_master set building_id=10 where line_id=2738
update mh_lpa_line_master set building_id=10 where line_id=2739
update mh_lpa_line_master set building_id=10 where line_id=2740
update mh_lpa_line_master set building_id=10 where line_id=1483
update mh_lpa_line_master set building_id=11 where line_id=1473
update mh_lpa_line_master set building_id=11 where line_id=1471
update mh_lpa_line_master set building_id=11 where line_id=1472
update mh_lpa_line_master set building_id=11 where line_id=2682
update mh_lpa_line_master set building_id=11 where line_id=2683
update mh_lpa_line_master set building_id=11 where line_id=2684
update mh_lpa_line_master set building_id=11 where line_id=2681
update mh_lpa_line_master set building_id=11 where line_id=2680
update mh_lpa_line_master set building_id=11 where line_id=2689
update mh_lpa_line_master set building_id=11 where line_id=2685
update mh_lpa_line_master set building_id=11 where line_id=2686
update mh_lpa_line_master set building_id=11 where line_id=2688
update mh_lpa_line_master set building_id=11 where line_id=2687
update mh_lpa_line_master set building_id=11 where line_id=2709
update mh_lpa_line_master set building_id=11 where line_id=2707
update mh_lpa_line_master set building_id=11 where line_id=2708
update mh_lpa_line_master set building_id=12 where line_id=2690
update mh_lpa_line_master set building_id=12 where line_id=2691
update mh_lpa_line_master set building_id=12 where line_id=2692
update mh_lpa_line_master set building_id=12 where line_id=1479
update mh_lpa_line_master set building_id=12 where line_id=1478
update mh_lpa_line_master set building_id=12 where line_id=2693
update mh_lpa_line_master set building_id=12 where line_id=2695
update mh_lpa_line_master set building_id=12 where line_id=2694
update mh_lpa_line_master set building_id=12 where line_id=2696
update mh_lpa_line_master set building_id=12 where line_id=2697
update mh_lpa_line_master set building_id=12 where line_id=2698
update mh_lpa_line_master set building_id=12 where line_id=2699
update mh_lpa_line_master set building_id=12 where line_id=2700
update mh_lpa_line_master set building_id=12 where line_id=2701
update mh_lpa_line_master set building_id=12 where line_id=2702
update mh_lpa_line_master set building_id=12 where line_id=2703
update mh_lpa_line_master set building_id=12 where line_id=2704
update mh_lpa_line_master set building_id=12 where line_id=2705
update mh_lpa_line_master set building_id=12 where line_id=2675
update mh_lpa_line_master set building_id=12 where line_id=2676
update mh_lpa_line_master set building_id=12 where line_id=2677
update mh_lpa_line_master set building_id=12 where line_id=2678
update mh_lpa_line_master set building_id=12 where line_id=2679
update mh_lpa_line_master set building_id=12 where line_id=2710
update mh_lpa_line_master set building_id=12 where line_id=2711
update mh_lpa_line_master set building_id=12 where line_id=2712
update mh_lpa_line_master set building_id=12 where line_id=2713
update mh_lpa_line_master set building_id=12 where line_id=2714
update mh_lpa_line_master set building_id=12 where line_id=2715
update mh_lpa_line_master set building_id=12 where line_id=2716
update mh_lpa_line_master set building_id=12 where line_id=2717
update mh_lpa_line_master set building_id=12 where line_id=2718
update mh_lpa_line_master set building_id=12 where line_id=2719
update mh_lpa_line_master set building_id=12 where line_id=2720
update mh_lpa_line_master set building_id=12 where line_id=2721
update mh_lpa_line_master set building_id=12 where line_id=2722
update mh_lpa_line_master set building_id=12 where line_id=2723
update mh_lpa_line_master set building_id=12 where line_id=2724
GO


update mh_lpa_line_master set building_id=13 where line_id=2013
update mh_lpa_line_master set building_id=13 where line_id=2014
update mh_lpa_line_master set building_id=13 where line_id=2018
update mh_lpa_line_master set building_id=13 where line_id=2021
update mh_lpa_line_master set building_id=13 where line_id=2036
update mh_lpa_line_master set building_id=13 where line_id=2517
update mh_lpa_line_master set building_id=13 where line_id=2519
update mh_lpa_line_master set building_id=13 where line_id=2927
update mh_lpa_line_master set building_id=13 where line_id=2928
update mh_lpa_line_master set building_id=13 where line_id=2520
update mh_lpa_line_master set building_id=13 where line_id=2523
update mh_lpa_line_master set building_id=13 where line_id=2524
update mh_lpa_line_master set building_id=13 where line_id=2525
update mh_lpa_line_master set building_id=13 where line_id=2527
update mh_lpa_line_master set building_id=13 where line_id=2515
update mh_lpa_line_master set building_id=13 where line_id=2749
update mh_lpa_line_master set building_id=13 where line_id=2964
update mh_lpa_line_master set building_id=13 where line_id=2965
update mh_lpa_line_master set building_id=13 where line_id=2929
update mh_lpa_line_master set building_id=13 where line_id=2933
update mh_lpa_line_master set building_id=13 where line_id=2934
update mh_lpa_line_master set building_id=13 where line_id=2937
update mh_lpa_line_master set building_id=13 where line_id=2938
update mh_lpa_line_master set building_id=13 where line_id=2939
update mh_lpa_line_master set building_id=13 where line_id=2940
update mh_lpa_line_master set building_id=13 where line_id=2941
update mh_lpa_line_master set building_id=13 where line_id=2942
update mh_lpa_line_master set building_id=13 where line_id=2944
update mh_lpa_line_master set building_id=13 where line_id=2947
update mh_lpa_line_master set building_id=13 where line_id=2948
update mh_lpa_line_master set building_id=13 where line_id=2949
update mh_lpa_line_master set building_id=13 where line_id=2950
update mh_lpa_line_master set building_id=13 where line_id=2952
update mh_lpa_line_master set building_id=13 where line_id=2953
update mh_lpa_line_master set building_id=13 where line_id=2955
update mh_lpa_line_master set building_id=13 where line_id=2959
update mh_lpa_line_master set building_id=13 where line_id=2961
update mh_lpa_line_master set building_id=13 where line_id=2963
update mh_lpa_line_master set building_id=13 where line_id=2966
update mh_lpa_line_master set building_id=13 where line_id=2976
update mh_lpa_line_master set building_id=13 where line_id=2920
update mh_lpa_line_master set building_id=13 where line_id=2922
update mh_lpa_line_master set building_id=14 where line_id=2005
update mh_lpa_line_master set building_id=14 where line_id=2006
update mh_lpa_line_master set building_id=14 where line_id=2007
update mh_lpa_line_master set building_id=15 where line_id=2035
update mh_lpa_line_master set building_id=16 where line_id=2513
update mh_lpa_line_master set building_id=17 where line_id=2008
update mh_lpa_line_master set building_id=17 where line_id=2009
update mh_lpa_line_master set building_id=17 where line_id=2010
update mh_lpa_line_master set building_id=18 where line_id=2015
update mh_lpa_line_master set building_id=18 where line_id=2016
update mh_lpa_line_master set building_id=19 where line_id=2962
update mh_lpa_line_master set building_id=20 where line_id=2960
update mh_lpa_line_master set building_id=21 where line_id=1997
update mh_lpa_line_master set building_id=21 where line_id=1998
update mh_lpa_line_master set building_id=21 where line_id=1999
update mh_lpa_line_master set building_id=21 where line_id=2004
update mh_lpa_line_master set building_id=21 where line_id=2745
update mh_lpa_line_master set building_id=21 where line_id=2945
update mh_lpa_line_master set building_id=21 where line_id=2946
update mh_lpa_line_master set building_id=21 where line_id=2956
update mh_lpa_line_master set building_id=21 where line_id=2957
update mh_lpa_line_master set building_id=21 where line_id=2958
update mh_lpa_line_master set building_id=22 where line_id=2913
update mh_lpa_line_master set building_id=22 where line_id=2914
update mh_lpa_line_master set building_id=22 where line_id=2915
update mh_lpa_line_master set building_id=22 where line_id=2916
update mh_lpa_line_master set building_id=22 where line_id=2917
update mh_lpa_line_master set building_id=22 where line_id=2918
update mh_lpa_line_master set building_id=22 where line_id=2926
update mh_lpa_line_master set building_id=23 where line_id=2023
update mh_lpa_line_master set building_id=23 where line_id=2024
update mh_lpa_line_master set building_id=23 where line_id=2025
update mh_lpa_line_master set building_id=23 where line_id=2026
update mh_lpa_line_master set building_id=23 where line_id=2030
update mh_lpa_line_master set building_id=23 where line_id=2031
update mh_lpa_line_master set building_id=23 where line_id=2032
update mh_lpa_line_master set building_id=23 where line_id=2033
update mh_lpa_line_master set building_id=23 where line_id=2034
update mh_lpa_line_master set building_id=24 where line_id=2935
update mh_lpa_line_master set building_id=24 where line_id=2951
update mh_lpa_line_master set building_id=25 where line_id=2027
update mh_lpa_line_master set building_id=25 where line_id=2936
update mh_lpa_line_master set building_id=26 where line_id=1996
update mh_lpa_line_master set building_id=26 where line_id=2040
update mh_lpa_line_master set building_id=26 where line_id=2968
update mh_lpa_line_master set building_id=26 where line_id=2921







update mh_lpa_line_master set building_id=27 where line_id=2671
update mh_lpa_line_master set building_id=27 where line_id=2556
update mh_lpa_line_master set building_id=27 where line_id=2615
update mh_lpa_line_master set building_id=27 where line_id=2557
update mh_lpa_line_master set building_id=27 where line_id=2551
update mh_lpa_line_master set building_id=27 where line_id=2547
update mh_lpa_line_master set building_id=27 where line_id=2548
update mh_lpa_line_master set building_id=27 where line_id=2552
update mh_lpa_line_master set building_id=28 where line_id=2540
update mh_lpa_line_master set building_id=28 where line_id=2569
update mh_lpa_line_master set building_id=28 where line_id=2570
update mh_lpa_line_master set building_id=28 where line_id=2571
update mh_lpa_line_master set building_id=28 where line_id=2572
update mh_lpa_line_master set building_id=28 where line_id=2573
update mh_lpa_line_master set building_id=28 where line_id=2979
update mh_lpa_line_master set building_id=28 where line_id=2980
update mh_lpa_line_master set building_id=28 where line_id=2574
update mh_lpa_line_master set building_id=28 where line_id=2575
update mh_lpa_line_master set building_id=28 where line_id=2576
update mh_lpa_line_master set building_id=28 where line_id=2577
update mh_lpa_line_master set building_id=28 where line_id=2581
update mh_lpa_line_master set building_id=28 where line_id=2590
update mh_lpa_line_master set building_id=28 where line_id=2591
update mh_lpa_line_master set building_id=28 where line_id=2592
update mh_lpa_line_master set building_id=28 where line_id=2593
update mh_lpa_line_master set building_id=28 where line_id=2594
update mh_lpa_line_master set building_id=28 where line_id=2563
update mh_lpa_line_master set building_id=28 where line_id=2597
update mh_lpa_line_master set building_id=28 where line_id=2598
update mh_lpa_line_master set building_id=28 where line_id=2599
update mh_lpa_line_master set building_id=28 where line_id=2564
update mh_lpa_line_master set building_id=28 where line_id=2565
update mh_lpa_line_master set building_id=28 where line_id=2630
update mh_lpa_line_master set building_id=28 where line_id=2567
update mh_lpa_line_master set building_id=28 where line_id=2568
update mh_lpa_line_master set building_id=28 where line_id=2553
update mh_lpa_line_master set building_id=28 where line_id=2554
update mh_lpa_line_master set building_id=28 where line_id=2578
update mh_lpa_line_master set building_id=28 where line_id=2579
update mh_lpa_line_master set building_id=28 where line_id=2580
update mh_lpa_line_master set building_id=28 where line_id=2582
update mh_lpa_line_master set building_id=28 where line_id=2583
update mh_lpa_line_master set building_id=28 where line_id=2584
update mh_lpa_line_master set building_id=28 where line_id=2585
update mh_lpa_line_master set building_id=28 where line_id=2586
update mh_lpa_line_master set building_id=28 where line_id=2587
update mh_lpa_line_master set building_id=28 where line_id=2588
update mh_lpa_line_master set building_id=28 where line_id=2589
update mh_lpa_line_master set building_id=28 where line_id=2600
update mh_lpa_line_master set building_id=28 where line_id=2601
update mh_lpa_line_master set building_id=28 where line_id=2602
update mh_lpa_line_master set building_id=28 where line_id=2603
update mh_lpa_line_master set building_id=28 where line_id=2604
update mh_lpa_line_master set building_id=28 where line_id=2605
update mh_lpa_line_master set building_id=28 where line_id=2606
update mh_lpa_line_master set building_id=28 where line_id=2607
update mh_lpa_line_master set building_id=28 where line_id=3001
update mh_lpa_line_master set building_id=28 where line_id=3000
update mh_lpa_line_master set building_id=28 where line_id=3002
update mh_lpa_line_master set building_id=28 where line_id=2611
update mh_lpa_line_master set building_id=28 where line_id=2612
update mh_lpa_line_master set building_id=28 where line_id=2613
update mh_lpa_line_master set building_id=28 where line_id=2978
update mh_lpa_line_master set building_id=28 where line_id=2614
update mh_lpa_line_master set building_id=29 where line_id=2616
update mh_lpa_line_master set building_id=29 where line_id=2624
update mh_lpa_line_master set building_id=29 where line_id=2626
update mh_lpa_line_master set building_id=29 where line_id=2627
update mh_lpa_line_master set building_id=29 where line_id=2638
update mh_lpa_line_master set building_id=29 where line_id=2639
update mh_lpa_line_master set building_id=29 where line_id=2640
update mh_lpa_line_master set building_id=29 where line_id=2641
update mh_lpa_line_master set building_id=29 where line_id=2642
update mh_lpa_line_master set building_id=29 where line_id=2643
update mh_lpa_line_master set building_id=29 where line_id=2644
update mh_lpa_line_master set building_id=29 where line_id=2645
update mh_lpa_line_master set building_id=29 where line_id=2646
update mh_lpa_line_master set building_id=29 where line_id=2539
update mh_lpa_line_master set building_id=29 where line_id=2546
update mh_lpa_line_master set building_id=29 where line_id=2991
update mh_lpa_line_master set building_id=29 where line_id=2550
update mh_lpa_line_master set building_id=29 where line_id=2617
update mh_lpa_line_master set building_id=29 where line_id=2619
update mh_lpa_line_master set building_id=29 where line_id=2620
update mh_lpa_line_master set building_id=29 where line_id=2621
update mh_lpa_line_master set building_id=29 where line_id=2622
update mh_lpa_line_master set building_id=29 where line_id=2623
update mh_lpa_line_master set building_id=29 where line_id=2625
update mh_lpa_line_master set building_id=29 where line_id=2628
update mh_lpa_line_master set building_id=29 where line_id=2538
update mh_lpa_line_master set building_id=29 where line_id=2541
update mh_lpa_line_master set building_id=29 where line_id=2542
update mh_lpa_line_master set building_id=29 where line_id=2543

update mh_lpa_line_master set building_id=30 where line_id=2725
update mh_lpa_line_master set building_id=31 where line_id=2409
update mh_lpa_line_master set building_id=31 where line_id=2410
update mh_lpa_line_master set building_id=31 where line_id=2411
update mh_lpa_line_master set building_id=31 where line_id=2412
update mh_lpa_line_master set building_id=32 where line_id=2469
update mh_lpa_line_master set building_id=32 where line_id=2470
update mh_lpa_line_master set building_id=32 where line_id=2471
update mh_lpa_line_master set building_id=33 where line_id=2653
update mh_lpa_line_master set building_id=33 where line_id=2654
update mh_lpa_line_master set building_id=3 where line_id=1702
update mh_lpa_line_master set building_id=34 where line_id=1703
update mh_lpa_line_master set building_id=9 where line_id=1704
update mh_lpa_line_master set building_id=3 where line_id=1705
update mh_lpa_line_master set building_id=35 where line_id=1706
update mh_lpa_line_master set building_id=35 where line_id=1707
update mh_lpa_line_master set building_id=35 where line_id=1708
update mh_lpa_line_master set building_id=35 where line_id=1709
update mh_lpa_line_master set building_id=36 where line_id=1710
update mh_lpa_line_master set building_id=36 where line_id=1711
update mh_lpa_line_master set building_id=37 where line_id=1712
update mh_lpa_line_master set building_id=38 where line_id=1713
update mh_lpa_line_master set building_id=36 where line_id=1714
update mh_lpa_line_master set building_id=36 where line_id=1715
update mh_lpa_line_master set building_id=36 where line_id=1716
update mh_lpa_line_master set building_id=36 where line_id=1717
update mh_lpa_line_master set building_id=36 where line_id=1718
update mh_lpa_line_master set building_id=36 where line_id=1719
update mh_lpa_line_master set building_id=36 where line_id=1720
update mh_lpa_line_master set building_id=36 where line_id=1721
update mh_lpa_line_master set building_id=36 where line_id=1722
update mh_lpa_line_master set building_id=36 where line_id=1723
update mh_lpa_line_master set building_id=36 where line_id=1724
update mh_lpa_line_master set building_id=39 where line_id=1725
update mh_lpa_line_master set building_id=39 where line_id=1726
update mh_lpa_line_master set building_id=40 where line_id=1727
update mh_lpa_line_master set building_id=41 where line_id=1728
update mh_lpa_line_master set building_id=41 where line_id=1729
update mh_lpa_line_master set building_id=42 where line_id=1730
update mh_lpa_line_master set building_id=42 where line_id=1731
update mh_lpa_line_master set building_id=42 where line_id=1732
update mh_lpa_line_master set building_id=42 where line_id=1733
update mh_lpa_line_master set building_id=42 where line_id=1734
update mh_lpa_line_master set building_id=41 where line_id=1735
update mh_lpa_line_master set building_id=43 where line_id=1736
update mh_lpa_line_master set building_id=44 where line_id=1738
update mh_lpa_line_master set building_id=45 where line_id=1739
update mh_lpa_line_master set building_id=44 where line_id=1740
update mh_lpa_line_master set building_id=46 where line_id=1741
update mh_lpa_line_master set building_id=37 where line_id=1742
update mh_lpa_line_master set building_id=31 where line_id=1743
update mh_lpa_line_master set building_id=47 where line_id=1744
update mh_lpa_line_master set building_id=47 where line_id=1745
update mh_lpa_line_master set building_id=47 where line_id=1746
update mh_lpa_line_master set building_id=47 where line_id=1747
update mh_lpa_line_master set building_id=47 where line_id=1748
update mh_lpa_line_master set building_id=47 where line_id=1749
update mh_lpa_line_master set building_id=48 where line_id=1750
update mh_lpa_line_master set building_id=48 where line_id=1752
update mh_lpa_line_master set building_id=48 where line_id=1753
update mh_lpa_line_master set building_id=31 where line_id=1754
update mh_lpa_line_master set building_id=48 where line_id=1755
update mh_lpa_line_master set building_id=31 where line_id=1756
update mh_lpa_line_master set building_id=33 where line_id=1757
update mh_lpa_line_master set building_id=49 where line_id=1759
update mh_lpa_line_master set building_id=50 where line_id=1760
update mh_lpa_line_master set building_id=51 where line_id=1761
update mh_lpa_line_master set building_id=52 where line_id=1762
update mh_lpa_line_master set building_id=49 where line_id=1763
update mh_lpa_line_master set building_id=31 where line_id=1764
update mh_lpa_line_master set building_id=53 where line_id=1765
update mh_lpa_line_master set building_id=53 where line_id=1766
update mh_lpa_line_master set building_id=53 where line_id=1767
update mh_lpa_line_master set building_id=51 where line_id=1768
update mh_lpa_line_master set building_id=0 where line_id=1769
update mh_lpa_line_master set building_id=0 where line_id=1770
update mh_lpa_line_master set building_id=30 where line_id=1771
update mh_lpa_line_master set building_id=30 where line_id=1772
update mh_lpa_line_master set building_id=30 where line_id=1773
update mh_lpa_line_master set building_id=30 where line_id=1774
update mh_lpa_line_master set building_id=32 where line_id=2442
update mh_lpa_line_master set building_id=55 where line_id=2443
update mh_lpa_line_master set building_id=32 where line_id=2444
update mh_lpa_line_master set building_id=55 where line_id=2668
update mh_lpa_line_master set building_id=36 where line_id=2891
update mh_lpa_line_master set building_id=36 where line_id=2892


update mh_lpa_line_master set building_id=56 where line_id=88
update mh_lpa_line_master set building_id=57 where line_id=1428
update mh_lpa_line_master set building_id=57 where line_id=1429
update mh_lpa_line_master set building_id=81 where line_id=1421
update mh_lpa_line_master set building_id=81 where line_id=1423
update mh_lpa_line_master set building_id=81 where line_id=1424
update mh_lpa_line_master set building_id=81 where line_id=1425
update mh_lpa_line_master set building_id=81 where line_id=1426
update mh_lpa_line_master set building_id=82 where line_id=1431
update mh_lpa_line_master set building_id=82 where line_id=1437
update mh_lpa_line_master set building_id=82 where line_id=2510
update mh_lpa_line_master set building_id=82 where line_id=2746
update mh_lpa_line_master set building_id=82 where line_id=2815
update mh_lpa_line_master set building_id=82 where line_id=2988
update mh_lpa_line_master set building_id=82 where line_id=2747
update mh_lpa_line_master set building_id=82 where line_id=2748
update mh_lpa_line_master set building_id=17 where line_id=87
update mh_lpa_line_master set building_id=18 where line_id=89
update mh_lpa_line_master set building_id=58 where line_id=2854
update mh_lpa_line_master set building_id=59 where line_id=100
update mh_lpa_line_master set building_id=0 where line_id=2985
update mh_lpa_line_master set building_id=61 where line_id=2986
update mh_lpa_line_master set building_id=62 where line_id=98
update mh_lpa_line_master set building_id=63 where line_id=91
update mh_lpa_line_master set building_id=64 where line_id=92
update mh_lpa_line_master set building_id=65 where line_id=2989
update mh_lpa_line_master set building_id=66 where line_id=97
update mh_lpa_line_master set building_id=67 where line_id=2984
update mh_lpa_line_master set building_id=68 where line_id=99
update mh_lpa_line_master set building_id=68 where line_id=2814
update mh_lpa_line_master set building_id=69 where line_id=1435
update mh_lpa_line_master set building_id=70 where line_id=82
update mh_lpa_line_master set building_id=70 where line_id=81
update mh_lpa_line_master set building_id=71 where line_id=1436
update mh_lpa_line_master set building_id=72 where line_id=90
update mh_lpa_line_master set building_id=73 where line_id=85
update mh_lpa_line_master set building_id=74 where line_id=2990
update mh_lpa_line_master set building_id=75 where line_id=2743
update mh_lpa_line_master set building_id=76 where line_id=84
update mh_lpa_line_master set building_id=77 where line_id=86
update mh_lpa_line_master set building_id=78 where line_id=1442
--update mh_lpa_line_master set building_id=79 where line_id=???
--update mh_lpa_line_master set building_id=79 where line_id=???
update mh_lpa_line_master set building_id=26 where line_id=2987



update mh_lpa_line_master set building_id=83 where line_id=2503
update mh_lpa_line_master set building_id=83 where line_id=1779
update mh_lpa_line_master set building_id=83 where line_id=1803
update mh_lpa_line_master set building_id=83 where line_id=1806
update mh_lpa_line_master set building_id=83 where line_id=1807
update mh_lpa_line_master set building_id=83 where line_id=1808
update mh_lpa_line_master set building_id=83 where line_id=1809
update mh_lpa_line_master set building_id=83 where line_id=1810
update mh_lpa_line_master set building_id=83 where line_id=1811
update mh_lpa_line_master set building_id=83 where line_id=2060
update mh_lpa_line_master set building_id=83 where line_id=2062
update mh_lpa_line_master set building_id=83 where line_id=2063
update mh_lpa_line_master set building_id=83 where line_id=2064
update mh_lpa_line_master set building_id=83 where line_id=2065
update mh_lpa_line_master set building_id=83 where line_id=2066
update mh_lpa_line_master set building_id=83 where line_id=2387
update mh_lpa_line_master set building_id=83 where line_id=2078
update mh_lpa_line_master set building_id=83 where line_id=2079
update mh_lpa_line_master set building_id=83 where line_id=2080
update mh_lpa_line_master set building_id=83 where line_id=2081
update mh_lpa_line_master set building_id=83 where line_id=2082
update mh_lpa_line_master set building_id=83 where line_id=2083
update mh_lpa_line_master set building_id=83 where line_id=2084
update mh_lpa_line_master set building_id=83 where line_id=2117
update mh_lpa_line_master set building_id=84 where line_id=2498
update mh_lpa_line_master set building_id=84 where line_id=2499
update mh_lpa_line_master set building_id=84 where line_id=2500
update mh_lpa_line_master set building_id=84 where line_id=1815
update mh_lpa_line_master set building_id=84 where line_id=1817
update mh_lpa_line_master set building_id=84 where line_id=1820
update mh_lpa_line_master set building_id=84 where line_id=1824
update mh_lpa_line_master set building_id=84 where line_id=2105
update mh_lpa_line_master set building_id=84 where line_id=2106
update mh_lpa_line_master set building_id=84 where line_id=2107
update mh_lpa_line_master set building_id=84 where line_id=2109
update mh_lpa_line_master set building_id=84 where line_id=2110
update mh_lpa_line_master set building_id=84 where line_id=2111
update mh_lpa_line_master set building_id=84 where line_id=2112
update mh_lpa_line_master set building_id=84 where line_id=2113
update mh_lpa_line_master set building_id=84 where line_id=2114
update mh_lpa_line_master set building_id=84 where line_id=2115
update mh_lpa_line_master set building_id=84 where line_id=2116
update mh_lpa_line_master set building_id=84 where line_id=2902
update mh_lpa_line_master set building_id=84 where line_id=2903
update mh_lpa_line_master set building_id=84 where line_id=2904
update mh_lpa_line_master set building_id=84 where line_id=2905
update mh_lpa_line_master set building_id=84 where line_id=2906
update mh_lpa_line_master set building_id=84 where line_id=2907
update mh_lpa_line_master set building_id=85 where line_id=1782
update mh_lpa_line_master set building_id=85 where line_id=1784
update mh_lpa_line_master set building_id=85 where line_id=1786
update mh_lpa_line_master set building_id=85 where line_id=1788
update mh_lpa_line_master set building_id=85 where line_id=1789
update mh_lpa_line_master set building_id=85 where line_id=1793
update mh_lpa_line_master set building_id=85 where line_id=1794
update mh_lpa_line_master set building_id=85 where line_id=2067
update mh_lpa_line_master set building_id=85 where line_id=2068
update mh_lpa_line_master set building_id=85 where line_id=2069
update mh_lpa_line_master set building_id=85 where line_id=2071
update mh_lpa_line_master set building_id=85 where line_id=2072
update mh_lpa_line_master set building_id=85 where line_id=2073
update mh_lpa_line_master set building_id=85 where line_id=2074
update mh_lpa_line_master set building_id=85 where line_id=2075
update mh_lpa_line_master set building_id=85 where line_id=2077
update mh_lpa_line_master set building_id=85 where line_id=2909
update mh_lpa_line_master set building_id=86 where line_id=2395
update mh_lpa_line_master set building_id=86 where line_id=2085
update mh_lpa_line_master set building_id=86 where line_id=2086
update mh_lpa_line_master set building_id=86 where line_id=2087
update mh_lpa_line_master set building_id=86 where line_id=2088
update mh_lpa_line_master set building_id=86 where line_id=2089
update mh_lpa_line_master set building_id=86 where line_id=2090
update mh_lpa_line_master set building_id=86 where line_id=2091
update mh_lpa_line_master set building_id=86 where line_id=2093
update mh_lpa_line_master set building_id=86 where line_id=2094
update mh_lpa_line_master set building_id=86 where line_id=2095
update mh_lpa_line_master set building_id=86 where line_id=2096
update mh_lpa_line_master set building_id=86 where line_id=2097
update mh_lpa_line_master set building_id=86 where line_id=2098
update mh_lpa_line_master set building_id=86 where line_id=2099
update mh_lpa_line_master set building_id=86 where line_id=2100
update mh_lpa_line_master set building_id=86 where line_id=2101
update mh_lpa_line_master set building_id=86 where line_id=2102
update mh_lpa_line_master set building_id=86 where line_id=2103
update mh_lpa_line_master set building_id=86 where line_id=2910
update mh_lpa_line_master set building_id=87 where line_id=2058
update mh_lpa_line_master set building_id=87 where line_id=2059
update mh_lpa_line_master set building_id=87 where line_id=2473
update mh_lpa_line_master set building_id=87 where line_id=2475
update mh_lpa_line_master set building_id=87 where line_id=2476
update mh_lpa_line_master set building_id=87 where line_id=2477
update mh_lpa_line_master set building_id=87 where line_id=2478
update mh_lpa_line_master set building_id=87 where line_id=2479
update mh_lpa_line_master set building_id=87 where line_id=2480
update mh_lpa_line_master set building_id=87 where line_id=2481
update mh_lpa_line_master set building_id=87 where line_id=1780
update mh_lpa_line_master set building_id=87 where line_id=1790
update mh_lpa_line_master set building_id=87 where line_id=1798
update mh_lpa_line_master set building_id=87 where line_id=1802
update mh_lpa_line_master set building_id=87 where line_id=1813
update mh_lpa_line_master set building_id=87 where line_id=1814
update mh_lpa_line_master set building_id=87 where line_id=1816
update mh_lpa_line_master set building_id=87 where line_id=1823
update mh_lpa_line_master set building_id=87 where line_id=2061
update mh_lpa_line_master set building_id=87 where line_id=2104
update mh_lpa_line_master set building_id=87 where line_id=2506
update mh_lpa_line_master set building_id=87 where line_id=2911
update mh_lpa_line_master set building_id=87 where line_id=2912
update mh_lpa_line_master set building_id=88 where line_id=2474
update mh_lpa_line_master set building_id=88 where line_id=1781
update mh_lpa_line_master set building_id=88 where line_id=1783
update mh_lpa_line_master set building_id=88 where line_id=1785
update mh_lpa_line_master set building_id=88 where line_id=1791
update mh_lpa_line_master set building_id=88 where line_id=1792
update mh_lpa_line_master set building_id=88 where line_id=1795
update mh_lpa_line_master set building_id=88 where line_id=1818
update mh_lpa_line_master set building_id=88 where line_id=2070
update mh_lpa_line_master set building_id=88 where line_id=2076
update mh_lpa_line_master set building_id=88 where line_id=2108
GO
