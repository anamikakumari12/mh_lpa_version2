USE [mhlpa_db_dev]
GO
/****** Object:  StoredProcedure [dbo].[sp_getLPAPlanVsActuals]    Script Date: 1/2/2020 4:57:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER  procedure [dbo].[sp_getLPAPlanVsActuals]
(
	@p_region_id          INT=null,
	@p_country_id        INT=null,
	@p_location_id              INT=null,
	@p_building_id	INT=NULL,
	@p_line_id                  INT=null,
	@p_date                           DATE
)
AS
BEGIN
 
	DECLARE @l_region_id       INT;
	DECLARE @l_country_id             INT;
	DECLARE @l_location_id            INT;
	DECLARE @l_line_id         INT;
	DECLARE @i					INT;
	DECLARE @l_count				INT;
	DECLARE @l_date				DATE;
	   DECLARE @l_building_id            INT;

	DECLARE @l_tempTable TABLE
	(
	cy_audit_planned	INT,
	cy_audit_performed	INT,
	ly_audit_planned	INT,
	ly_audit_performed	INT,
	audit_date			DATE
	);
		IF @p_building_id IS NULL OR @p_building_id = 0 
              SET @l_building_id = NULL;
       ELSE
              SET @l_building_id = @p_building_id;

       IF @p_region_id IS NULL OR @p_region_id = 0 
              SET @l_region_id = NULL;
       ELSE
              SET @l_region_id = @p_region_id;
 
       IF @p_country_id IS NULL OR @p_country_id = 0 
              SET @l_country_id = NULL;
       ELSE
              SET @l_country_id = @p_country_id;
 
       IF @p_location_id IS NULL OR @p_location_id = 0 
              SET @l_location_id = NULL;
       ELSE
              SET @l_location_id = @p_location_id;
 
       IF @p_line_id IS NULL OR @p_line_id = 0 
              SET @l_line_id = NULL;
       ELSE
              SET @l_line_id = @p_line_id;



		INSERT INTO @l_tempTable
		SELECT 
                    count(*) cy_audit_planned,
                    0 cy_audit_performed,
					0 ly_audit_planned,
					0 ly_audit_performed,
                    CONVERT(DATE, '01-'+FORMAT(planned_date,'MM-yyyy'),105) dt
        FROM    mh_lpa_audit_plan p,
				mh_lpa_line_master l
        WHERE   p.line_id = l.line_id
		AND     l.region_id = ISNULL(@l_region_id, l.region_id)
        AND     l.country_id = ISNULL(@l_country_id, l.country_id)
        AND     l.location_id = ISNULL(@l_location_id, l.location_id)
        AND     l.line_id = ISNULL(@l_line_id, l.line_id)
		AND		l.building_id=ISNULL(@l_building_id,l.building_id)
		AND (l.end_date IS NULL OR l.end_date>GETDATE())
        AND     planned_date BETWEEN DATEADD(year,-1,@p_date) AND @p_date
        GROUP BY CONVERT(DATE, '01-'+FORMAT(planned_date,'MM-yyyy'),105)
        UNION
        SELECT 
                    0 cy_audit_planned,
                    count(*) cy_audit_performed,
					0 ly_audit_planned,
					0 ly_audit_performed,
                    CONVERT(DATE, '01-'+FORMAT(audit_date,'MM-yyyy'),105) dt
        FROM    mh_lpa_score_summary sc LEFT JOIN mh_lpa_line_master lm ON sc.line_id=lm.line_id
        WHERE   sc.region_id = ISNULL(@l_region_id, sc.region_id)
        AND     sc.country_id = ISNULL(@l_country_id, sc.country_id)
        AND     sc.location_id = ISNULL(@l_location_id, sc.location_id)
        AND     sc.line_id = ISNULL(@l_line_id, sc.line_id)
		AND		lm.building_id=ISNULL(@l_building_id,lm.building_id)
        AND     sc.audit_date BETWEEN DATEADD(year,-1,@p_date) AND @p_date
        GROUP BY CONVERT(DATE, '01-'+FORMAT(sc.audit_date,'MM-yyyy'),105)
		UNION
        SELECT 
					0 cy_audit_planned,
					0 cy_audit_performed,
                    count(*) ly_audit_planned,
                    0 ly_audit_performed,
                    CONVERT(DATE, '01-'+FORMAT(DATEADD(year,+1,planned_date),'MM-yyyy'),105) dt
        FROM    mh_lpa_audit_plan p,
				mh_lpa_line_master l
        WHERE   p.line_id = l.line_id
		AND     l.region_id = ISNULL(@l_region_id, l.region_id)
        AND     l.country_id = ISNULL(@l_country_id, l.country_id)
        AND     l.location_id = ISNULL(@l_location_id, l.location_id)
        AND     l.line_id = ISNULL(@l_line_id, l.line_id)
		AND		l.building_id=ISNULL(@l_building_id,l.building_id)
		AND (l.end_date IS NULL OR l.end_date>GETDATE())
        AND     planned_date BETWEEN DATEADD(year,-2,@p_date)  AND  DATEADD(year,-1,@p_date)
        GROUP BY CONVERT(DATE, '01-'+FORMAT(DATEADD(year,+1,planned_date),'MM-yyyy'),105)
        UNION
        SELECT 
					0 cy_audit_planned,
					0 cy_audit_performed,
                    0 ly_audit_planned,
                    count(*) ly_audit_performed,
                    CONVERT(DATE, '01-'+FORMAT(DATEADD(year,+1,audit_date),'MM-yyyy'),105) dt
        FROM    mh_lpa_score_summary sc LEFT JOIN mh_lpa_line_master lm ON sc.line_id=lm.line_id
        WHERE   sc.region_id = ISNULL(@l_region_id, sc.region_id)
        AND     sc.country_id = ISNULL(@l_country_id, sc.country_id)
        AND     sc.location_id = ISNULL(@l_location_id, sc.location_id)
        AND     sc.line_id = ISNULL(@l_line_id, sc.line_id)
		AND		lm.building_id=ISNULL(@l_building_id,lm.building_id)
        AND     audit_date BETWEEN DATEADD(year,-2,@p_date) AND  DATEADD(year,-1,@p_date)
        GROUP BY CONVERT(DATE, '01-'+FORMAT(DATEADD(year,+1,audit_date),'MM-yyyy'),105);


	SET @i = 0;
	WHILE @i < 13
	BEGIN
		SET @l_count = 0;
		SET @l_date = CONVERT(DATE, '01-'+FORMAT(DATEADD(month,-@i,@p_date),'MM-yyyy'),105);
		SELECT @l_count = count(*)
		FROM   @l_tempTable
		WHERE  audit_date = @l_date;

		IF @l_count = 0 
			INSERT INTO @l_tempTable VALUES(0,0,0,0, @l_date);
			
		SET @i = @i+1; 
	END;

	SELECT tmp1.audit_period, tmp1.dt,
			CASE 
				WHEN tmp1.cy_audit_planned = 0 THEN 0
				ELSE ROUND(CAST(CAST(tmp1.cy_audit_performed AS NUMERIC(10,2))/CAST(tmp1.cy_audit_planned AS NUMERIC(10,2)) AS NUMERIC(10,2))*100,0)
			END  cy_perform_rate,
			CASE 
				WHEN tmp1.ly_audit_planned = 0 THEN 0
				ELSE ROUND(CAST(CAST(tmp1.ly_audit_performed AS NUMERIC(10,2))/CAST(tmp1.ly_audit_planned AS NUMERIC(10,2)) AS NUMERIC(10,2))*100,0)
			END  ly_perform_rate,
			cy_audit_planned,
			cy_audit_performed
	FROM (select 
				FORMAT(audit_date,'MMM-yyyy') audit_period,
				audit_date dt,
				SUM(cy_audit_planned) cy_audit_planned,
				SUM(cy_audit_performed) cy_audit_performed,
				SUM(ly_audit_planned) ly_audit_planned,
				SUM(ly_audit_performed) ly_audit_performed
		FROM @l_tempTable
		GROUP BY FORMAT(audit_date,'MMM-yyyy'),
				audit_date) tmp1
	ORDER BY dt;




 
/*
	SELECT tmp1.audit_period, tmp1.dt,
			CASE 
				WHEN tmp1.cy_audit_planned = 0 THEN 0
				ELSE ROUND(CAST(CAST(tmp1.cy_audit_performed AS NUMERIC(10,2))/CAST(tmp1.cy_audit_planned AS NUMERIC(10,2)) AS NUMERIC(10,2))*100,0)
			END  cy_perform_rate,
			CASE 
				WHEN tmp1.ly_audit_planned = 0 THEN 0
				ELSE ROUND(CAST(CAST(tmp1.ly_audit_performed AS NUMERIC(10,2))/CAST(tmp1.ly_audit_planned AS NUMERIC(10,2)) AS NUMERIC(10,2))*100,0)
			END  ly_perform_rate
	FROM
	(
       SELECT audit_period,
              SUM(cy_audit_planned) cy_audit_planned,
              SUM(cy_audit_performed) cy_audit_performed,
              SUM(ly_audit_planned) ly_audit_planned,
              SUM(ly_audit_performed) ly_audit_performed,
              dt
       FROM (
              SELECT FORMAT(planned_date,'MMM-yyyy') audit_period,
                           count(*) cy_audit_planned,
                           0 cy_audit_performed,
						   0 ly_audit_planned,
						   0 ly_audit_performed,
                           CONVERT(DATE, '01-'+FORMAT(planned_date,'MM-yyyy'),105) dt
              FROM    mh_lpa_audit_plan p,
						mh_lpa_line_master l
              WHERE   p.line_id = l.line_id
			  AND     l.region_id = ISNULL(@l_region_id, l.region_id)
              AND     l.country_id = ISNULL(@l_country_id, l.country_id)
              AND     l.location_id = ISNULL(@l_location_id, l.location_id)
              AND     l.line_id = ISNULL(@l_line_id, l.line_id)
              AND     planned_date BETWEEN DATEADD(year,-1,@p_date) AND @p_date
              GROUP BY FORMAT(planned_date,'MMM-yyyy'), CONVERT(DATE, '01-'+FORMAT(planned_date,'MM-yyyy'),105)
              UNION
              SELECT FORMAT(audit_date,'MMM-yyyy') audit_period,
                           0 cy_audit_planned,
                           count(*) cy_audit_performed,
						   0 ly_audit_planned,
						   0 ly_audit_performed,
                           CONVERT(DATE, '01-'+FORMAT(audit_date,'MM-yyyy'),105) dt
              FROM    mh_lpa_score_summary
              WHERE   region_id = ISNULL(@l_region_id, region_id)
              AND     country_id = ISNULL(@l_country_id, country_id)
              AND     location_id = ISNULL(@l_location_id, location_id)
              AND     line_id = ISNULL(@l_line_id, line_id)
              AND     audit_date BETWEEN DATEADD(year,-1,@p_date) AND @p_date
              GROUP BY FORMAT(audit_date,'MMM-yyyy'), CONVERT(DATE, '01-'+FORMAT(audit_date,'MM-yyyy'),105)
			  UNION
              SELECT FORMAT(DATEADD(year,1,planned_date),'MMM-yyyy') audit_period,
						   0 cy_audit_planned,
						   0 cy_audit_performed,
                           count(*) ly_audit_planned,
                           0 ly_audit_performed,
                           CONVERT(DATE, '01-'+FORMAT(DATEADD(year,+1,planned_date),'MM-yyyy'),105) dt
              FROM    mh_lpa_audit_plan p,
						mh_lpa_line_master l
              WHERE   p.line_id = l.line_id
			  AND     l.region_id = ISNULL(@l_region_id, l.region_id)
              AND     l.country_id = ISNULL(@l_country_id, l.country_id)
              AND     l.location_id = ISNULL(@l_location_id, l.location_id)
              AND     l.line_id = ISNULL(@l_line_id, l.line_id)
              AND     planned_date BETWEEN DATEADD(year,-2,@p_date)  AND  DATEADD(year,-1,@p_date)
              GROUP BY FORMAT(DATEADD(year,1,planned_date),'MMM-yyyy'), CONVERT(DATE, '01-'+FORMAT(DATEADD(year,+1,planned_date),'MM-yyyy'),105)
              UNION
              SELECT FORMAT(DATEADD(year,1,audit_date),'MMM-yyyy') audit_period,
						   0 cy_audit_planned,
						   0 cy_audit_performed,
                           0 ly_audit_planned,
                           count(*) ly_audit_performed,
                           CONVERT(DATE, '01-'+FORMAT(DATEADD(year,+1,audit_date),'MM-yyyy'),105) dt
              FROM    mh_lpa_score_summary
              WHERE   region_id = ISNULL(@l_region_id, region_id)
              AND     country_id = ISNULL(@l_country_id, country_id)
              AND     location_id = ISNULL(@l_location_id, location_id)
              AND     line_id = ISNULL(@l_line_id, line_id)
              AND     audit_date BETWEEN DATEADD(year,-2,@p_date) AND  DATEADD(year,-1,@p_date)
              GROUP BY FORMAT(DATEADD(year,1,audit_date),'MMM-yyyy'), CONVERT(DATE, '01-'+FORMAT(DATEADD(year,+1,audit_date),'MM-yyyy'),105)
              ) tmp
       GROUP BY audit_period, dt
	) tmp1
       ORDER BY tmp1.dt
  
 */
 
 
END


-- ========================================================
