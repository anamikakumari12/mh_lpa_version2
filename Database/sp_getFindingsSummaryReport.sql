USE [mhlpa_db_dev]
GO
/****** Object:  StoredProcedure [dbo].[sp_getAuditPlanReport]    Script Date: 2/12/2020 11:00:24 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec sp_getFindingsSummaryReport 1,1,1,2017,12
ALTER PROCEDURE [dbo].[sp_getFindingsSummaryReport]
(
@l_region_id INT=NULL,
@l_country_id INT=NULL,
@l_location_id INT=NULL,
@year INT,
@Month INT
)
AS
BEGIN
if(@l_region_id=0)
SET  @l_region_id = null
if(@l_country_id=0)
SET  @l_country_id = null
if(@l_location_id=0)
SET  @l_location_id = null

select loc.location_name, count(*) close_count INTO #temp1
FROM mh_lpa_local_answers ans JOIN mh_lpa_location_master loc ON ans.location_id=loc.location_id
WHERE
  loc.location_id = ISNULL(@l_location_id, loc.location_id)
AND  loc.country_id = ISNULL(@l_country_id, loc.country_id)
AND  loc.region_id = ISNULL(@l_region_id, loc.region_id)
AND MONTH(audit_date)=@month
AND YEAR(audit_date)=@year
AND answer=1 AND review_closed_status=1
GROUP BY loc.location_name

select loc.location_name, count(*) open_count INTO #temp2
FROM mh_lpa_local_answers ans JOIN mh_lpa_location_master loc ON ans.location_id=loc.location_id
WHERE
  loc.location_id = ISNULL(@l_location_id, loc.location_id)
AND  loc.country_id = ISNULL(@l_country_id, loc.country_id)
AND  loc.region_id = ISNULL(@l_region_id, loc.region_id)
AND MONTH(audit_date)=@month
AND YEAR(audit_date)=@year
AND answer=1 AND review_closed_status<>1
GROUP BY loc.location_name

--select * from #temp1
--select * from #temp2

select t1.location_name, t1.close_count, t2.open_count from #temp1 t1 JOIN #temp2 t2 ON t1.location_name=t2.location_name


select loc.location_name, question_id, count(*) no_count
FROM mh_lpa_local_answers ans JOIN mh_lpa_location_master loc ON ans.location_id=loc.location_id
WHERE
 loc.location_id = ISNULL(@l_location_id, loc.location_id)
AND  loc.country_id = ISNULL(@l_country_id, loc.country_id)
AND  loc.region_id = ISNULL(@l_region_id, loc.region_id)
 AND MONTH(audit_date)=@month
AND YEAR(audit_date)=@year
AND answer=1
group by loc.location_name, question_id


END




