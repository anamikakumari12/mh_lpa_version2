USE [mhlpa_db_dev]
GO
/****** Object:  StoredProcedure [dbo].[sp_getLPAResultMonthly]    Script Date: 1/2/2020 3:24:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER procedure [dbo].[sp_getLPAResultMonthly]
(
	@p_region_id	INT=null,
	@p_country_id	INT=null,
	@p_location_id  INT=null,
	@p_building_id	INT=NULL,
	@p_line_id      INT=null,
	@p_date         DATE
)
AS
BEGIN
 
	   DECLARE @l_region_id       INT;
       DECLARE @l_country_id             INT;
       DECLARE @l_location_id            INT;
       DECLARE @l_line_id         INT;
	   DECLARE @i					INT;
	   DECLARE @l_count				INT;
	   DECLARE @l_date				DATE;
	   DECLARE @l_building_id            INT;

	   DECLARE @l_tempTable TABLE
	   (
		avg_score		NUMERIC(10,5),
		prev_score		NUMERIC(10,5),
		audit_date		DATE
	   );
 
       IF @p_building_id IS NULL OR @p_building_id = 0 
              SET @l_building_id = NULL;
       ELSE
              SET @l_building_id = @p_building_id;
	
		IF @p_region_id IS NULL OR @p_region_id = 0 
              SET @l_region_id = NULL;
       ELSE
              SET @l_region_id = @p_region_id;
 
       IF @p_country_id IS NULL OR @p_country_id = 0 
              SET @l_country_id = NULL;
       ELSE
              SET @l_country_id = @p_country_id;
 
       IF @p_location_id IS NULL OR @p_location_id = 0 
              SET @l_location_id = NULL;
       ELSE
              SET @l_location_id = @p_location_id;
 
       IF @p_line_id IS NULL OR @p_line_id = 0 
              SET @l_line_id = NULL;
       ELSE
              SET @l_line_id = @p_line_id;

		INSERT INTO @l_tempTable
		SELECT 
                    AVG(sc.total_score) average_score,
                    CAST(0.0 as numeric(10,5)) previous_score,
                    CONVERT(DATE, '01-'+FORMAT(sc.audit_date,'MM-yyyy'),105) a
        FROM    mh_lpa_score_summary  sc LEFT JOIN mh_lpa_line_master lm ON sc.line_id=lm.line_id
        WHERE   sc.region_id = ISNULL(@l_region_id, sc.region_id)
        AND     sc.country_id = ISNULL(@l_country_id, sc.country_id)
        AND     sc.location_id = ISNULL(@l_location_id, sc.location_id)
        AND     sc.line_id = ISNULL(@l_line_id, sc.line_id)
		AND		lm.building_id=ISNULL(@l_building_id,lm.building_id)
        AND     sc.audit_date BETWEEN DATEADD(year,-1,@p_date) AND @p_date
        GROUP BY FORMAT(audit_date,'MMM-yyyy'), CONVERT(DATE, '01-'+FORMAT(audit_date,'MM-yyyy'),105)
        UNION
        SELECT 
                    0.0 average_score,
                    AVG(CAST(sc.total_score as numeric(10,5)) )   previous_score,
                    CONVERT(DATE, '01-'+FORMAT(DATEADD(year,+1,sc.audit_date),'MM-yyyy'),105) a
        FROM    mh_lpa_score_summary sc LEFT JOIN mh_lpa_line_master lm ON sc.line_id=lm.line_id
		
        WHERE   sc.region_id = ISNULL(@l_region_id, sc.region_id)
        AND     sc.country_id = ISNULL(@l_country_id, sc.country_id)
        AND     sc.location_id = ISNULL(@l_location_id, sc.location_id)
        AND     sc.line_id = ISNULL(@l_line_id, sc.line_id)
		AND		lm.building_id=ISNULL(@l_building_id,lm.building_id)
        AND     sc.audit_date BETWEEN DATEADD(year,-2,@p_date) AND  DATEADD(year,-1,@p_date)
        GROUP BY FORMAT(DATEADD(year,1,sc.audit_date),'MMM-yyyy'), CONVERT(DATE, '01-'+FORMAT(DATEADD(year,+1,sc.audit_date),'MM-yyyy'),105);

		SET @i = 0;
		WHILE @i < 13
		BEGIN
			SET @l_count = 0;
			SET @l_date = CONVERT(DATE, '01-'+FORMAT(DATEADD(month,-@i,@p_date),'MM-yyyy'),105);
			SELECT @l_count = count(*)
			FROM   @l_tempTable
			WHERE  audit_date = @l_date;

			IF @l_count = 0 
				INSERT INTO @l_tempTable VALUES(0,0, @l_date);

			SET @i = @i+1; 
		END;

		SELECT * FROM (select SUM(avg_score) average_score, 
							SUM(prev_score) previous_score, 
							FORMAT(audit_date,'MMM-yyyy') audit_period,
							audit_date a	
						FROM @l_tempTable
						GROUP BY FORMAT(audit_date,'MMM-yyyy'),
							audit_date) t
						ORDER BY a;
/* 
       SELECT audit_period,
              SUM(average_score) average_score,
              SUM(previous_score) previous_score,
              a
       FROM (
              SELECT FORMAT(audit_date,'MMM-yyyy') audit_period,
                           AVG(total_score) average_score,
                           CAST(0.0 as numeric(10,5)) previous_score,
                           CONVERT(DATE, '01-'+FORMAT(audit_date,'MM-yyyy'),105) a
              FROM    mh_lpa_score_summary
              WHERE   region_id = ISNULL(@l_region_id, region_id)
              AND     country_id = ISNULL(@l_country_id, country_id)
              AND     location_id = ISNULL(@l_location_id, location_id)
              AND     line_id = ISNULL(@l_line_id, line_id)
              AND     audit_date BETWEEN DATEADD(year,-1,@p_date) AND @p_date
              GROUP BY FORMAT(audit_date,'MMM-yyyy'), CONVERT(DATE, '01-'+FORMAT(audit_date,'MM-yyyy'),105)
              UNION
              SELECT FORMAT(DATEADD(year,1,audit_date),'MMM-yyyy') audit_period,
                           0.0 average_score,
                           AVG(CAST(total_score as numeric(10,5)) )   previous_score,
                           CONVERT(DATE, '01-'+FORMAT(DATEADD(year,+1,audit_date),'MM-yyyy'),105) a
              FROM    mh_lpa_score_summary
              WHERE   region_id = ISNULL(@l_region_id, region_id)
              AND     country_id = ISNULL(@l_country_id, country_id)
              AND     location_id = ISNULL(@l_location_id, location_id)
              AND     line_id = ISNULL(@l_line_id, line_id)
              AND     audit_date BETWEEN DATEADD(year,-2,@p_date) AND  DATEADD(year,-1,@p_date)
              GROUP BY FORMAT(DATEADD(year,1,audit_date),'MMM-yyyy'), CONVERT(DATE, '01-'+FORMAT(DATEADD(year,+1,audit_date),'MM-yyyy'),105)
              ) tmp
       GROUP BY audit_period, a
       ORDER BY a
       
*/
 
 
END