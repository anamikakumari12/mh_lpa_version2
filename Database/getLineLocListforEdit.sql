
/****** Object:  StoredProcedure [dbo].[getLineLocListforEdit]    Script Date: 12/6/2019 1:59:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--exec getLineLocListforEdit 17,0
ALTER PROCEDURE [dbo].[getLineLocListforEdit](@p_user_id		INT, @p_inactive_flag INT)
AS
BEGIN
	DECLARE @l_count 			INT;
	DECLARE @l_location_id 			INT;
	DECLARE @l_global_admin			VARCHAR(1);
	DECLARE @l_local_admin			VARCHAR(1);


	SELECT @l_global_admin = ISNULL(global_admin_flag,'N'),
		@l_local_admin = ISNULL(site_admin_flag,'N'),
		@l_location_id = location_id
	FROM   mh_lpa_user_master
	WHERE  user_id = @p_user_id;
	print @l_global_admin
	print @l_local_admin
	print @l_location_id

	SELECT  rm.region_id,
			rm.region_name,
			cm.country_id,
			cm.country_name,
			lm.location_id,
			lm.location_name,
			lne.line_id,
			lne.line_name,
			lne.line_code,
			dbo.getPartsForLine(lne.line_id) product_code
	FROM   mh_lpa_region_master rm,
			mh_lpa_country_master cm,
			mh_lpa_location_master lm,
			mh_lpa_line_master lne
	WHERE  lne.region_id = rm.region_id
	AND    lne.country_id = cm.country_id
	AND    lne.location_id = lm.location_id
	AND		(
			(ISNULL(@l_local_admin,'N') = 'Y' AND lm.location_id = @l_location_id) OR
			(ISNULL(@l_global_admin,'N') = 'Y')
			)
	AND   lne.line_id IN (select rel.line_id from mh_lpa_line_product_relationship rel where rel.line_id =  lne.line_id
						  and ((@p_inactive_flag=0 AND ISNULL(rel.end_date, DATEADD(day,1,getdate())) >= FORMAT(getdate(),'yyyy-MM-dd')) OR (@p_inactive_flag>0))
						  --ISNULL(rel.end_date, DateAdd(day, 1, getdate())) > getdate() 
						  ) 
	AND ((@p_inactive_flag=0 AND ISNULL(lne.end_date, DATEADD(day,1,getdate())) >= FORMAT(getdate(),'yyyy-MM-dd')) OR (@p_inactive_flag>0))
	--and ISNULL(lne.end_date , dateadd(day,1,getdate())) > getdate()	  
	ORDER BY line_name;

END;
