
/****** Object:  StoredProcedure [dbo].[sp_getAuditScheduleReport]    Script Date: 2/12/2020 11:44:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[sp_getAuditScheduleReport]
(
@l_region_id INT=NULL,
@l_country_id INT=NULL,
@l_location_id INT=NULL,
@p_date_start DATE,
@p_date_end DATE
)
AS
BEGIN
if(@l_region_id=0)
SET  @l_region_id = null
if(@l_country_id=0)
SET  @l_country_id = null
if(@l_location_id=0)
SET  @l_location_id = null

select distinct l.location_name, lm.line_name, u.emp_full_name, u.Role, pl.planned_date, pl.planned_date_end
from mh_lpa_audit_plan pl 
JOIN mh_lpa_line_master lm ON pl.line_id=lm.line_id
JOIN mh_lpa_location_master l ON l.location_id=pl.location_id
JOIN mh_lpa_user_master u ON u.user_id=pl.to_be_audited_by_user_id
WHERE pl.planned_date BETWEEN @p_date_start AND @p_date_end
AND (lm.end_date is null OR lm.end_date>=GETDATE())
AND  l.location_id = ISNULL(@l_location_id, l.location_id)
AND  l.country_id = ISNULL(@l_country_id, l.country_id)
AND  l.region_id = ISNULL(@l_region_id, l.region_id)

END

