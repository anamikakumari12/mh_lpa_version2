
/****** Object:  StoredProcedure [dbo].[sp_getAuditPlanReport]    Script Date: 2/12/2020 11:42:18 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec sp_getAuditPlanReport 27,'2019-02-01 20:58:16.847', '2019-06-01 20:58:16.847'
ALTER PROCEDURE [dbo].[sp_getAuditPlanReport]
(
@l_region_id INT=NULL,
@l_country_id INT=NULL,
@l_location_id INT=NULL,
@p_date_start DATE,
@p_date_end DATE
)
AS
BEGIN
if(@l_region_id=0)
SET  @l_region_id = null
if(@l_country_id=0)
SET  @l_country_id = null
if(@l_location_id=0)
SET  @l_location_id = null

select distinct l.location_name, lm.line_name, u.emp_full_name, u.Role, pl.planned_date, pl.planned_date_end, u1.emp_full_name Auditor, 
res.audit_date, res.Shift_no, sc.total_score*100 Score
from mh_lpa_audit_plan pl 
JOIN mh_lpa_line_master lm ON pl.line_id=lm.line_id
JOIN mh_lpa_location_master l ON l.location_id=pl.location_id
JOIN mh_lpa_user_master u ON u.user_id=pl.to_be_audited_by_user_id
JOIN [dbo].[mh_lpa_local_answers] res ON res.line_id=pl.line_id AND res.audit_date BETWEEN pl.planned_date AND pl.planned_date_end
JOIN mh_lpa_user_master u1 ON u1.user_id=res.audited_by_user_id
JOIN [dbo].[mh_lpa_score_summary] sc ON sc.audit_id=res.audit_id
WHERE pl.planned_date BETWEEN @p_date_start AND @p_date_end
AND (lm.end_date is null OR lm.end_date>=GETDATE())
AND  l.location_id = ISNULL(@l_location_id, l.location_id)
AND  l.country_id = ISNULL(@l_country_id, l.country_id)
AND  l.region_id = ISNULL(@l_region_id, l.region_id)

END


--select * from [dbo].[mh_lpa_local_answers]

--select * from [dbo].[mh_lpa_score_summary] where audit_id=10855