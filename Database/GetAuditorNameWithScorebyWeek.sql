
/****** Object:  UserDefinedFunction [dbo].[GetAuditorNameWithScorebyWeek]    Script Date: 2/27/2020 11:28:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER FUNCTION [dbo].[GetAuditorNameWithScorebyWeek](@p_line_id int,
									@p_emp_id			INT,
									@p_wk_no			INT,
									@p_year				INT)  
 RETURNS NVARCHAR(1000)  
AS  
BEGIN  

	DECLARE @l_total_answers int;
	DECLARE @l_yes_answers	int;
	DECLARE @l_audit_id		int;

	DECLARE @l_audit_score			FLOAT;
	DECLARE @l_auditor_name		NVARCHAR(100);
	DECLARE @l_out_val  		NVARCHAR(500);



	SELECT @l_auditor_name = emp_full_name 
	FROM   mh_lpa_user_master 
	WHERE  user_id=@p_emp_id;

	select @l_audit_score = total_score*100
	FROM   mh_lpa_score_summary 
	WHERE  line_id = @p_line_id
	AND    DATEPART(wk, audit_date) = @p_wk_no
	and    YEAR(audit_date) = @p_year;

	IF @l_audit_score IS NOT NULL
	
		SET @l_out_val = CONCAT(@l_auditor_name, '(', @l_audit_score, ')' );

	ELSE

		SET @l_out_val = @l_auditor_name;


	RETURN  @l_out_val;

END


