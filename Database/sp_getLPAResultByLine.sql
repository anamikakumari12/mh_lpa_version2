
/****** Object:  StoredProcedure [dbo].[sp_getLPAResultByLine]    Script Date: 22-04-2020 10:08:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER procedure [dbo].[sp_getLPAResultByLine](@p_region_id  INT=null,
           @p_country_id  INT=null,
           @p_location_id  INT=null,
	@p_building_id	INT=NULL,
           @p_line_id   INT=null,
           @p_date    DATE)
AS
BEGIN
DECLARE @l_region_id  INT;
 DECLARE @l_country_id  INT;
 DECLARE @l_location_id  INT;
 DECLARE @l_line_id  INT;
	   DECLARE @l_building_id            INT;

	   IF @p_building_id IS NULL OR @p_building_id = 0 
              SET @l_building_id = NULL;
       ELSE
              SET @l_building_id = @p_building_id;

 IF @p_region_id IS NULL OR @p_region_id = 0 
  SET @l_region_id = NULL;
 ELSE
  SET @l_region_id = @p_region_id;
 IF @p_country_id IS NULL OR @p_country_id = 0 
  SET @l_country_id = NULL;
 ELSE
  SET @l_country_id = @p_country_id;
 IF @p_location_id IS NULL OR @p_location_id = 0 
  SET @l_location_id = NULL;
 ELSE
  SET @l_location_id = @p_location_id;
 IF @p_line_id IS NULL OR @p_line_id = 0 
  SET @l_line_id = NULL;
 ELSE
  SET @l_line_id = @p_line_id;


 SELECT  TOP(10) line_name,
				average_score
 FROM   (
SELECT  TOP(10) lm.line_name,
   AVG(total_score) average_score
 FROM    mh_lpa_score_summary s,
   mh_lpa_line_master lm
 WHERE   s.region_id = ISNULL(@l_region_id, s.region_id)
 AND     s.country_id = ISNULL(@l_country_id, s.country_id)
 AND     s.location_id = ISNULL(@l_location_id, s.location_id)
 AND     s.line_id = ISNULL(@l_line_id, s.line_id)
 AND     s.line_id = lm.line_id
		AND		lm.building_id=ISNULL(@l_building_id,lm.building_id)
 AND     audit_date BETWEEN DATEADD(year,-2,@p_date) AND @p_date
 GROUP BY lm.line_name
 --HAVING  AVG(total_score) < 0.6
 ORDER BY average_score desc) t

END

