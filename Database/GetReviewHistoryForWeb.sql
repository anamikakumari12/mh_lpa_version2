USE [MH_LPA-TEST]
GO
/****** Object:  StoredProcedure [dbo].[GetReviewHistoryForWeb]    Script Date: 17-06-2020 10:24:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[GetReviewHistoryForWeb](
									@p_line_id int,
									@p_question_id			int)  
AS
BEGIN
	SELECT  la.review_closed_on,
				CASE WHEN la.review_closed_status=1 THEN 'Close'
				ELSE 'Open' End review_closed_status,
				ISNULL(la.review_comments,'Comments Not Entered') review_comments
	FROM    mh_lpa_local_answers la
	WHERE  1=1 -- la.audit_date BETWEEN DATEADD(month, -1, CURRENT_TIMESTAMP)  AND CURRENT_TIMESTAMP
	AND    la.line_id = @p_line_id
	AND    la.question_id = ISNULL(@p_question_id,la.question_id)
	AND    la.review_comments IS NOT NULL
	ORDER BY la.review_closed_on DESC
END



