
ALTER FUNCTION [dbo].[GetQRDetails](@p_line_id int)  
 RETURNS NVARCHAR(MAX)  
AS  
BEGIN  
   RETURN (
   SELECT 
			region_name,
			region_id,
			country_name,
			country_id,
			location_name,
			line_name,
			location_id,
			line_id,
			shift_no,
			PartDetails,
			building_id
   FROM (
   		SELECT DISTINCT 
				rm.region_name,
				rm.region_id,
				cm.country_name,
				cm.country_id,
		   		loc.location_name,
				lm.line_name,
				rel.location_id,
				rel.line_id,
				dbo.getShift(loc.location_id) Shift_no,
				dbo.GetPartDetailsforLine(rel.line_id)  as PartDetails,
				lm.building_id
		FROM mh_lpa_line_product_relationship rel,
			mh_lpa_line_master lm,
			mh_lpa_product_master pm,
			mh_lpa_location_master loc,
			mh_lpa_country_master cm,
			mh_lpa_region_master rm
		WHERE rel.line_id = @p_line_id
		AND   rel.line_id = lm.line_id
		AND   rel.product_id = pm.product_id
		AND   rel.location_id = loc.location_id
		AND   rel.country_id = cm.country_id
		AND   rel.region_id = rm.region_id
		and ISNULL(lm.end_date , dateadd(day,1,getdate())) > getdate()	  
		and ISNULL(rel.end_date , dateadd(day,1,getdate())) > getdate()	  

		) tmp
    FOR JSON AUTO)  
END

