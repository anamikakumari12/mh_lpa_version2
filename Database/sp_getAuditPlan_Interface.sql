
/****** Object:  StoredProcedure [dbo].[sp_getAuditPlan_Interface]    Script Date: 2/27/2020 11:20:46 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--exec sp_getAuditPlan_Interface 10,2,2019

ALTER PROCEDURE [dbo].[sp_getAuditPlan_Interface](@p_location_id  int, @p_building_id int,
     @p_year    int)
AS
BEGIN
 SELECT int.location_id
      ,int.line_id
      ,lm.line_name
	  , (select count(*) from mh_lpa_audit_plan where line_id = int.line_id and year(planned_date) = @p_year) Plan_count
      ,[year]
      ,[wk1]
	  ,[wk2]
	  ,[wk3]
	  ,[wk4]
	  ,[wk5]
	  ,[wk6]
	  ,[wk7]
	  ,[wk8]
	  ,[wk9]
	  ,[wk10]
	  ,[wk11]
	  ,[wk12]
	  ,[wk13]
	  ,[wk14]
	  ,[wk15]
	  ,[wk16]
	  ,[wk17]
	  ,[wk18]
	  ,[wk19]
	  ,[wk20]
,[wk21]

,[wk22]
,[wk23]
,[wk24]
,[wk25]
,[wk26]
,[wk27]
,[wk28]
,[wk29]
,[wk30]
,[wk31]
,[wk32]
,[wk33]
,[wk34]
,[wk35]
,[wk36]
,[wk37]
,[wk38]
,[wk39]
,[wk40]
,[wk41]
,[wk42]
,[wk43]
	  ,[wk44]
	  ,[wk45]
	  ,[wk46]
	  ,[wk47]
	  ,[wk48]
	  ,[wk49]
	  ,[wk50]
	  ,[wk51]
	  ,[wk52]

/*
	  ,(select emp_full_name from mh_lpa_user_master WHERE user_id=wk1) wkn1
      
	  ,(select emp_full_name from mh_lpa_user_master WHERE user_id=wk2) wkn2
      
	  ,(select emp_full_name from mh_lpa_user_master WHERE user_id=wk3) wkn3
      
	  ,(select emp_full_name from mh_lpa_user_master WHERE user_id=wk4) wkn4
         
	  ,(select emp_full_name from mh_lpa_user_master WHERE user_id=wk5) wkn5
         
	  ,(select emp_full_name from mh_lpa_user_master WHERE user_id=wk6) wkn6
         
	  ,(select emp_full_name from mh_lpa_user_master WHERE user_id=wk7) wkn7
         
	  ,(select emp_full_name from mh_lpa_user_master WHERE user_id=wk8) wkn8
         
	  ,(select emp_full_name from mh_lpa_user_master WHERE user_id=wk9) wkn9
        
	  ,(select emp_full_name from mh_lpa_user_master WHERE user_id=wk10) wkn10
        
	  ,(select emp_full_name from mh_lpa_user_master WHERE user_id=wk11) wkn11
        
	  ,(select emp_full_name from mh_lpa_user_master WHERE user_id=wk12) wkn12
        
	  ,(select emp_full_name from mh_lpa_user_master WHERE user_id=wk13) wkn13
        
	  ,(select emp_full_name from mh_lpa_user_master WHERE user_id=wk14) wkn14

	  ,(select emp_full_name from mh_lpa_user_master WHERE user_id=wk15) wkn15
 
	  ,(select emp_full_name from mh_lpa_user_master WHERE user_id=wk16) wkn16
 
	  ,(select emp_full_name from mh_lpa_user_master WHERE user_id=wk17) wkn17

	  ,(select emp_full_name from mh_lpa_user_master WHERE user_id=wk18) wkn18

	  ,(select emp_full_name from mh_lpa_user_master WHERE user_id=wk19) wkn19

	  ,(select emp_full_name from mh_lpa_user_master WHERE user_id=wk20) wkn20

	  ,(select emp_full_name from mh_lpa_user_master WHERE user_id=wk21) wkn21
 
	  ,(select emp_full_name from mh_lpa_user_master WHERE user_id=wk22) wkn22

	  ,(select emp_full_name from mh_lpa_user_master WHERE user_id=wk23) wkn23

	  ,(select emp_full_name from mh_lpa_user_master WHERE user_id=wk24) wkn24

	  ,(select emp_full_name from mh_lpa_user_master WHERE user_id=wk25) wkn25
      ,(select emp_full_name from mh_lpa_user_master WHERE user_id=wk26) wkn26
      
	  ,(select emp_full_name from mh_lpa_user_master WHERE user_id=wk27) wkn27  
	  ,(select emp_full_name from mh_lpa_user_master WHERE user_id=wk28) wkn28

	  ,(select emp_full_name from mh_lpa_user_master WHERE user_id=wk29) wkn29

	  ,(select emp_full_name from mh_lpa_user_master WHERE user_id=wk30) wkn30

	  ,(select emp_full_name from mh_lpa_user_master WHERE user_id=wk31) wkn31

	  ,(select emp_full_name from mh_lpa_user_master WHERE user_id=wk32) wkn32

	  ,(select emp_full_name from mh_lpa_user_master WHERE user_id=wk33) wkn33
 
	  ,(select emp_full_name from mh_lpa_user_master WHERE user_id=wk34) wkn34
 
	  ,(select emp_full_name from mh_lpa_user_master WHERE user_id=wk35) wkn35

	  ,(select emp_full_name from mh_lpa_user_master WHERE user_id=wk36) wkn36
 
	  ,(select emp_full_name from mh_lpa_user_master WHERE user_id=wk37) wkn37
 
	  ,(select emp_full_name from mh_lpa_user_master WHERE user_id=wk38) wkn38
 
	  ,(select emp_full_name from mh_lpa_user_master WHERE user_id=wk39) wkn39

	  ,(select emp_full_name from mh_lpa_user_master WHERE user_id=wk40) wkn40

	  ,(select emp_full_name from mh_lpa_user_master WHERE user_id=wk41) wkn41
 
	  ,(select emp_full_name from mh_lpa_user_master WHERE user_id=wk42) wkn42

	  ,(select emp_full_name from mh_lpa_user_master WHERE user_id=wk43) wkn43
 
	  ,(select emp_full_name from mh_lpa_user_master WHERE user_id=wk44) wkn44
 
	  ,(select emp_full_name from mh_lpa_user_master WHERE user_id=wk45) wkn45

	  ,(select emp_full_name from mh_lpa_user_master WHERE user_id=wk46) wkn46
 
	  ,(select emp_full_name from mh_lpa_user_master WHERE user_id=wk47) wkn47
 
	  ,(select emp_full_name from mh_lpa_user_master WHERE user_id=wk48) wkn48

	  ,(select emp_full_name from mh_lpa_user_master WHERE user_id=wk49) wkn49
	  ,(select emp_full_name from mh_lpa_user_master WHERE user_id=wk50) wkn50

	  ,(select emp_full_name from mh_lpa_user_master WHERE user_id=wk51) wkn51

	  ,(select emp_full_name from mh_lpa_user_master WHERE user_id=wk52) wkn52
	  
	  */
	  ,dbo.GetAuditorNameWithScorebyWeek(int.line_id, wk1, 1, @p_year) wkn1
	  ,dbo.GetAuditorNameWithScorebyWeek(int.line_id, wk2, 2, @p_year) wkn2
	  , dbo.GetAuditorNameWithScorebyWeek(int.line_id, wk3, 3, @p_year) wkn3
	  , dbo.GetAuditorNameWithScorebyWeek(int.line_id, wk4, 4, @p_year) wkn4
	  , dbo.GetAuditorNameWithScorebyWeek(int.line_id, wk5, 5, @p_year) wkn5
	  , dbo.GetAuditorNameWithScorebyWeek(int.line_id, wk6, 6, @p_year) wkn6
	  , dbo.GetAuditorNameWithScorebyWeek(int.line_id, wk7, 7, @p_year) wkn7
	  , dbo.GetAuditorNameWithScorebyWeek(int.line_id, wk8, 8, @p_year) wkn8
	  , dbo.GetAuditorNameWithScorebyWeek(int.line_id, wk9, 9, @p_year) wkn9
	  , dbo.GetAuditorNameWithScorebyWeek(int.line_id, wk10, 10, @p_year) wkn10
	  , dbo.GetAuditorNameWithScorebyWeek(int.line_id, wk11, 11, @p_year) wkn11
	  , dbo.GetAuditorNameWithScorebyWeek(int.line_id, wk12, 12, @p_year) wkn12
	  , dbo.GetAuditorNameWithScorebyWeek(int.line_id, wk13, 13, @p_year) wkn13
	  , dbo.GetAuditorNameWithScorebyWeek(int.line_id, wk14, 14, @p_year) wkn14
	  , dbo.GetAuditorNameWithScorebyWeek(int.line_id, wk15, 15, @p_year) wkn15
	  , dbo.GetAuditorNameWithScorebyWeek(int.line_id, wk16, 16, @p_year) wkn16
	  , dbo.GetAuditorNameWithScorebyWeek(int.line_id, wk17, 17, @p_year) wkn17
	  , dbo.GetAuditorNameWithScorebyWeek(int.line_id, wk18, 18, @p_year) wkn18
	  , dbo.GetAuditorNameWithScorebyWeek(int.line_id, wk19, 19, @p_year) wkn19
	  , dbo.GetAuditorNameWithScorebyWeek(int.line_id, wk20, 20, @p_year) wkn20
	  , dbo.GetAuditorNameWithScorebyWeek(int.line_id, wk21, 21, @p_year) wkn21
	  , dbo.GetAuditorNameWithScorebyWeek(int.line_id, wk22, 22, @p_year) wkn22
	  , dbo.GetAuditorNameWithScorebyWeek(int.line_id, wk23, 23, @p_year) wkn23
	  , dbo.GetAuditorNameWithScorebyWeek(int.line_id, wk24, 24, @p_year) wkn24
	  , dbo.GetAuditorNameWithScorebyWeek(int.line_id, wk25, 25, @p_year) wkn25
	  , dbo.GetAuditorNameWithScorebyWeek(int.line_id, wk26, 26, @p_year) wkn26
	  , dbo.GetAuditorNameWithScorebyWeek(int.line_id, wk27, 27, @p_year) wkn27
	  , dbo.GetAuditorNameWithScorebyWeek(int.line_id, wk28, 28, @p_year) wkn28
	  , dbo.GetAuditorNameWithScorebyWeek(int.line_id, wk29, 29, @p_year) wkn29
	  , dbo.GetAuditorNameWithScorebyWeek(int.line_id, wk30, 30, @p_year) wkn30
	  , dbo.GetAuditorNameWithScorebyWeek(int.line_id, wk31, 31, @p_year) wkn31
	  , dbo.GetAuditorNameWithScorebyWeek(int.line_id, wk32, 32, @p_year) wkn32
	  , dbo.GetAuditorNameWithScorebyWeek(int.line_id, wk33, 33, @p_year) wkn33
	  , dbo.GetAuditorNameWithScorebyWeek(int.line_id, wk34, 34, @p_year) wkn34
	  , dbo.GetAuditorNameWithScorebyWeek(int.line_id, wk35, 35, @p_year) wkn35
	  , dbo.GetAuditorNameWithScorebyWeek(int.line_id, wk36, 36, @p_year) wkn36
	  , dbo.GetAuditorNameWithScorebyWeek(int.line_id, wk37, 37, @p_year) wkn37
	  , dbo.GetAuditorNameWithScorebyWeek(int.line_id, wk38, 38, @p_year) wkn38
	  , dbo.GetAuditorNameWithScorebyWeek(int.line_id, wk39, 39, @p_year) wkn39
	  , dbo.GetAuditorNameWithScorebyWeek(int.line_id, wk40, 40, @p_year) wkn40
	  , dbo.GetAuditorNameWithScorebyWeek(int.line_id, wk41, 41, @p_year) wkn41
	  , dbo.GetAuditorNameWithScorebyWeek(int.line_id, wk42, 42, @p_year) wkn42
	  , dbo.GetAuditorNameWithScorebyWeek(int.line_id, wk43, 43, @p_year) wkn43
	  , dbo.GetAuditorNameWithScorebyWeek(int.line_id, wk44, 44, @p_year) wkn44
	  , dbo.GetAuditorNameWithScorebyWeek(int.line_id, wk45, 45, @p_year) wkn45
	  , dbo.GetAuditorNameWithScorebyWeek(int.line_id, wk46, 46, @p_year) wkn46
	  , dbo.GetAuditorNameWithScorebyWeek(int.line_id, wk47, 47, @p_year) wkn47
	  , dbo.GetAuditorNameWithScorebyWeek(int.line_id, wk48, 48, @p_year) wkn48
	  , dbo.GetAuditorNameWithScorebyWeek(int.line_id, wk49, 49, @p_year) wkn49
	  , dbo.GetAuditorNameWithScorebyWeek(int.line_id, wk50, 50, @p_year) wkn50
	  , dbo.GetAuditorNameWithScorebyWeek(int.line_id, wk51, 51, @p_year) wkn51
	  , dbo.GetAuditorNameWithScorebyWeek(int.line_id, wk52, 52, @p_year) wkn52
 FROM   mh_lpa_audit_plan_interface int,
   mh_lpa_line_master lm
 WHERE  int.location_id = @p_location_id
 AND    year = @p_year
 AND   int.line_id = lm.line_id
 AND lm.building_id=@p_building_id
-- AND um.user_id=int.wk1
-- AND um.user_id=int.wk2
-- AND um.user_id=int.wk3
-- AND um.user_id=int.wk4
-- AND um.user_id=int.wk5
--AND um.user_id=int.wk6
--AND um.user_id=int.wk7
--AND um.user_id=int.wk8
--AND um.user_id=int.wk9
--AND um.user_id=int.wk10
--AND um.user_id=int.wk11
--AND um.user_id=int.wk12
--AND um.user_id=int.wk13
--AND um.user_id=int.wk14
--AND um.user_id=int.wk15
--AND um.user_id=int.wk16
--AND um.user_id=int.wk17
--AND um.user_id=int.wk18
--AND um.user_id=int.wk19
--AND um.user_id=int.wk20
--AND um.user_id=int.wk21
--AND um.user_id=int.wk22
--AND um.user_id=int.wk23
-- AND um.user_id=int.wk24
-- AND um.user_id=int.wk25
-- AND um.user_id=int.wk26
-- AND um.user_id=int.wk27
-- AND um.user_id=int.wk28
-- AND um.user_id=int.wk29
-- AND um.user_id=int.wk30
-- AND um.user_id=int.wk31
-- AND um.user_id=int.wk32
-- AND um.user_id=int.wk33
-- AND um.user_id=int.wk34
-- AND um.user_id=int.wk35
-- AND um.user_id=int.wk36
-- AND um.user_id=int.wk37
-- AND um.user_id=int.wk38
-- AND um.user_id=int.wk39
-- AND um.user_id=int.wk40
-- AND um.user_id=int.wk41
-- AND um.user_id=int.wk42
-- AND um.user_id=int.wk43
-- AND um.user_id=int.wk44
-- AND um.user_id=int.wk45
-- AND um.user_id=int.wk46
-- AND um.user_id=int.wk47
-- AND um.user_id=int.wk48
-- AND um.user_id=int.wk49
-- AND um.user_id=int.wk50
-- AND um.user_id=int.wk51
-- AND um.user_id=int.wk52
 AND  isnull(end_date, dateadd(day, 1,getdate())) > getdate()
 UNION
 SELECT [location_id]
      ,[line_id]
      ,[line_name]
	  , (select count(*) from mh_lpa_audit_plan p where p.line_id = lm.line_id and year(planned_date) = @p_year) Plan_count
   ,@p_year
      , 0 wk1 
      , 0 wk2 
      , 0 wk3 
      , 0 wk4 
      , 0 wk5 
      , 0 wk6 
      , 0 wk7 
      , 0 wk8 
      , 0 wk9 
      , 0 wk10
      , 0 wk11
      , 0 wk12
      , 0 wk13
      , 0 wk14
      , 0 wk15
      , 0 wk16
      , 0 wk17
      , 0 wk18
      , 0 wk19
      , 0 wk20
      , 0 wk21
      , 0 wk22
      , 0 wk23
      , 0 wk24
      , 0 wk25
      , 0 wk26
      , 0 wk27
      , 0 wk28
      , 0 wk29
      , 0 wk30
      , 0 wk31
      , 0 wk32
      , 0 wk33
      , 0 wk34
      , 0 wk35
      , 0 wk36
      , 0 wk37
      , 0 wk38
      , 0 wk39
      , 0 wk40
      , 0 wk41
      , 0 wk42
      , 0 wk43
      , 0 wk44
      , 0 wk45
      , 0 wk46
      , 0 wk47
      , 0 wk48
      , 0 wk49
      , 0 wk50
      , 0 wk51
      , 0 wk52

	  ,''
	  ,''
	  ,''
	  ,''
	  ,''
	  ,''
	  ,''
	  ,''
	  ,''
	   ,''
	   ,''
	   ,''
	   ,''
	   ,''
	   ,''
	   ,''
	   ,''
	   ,''
	   ,''
	   ,''
	   ,''
	   ,''
	   ,''
	   ,''
	   ,''
	   ,''
	   ,''
	   ,''
	   ,''
	   ,''
	   ,''
	   ,''
	   ,''
	   ,''
	   ,''
	   ,''
	   ,''
	   ,''
	   ,''
	   ,''
	   ,''
	   ,''
	   ,''
	   ,''
	   ,''
	   ,''
	   ,''
	   ,''
	   ,''
	   ,''
	   ,''
	   ,'' 

 FROM  mh_lpa_line_master lm
 WHERE lm.line_id NOT IN (select line_id from mh_lpa_audit_plan_interface
       WHERE  location_id = @p_location_id
       AND    year = @p_year)
 AND  lm.location_id = @p_location_id
 AND lm.building_id=@p_building_id
 AND  isnull(end_date, dateadd(day, 1,getdate())) > getdate()
 order by line_name
END;