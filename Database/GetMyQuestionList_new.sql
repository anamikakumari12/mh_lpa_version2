
/****** Object:  UserDefinedFunction [dbo].[GetMyQuestionList_new]    Script Date: 01-04-2020 11:55:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER FUNCTION [dbo].[GetMyQuestionList_new](@p_line_id int, @language_code VARCHAR(100))  
 RETURNS NVARCHAR(MAX)  
AS  
BEGIN  
   RETURN (
   SELECT section_id,
		section_name,
		section_display_sequence,
		question_id,
		question,
		dbo.IsCommentsExists(@p_line_id, question_id) comment_available,
		na_flag,
		max_no_of_pics_allowed,
		help_text_full,
		help_text,
		how_to_check,
		question_display_sequence
		FROM (
				SELECT  sm.section_id,
						sm.section_name,
						sm.display_sequence  section_display_sequence,
						qm.question_id,
						--qm.question,
						(select CASE @language_code 
						WHEN 'fr' THEN French
						WHEN 'fr-CA' THEN French
						WHEN 'es' THEN Spanish
						WHEN 'es-MX' THEN Spanish
						WHEN 'de' THEN German
						WHEN 'cs' THEN czech 
						WHEN 'zh-Hans' THEN Chinese
						WHEN 'zh-Hant' THEN Chinese
						WHEN 'zh-HK' THEN Chinese
						WHEN 'ko' THEN Korean
						WHEN 'th' THEN Thai
						WHEN '' THEN Bosnian
						WHEN 'pt' THEN Portuguese
						ELSE English END FROM mh_lpa_question_language_master WHERE Question_id=qm.question_id
						) 'question',
						ISNULL(qm.na_flag,'N') na_flag,
						max_no_of_pics_allowed,
						--'What Should be in Place? \n\n'+qm.help_text+'\n\n\n How to check it? \n\n'+qm.how_to_check help_text_full,
						--qm.help_text,
						--qm.how_to_check,
						
						(select CASE @language_code 
						WHEN 'fr' THEN French
						WHEN 'fr-CA' THEN French
						WHEN 'es' THEN Spanish
						WHEN 'es-MX' THEN Spanish
						WHEN 'de' THEN German
						WHEN 'cs' THEN czech 
						WHEN 'zh-Hans' THEN Chinese
						WHEN 'zh-Hant' THEN Chinese
						WHEN 'zh-HK' THEN Chinese
						WHEN 'ko' THEN Korean
						WHEN 'th' THEN Thai
						WHEN '' THEN Bosnian
						WHEN 'pt' THEN Portuguese
						ELSE English END FROM mh_lpa_language_master WHERE code='help1'
						)+
						--'What Should be in Place? \n\n'
						' \n\n'
						+(select CASE @language_code 
						WHEN 'fr' THEN French
						WHEN 'fr-CA' THEN French
						WHEN 'es' THEN Spanish
						WHEN 'es-MX' THEN Spanish
						WHEN 'de' THEN German
						WHEN 'cs' THEN czech 
						WHEN 'zh-Hans' THEN Chinese
						WHEN 'zh-Hant' THEN Chinese
						WHEN 'zh-HK' THEN Chinese
						WHEN 'ko' THEN Korean
						WHEN 'th' THEN Thai
						WHEN '' THEN Bosnian
						WHEN 'pt' THEN Portuguese
						ELSE English END FROM mh_lpa_help_text_language_master WHERE Question_id=qm.question_id
						)+
						'\n\n\n '+
						(select CASE @language_code 
						WHEN 'fr' THEN French
						WHEN 'fr-CA' THEN French
						WHEN 'es' THEN Spanish
						WHEN 'es-MX' THEN Spanish
						WHEN 'de' THEN German
						WHEN 'cs' THEN czech 
						WHEN 'zh-Hans' THEN Chinese
						WHEN 'zh-Hant' THEN Chinese
						WHEN 'zh-HK' THEN Chinese
						WHEN 'ko' THEN Korean
						WHEN 'th' THEN Thai
						WHEN '' THEN Bosnian
						WHEN 'pt' THEN Portuguese
						ELSE English END FROM mh_lpa_language_master WHERE code='help2'
						)+
						' \n\n'
						--'\n\n\n How to check it? \n\n'
						+(select CASE @language_code 
						WHEN 'fr' THEN French
						WHEN 'fr-CA' THEN French
						WHEN 'es' THEN Spanish
						WHEN 'es-MX' THEN Spanish
						WHEN 'de' THEN German
						WHEN 'cs' THEN czech 
						WHEN 'zh-Hans' THEN Chinese
						WHEN 'zh-Hant' THEN Chinese
						WHEN 'zh-HK' THEN Chinese
						WHEN 'ko' THEN Korean
						WHEN 'th' THEN Thai
						WHEN '' THEN Bosnian
						WHEN 'pt' THEN Portuguese
						ELSE English END FROM mh_lpa_how_to_check_language_master WHERE Question_id=qm.question_id
						) help_text_full,
						(select CASE @language_code 
						WHEN 'fr' THEN French
						WHEN 'fr-CA' THEN French
						WHEN 'es' THEN Spanish
						WHEN 'es-MX' THEN Spanish
						WHEN 'de' THEN German
						WHEN 'cs' THEN czech 
						WHEN 'zh-Hans' THEN Chinese
						WHEN 'zh-Hant' THEN Chinese
						WHEN 'zh-HK' THEN Chinese
						WHEN 'ko' THEN Korean
						WHEN 'th' THEN Thai
						WHEN '' THEN Bosnian
						WHEN 'pt' THEN Portuguese
						ELSE English END FROM mh_lpa_help_text_language_master WHERE Question_id=qm.question_id
						) 'help_text',
						(select CASE @language_code 
						WHEN 'fr' THEN French
						WHEN 'fr-CA' THEN French
						WHEN 'es' THEN Spanish
						WHEN 'es-MX' THEN Spanish
						WHEN 'de' THEN German
						WHEN 'cs' THEN czech 
						WHEN 'zh-Hans' THEN Chinese
						WHEN 'zh-Hant' THEN Chinese
						WHEN 'zh-HK' THEN Chinese
						WHEN 'ko' THEN Korean
						WHEN 'th' THEN Thai
						WHEN '' THEN Bosnian
						WHEN 'pt' THEN Portuguese
						ELSE English END FROM mh_lpa_how_to_check_language_master WHERE Question_id=qm.question_id
						) 'how_to_check',
						lq.display_sequence  question_display_sequence
				FROM    mh_lpa_question_master qm,
						mh_lpa_local_questions lq,
						mh_lpa_section_master sm,
						mh_lpa_line_master lm
				WHERE  lm.line_id = @p_line_id
				AND    lq.location_id = lm.location_id
				AND    lq.question_id < 10000
				AND    lq.question_id = qm.question_id
				AND    qm.section_id = sm.section_id
				UNION
				SELECT  sm.section_id,
						sm.section_name,
						sm.display_sequence  section_display_sequence,
						qm.local_question_id,
						qm.local_question,
						ISNULL(qm.na_flag,'N') na_flag,
						max_no_of_pics_allowed,
						'What Should be in Place? \n\n'+qm.local_help_text+'\n\n\nHow to check it? \n\n'+qm.how_to_check help_text_full,
						qm.local_help_text,
						qm.how_to_check,
						lq.display_sequence  question_display_sequence
				FROM    mh_lpa_local_question_master qm,
						mh_lpa_local_questions lq,
						mh_lpa_section_master sm,
						mh_lpa_line_master lm
				WHERE  lm.line_id = @p_line_id
				AND    lq.location_id = lm.location_id
				AND    lq.question_id = qm.local_question_id
				AND    lq.section_id = sm.section_id
				 ) tmp
		ORDER BY question_display_sequence
        FOR JSON AUTO)  
END