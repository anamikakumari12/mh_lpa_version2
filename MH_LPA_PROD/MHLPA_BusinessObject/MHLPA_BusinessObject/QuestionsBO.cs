﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MHLPA_BusinessObject
{
    public class QuestionsBO
    {
        public int p_user_id { get; set; }
        public int p_question_id { get; set; }
        public string p_section_name { get; set; }
        public string p_question { get; set; }
        public string p_help_text { get; set; }
        public string p_how_to_check{ get; set; }
        public int p_display_seq { get; set; }
        public int error_code { get; set; }
        public string error_msg { get; set; }
        public int p_location_id { get; set; }
        public string p_na_flag { get; set; }
    }
}
