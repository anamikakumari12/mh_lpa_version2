﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MHLPA_DataAccess;
using MHLPA_BusinessObject;
using System.Data;

namespace MHLPA_BusinessLogic
{
    public class UsersBL
    {
        UsersDAL objUserDAL = new UsersDAL();
        public UsersBO SaveUserDetailsBL(UsersBO objUserBO)
        {
            return objUserDAL.SaveUserDetailsDAL(objUserBO);
        }

        public DataTable GetUserDetailsBL(UsersBO objUserBO)
        {
            return objUserDAL.GetUserDetailsDAL(objUserBO);
        }

        public string ChangePasswordBL(UsersBO objUserBO)
        {
            return objUserDAL.ChangePasswordDAL(objUserBO);
        }
    }
}
