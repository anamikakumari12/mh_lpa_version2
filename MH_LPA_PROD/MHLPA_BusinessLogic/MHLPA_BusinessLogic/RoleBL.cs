﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MHLPA_BusinessObject;
using MHLPA_DataAccess;

namespace MHLPA_BusinessLogic
{
    public class RoleBL
    {
        RoleDAL objRoleDAL = new RoleDAL();
        public DataTable GetRoleBL()
        {
            return objRoleDAL.GetRoleDAL();
        }
        public RoleBO SaveRoleBL(RoleBO objRoleBL)
        {
            return objRoleDAL.SaveRoleDAL(objRoleBL);
        }

        public RoleBO DeleteRoleBL(RoleBO objRoleBO)
        {
            return objRoleDAL.DeleteRoleDAL(objRoleBO);
        }
    }
}
