﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MHLPA_BusinessObject;
using MHLPA_DataAccess;

namespace MHLPA_BusinessLogic
{
    public class DashboardBL
    {
        DashboardDAL objDAL = new DashboardDAL();
        public DataTable getDataforLPAResultsReportBL(DashboardBO objDashBO)
        {
            return objDAL.getDataforLPAResultsReportDAL(objDashBO);
        }

        public DataTable getLPAResultPerSectionBL(DashboardBO objDashBO)
        {
            return objDAL.getLPAResultPerSectionDAL(objDashBO);
        }
        public DataTable getLPAResultPerSectionMonthlyBL(DashboardBO objDashBO)
        {
            return objDAL.getLPAResultPerSectionMonthlyDAL(objDashBO);
        }

        public DataTable getLPAResultByLineBL(DashboardBO objDashBO)
        {
            return objDAL.getLPAResultByLineDAL(objDashBO);
        }

        public DataTable getLPAResultByQuestionBL(DashboardBO objDashBO)
        {
            return objDAL.getLPAResultByQuestionDAL(objDashBO);
        }

        public DataSet getFilterDataBL(DashboardBO objDashBO)
        {
            return objDAL.getFilterDataDAL(objDashBO);
        }

        public DataTable getDataforLPAComparisionReportBL(DashboardBO objDashBO)
        {
            return objDAL.getDataforLPAComparisionReportDAL(objDashBO);
        }

        public DataTable GetAuditReportsBL(DashboardBO objDashBO)
        {
           return objDAL.GetAuditReportsDAL(objDashBO);
        }

        public DataTable GetAuditResultsForDownloadBL(DashboardBO objDashBO)
        {
            return objDAL.GetAuditResultsForDownloadDAL(objDashBO);
        }

        public DataTable GetPerformanceReportForDownloadBL(DashboardBO objDashBO)
        {
            return objDAL.GetPerformanceReportForDownloadDAL(objDashBO);
        }
        public DataTable GetAuditScheduleReportBL(DashboardBO objDashBO)
        {
            return objDAL.GetAuditScheduleReportDAL(objDashBO);
        }

        public DataTable GetAuditPlanReportBL(DashboardBO objDashBO)
        {
            return objDAL.GetAuditPlanReportDAL(objDashBO);
        }

        public DataTable GetWorkCenterReportBL(DashboardBO objDashBO)
        {
            return objDAL.GetAuditScheduleReportDAL(objDashBO);
        }

        public DataTable GetPlantCoverageReportBL(DashboardBO objDashBO)
        {
            return objDAL.GetAuditScheduleReportDAL(objDashBO);
        }

        public DataTable GetSqdclpaReportBL(DashboardBO objDashBO)
        {
            return objDAL.GetAuditScheduleReportDAL(objDashBO);
        }

        public DataTable GetRegionalFindingsReportBL(DashboardBO objDashBO)
        {
            return objDAL.GetAuditScheduleReportDAL(objDashBO);
        }

        public DataTable GetFindingSummaryReportBL(DashboardBO objDashBO)
        {
            return objDAL.GetAuditScheduleReportDAL(objDashBO);
        }

        public DataTable GetOpenFindingDetailReportBL(DashboardBO objDashBO)
        {
            return objDAL.GetAuditScheduleReportDAL(objDashBO);
        }

        public DataTable GetOrphanedFindingsReportBL(DashboardBO objDashBO)
        {
            return objDAL.GetAuditScheduleReportDAL(objDashBO);
        }

        public DataTable GetUserRolesReportBL(DashboardBO objDashBO)
        {
            return objDAL.GetAuditScheduleReportDAL(objDashBO);
        }

        public DataTable getLPAResultByLineBottomBL(DashboardBO objDashBO)
        {
            return objDAL.getLPAResultByLineBottomDAL(objDashBO);
        }
    }
}
