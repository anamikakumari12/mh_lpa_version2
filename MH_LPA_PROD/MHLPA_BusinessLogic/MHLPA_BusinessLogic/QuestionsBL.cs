﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MHLPA_BusinessObject;
using MHLPA_DataAccess;

namespace MHLPA_BusinessLogic
{
    public class QuestionsBL
    {
        QuestionsDAL objQuesDAL = new QuestionsDAL();
        public DataTable getQuestionListforEditBL(QuestionsBO objQuesBO)
        {
            return objQuesDAL.getQuestionListforEditDAL(objQuesBO);
        }

        public QuestionsBO SaveQuestionsBL(QuestionsBO objQuesBO)
        {
            return objQuesDAL.SaveQuestionsDAL(objQuesBO);
        }

        public QuestionsBO SaveLocalQuestionsBL(QuestionsBO objQuesBO)
        {
            return objQuesDAL.SaveLocalQuestionsDAL(objQuesBO);
        }
    }
}
