﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MHLPA_DataAccess;
using MHLPA_BusinessObject;
using System.Data;

namespace MHLPA_BusinessLogic
{
   
  public  class DelAuditBL
    {
      DelAuditDAL objDelDAL = new DelAuditDAL();

        public DataTable getAuditDetailBL(DelAuditBO objDelBO)
        {
            return objDelDAL.getAuditDetailDAL(objDelBO);
        }

        public DelAuditBO DeleteAuditBL(DelAuditBO objDelAuditBO)
        {
            return objDelDAL.DeleteAuditDAL(objDelAuditBO);
        }
    }
}
