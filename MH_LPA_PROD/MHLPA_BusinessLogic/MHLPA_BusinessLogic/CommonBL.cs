﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MHLPA_DataAccess;
using MHLPA_BusinessObject;
using System.Data;
using System.Runtime.Serialization.Json;
using System.IO;
using System.Web.Script.Serialization;

namespace MHLPA_BusinessLogic
{
    public class CommonBL
    {
        CommonDAL objComDAL = new CommonDAL();
        public DataSet GetAllMasterBL(UsersBO objUserBO)
        {
            return objComDAL.GetAllMasterDAL(objUserBO);
        }


        public IEnumerable<ImproperCalendarEvent> getEventsBL(LocationBO objLocBO)
        {
            return objComDAL.getEventsDAL(objLocBO);
        }

        public DataTable GetLineListDropDownBL(UsersBO objUserBO)
        {
            return objComDAL.GetLineListDropDownDAL(objUserBO);
        }

        public DataTable GetCountryBasedOnRegionBL(UsersBO objUserBO)
        {
            return objComDAL.GetCountryBasedOnRegionDAL(objUserBO);
        }
        public DataTable GetLocationBasedOnCountryBL(UsersBO objUserBO)
        {
            return objComDAL.GetLocationBasedOnCountryDAL(objUserBO);
        }
        /// <summary>
        /// JSON Serialization
        /// </summary>
        public string JsonSerializer<T>(T t)
        {
            DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(T));
            MemoryStream ms = new MemoryStream();
            ser.WriteObject(ms, t);
            string jsonString = Encoding.UTF8.GetString(ms.ToArray());
            ms.Close();
            return jsonString;
        }
        /// <summary>
        /// JSON Deserialization
        /// </summary>
        public T JsonDeserialize<T>(string jsonString)
        {
            //DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(T));
            //MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(jsonString));
            //T obj = (T)ser.ReadObject(ms);

            JavaScriptSerializer json_serializer = new JavaScriptSerializer();
            T obj =(T)json_serializer.DeserializeObject(jsonString);
            return obj;
        }

        public DataTable GetPartBasedOnLineBL(UsersBO objUserBO)
        {
            return objComDAL.GetPartBasedOnLineDAL(objUserBO);
        }

        public DataTable GetEmployeesBasedOnLocationBL(UsersBO objUserBO)
        {
            return objComDAL.GetEmployeesBasedOnLocationDAL(objUserBO);
        }

        public DataTable GetEmailListBL(UsersBO objUserBO)
        {
            return objComDAL.GetEmailListDAL(objUserBO);
        }

        public DataTable GetPartBasedOnLocBL(UsersBO objUserBO)
        {
            return objComDAL.GetPartBasedOnLocDAL(objUserBO);
        }

        public DataTable GetTimeZoneByCountryBL(UsersBO objUserBO)
        {
            return objComDAL.GetTimeZoneByCountryDAL(objUserBO);
        }

        public DataTable GetBuildingDropDownBL(UsersBO objUserBO)
        {
            return objComDAL.GetBuildingDropDownDAL(objUserBO);
        }

        public DataTable GetBuildingListDropDownBL(UsersBO objUserBO)
        {
            return objComDAL.GetBuildingListDropDownDAL(objUserBO);
        }

        public static string DataTableToJSONWithStringBuilder(DataTable table)
        {
            var JSONString = new StringBuilder();
            if (table.Rows.Count > 0)
            {
                JSONString.Append("[");
                for (int i = 0; i < table.Rows.Count; i++)
                {
                    JSONString.Append("{");
                    for (int j = 0; j < table.Columns.Count; j++)
                    {
                        if (j < table.Columns.Count - 1)
                        {
                            JSONString.Append("\"" + table.Columns[j].ColumnName.ToString() + "\":" + "\"" + table.Rows[i][j].ToString() + "\",");
                        }
                        else if (j == table.Columns.Count - 1)
                        {
                            JSONString.Append("\"" + table.Columns[j].ColumnName.ToString() + "\":" + "\"" + table.Rows[i][j].ToString() + "\"");
                        }
                    }
                    if (i == table.Rows.Count - 1)
                    {
                        JSONString.Append("}");
                    }
                    else
                    {
                        JSONString.Append("},");
                    }
                }
                JSONString.Append("]");
            }
            return JSONString.ToString();
        }
    }
}
