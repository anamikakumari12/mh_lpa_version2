﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using MHLPA_BusinessObject;


namespace MHLPA_DataAccess
{
    public class LocationDAL
    {
        CommonFunctions objCom = new CommonFunctions();
        public DataTable GetLinProductDAL(LocationBO objLoc)
        {
            DataTable dtOutput = new DataTable();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csMHLPA"].ToString());
            SqlDataAdapter sqlda = null;
            SqlCommand sqlcmd = null;
            try
            {
                sqlcmd = new SqlCommand(ResourceFileDAL.LineProductProcedure, sqlconn);
                sqlconn.Open();
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add(ResourceFileDAL.ParamUserId, SqlDbType.Int).Value = objLoc.UserId;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_inactive_flag, SqlDbType.Int).Value = objLoc.Inactive_flag;
                sqlda = new SqlDataAdapter(sqlcmd);
                sqlda.Fill(dtOutput);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            finally
            {
                sqlconn.Close();
            }
            return dtOutput;
        }

        public LocationBO SaveLineProductBL(LocationBO objLocBO)
        {
            LocationBO objOutput = new LocationBO();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csMHLPA"].ToString());
            SqlCommand sqlcmd = null;
            try
            {
                sqlcmd = new SqlCommand(ResourceFileDAL.InsLineProductProcedure, sqlconn);
                sqlconn.Open();
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_line_product_rel_id, SqlDbType.Int).Value = objLocBO.line_product_rel_id;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_region_name, SqlDbType.VarChar, 100).Value = objLocBO.region_name;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_country_name, SqlDbType.VarChar, 1000).Value = objLocBO.country_name;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_location_name, SqlDbType.VarChar, 1000).Value = objLocBO.location_name;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_line_name, SqlDbType.VarChar, 100).Value = objLocBO.line_name;
                //sqlcmd.Parameters.Add(ResourceFileDAL.p_product_name, SqlDbType.VarChar, 100).Value = objLocBO.part_name;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_product_code, SqlDbType.VarChar, 100).Value = objLocBO.part_number;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_start_date, SqlDbType.Date).Value = objLocBO.startdate;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_end_date, SqlDbType.Date).Value = objLocBO.enddate;
                sqlcmd.Parameters.Add(ResourceFileDAL.l_err_code, SqlDbType.VarChar, 1).Direction = ParameterDirection.Output;
                sqlcmd.Parameters.Add(ResourceFileDAL.l_err_message, SqlDbType.VarChar, 100).Direction=ParameterDirection.Output;
                sqlcmd.ExecuteNonQuery();

                objOutput.error_code = Convert.ToInt32(sqlcmd.Parameters[ResourceFileDAL.l_err_code].Value);
                objOutput.error_msg = Convert.ToString(sqlcmd.Parameters[ResourceFileDAL.l_err_message].Value);

            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            finally
            {
                sqlconn.Close();
            }
            return objOutput;
        }

        public DataTable GetLocationDetailsDAL(LocationBO objLocBO)
        {
            DataTable dtOutput = new DataTable();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csMHLPA"].ToString());
            SqlDataAdapter sqlda = null;
            SqlCommand sqlcmd = null;
            try
            {
                sqlcmd = new SqlCommand(ResourceFileDAL.GetLocationDetailsProcedure, sqlconn);
                sqlconn.Open();
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add(ResourceFileDAL.ParamUserId, SqlDbType.Int).Value = objLocBO.UserId;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_inactive_flag, SqlDbType.Int).Value = objLocBO.Inactive_flag;
                sqlda = new SqlDataAdapter(sqlcmd);
                sqlda.Fill(dtOutput);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            finally
            {
                sqlconn.Close();
            }
            return dtOutput;
        }

        public LocationBO SaveLocationDAL(LocationBO objLocBO)
        {
            LocationBO objOutput = new LocationBO();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csMHLPA"].ToString());
            SqlCommand sqlcmd = null;
            try
            {
                sqlcmd = new SqlCommand(ResourceFileDAL.InsLocationProcedure, sqlconn);
                sqlconn.Open();
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_location_id, SqlDbType.Int).Value = Convert.ToInt32(objLocBO.location_id);
                sqlcmd.Parameters.Add(ResourceFileDAL.p_region_name, SqlDbType.VarChar, 100).Value = objLocBO.region_name;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_country_name, SqlDbType.VarChar, 1000).Value = objLocBO.country_name;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_location_code, SqlDbType.VarChar, 1000).Value = objLocBO.location_code;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_location_name, SqlDbType.VarChar, 1000).Value = objLocBO.location_name;
                sqlcmd.Parameters.Add(ResourceFileDAL.ParamStartDate, SqlDbType.Date).Value = objLocBO.startdate;
                sqlcmd.Parameters.Add(ResourceFileDAL.ParamEndDate, SqlDbType.Date).Value = objLocBO.enddate;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_no_of_shifts, SqlDbType.Int).Value = objLocBO.no_of_shifts;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_shift1_start_time, SqlDbType.Time).Value = objLocBO.shift1start;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_shift1_end_time, SqlDbType.Time).Value = objLocBO.shift1end;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_shift2_start_time, SqlDbType.Time).Value = objLocBO.shift2start;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_shift2_end_time, SqlDbType.Time).Value = objLocBO.shift2end;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_shift3_start_time, SqlDbType.Time).Value = objLocBO.shift3start;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_shift3_end_time, SqlDbType.Time).Value = objLocBO.shift3end;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_timezone_code, SqlDbType.VarChar, 1000).Value = objLocBO.p_timezone_code;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_timezone_desc, SqlDbType.VarChar, 1000).Value = objLocBO.p_timezone_desc;

                sqlcmd.Parameters.Add(ResourceFileDAL.l_err_code, SqlDbType.VarChar, 1).Direction = ParameterDirection.Output;
                sqlcmd.Parameters.Add(ResourceFileDAL.l_err_message, SqlDbType.VarChar, 100).Direction = ParameterDirection.Output;
                sqlcmd.ExecuteNonQuery();

                objOutput.error_code = Convert.ToInt32(sqlcmd.Parameters[ResourceFileDAL.l_err_code].Value);
                objOutput.error_msg = Convert.ToString(sqlcmd.Parameters[ResourceFileDAL.l_err_message].Value);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            finally
            {
                sqlconn.Close();
            }
            return objOutput;
        }

        public DataTable GetLineDetailsDAL(LocationBO objLocBO)
        {
            DataTable dtOutput = new DataTable();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csMHLPA"].ToString());
            SqlDataAdapter sqlda = null;
            SqlCommand sqlcmd = null;
            try
            {
                sqlcmd = new SqlCommand(ResourceFileDAL.getLineListforEditProcedure, sqlconn);
                sqlconn.Open();
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add(ResourceFileDAL.ParamUserId, SqlDbType.Int).Value = objLocBO.UserId;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_inactive_flag, SqlDbType.Int).Value = objLocBO.Inactive_flag;
                sqlda = new SqlDataAdapter(sqlcmd);
                sqlda.Fill(dtOutput);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            finally
            {
                sqlconn.Close();
            }
            return dtOutput;
        }

        public LocationBO SaveLineDetailsDAL(LocationBO objLocBO)
        {
            LocationBO objOutput = new LocationBO();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csMHLPA"].ToString());
            SqlCommand sqlcmd = null;
            try
            {
                sqlcmd = new SqlCommand(ResourceFileDAL.InsLineProcedure, sqlconn);
                sqlconn.Open();
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_line_id, SqlDbType.Int).Value = objLocBO.line_id;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_region_name, SqlDbType.VarChar, 100).Value = objLocBO.region_name;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_country_name, SqlDbType.VarChar, 1000).Value = objLocBO.country_name;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_location_name, SqlDbType.VarChar, 1000).Value = objLocBO.location_name;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_distribution_list, SqlDbType.VarChar,-1).Value = objLocBO.DistributionList;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_line_name, SqlDbType.VarChar, -1).Value = objLocBO.line_name;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_line_code, SqlDbType.VarChar, 1000).Value = objLocBO.line_number;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_building_name, SqlDbType.VarChar, -1).Value = objLocBO.building;
                sqlcmd.Parameters.Add(ResourceFileDAL.ParamStartDate, SqlDbType.Date).Value = objLocBO.startdate;
                sqlcmd.Parameters.Add(ResourceFileDAL.ParamEndDate, SqlDbType.Date).Value = objLocBO.enddate;
                sqlcmd.Parameters.Add(ResourceFileDAL.l_err_code, SqlDbType.VarChar, 1).Direction = ParameterDirection.Output;
                sqlcmd.Parameters.Add(ResourceFileDAL.l_err_message, SqlDbType.VarChar, 100).Direction = ParameterDirection.Output;
                //sqlcmd.Parameters.Add(ResourceFileDAL.l_mail_to_list, SqlDbType.VarChar, -1).Direction = ParameterDirection.Output;
                sqlcmd.ExecuteNonQuery();

                objOutput.error_code = Convert.ToInt32(sqlcmd.Parameters[ResourceFileDAL.l_err_code].Value);
                objOutput.error_msg = Convert.ToString(sqlcmd.Parameters[ResourceFileDAL.l_err_message].Value);
                //objOutput.mailIds = Convert.ToString(sqlcmd.Parameters[ResourceFileDAL.l_mail_to_list].Value);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            finally
            {
                sqlconn.Close();
            }
            return objOutput;
        }

        public LocationBO DeleteLineDetailsDAL(LocationBO objLocBO)
        {
            LocationBO objOutput = new LocationBO();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csMHLPA"].ToString());
            SqlCommand sqlcmd = null;
            try
            {
                sqlcmd = new SqlCommand(ResourceFileDAL.DeleteLineProcedure, sqlconn);
                sqlconn.Open();
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_line_id, SqlDbType.Int).Value = objLocBO.line_id;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_end_date, SqlDbType.Date).Value = objLocBO.enddate;
                sqlcmd.Parameters.Add(ResourceFileDAL.l_err_code, SqlDbType.VarChar, 1).Direction = ParameterDirection.Output;
                sqlcmd.Parameters.Add(ResourceFileDAL.l_err_message, SqlDbType.VarChar, 100).Direction = ParameterDirection.Output;
                sqlcmd.ExecuteNonQuery();

                objOutput.error_code = Convert.ToInt32(sqlcmd.Parameters[ResourceFileDAL.l_err_code].Value);
                objOutput.error_msg = Convert.ToString(sqlcmd.Parameters[ResourceFileDAL.l_err_message].Value);

            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            finally
            {
                sqlconn.Close();
            }
            return objOutput;
        }

        public DataTable SavePartsDAL(DataTable Exceldt, string flag)
        {
            DataTable dtOutput = new DataTable();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csMHLPA"].ToString());
            SqlCommand sqlcmd = null;
            SqlDataAdapter sqlda;
            try
            {
                sqlcmd = new SqlCommand(ResourceFileDAL.sp_insLineProduct_bulk, sqlconn);
                sqlconn.Open();
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.AddWithValue(ResourceFileDAL.p_parts_table, Exceldt);
                sqlcmd.Parameters.Add(ResourceFileDAL.p_add_edit_flag, SqlDbType.VarChar,50).Value = flag;
                sqlda = new SqlDataAdapter(sqlcmd);
                sqlda.Fill(dtOutput);

            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            finally
            {
                sqlconn.Close();
            }
            return dtOutput;
        }

        public LocationBO DeletePartDAL(LocationBO objLocBO)
        {
            LocationBO objOutput = new LocationBO();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csMHLPA"].ToString());
            SqlCommand sqlcmd = null;
            try
            {
                sqlcmd = new SqlCommand(ResourceFileDAL.DelPart, sqlconn);
                sqlconn.Open();
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_line_id, SqlDbType.Int).Value = objLocBO.line_id;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_product_id, SqlDbType.Int).Value = null;
                sqlcmd.Parameters.Add(ResourceFileDAL.l_err_code, SqlDbType.VarChar, 1).Direction = ParameterDirection.Output;
                sqlcmd.Parameters.Add(ResourceFileDAL.l_err_message, SqlDbType.VarChar, 100).Direction = ParameterDirection.Output;
                sqlcmd.ExecuteNonQuery();

                objOutput.error_code = Convert.ToInt32(sqlcmd.Parameters[ResourceFileDAL.l_err_code].Value);
                objOutput.error_msg = Convert.ToString(sqlcmd.Parameters[ResourceFileDAL.l_err_message].Value);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            finally
            {
                sqlconn.Close();
            }
            return objOutput;
        }
    }
}
