﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MHLPA_BusinessObject;


namespace MHLPA_DataAccess
{
    public class QuestionsDAL
    {
        CommonFunctions objCom = new CommonFunctions();
        public DataTable getQuestionListforEditDAL(QuestionsBO objQuesBO)
        {
            DataTable dtOutput = new DataTable(); 
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csMHLPA"].ToString());
            SqlDataAdapter sqlda = null;
            SqlCommand sqlcmd = null;
            try
            {
                sqlcmd = new SqlCommand(ResourceFileDAL.getQuestionListforEditProcedure, sqlconn);
                sqlconn.Open();
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add(ResourceFileDAL.ParamUserId, SqlDbType.Int).Value = objQuesBO.p_user_id;
                sqlda = new SqlDataAdapter(sqlcmd);
                sqlda.Fill(dtOutput);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            finally
            {
                sqlconn.Close();
            }
            return dtOutput;
        }


        public QuestionsBO SaveQuestionsDAL(QuestionsBO objQuesBO)
        {
            QuestionsBO objOutput = new QuestionsBO();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csMHLPA"].ToString());
            SqlCommand sqlcmd = null;
            try
            {
                sqlcmd = new SqlCommand(ResourceFileDAL.InsQuestionProcedure, sqlconn);
                sqlconn.Open();
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_question_id, SqlDbType.Int).Value = objQuesBO.p_question_id;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_section_name, SqlDbType.VarChar, 100).Value = objQuesBO.p_section_name;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_question, SqlDbType.VarChar, 1000).Value = objQuesBO.p_question;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_na_flag, SqlDbType.VarChar, 2).Value = objQuesBO.p_na_flag;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_help_text, SqlDbType.VarChar, 1000).Value = objQuesBO.p_help_text;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_how_to_check, SqlDbType.VarChar, 1000).Value = objQuesBO.p_how_to_check;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_display_seq, SqlDbType.Int).Value = objQuesBO.p_display_seq;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_location_id, SqlDbType.Int).Value = objQuesBO.p_location_id;
                sqlcmd.Parameters.Add(ResourceFileDAL.l_err_code, SqlDbType.VarChar, 1).Direction = ParameterDirection.Output;
                sqlcmd.Parameters.Add(ResourceFileDAL.l_err_message, SqlDbType.VarChar, 100).Direction = ParameterDirection.Output;


                sqlcmd.ExecuteNonQuery();

                objOutput.error_code = Convert.ToInt32(sqlcmd.Parameters[ResourceFileDAL.l_err_code].Value);
                objOutput.error_msg = Convert.ToString(sqlcmd.Parameters[ResourceFileDAL.l_err_message].Value);

            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            finally
            {
                sqlconn.Close();
            }
            return objOutput;
        }



        public QuestionsBO SaveLocalQuestionsDAL(QuestionsBO objQuesBO)
        {
            QuestionsBO objOutput = new QuestionsBO();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csMHLPA"].ToString());
            SqlCommand sqlcmd = null;
            try
            {
                sqlcmd = new SqlCommand(ResourceFileDAL.InsLocalQuestionProcedure, sqlconn);
                sqlconn.Open();
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_local_question_id, SqlDbType.Int).Value = objQuesBO.p_question_id;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_location_name, SqlDbType.VarChar,100).Value = objQuesBO.p_location_id;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_section_name, SqlDbType.VarChar, 100).Value = objQuesBO.p_section_name;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_question, SqlDbType.VarChar, 1000).Value = objQuesBO.p_question;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_na_flag, SqlDbType.VarChar, 2).Value = objQuesBO.p_na_flag;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_help_text, SqlDbType.VarChar, 1000).Value = objQuesBO.p_help_text;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_how_to_check, SqlDbType.VarChar, 1000).Value = objQuesBO.p_how_to_check;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_display_seq, SqlDbType.Int).Value = objQuesBO.p_display_seq;
                //sqlcmd.Parameters.Add(ResourceFileDAL.p_location_id, SqlDbType.Int).Value = objQuesBO.p_location_id;
                sqlcmd.Parameters.Add(ResourceFileDAL.l_err_code, SqlDbType.VarChar, 1).Direction = ParameterDirection.Output;
                sqlcmd.Parameters.Add(ResourceFileDAL.l_err_message, SqlDbType.VarChar, 100).Direction = ParameterDirection.Output;


                sqlcmd.ExecuteNonQuery();

                objOutput.error_code = Convert.ToInt32(sqlcmd.Parameters[ResourceFileDAL.l_err_code].Value);
                objOutput.error_msg = Convert.ToString(sqlcmd.Parameters[ResourceFileDAL.l_err_message].Value);

            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            finally
            {
                sqlconn.Close();
            }
            return objOutput;
        }
    }
}
