﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MHLPA_BusinessObject;
using System.Configuration;
using System.Globalization;

namespace MHLPA_DataAccess
{
    public class CommonDAL
    {
        CommonFunctions objCom = new CommonFunctions();
        public DataSet GetAllMasterDAL(UsersBO objUser)
        {
            DataSet dsOutput = new DataSet();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csMHLPA"].ToString());
            SqlDataAdapter sqlda = null;
            SqlCommand sqlcmd = null;
            try
            {
                sqlcmd = new SqlCommand(ResourceFileDAL.getAllMastersProcedure, sqlconn);
                sqlconn.Open();
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add(ResourceFileDAL.ParamUserId, SqlDbType.Int).Value = objUser.UserId;
                sqlda = new SqlDataAdapter(sqlcmd);
                sqlda.Fill(dsOutput);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            finally
            {
                sqlconn.Close();
            }
            return dsOutput;
        }

        public IEnumerable<ImproperCalendarEvent> getEventsDAL(LocationBO objLocBO)
        {
            DataTable dtOutput;
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csMHLPA"].ToString());
            SqlCommand sqlcmd = null;
            List<ImproperCalendarEvent> events = new List<ImproperCalendarEvent>();
            try
            {
                dtOutput = new DataTable();
                sqlcmd = new SqlCommand(ResourceFileDAL.GetAuditPlanProcedure, sqlconn);
                sqlconn.Open();
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.CommandTimeout = 10000000;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_location_id, SqlDbType.Int).Value = Convert.ToInt32(objLocBO.p_location_id);
                SqlDataReader reader = sqlcmd.ExecuteReader();
                
                while (reader.Read())
                {
                    ImproperCalendarEvent cevent = new ImproperCalendarEvent();
                    cevent.audit_plan_id = Convert.ToInt32(reader["audit_plan_id"]);
                    cevent.Emp_name = Convert.ToString(reader["emp_name"]);
                    cevent.planned_date = Convert.ToString(reader["planned_date"]);
                    cevent.line_id = Convert.ToInt32(reader["line_id"]);
                    cevent.line_name = Convert.ToString(reader["line_name"]);
                    cevent.to_be_audited_by_user_id = Convert.ToInt32(reader["to_be_audited_by_user_id"]);
                    cevent.planned_end_date = Convert.ToString(reader["planned_date_end"]);
                    cevent.Audit_Status = Convert.ToInt32(reader["Audit_Status"]);

                    //cevent.shift_no = Convert.ToInt32(reader["shift_no"]);
                    //cevent.line_product_rel_id = Convert.ToInt32(reader["line_product_rel_id"]);
                    //cevent.product_id = Convert.ToInt32(reader["product_id"]);
                    //cevent.Userflag = Convert.ToString(reader["emp_name_flag"]);
                    events.Add(cevent);
                }
            }
            
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            finally
            {
                sqlconn.Close();
            }
            return events;
        }

        public DataTable GetLineListDropDownDAL(UsersBO objUserBO)
        {
            DataTable dtOutput = new DataTable();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csMHLPA"].ToString());
            SqlDataAdapter sqlda = null;
            SqlCommand sqlcmd = null;
            try
            {

                sqlcmd = new SqlCommand(ResourceFileDAL.GetLineListforDropDown, sqlconn);
                sqlconn.Open();
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_location_id, SqlDbType.Int).Value = objUserBO.location_id;
                //sqlcmd.Parameters.Add(ResourceFileDAL.p_line_id, SqlDbType.Int).Value = objUserBO.line_id;
                sqlda = new SqlDataAdapter(sqlcmd);
                sqlda.Fill(dtOutput);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            finally
            {
                sqlconn.Close();
            }
            return dtOutput;
        }

        public DataTable GetCountryBasedOnRegionDAL(UsersBO objUserBO)
        {
            DataTable dtOutput = new DataTable();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csMHLPA"].ToString());
            SqlDataAdapter sqlda = null;
            SqlCommand sqlcmd = null;
            try
            {

                sqlcmd = new SqlCommand(ResourceFileDAL.GetCountryListProcedure, sqlconn);
                sqlconn.Open();
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_user_id, SqlDbType.Int).Value = objUserBO.UserId;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_region_id, SqlDbType.Int).Value = objUserBO.region_id;
                sqlda = new SqlDataAdapter(sqlcmd);
                sqlda.Fill(dtOutput);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            finally
            {
                sqlconn.Close();
            }
            return dtOutput;
        }

        public DataTable GetLocationBasedOnCountryDAL(UsersBO objUserBO)
        {
            DataTable dtOutput = new DataTable();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csMHLPA"].ToString());
            SqlDataAdapter sqlda = null;
            SqlCommand sqlcmd = null;
            try
            {

                sqlcmd = new SqlCommand(ResourceFileDAL.GetLocationListProcedure, sqlconn);
                sqlconn.Open();
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_user_id, SqlDbType.Int).Value = objUserBO.UserId;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_country_id, SqlDbType.Int).Value = objUserBO.country_id;
                sqlda = new SqlDataAdapter(sqlcmd);
                sqlda.Fill(dtOutput);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            finally
            {
                sqlconn.Close();
            }
            return dtOutput;
        }

        public DataTable GetPartBasedOnLineDAL(UsersBO objUserBO)
        {
            DataTable dtOutput = new DataTable();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csMHLPA"].ToString());
            SqlDataAdapter sqlda = null;
            SqlCommand sqlcmd = null;
            try
            {

                sqlcmd = new SqlCommand(ResourceFileDAL.GetPartDetailsforLineProcedure, sqlconn);
                sqlconn.Open();
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_line_id, SqlDbType.Int).Value = objUserBO.line_id;
                sqlda = new SqlDataAdapter(sqlcmd);
                sqlda.Fill(dtOutput);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            finally
            {
                sqlconn.Close();
            }
            return dtOutput;
        }

        public DataTable GetEmployeesBasedOnLocationDAL(UsersBO objUserBO)
        {
            DataTable dtOutput = new DataTable();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csMHLPA"].ToString());
            SqlDataAdapter sqlda = null;
            SqlCommand sqlcmd = null;
            try
            {

                sqlcmd = new SqlCommand(ResourceFileDAL.GetEmployeeListforWeb, sqlconn);
                sqlconn.Open();
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_location_id, SqlDbType.Int).Value = objUserBO.location_id;
                sqlda = new SqlDataAdapter(sqlcmd);
                sqlda.Fill(dtOutput);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            finally
            {
                sqlconn.Close();
            }
            return dtOutput;
        }

        public DataTable GetEmailListDAL(UsersBO objUserBO)
        {
            DataTable dtOutput = new DataTable();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csMHLPA"].ToString());
            SqlDataAdapter sqlda = null;
            SqlCommand sqlcmd = null;
            try
            {

                sqlcmd = new SqlCommand(ResourceFileDAL.sp_getEmailList, sqlconn);
                sqlconn.Open();
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_location_id, SqlDbType.Int).Value = objUserBO.location_id;
                sqlda = new SqlDataAdapter(sqlcmd);
                sqlda.Fill(dtOutput);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            finally
            {
                sqlconn.Close();
            }
            return dtOutput;
        }

        public DataTable GetPartBasedOnLocDAL(UsersBO objUserBO)
        {
            DataTable dtOutput = new DataTable();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csMHLPA"].ToString());
            SqlDataAdapter sqlda = null;
            SqlCommand sqlcmd = null;
            try
            {

                sqlcmd = new SqlCommand(ResourceFileDAL.GetPartBasedOnLocforWeb, sqlconn);
                sqlconn.Open();
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_location_id, SqlDbType.Int).Value = objUserBO.location_id;
                sqlda = new SqlDataAdapter(sqlcmd);
                sqlda.Fill(dtOutput);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            finally
            {
                sqlconn.Close();
            }
            return dtOutput;
        }

        public DataTable GetTimeZoneByCountryDAL(UsersBO objUserBO)
        {
            DataTable dtOutput = new DataTable();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csMHLPA"].ToString());
            SqlDataAdapter sqlda = null;
            SqlCommand sqlcmd = null;
            try
            {

                sqlcmd = new SqlCommand(ResourceFileDAL.sp_getTimeZoneForCountry, sqlconn);
                sqlconn.Open();
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_country_name, SqlDbType.VarChar,200).Value = objUserBO.country_name;
                sqlda = new SqlDataAdapter(sqlcmd);
                sqlda.Fill(dtOutput);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            finally
            {
                sqlconn.Close();
            }
            return dtOutput;
        }

        public DataTable GetBuildingDropDownDAL(UsersBO objUserBO)
        {
            DataTable dtOutput = new DataTable();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csMHLPA"].ToString());
            SqlDataAdapter sqlda = null;
            SqlCommand sqlcmd = null;
            try
            {

                sqlcmd = new SqlCommand(ResourceFileDAL.sp_getBuildingList, sqlconn);
                sqlconn.Open();
                sqlcmd.CommandType = CommandType.StoredProcedure;
                //sqlcmd.Parameters.Add(ResourceFileDAL.p_country_name, SqlDbType.VarChar, 200).Value = objUserBO.country_name;
                sqlda = new SqlDataAdapter(sqlcmd);
                sqlda.Fill(dtOutput);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            finally
            {
                sqlconn.Close();
            }
            return dtOutput;
        }

        public DataTable GetBuildingListDropDownDAL(UsersBO objUserBO)
        {
            DataTable dtOutput = new DataTable();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csMHLPA"].ToString());
            SqlDataAdapter sqlda = null;
            SqlCommand sqlcmd = null;
            try
            {

                sqlcmd = new SqlCommand(ResourceFileDAL.GetBuildingforLocation, sqlconn);
                sqlconn.Open();
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_location_id, SqlDbType.VarChar, 200).Value = objUserBO.location_id;
                sqlda = new SqlDataAdapter(sqlcmd);
                sqlda.Fill(dtOutput);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            finally
            {
                sqlconn.Close();
            }
            return dtOutput;
        }
    }
}
