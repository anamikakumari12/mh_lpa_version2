﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MHLPA_BusinessObject;

namespace MHLPA_DataAccess
{
    public class GroupsDAL
    {
        CommonFunctions objCom = new CommonFunctions();
        public DataSet GetGroupMemberListDAL(GroupsBO objGrpBO)
        {
            DataSet dtOutput = new DataSet();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csMHLPA"].ToString());
            SqlCommand sqlcmd = null;
            SqlDataAdapter sqlada;
            try
            {
                sqlcmd = new SqlCommand(ResourceFileDAL.GetGroupMemberListProcedure, sqlconn);
                sqlconn.Open();
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_location_name, SqlDbType.VarChar,100).Value = objGrpBO.p_location_name;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_location_id, SqlDbType.Int).Value = objGrpBO.p_location_id;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_group_name, SqlDbType.VarChar, 100).Value = objGrpBO.p_group_name;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_group_id, SqlDbType.Int).Value = objGrpBO.p_group_id;
                sqlada = new SqlDataAdapter(sqlcmd);
                sqlada.Fill(dtOutput);

            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            return dtOutput;
        }

        public int SaveGroupMemberDAL(GroupsBO objGrpBO, DataTable dtUserId)
        {
            RoleBO objOutput = new RoleBO();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csMHLPA"].ToString());
            SqlCommand sqlcmd = null;
            try
            {
                sqlcmd = new SqlCommand(ResourceFileDAL.InsGroupMembersProcedure, sqlconn);
                sqlconn.Open();
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_location_name, SqlDbType.VarChar, 100).Value = objGrpBO.p_location_name;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_group_name, SqlDbType.VarChar, 1000).Value = objGrpBO.p_group_name;
                sqlcmd.Parameters.AddWithValue(ResourceFileDAL.p_group_members, dtUserId);
                sqlcmd.ExecuteNonQuery();
                return 0;
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
                return 1;
            }
            finally
            {
                sqlconn.Close();
            }
        }
    }
}
