﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using MHLPA_BusinessObject;

namespace MHLPA_DataAccess
{
  public  class DelAuditDAL
    {
      CommonFunctions objCom = new CommonFunctions();
      public DataTable getAuditDetailDAL(DelAuditBO objDelBO)
        {
            DataTable dtOutput = new DataTable();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csMHLPA"].ToString());
            SqlDataAdapter sqlda = null;
            SqlCommand sqlcmd = null;
            try
            {
                sqlcmd = new SqlCommand(ResourceFileDAL.getAuditDetail, sqlconn);
                sqlconn.Open();
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_audit_id, SqlDbType.VarChar,200).Value = objDelBO.audit_id;
                sqlda = new SqlDataAdapter(sqlcmd);
                sqlda.Fill(dtOutput);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            finally
            {
                sqlconn.Close();
            }
            return dtOutput;
        }
      public DelAuditBO DeleteAuditDAL(DelAuditBO objDelAuditBO)
      {
          DelAuditBO objOutput = new DelAuditBO();
          SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csMHLPA"].ToString());
          SqlCommand sqlcmd = null;
          try
          {
              sqlcmd = new SqlCommand(ResourceFileDAL.sp_deleteAudit, sqlconn);
              sqlconn.Open();
              sqlcmd.CommandType = CommandType.StoredProcedure;
              sqlcmd.Parameters.Add(ResourceFileDAL.p_audit_id, SqlDbType.Int).Value = objDelAuditBO.audit_id;
              sqlcmd.ExecuteNonQuery();
          }
          catch (Exception ex)
          {
              objCom.ErrorLog(ex);
          }
          finally
          {
              sqlconn.Close();
          }
          return objOutput;
      }

     
    }
}
