﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MHLPA_BusinessObject;
using MHLPA_BusinessLogic;
using System.Data;
using System.Globalization;
using System.Configuration;
using System.IO;


namespace MHLPA_Application
{
    public partial class DeleteAudit : System.Web.UI.Page
    {
        #region Global Declaration
        DelAuditBO objDelBO;
        DelAuditBL objDelBL;
        CommonBL objComBL;
        CommonFunctions objCom = new CommonFunctions();
        #endregion

        #region Events
        /// <summary>
        /// Author: Neha(KNS)
        /// Date: Jan 07,2020
        /// Desc:Desc: Check for login session and load the login page if session timeout
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            int UserId;
            try
            {
                if (string.IsNullOrEmpty(Convert.ToString(Session["UserId"]))) { Response.Redirect("Login.aspx"); return; }
                else
                {
                    UserId = Convert.ToInt32(Session["UserId"]);
                }
                if (!Page.IsPostBack)
                {
                    if (!string.IsNullOrEmpty(Convert.ToString(Session["UserId"])))
                    {
                        tblAuditResult.Visible = false;
                        btnclear.Visible = false;
                        btnDelete.Visible = false;
                        // btnSubmit_Click(null, null);
                        //btnCancel_Click(null, null);
                        //btnClear_Click(null, null);
                    }
                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }
        /// <summary>
        /// Author: Anamika(KNS)
        /// Date : Jan 07,2020
        /// Desc: On click of Submit button, it will call the getAuditDetailBL with all the details to save to database.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            tblAuditResult.Visible = true;
            btnclear.Visible = true;
            btnDelete.Visible = true;
            DeleteAudit Output = new DeleteAudit();
            try
            {
                objDelBO = new DelAuditBO();
                objDelBL = new DelAuditBL();
                DataTable dtResult = new DataTable();
                string audit_id = Convert.ToString(txtAuditID.Text);
                if (audit_id.Contains('-'))
                {
                    objDelBO.audit_id = audit_id.Substring(audit_id.LastIndexOf('-')+1, audit_id.Length - audit_id.LastIndexOf('-')-1);
                }
                else
                {
                    objDelBO.audit_id = audit_id;
                }
                dtResult = objDelBL.getAuditDetailBL(objDelBO);
                if (dtResult.Rows.Count != 0)
                {
                    if (dtResult.Rows.Count > 0)
                    {
                        lblaud_id.Text = Convert.ToString(dtResult.Rows[0]["Audit_ID"]);
                        lblocation_name.Text = Convert.ToString(dtResult.Rows[0]["location_name"]);
                        lblproduct.Text = Convert.ToString(dtResult.Rows[0]["product_code"]);
                        lblconducted_on.Text = Convert.ToString(dtResult.Rows[0]["conducted_on"]);
                        lblconducted_by.Text = Convert.ToString(dtResult.Rows[0]["conducted_by"]);
                        lblBuilding_name.Text = Convert.ToString(dtResult.Rows[0]["building_name"]);
                        lblinename.Text = Convert.ToString(dtResult.Rows[0]["line_name"]);
                        lblscore.Text = Convert.ToString(dtResult.Rows[0]["score"]);
                        lblnoshifts.Text = Convert.ToString(dtResult.Rows[0]["no_of_shifts"]);

                    }
                }
                else
                {
                    lblError.Text = "No audit available for Delete.";
                    tblAuditResult.Visible = false;
                    btnclear.Visible = false;
                    btnDelete.Visible = false;
                }

                //    ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "alert('No audit details found.');", true);
                //else
                //    ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "alert('abc.');", true);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }
        /// <summary>
        /// Author:Neha(KNS)
        /// Date  : Jan 09,2020
        /// Desc: On click of Cancel button, it will clear Audit id textbox.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            try
            {
                txtAuditID.Text = "";
                pnlaudit.Visible = false;
                Response.Redirect(Request.RawUrl);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }
        /// <summary>
        /// Author:Neha(KNS)
        /// Date  : Jan 09,2020
        /// Desc: On click of Cancel button, it will clear Audit id textbox.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnClear_Click(object sender, EventArgs e)
        {
            try
            {
                txtAuditID.Text = "";
                pnlaudit.Visible = false;
                tblAuditResult.Visible = false;
                btnclear.Visible = false;
                btnDelete.Visible = false;
                Response.Redirect(Request.RawUrl);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }
        /// <summary>
        /// Author:Neha(KNS)
        /// Date  : Jan 09,2020
        /// Desc: On click of Delete button, it will call the DeleteAuditBL with all the details to delete audit id from database.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnDelete_Click(object sender, EventArgs e)
        {
            int flag = 0;
            try
            {
                string audit_id = Convert.ToString(txtAuditID.Text);
                string confirmValue = Request.Form["confirm_value"];
                if (confirmValue == "Yes")
                {
                    objDelBO = new DelAuditBO();
                    objDelBL = new DelAuditBL();
                    if (audit_id.Contains('-'))
                    {
                        objDelBO.audit_id = audit_id.Substring(audit_id.LastIndexOf('-') + 1, audit_id.Length - audit_id.LastIndexOf('-') - 1);
                    }
                    else
                    {
                        objDelBO.audit_id = audit_id;
                    }
                    objDelBO = objDelBL.DeleteAuditBL(objDelBO);
                    LoadDeletedReports();
                    txtAuditID.Text = "";
                    pnlaudit.Visible = false;
                    btnclear.Visible = false;
                    btnDelete.Visible = false;
                }

                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "alert('Sucessfully deleted.');", true);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }


        #endregion

        #region Methods
        /// <summary>
        /// Author:Neha(KNS)
        /// Date  : Jan 09,2020
        /// Desc: On click of Delete button, it will call the loadreports function  to move file from Audit_Reports folder to DeleteAudit_Reports
        /// </summary>
        private void LoadDeletedReports()
        {
            try
            {
                int auditid = Convert.ToInt32(txtAuditID.Text);
                string filename = "Audit_Result_" + Convert.ToString(auditid) + ".pdf";
                string filepath = ConfigurationManager.AppSettings["DeletedAudit"].ToString();
                string SourceFile = ConfigurationManager.AppSettings["PDF_Folder"].ToString();
                string completepath = filepath + filename;
                string Source = System.IO.Path.Combine(SourceFile, filename);
                string Destination = System.IO.Path.Combine(filepath, filename);
                System.IO.File.Move(Source, Destination);
                //ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "alert('abc.');", true);             
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }
        #endregion
    }
}