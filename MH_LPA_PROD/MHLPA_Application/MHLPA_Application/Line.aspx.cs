﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MHLPA_BusinessObject;
using MHLPA_BusinessLogic;
using System.Data;
using System.Globalization;
using QRCoder;
using System.Drawing;
using System.IO;
using AjaxControlToolkit;
using System.Web.Services;

namespace MHLPA_Application
{
    public partial class Line : System.Web.UI.Page
    {
        #region Global Declaration
        LocationBO objLocBO;
        LocationBL objLocBL;
        DataSet dsDropDownData;
        UsersBO objUserBO;
        CommonBL objComBL;
        CommonFunctions objCom = new CommonFunctions();
        int inactive_flag = 0;
        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                int UserId;
                int vwline_id;
                if (string.IsNullOrEmpty(Convert.ToString(Session["UserId"]))) { Response.Redirect("Login.aspx"); return; }
                else
                {
                    UserId = Convert.ToInt32(Session["UserId"]);
                }
                if (!Page.IsPostBack)
                {
                    if (!string.IsNullOrEmpty(Convert.ToString(Session["UserId"])))
                    {
                        GridBind();
                        DataTable dtGrid = new DataTable();
                        dtGrid = (DataTable)Session["dtLine"];

                        vwline_id = Convert.ToInt32(dtGrid.Rows[0]["line_id"]);
                        Session["line_id"] = vwline_id;
                        DataRow drfirst = selectedRow(vwline_id);
                        LoadViewpanel(drfirst);
                        Session["PrintFlag"] = 0;

                    }
                }
                else
                {
                    if (Convert.ToString(Session["Print"]) == "yes")
                    {
                        Session["Print"] = "No";
                        System.Web.UI.WebControls.Image imgBarCode = new System.Web.UI.WebControls.Image();
                        imgBarCode = (System.Web.UI.WebControls.Image)pnlQRCode.FindControl("image");
                        if (imgBarCode == null)
                        {
                            GridBind();
                            DataTable dtGrid = new DataTable();
                            dtGrid = (DataTable)Session["dtLine"];
                            vwline_id = Convert.ToInt32(Session["line_id"]);
                            DataRow drfirst = selectedRow(vwline_id);
                            LoadViewpanel(drfirst);
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {

            try
            {
                Clear();
                GetMasterDropDown();
                editpanel.Visible = true;
                viewpanel.Visible = false;
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }


        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            LocationBO Output = new LocationBO();
            try
            {
                objLocBO = new LocationBO();
                objLocBL = new LocationBL();
                objLocBO.UserId = Convert.ToInt32(Session["UserId"]);
                if (!string.IsNullOrEmpty(Convert.ToString(Session["line_id"])))
                    objLocBO.line_id = Convert.ToInt32(Session["line_id"]);
                objLocBO.region_name = Convert.ToString(ddlRegion.SelectedItem);
                objLocBO.country_name = Convert.ToString(ddlCountry.SelectedItem);
                objLocBO.location_name = Convert.ToString(ddlLocation.SelectedItem);
                objLocBO.line_name = Convert.ToString(txtLineName.Text);
                objLocBO.DistributionList = Convert.ToString(txtList.Text);
                objLocBO.building = Convert.ToString(ddlBuilding.SelectedItem);
                if (String.IsNullOrEmpty(objLocBO.building))
                {
                    objLocBO.building = Convert.ToString(txtBuilding.Text);
                }
                string UIpattern = "dd-MM-yyyy";
                string DBPattern = "MM-dd-yyyy";
                DateTime parsedDate;

                if (DateTime.TryParseExact(txtStartDate.Text, UIpattern, null, DateTimeStyles.None, out parsedDate))
                {
                    string frmdate = parsedDate.ToString(DBPattern);
                    objLocBO.startdate = DateTime.ParseExact(frmdate, "MM-dd-yyyy", null);
                }
                else
                {
                    if (string.IsNullOrEmpty(txtStartDate.Text))
                    {
                        objLocBO.startdate = null;
                    }
                    else
                    {
                        objLocBO.startdate = Convert.ToDateTime(txtStartDate.Text);
                    }
                }
                if (!string.IsNullOrEmpty(txtEndDate.Text))
                {

                    if (DateTime.TryParseExact(txtEndDate.Text, UIpattern, null, DateTimeStyles.None, out parsedDate))
                    {
                        string frmdate = parsedDate.ToString(DBPattern); // one extra day is adding for existing date. sql 
                        objLocBO.enddate = DateTime.ParseExact(frmdate, "MM-dd-yyyy", null);
                    }
                    else
                    {
                        if (string.IsNullOrEmpty(txtEndDate.Text))
                        {
                            objLocBO.enddate = null;
                        }
                        else
                        {
                            objLocBO.enddate = Convert.ToDateTime(txtEndDate.Text);
                        }
                    }
                }
                Output = objLocBL.SaveLineDetailsBL(objLocBO);

                if (Output.error_code == 0)
                {

                    if (objLocBO.line_id == 0)
                    {
                        GridBind();
                        DataTable dtGrid = new DataTable();
                        dtGrid = (DataTable)Session["dtLine"];
                        DataRow drfirst = selectedRow(Convert.ToInt32(dtGrid.Rows[0]["line_id"]));
                        LoadViewpanel(drfirst);
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "alert('A new line is created successfully.');", true);
                    }
                    else
                    {
                        GridBind();
                        DataTable dtGrid = new DataTable();
                        dtGrid = (DataTable)Session["dtLine"];
                        DataRow drfirst = selectedRow(Convert.ToInt32(dtGrid.Rows[0]["line_id"]));
                        LoadViewpanel(drfirst);
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "alert('Line is modified successfully.');", true);
                    }
                        
                    //SendMails(objLocBO);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "alert('" + Output.error_msg + "');", true);
                }

            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        protected void Select(object sender, EventArgs e)
        {
            try
            {
                int line_id = Convert.ToInt32((sender as LinkButton).CommandArgument);
                string old = Convert.ToString(Session["line_id"]);
                foreach (Control item in pnlQRCode.Controls.OfType<System.Web.UI.WebControls.Image>())
                {
                    if (item != null)
                        pnlQRCode.Controls.Remove(item);
                }
                foreach (Control item in pnlprintQRCode.Controls.OfType<System.Web.UI.WebControls.Image>())
                {
                    if (item != null)
                        pnlprintQRCode.Controls.Remove(item);
                }
                Session["line_id"] = line_id;
                DataRow drselect = selectedRow(line_id);
                LoadViewpanel(drselect);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        protected void Edit(object sender, EventArgs e)
        {
            try
            {
                int line_id = Convert.ToInt32((sender as ImageButton).CommandArgument);
                Session["line_id"] = line_id;
                DataRow drselect = selectedRow(line_id);
                LoadEditPanel(drselect);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        protected void ddlRegion_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dtLineProduct = new DataTable();
            DataTable dtCountry = new DataTable();
            DataTable dtLocation = new DataTable();
            DataTable dtEmail = new DataTable();
            try
            {
                dsDropDownData = new DataSet();
                objUserBO = new UsersBO();
                objUserBO.UserId = Convert.ToInt32(Session["UserId"]);
                objComBL = new CommonBL();

                objUserBO.region_id = Convert.ToInt32(ddlRegion.SelectedValue);
                dtCountry = objComBL.GetCountryBasedOnRegionBL(objUserBO);

                if (dtCountry.Rows.Count > 0)
                {
                    ddlCountry.DataSource = dtCountry;
                    ddlCountry.DataTextField = "country_name";
                    ddlCountry.DataValueField = "country_id";
                    ddlCountry.DataBind();
                    objUserBO.country_id = Convert.ToInt32(ddlCountry.SelectedValue);
                }
                else
                {
                    ddlCountry.Items.Clear();
                    ddlCountry.DataSource = null;
                    ddlCountry.DataBind();
                }

                //dtLocation = objComBL.GetLocationBasedOnCountryBL(objUserBO);
                //if (dtLocation.Rows.Count > 0)
                //{
                //    ddlLocation.DataSource = dtLocation;
                //    ddlLocation.DataTextField = "location_name";
                //    ddlLocation.DataValueField = "location_id";
                //    ddlLocation.DataBind();
                //    objUserBO.location_id = Convert.ToInt32(ddlLocation.SelectedValue);
                //}
                //else
                //{
                //    ddlLocation.Items.Clear();
                //    ddlLocation.DataSource = null;
                //    ddlLocation.DataBind();
                //}
                //objUserBO.location_id = Convert.ToInt32(ddlLocation.SelectedValue);
                //dtEmail = objComBL.GetEmailListBL(objUserBO);
                //if (dtEmail != null)
                //{
                //    if (dtEmail.Rows.Count > 0)
                //    {
                //        Session["Emails"] = dtEmail;
                //    }
                //    else
                //    {
                //    }
                //}
                //else
                //{
                //    //lstEmail.Items.Clear();
                //    //lstEmail.DataSource = null;
                //    //lstEmail.DataBind();
                //}

                ddlCountry_SelectedIndexChanged(null, null);

            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        protected void ddlCountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dtLineProduct = new DataTable();
            DataTable dtCountry = new DataTable();
            DataTable dtLocation = new DataTable();
            DataTable dtEmail = new DataTable();
            try
            {
                dsDropDownData = new DataSet();
                objUserBO = new UsersBO();
                objUserBO.UserId = Convert.ToInt32(Session["UserId"]);
                objComBL = new CommonBL();

                objUserBO.country_id = Convert.ToInt32(ddlCountry.SelectedValue);
                dtLocation = objComBL.GetLocationBasedOnCountryBL(objUserBO);
                if (dtLocation.Rows.Count > 0)
                {
                    ddlLocation.DataSource = dtLocation;
                    ddlLocation.DataTextField = "location_name";
                    ddlLocation.DataValueField = "location_id";
                    ddlLocation.DataBind();
                }
                else
                {
                    ddlLocation.Items.Clear();
                    ddlLocation.DataSource = null;
                    ddlLocation.DataBind();
                }
                //objUserBO.location_id = Convert.ToInt32(ddlLocation.SelectedValue);
                //// objUserBO.location_id = Convert.ToInt32(ddlLocation.SelectedValue);
                //dtEmail = objComBL.GetEmailListBL(objUserBO);
                //if (dtEmail != null)
                //{
                //    if (dtEmail.Rows.Count > 0)
                //    {
                //        Session["Emails"] = dtEmail;
                //    }
                //    else
                //    {
                //        //lstEmail.Items.Clear();
                //        //lstEmail.DataSource = null;
                //        //lstEmail.DataBind();
                //    }
                //}
                //else
                //{
                //    //lstEmail.Items.Clear();
                //    //lstEmail.DataSource = null;
                //    //lstEmail.DataBind();
                //}
                ddlLocation_SelectedIndexChanged(sender, e);

            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        protected void ddlLocation_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dtEmail = new DataTable();
            DataTable dtBuilding = new DataTable();
            try
            {
                objUserBO = new UsersBO();
                objUserBO.UserId = Convert.ToInt32(Session["UserId"]);
                objComBL = new CommonBL();
                objUserBO.location_id = Convert.ToInt32(ddlLocation.SelectedValue);

                objUserBO.location_id = Convert.ToInt32(ddlLocation.SelectedValue);
                dtEmail = objComBL.GetEmailListBL(objUserBO);
                if (dtEmail != null)
                {
                    if (dtEmail.Rows.Count > 0)
                    {
                        Session["Emails"] = dtEmail;
                        //lstEmail.DataSource = dtEmail;
                        //lstEmail.DataTextField = "email_id";
                        //lstEmail.DataValueField = "email_id";
                        //lstEmail.DataBind();
                        //DropDownListdataplaceholder1.DataSource = dtEmail;
                        //DropDownListdataplaceholder1.DataTextField = "email_id";
                        //DropDownListdataplaceholder1.DataValueField = "email_id";
                        //DropDownListdataplaceholder1.DataBind();
                        //DropDownListdataplaceholder1.Items.Insert(0, new ListItem("", "0"));
                    }
                    else
                    {
                        //lstEmail.Items.Clear();
                        //lstEmail.DataSource = null;
                        //lstEmail.DataBind();
                    }
                }
                else
                {
                    //lstEmail.Items.Clear();
                    //lstEmail.DataSource = null;
                    //lstEmail.DataBind();
                }
                dtBuilding = objComBL.GetBuildingDropDownBL(objUserBO);
                if (dtBuilding.Rows.Count > 0)
                {
                    txtBuilding.Text = "";
                    ddlBuilding.DataSource = dtBuilding;
                    ddlBuilding.DataTextField = "building_name";
                    ddlBuilding.DataValueField = "building_id";
                    ddlBuilding.DataBind();
                }
                else
                {
                    txtBuilding.Text = "";
                    ddlBuilding.Items.Clear();
                    ddlBuilding.DataSource = null;
                    ddlBuilding.DataBind();
                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        //protected void ddlLineNumber_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        txtLineNumber.Text = Convert.ToString(ddlLineNumber.SelectedItem);
        //        PopupControlExtender5.Commit(Convert.ToString(ddlLineNumber.SelectedItem));
        //        ddlLineName.SelectedValue = Convert.ToString(ddlLineNumber.SelectedValue);
        //        txtLineName.Text = Convert.ToString(ddlLineName.SelectedItem);
        //    }
        //    catch (Exception ex)
        //    {
        //        objCom.ErrorLog(ex);
        //    }
        //}

        //protected void ddlLineName_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        txtLineName.Text = Convert.ToString(ddlLineName.SelectedItem);
        //        PopupControlExtender5.Commit(Convert.ToString(ddlLineName.SelectedItem));
        //    }
        //    catch (Exception ex)
        //    {
        //        objCom.ErrorLog(ex);
        //    }
        //}

        //protected void lstEmail_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        txtemail.Text = Convert.ToString(lstEmail.SelectedItem);
        //        PopupControlExtender1.Commit(Convert.ToString(lstEmail.SelectedItem));
        //    }
        //    catch (Exception ex)
        //    {
        //        objCom.ErrorLog(ex);
        //    }
        //}

        protected void grdLineProduct_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdLineProduct.PageIndex = e.NewPageIndex;
                grdLineProduct.DataSource = (DataTable)Session["FilteredData"];
                grdLineProduct.DataBind();
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            LocationBO Output = new LocationBO();
            try
            {
                objLocBO = new LocationBO();
                objLocBL = new LocationBL();
                objLocBO.UserId = Convert.ToInt32(Session["UserId"]);
                if (!string.IsNullOrEmpty(Convert.ToString(Session["line_id"])))
                    objLocBO.line_id = Convert.ToInt32(Session["line_id"]);
                objLocBO.region_name = Convert.ToString(ddlRegion.SelectedItem);
                objLocBO.country_name = Convert.ToString(ddlCountry.SelectedItem);
                objLocBO.location_name = Convert.ToString(ddlLocation.SelectedItem);
                //objLocBO.line_number = Convert.ToString(txtLineNumber.Text);
                objLocBO.line_name = Convert.ToString(txtLineName.Text);
                objLocBO.enddate = DateTime.Now;
                Output = objLocBL.DeleteLineDetailsBL(objLocBO);

                if (Output.error_code == 0)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "alert('Line is deleted successfully.');", true);
                    GridBind();
                    DataTable dtGrid = new DataTable();
                    dtGrid = (DataTable)Session["dtLine"];
                    DataRow drfirst = selectedRow(Convert.ToInt32(dtGrid.Rows[0]["line_id"]));
                    LoadViewpanel(drfirst);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "alert('" + Output.error_msg + "');", true);
                }

            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }


        protected void grdLineProduct_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            DataTable dtData;
            DropDownList ddlFilter;
            DataView view;
            DataTable distinctValues;
            try
            {

                if (e.Row.RowType == DataControlRowType.Header)
                {
                    ddlFilter = e.Row.FindControl("ddlRegion_grid") as DropDownList;
                    if (ddlFilter != null)
                    {
                        dtData = new DataTable();
                        if (Session["dtOriginalLine"] != null)
                        {
                            dtData = (DataTable)Session["dtOriginalLine"];
                        }
                        view = new DataView(dtData);
                        distinctValues = view.ToTable(true, "region_name");

                        DataView dv = distinctValues.DefaultView;
                        dv.Sort = "region_name asc";
                        distinctValues = dv.ToTable();

                        ddlFilter.DataSource = distinctValues;
                        ddlFilter.DataTextField = "region_name";
                        ddlFilter.DataValueField = "region_name";
                        ddlFilter.DataBind();
                        ddlFilter.Items.Insert(0, new ListItem("All", "0"));
                        if (Session["ddlSelectedRegion_grid"] != null)
                        {
                            ddlFilter.SelectedValue = Convert.ToString(Session["ddlSelectedRegion_grid"]);
                        }
                    }
                    ddlFilter = e.Row.FindControl("ddlCountry_grid") as DropDownList;
                    if (ddlFilter != null)
                    {
                        dtData = new DataTable();
                        if (Session["FilteredRegionData"] != null)
                        {
                            dtData = (DataTable)Session["FilteredRegionData"];
                        }
                        else if (Session["dtOriginalLine"] != null)
                        {
                            dtData = (DataTable)Session["dtOriginalLine"];
                        }
                        view = new DataView(dtData);
                        distinctValues = view.ToTable(true, "country_name");

                        DataView dv = distinctValues.DefaultView;
                        dv.Sort = "country_name asc";
                        distinctValues = dv.ToTable();

                        ddlFilter.DataSource = distinctValues;
                        ddlFilter.DataTextField = "country_name";
                        ddlFilter.DataValueField = "country_name";
                        ddlFilter.DataBind();
                        ddlFilter.Items.Insert(0, new ListItem("All", "0"));
                        if (Session["ddlSelectedCountry_grid"] != null)
                        {
                            ddlFilter.SelectedValue = Convert.ToString(Session["ddlSelectedCountry_grid"]);
                        }
                        if (ddlFilter.SelectedValue == "0" || ddlFilter.SelectedValue == "")
                        {
                            Session["FilteredCountryData"] = Session["FilteredRegionData"];
                        }
                    }
                    ddlFilter = e.Row.FindControl("ddlLocation_grid") as DropDownList;
                    if (ddlFilter != null)
                    {
                        dtData = new DataTable();
                        if (Session["FilteredCountryData"] != null)
                        {
                            dtData = (DataTable)Session["FilteredCountryData"];
                        }
                        else if (Session["FilteredRegionData"] != null)
                        {
                            dtData = (DataTable)Session["FilteredRegionData"];
                        }
                        else if (Session["dtOriginalLine"] != null)
                        {
                            dtData = (DataTable)Session["dtOriginalLine"];
                        }
                        view = new DataView(dtData);
                        distinctValues = view.ToTable(true, "location_name");

                        DataView dv = distinctValues.DefaultView;
                        dv.Sort = "location_name asc";
                        distinctValues = dv.ToTable();

                        ddlFilter.DataSource = distinctValues;
                        ddlFilter.DataTextField = "location_name";
                        ddlFilter.DataValueField = "location_name";
                        ddlFilter.DataBind();
                        ddlFilter.Items.Insert(0, new ListItem("All", "0"));
                        if (Session["ddlSelectedLocation_grid"] != null)
                        {
                            ddlFilter.SelectedValue = Convert.ToString(Session["ddlSelectedLocation_grid"]);
                        }
                        if (ddlFilter.SelectedValue == "0" || ddlFilter.SelectedValue == "")
                        {
                            Session["FilteredLocationData"] = Session["FilteredCountryData"];
                        }
                    }
                    ddlFilter = e.Row.FindControl("ddlBuilding_grid") as DropDownList;
                    if (ddlFilter != null)
                    {
                        dtData = new DataTable();
                        if (Session["FilteredLocationData"] != null)
                        {
                            dtData = (DataTable)Session["FilteredLocationData"];
                        }
                        else if (Session["FilteredCountryData"] != null)
                        {
                            dtData = (DataTable)Session["FilteredCountryData"];
                        }
                        else if (Session["FilteredRegionData"] != null)
                        {
                            dtData = (DataTable)Session["FilteredRegionData"];
                        }
                        else if (Session["dtOriginalLine"] != null)
                        {
                            dtData = (DataTable)Session["dtOriginalLine"];
                        }
                        view = new DataView(dtData);
                        distinctValues = view.ToTable(true, "building_name");

                        DataView dv = distinctValues.DefaultView;
                        dv.Sort = "building_name asc";
                        distinctValues = dv.ToTable();

                        ddlFilter.DataSource = distinctValues;
                        ddlFilter.DataTextField = "building_name";
                        ddlFilter.DataValueField = "building_name";
                        ddlFilter.DataBind();
                        ddlFilter.Items.Insert(0, new ListItem("All", "0"));
                        if (Session["ddlSelectedBuilding_grid"] != null)
                        {
                            ddlFilter.SelectedValue = Convert.ToString(Session["ddlSelectedBuilding_grid"]);
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        protected void ddlRegion_grid_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dtData;
            DataTable dtFilterData = new DataTable();
            try
            {
                dtData = new DataTable();
                if (Session["dtLine"] != null)
                {
                    dtData = (DataTable)Session["dtLine"];
                }
                DropDownList ddlCountry = (DropDownList)sender;
                Session["ddlSelectedRegion_grid"] = Convert.ToString(ddlCountry.SelectedValue);
                Session["ddlSelectedLocation_grid"] = "All";
                Session["ddlSelectedCountry_grid"] = "All";
                if (Convert.ToString(ddlCountry.SelectedValue) == "0")
                {
                    Session["FilteredData"] = dtData;
                    Session["FilteredRegionData"] = dtData;
                    grdLineProduct.DataSource = dtData;
                    grdLineProduct.DataBind();
                }
                else
                {
                    var rows = from row in dtData.AsEnumerable()
                               where row.Field<string>("region_name") == Convert.ToString(ddlCountry.SelectedValue)
                               select row;
                    DataRow[] rowsArray = rows.ToArray();
                    dtFilterData = rows.CopyToDataTable();
                    Session["FilteredData"] = dtFilterData;
                    Session["FilteredRegionData"] = dtFilterData;
                    grdLineProduct.DataSource = dtFilterData;
                    grdLineProduct.DataBind();
                }


                DataTable dtGrid = new DataTable();
                dtGrid = (DataTable)Session["FilteredData"];
                DataRow drfirst = selectedRow(Convert.ToInt32(dtGrid.Rows[0]["line_id"]));
                LoadViewpanel(drfirst);

            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        protected void ddlLocation_grid_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dtData;
            DataTable dtFilterData = new DataTable();
            try
            {
                dtData = new DataTable();

                if (Session["FilteredCountryData"] != null)
                {
                    dtData = (DataTable)Session["FilteredCountryData"];
                }
                else if (Session["FilteredRegionData"] != null)
                {
                    dtData = (DataTable)Session["FilteredRegionData"];
                }
                else if (Session["dtLine"] != null)
                {
                    dtData = (DataTable)Session["dtLine"];
                }
                DropDownList ddlLocation = (DropDownList)sender;
                Session["ddlSelectedLocation_grid"] = Convert.ToString(ddlLocation.SelectedValue);
                if (Convert.ToString(ddlLocation.SelectedValue) == "0")
                {
                    Session["FilteredData"] = dtData;
                    Session["FilteredLocationData"] = dtData;
                    grdLineProduct.DataSource = dtData;
                    grdLineProduct.DataBind();
                }
                else
                {
                    var rows = from row in dtData.AsEnumerable()
                               where row.Field<string>("location_name") == Convert.ToString(ddlLocation.SelectedValue)
                               select row;
                    DataRow[] rowsArray = rows.ToArray();
                    dtFilterData = rows.CopyToDataTable();
                    Session["FilteredData"] = dtFilterData;
                    Session["FilteredLocationData"] = dtFilterData;
                    grdLineProduct.DataSource = dtFilterData;
                    grdLineProduct.DataBind();
                }
                DataTable dtGrid = new DataTable();
                dtGrid = (DataTable)Session["FilteredData"];
                DataRow drfirst = selectedRow(Convert.ToInt32(dtGrid.Rows[0]["line_id"]));
                LoadViewpanel(drfirst);

            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        protected void ddlCountry_grid_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dtData;
            DataTable dtFilterData = new DataTable();
            try
            {
                dtData = new DataTable();

                if (Session["FilteredRegionData"] != null)
                {
                    dtData = (DataTable)Session["FilteredRegionData"];
                }
                else if (Session["dtLine"] != null)
                {
                    dtData = (DataTable)Session["dtLine"];
                }
                DropDownList ddlCountry = (DropDownList)sender;
                Session["ddlSelectedCountry_grid"] = Convert.ToString(ddlCountry.SelectedValue);
                Session["ddlSelectedLocation_grid"] = "All";
                if (Convert.ToString(ddlCountry.SelectedValue) == "0")
                {
                    Session["FilteredData"] = dtData;
                    Session["FilteredCountryData"] = dtData;
                    grdLineProduct.DataSource = dtData;
                    grdLineProduct.DataBind();
                }
                else
                {
                    var rows = from row in dtData.AsEnumerable()
                               where row.Field<string>("country_name") == Convert.ToString(ddlCountry.SelectedValue)
                               select row;
                    DataRow[] rowsArray = rows.ToArray();
                    dtFilterData = rows.CopyToDataTable();
                    Session["FilteredData"] = dtFilterData;
                    Session["FilteredCountryData"] = dtFilterData;
                    grdLineProduct.DataSource = dtFilterData;
                    grdLineProduct.DataBind();
                }

                DataTable dtGrid = new DataTable();
                dtGrid = (DataTable)Session["FilteredData"];
                DataRow drfirst = selectedRow(Convert.ToInt32(dtGrid.Rows[0]["line_id"]));
                LoadViewpanel(drfirst);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }


        protected void ddlBuilding_grid_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dtData;
            DataTable dtFilterData = new DataTable();
            try
            {
                dtData = new DataTable();

                if (Session["FilteredLocationData"] != null)
                {
                    dtData = (DataTable)Session["FilteredLocationData"];
                }
                else if (Session["FilteredCountryData"] != null)
                {
                    dtData = (DataTable)Session["FilteredCountryData"];
                }
                else if (Session["FilteredRegionData"] != null)
                {
                    dtData = (DataTable)Session["FilteredRegionData"];
                }
                else if (Session["dtLine"] != null)
                {
                    dtData = (DataTable)Session["dtLine"];
                }
                DropDownList ddlBuilding = (DropDownList)sender;
                Session["ddlSelectedBuilding_grid"] = Convert.ToString(ddlBuilding.SelectedValue);
                if (Convert.ToString(ddlBuilding.SelectedValue) == "0")
                {
                    Session["FilteredData"] = dtData;
                    grdLineProduct.DataSource = dtData;
                    grdLineProduct.DataBind();
                }
                else
                {
                    var rows = from row in dtData.AsEnumerable()
                               where row.Field<string>("building_name") == Convert.ToString(ddlBuilding.SelectedValue)
                               select row;
                    DataRow[] rowsArray = rows.ToArray();
                    dtFilterData = rows.CopyToDataTable();
                    Session["FilteredData"] = dtFilterData;
                    grdLineProduct.DataSource = dtFilterData;
                    grdLineProduct.DataBind();
                }
                DataTable dtGrid = new DataTable();
                dtGrid = (DataTable)Session["FilteredData"];
                DataRow drfirst = selectedRow(Convert.ToInt32(dtGrid.Rows[0]["line_id"]));
                LoadViewpanel(drfirst);

            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        protected void ddlBuilding_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtBuilding.Text = Convert.ToString(ddlBuilding.SelectedItem);
            PopupControlExtender2.Commit(Convert.ToString(ddlBuilding.SelectedItem));
            PopupControlExtender p = PopupControlExtender.GetProxyForCurrentPopup(this);
            p.Cancel();
        }

        /// <summary>
        /// Author: Anamika(KNS)
        /// Date : Feb 26,2018
        /// Desc : Event for loading inactive records also to the table 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void chkInActive_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (chkInActive.Checked)
                {
                    inactive_flag = 1;
                }
                else
                {
                    inactive_flag = 0;
                }
                GridBind();
                DataTable dtGrid = new DataTable();
                dtGrid = (DataTable)Session["dtLine"];
                int vwline_id = Convert.ToInt32(dtGrid.Rows[0]["line_id"]);
                Session["line_id"] = vwline_id;
                DataRow drfirst = selectedRow(vwline_id);
                LoadViewpanel(drfirst);
                Session["PrintFlag"] = 0;
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }
        #endregion

        #region Methods

        private void GetMasterDropDown()
        {
            DataTable dtEmail = new DataTable();
            try
            {
                dsDropDownData = new DataSet();
                objUserBO = new UsersBO();
                objUserBO.UserId = Convert.ToInt32(Session["UserId"]);
                objComBL = new CommonBL();
                dsDropDownData = objComBL.GetAllMasterBL(objUserBO);
                if (dsDropDownData.Tables.Count > 0)
                {
                    if (dsDropDownData.Tables[0].Rows.Count > 0)
                    {
                        ddlRegion.DataSource = dsDropDownData.Tables[0];
                        ddlRegion.DataTextField = "region_name";
                        ddlRegion.DataValueField = "region_id";
                        ddlRegion.DataBind();
                        ddlRegion.Items.Insert(0, new ListItem("", "0"));
                        if (Convert.ToString(Session["SiteAdminFlag"]) == "Y" && Convert.ToString(Session["GlobalAdminFlag"]) == "N")
                        {
                            ddlRegion.SelectedValue = Convert.ToString(Session["LoggedInRegionId"]);
                            ddlRegion.Attributes["disabled"] = "disabled";
                        }
                        else
                            ddlRegion.Enabled = true;
                    }
                    else
                    {
                        ddlRegion.DataSource = null;
                        ddlRegion.DataBind();
                    }
                    if (dsDropDownData.Tables[1].Rows.Count > 0)
                    {
                        ddlCountry.DataSource = dsDropDownData.Tables[1];
                        ddlCountry.DataTextField = "country_name";
                        ddlCountry.DataValueField = "country_id";
                        ddlCountry.DataBind();

                        ddlCountry.Items.Insert(0, new ListItem("", "0"));
                        if (Convert.ToString(Session["SiteAdminFlag"]) == "Y" && Convert.ToString(Session["GlobalAdminFlag"]) == "N")
                        {
                            ddlCountry.SelectedValue = Convert.ToString(Session["LoggedInCountryId"]);
                            ddlCountry.Attributes["disabled"] = "disabled";
                        }
                        else
                            ddlCountry.Enabled = true;
                    }
                    else
                    {
                        ddlCountry.DataSource = null;
                        ddlCountry.DataBind();
                    }
                    if (dsDropDownData.Tables[2].Rows.Count > 0)
                    {
                        ddlLocation.DataSource = dsDropDownData.Tables[2];
                        ddlLocation.DataTextField = "location_name";
                        ddlLocation.DataValueField = "location_id";
                        ddlLocation.DataBind();
                        ddlLocation.Items.Insert(0, new ListItem("", "0"));
                        if (Convert.ToString(Session["SiteAdminFlag"]) == "Y" && Convert.ToString(Session["GlobalAdminFlag"]) == "N")
                        {
                            ddlLocation.SelectedValue = Convert.ToString(Session["LocationId"]);
                            ddlLocation.Attributes["disabled"] = "disabled";
                        }
                        else
                            ddlLocation.Enabled = true;
                    }
                    else
                    {
                        ddlLocation.DataSource = null;
                        ddlLocation.DataBind();
                    }
                    if (Convert.ToString(Session["SiteAdminFlag"]) == "Y" && Convert.ToString(Session["GlobalAdminFlag"]) == "N")
                    {
                        ddlLocation_SelectedIndexChanged(null, null);
                    }
                    objUserBO.location_id = Convert.ToInt32(ddlLocation.SelectedValue);
                    dtEmail = objComBL.GetEmailListBL(objUserBO);
                    if (dtEmail != null)
                    {
                        if (dtEmail.Rows.Count > 0)
                        {
                            Session["Emails"] = dtEmail;
                            //lstEmail.DataSource = dtEmail;
                            //lstEmail.DataTextField = "email_id";
                            //lstEmail.DataValueField = "email_id";
                            //lstEmail.DataBind();
                            //DropDownListdataplaceholder1.DataSource = dtEmail;
                            //DropDownListdataplaceholder1.DataTextField = "email_id";
                            //DropDownListdataplaceholder1.DataValueField = "email_id";
                            //DropDownListdataplaceholder1.DataBind();
                            //DropDownListdataplaceholder1.Items.Insert(0, new ListItem("", "0"));
                            //CreateJsonFile(dtEmail);
                        }

                        else
                        {
                            //lstEmail.Items.Clear();
                            //lstEmail.DataSource = null;
                            //lstEmail.DataBind();
                        }
                    }
                    else
                    {
                        //lstEmail.Items.Clear();
                        //lstEmail.DataSource = null;
                        //lstEmail.DataBind();
                    }
                    //if (dsDropDownData.Tables[3].Rows.Count > 0)
                    //{
                    //    ddlLineNumber.DataSource = dsDropDownData.Tables[3];
                    //    ddlLineNumber.DataTextField = "line_code";
                    //    ddlLineNumber.DataValueField = "line_id";
                    //    ddlLineNumber.DataBind();
                    //    ddlLineNumber.SelectedIndex = 0;
                    //}
                    //else
                    //{
                    //    ddlLineNumber.DataSource = null;
                    //    ddlLineNumber.DataBind();
                    //}
                    //if (dsDropDownData.Tables[3].Rows.Count > 0)
                    //{
                    //    ddlLineName.DataSource = dsDropDownData.Tables[3];
                    //    ddlLineName.DataTextField = "line_name";
                    //    ddlLineName.DataValueField = "line_id";
                    //    ddlLineName.DataBind();
                    //    //ddlLineName.SelectedValue = ddlLineNumber.SelectedValue;
                    //}
                    //else
                    //{
                    //    ddlLineName.DataSource = null;
                    //    ddlLineName.DataBind();
                    //}

                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        private void GridBind()
        {
            DataTable dtResult;
            try
            {
                objLocBO = new LocationBO();
                objLocBL = new LocationBL();
                dtResult = new DataTable();
                objLocBO.UserId = Convert.ToInt32(Session["UserId"]);
                objLocBO.Inactive_flag = inactive_flag;
                dtResult = objLocBL.GetLineDetailsBL(objLocBO);
                if (dtResult.Rows.Count > 0)
                {
                    grdLineProduct.DataSource = dtResult;
                    Session["dtLine"] = dtResult;
                    Session["dtOriginalLine"] = dtResult;
                    Session["ddlSelectedRegion_grid"] = null;
                    Session["ddlSelectedCountry_grid"] = null;
                    Session["ddlSelectedLocation_grid"] = null;
                    Session["FilteredData"] = dtResult;
                    Session["FilteredRegionData"] = null;
                    Session["FilteredCountryData"] = null;
                }
                else
                {
                    grdLineProduct.DataSource = null;
                }
                grdLineProduct.DataBind();
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        private void LoadViewpanel(DataRow drselect)
        {

            try
            {
                lblregion.Text = Convert.ToString(drselect["region_name"]);
                lblCountry.Text = Convert.ToString(drselect["country_name"]);
                lblLocation.Text = Convert.ToString(drselect["location_name"]);
                //lblLineNumber.Text = Convert.ToString(drselect["line_code"]);
                lblLineName.Text = Convert.ToString(drselect["line_name"]);
                lblMailList.Text = Convert.ToString(drselect["distribution_list"]);

                //lnlprintregion.Text = Convert.ToString(drselect["region_name"]);
                //lblprintCountry.Text = Convert.ToString(drselect["country_name"]);
                //lblprintLocation.Text = Convert.ToString(drselect["location_name"]);
                //lblPrintLineName.Text = Convert.ToString(drselect["line_name"]);
                //lblPrintMailList.Text = Convert.ToString(drselect["distribution_list"]);
                // lblPrintLineNumber.Text = Convert.ToString(drselect["line_code"]);
                viewpanel.Visible = true;
                editpanel.Visible = false;
                pnlQRCode.Controls.Add(QRCodeGeneration(Convert.ToString(drselect["line_id"]), 150, 150));


                QRCodeGenerationForPrint(Convert.ToString(drselect["line_id"]), Convert.ToString(drselect["line_name"]));
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        private void QRCodeGenerationForPrint(string line_id, string line_name)
        {
            string code;
            QRCodeGenerator qrGenerator;
            //QRCodeGenerator.
            QRCode qrCode;
            System.Web.UI.WebControls.Image imgBarCode = new System.Web.UI.WebControls.Image();
            System.Web.UI.WebControls.Image imgBarCode1 = (System.Web.UI.WebControls.Image)pnlQRCode.FindControl("image");
            if (imgBarCode1 != null)
            {
                imgBarCode1.ID = "";
            }
            System.Web.UI.WebControls.Image imgBarCode2 = (System.Web.UI.WebControls.Image)pnlprintQRCode.FindControl("image");
            if (imgBarCode2 != null)
            {
                imgBarCode2.ID = "";
            }
            try
            {

                Panel divPrintQR1 = new Panel();
                divPrintQR1.CssClass = "colQR1";
                code = Convert.ToString(line_id);
                System.Web.UI.WebControls.Image imgBarCodePrint = new System.Web.UI.WebControls.Image();
                qrGenerator = new QRCodeGenerator();
                //qrCode = qrGenerator.CreateQrCode(code, QRCodeGenerator.ECCLevel.Q);
                qrCode = new QRCode(qrGenerator.CreateQrCode(code, QRCodeGenerator.ECCLevel.Q));

                imgBarCodePrint.Height = 138;
                imgBarCodePrint.Width = 138;
                imgBarCodePrint.ID = "imagePrint";

                Label lbl_printtitle = new Label();
                lbl_printtitle.Text = "Layered Process Audit";
                lbl_printtitle.CssClass = "qr-title";
                divPrintQR1.Controls.Add(lbl_printtitle);
                Label lbl_printtitle1 = new Label();
                lbl_printtitle1.Text = "QR Code";
                lbl_printtitle1.CssClass = "qr-title1";
                divPrintQR1.Controls.Add(lbl_printtitle1);
                divPrintQR1.Controls.Add(new LiteralControl("<br/>"));


                Label lbl_printloc = new Label();
                lbl_printloc.Text = Convert.ToString(ddlLocation.SelectedItem);
                lbl_printloc.CssClass = "mn_location_span";
                divPrintQR1.Controls.Add(lbl_printloc);
                divPrintQR1.Controls.Add(imgBarCodePrint);

                Label lbl_printline = new Label();
                lbl_printline.Text = line_name;
                lbl_printline.CssClass = "feedback-title";
                divPrintQR1.Controls.Add(lbl_printline);

                using (Bitmap bitMap = qrCode.GetGraphic(20))
                {
                    using (MemoryStream ms = new MemoryStream())
                    {
                        bitMap.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
                        byte[] byteImage = ms.ToArray();
                        imgBarCodePrint.ImageUrl = "data:image/png;base64," + Convert.ToBase64String(byteImage);
                    }
                }

                divPrintQR1.Controls.Add(new LiteralControl("<img style=\"width: 60px;float:right; margin: -40px 0 0 0;\" src=\"images/logo-neu_1.png\" />"));

                pnlprintQRCode.Controls.Add(divPrintQR1);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        private Control QRCodeGeneration(string p, int height, int width)
        {
            string code = p;
            System.Web.UI.WebControls.Image imgBarCode = new System.Web.UI.WebControls.Image();
            System.Web.UI.WebControls.Image imgBarCode1 = (System.Web.UI.WebControls.Image)pnlQRCode.FindControl("image");
            if (imgBarCode1 != null)
            {
                imgBarCode1.ID = "";
            }
            System.Web.UI.WebControls.Image imgBarCode2 = (System.Web.UI.WebControls.Image)pnlprintQRCode.FindControl("image");
            if (imgBarCode2 != null)
            {
                imgBarCode2.ID = "";
            }
            try
            {
                QRCodeGenerator qrGenerator = new QRCodeGenerator();
                QRCodeData qrCodeData = qrGenerator.CreateQrCode(code, QRCodeGenerator.ECCLevel.Q);
                QRCode qrCode = new QRCode(qrCodeData);

                //QRCodeGenerator qrGenerator = new QRCodeGenerator();
                //QRCodeGenerator.QRCode qrCode = qrGenerator.CreateQrCode(code, QRCodeGenerator.ECCLevel.Q);
                imgBarCode.Height = height;
                imgBarCode.Width = width;
                imgBarCode.ID = "image";
                using (Bitmap bitMap = qrCode.GetGraphic(20))
                {
                    using (MemoryStream ms = new MemoryStream())
                    {
                        bitMap.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
                        byte[] byteImage = ms.ToArray();
                        imgBarCode.ImageUrl = "data:image/png;base64," + Convert.ToBase64String(byteImage);
                    }
                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            return imgBarCode;
        }

        private DataRow selectedRow(int line_id)
        {
            DataTable dtGrid = (DataTable)Session["dtLine"];
            DataRow drselect = (from DataRow dr in dtGrid.Rows
                                where (int)dr["line_id"] == line_id
                                select dr).FirstOrDefault();

            return drselect;
        }

        private void LoadEditPanel(DataRow drselect)
        {
            DataTable dtBuilding = new DataTable();
            try
            {
                btnAdd_Click(null, null);
                Session["line_id"] = Convert.ToString(drselect["line_id"]);
                ddlRegion.SelectedValue = Convert.ToString(drselect["region_id"]);
                ddlCountry.SelectedValue = Convert.ToString(drselect["country_id"]);
                ddlLocation.SelectedValue = Convert.ToString(drselect["location_id"]);
                txtStartDate.Text = Convert.ToString(drselect["start_date"]);
                txtEndDate.Text = Convert.ToString(drselect["end_date"]);
                ddlLocation_SelectedIndexChanged(null, null);
                txtLineName.Text = Convert.ToString(drselect["line_name"]);
                txtList.Text = Convert.ToString(drselect["distribution_list"]);
                dtBuilding = objComBL.GetBuildingDropDownBL(objUserBO);
                //if (dtBuilding.Rows.Count > 0)
                //{
                //    txtBuilding.Text = "";
                //    ddlBuilding.DataSource = dtBuilding;
                //    ddlBuilding.DataTextField = "building_name";
                //    ddlBuilding.DataValueField = "building_id";
                //    ddlBuilding.DataBind();
                //}
                //else
                //{
                //    txtBuilding.Text = "";
                //    ddlBuilding.Items.Clear();
                //    ddlBuilding.DataSource = null;
                //    ddlBuilding.DataBind();
                //}
                txtBuilding.Text = Convert.ToString(drselect["building_name"]);
                ddlBuilding.SelectedValue = Convert.ToString(drselect["building_id"]);

                //btnDelete.Visible = true;
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }



        private void Clear()
        {
            txtLineName.Text = "";
            //txtLineNumber.Text = "";
            //DropDownListdataplaceholder1.Items.Clear();
            Session["line_id"] = "";
            //btnDelete.Visible = false;
            //txtemail.Text = "";
            txtList.Text = "";
            txtStartDate.Text = "";
            txtEndDate.Text = "";
        }

        [WebMethod]
        public static List<string> GetDistributionEmails(string Name)
        {
            List<string> empResult = new List<string>();
            DataTable dtEmails = new DataTable();
            dtEmails = (DataTable)HttpContext.Current.Session["Emails"];
            if (dtEmails.Rows.Count > 0)
            {
                DataRow[] dr = dtEmails.Select("email_id like '" + Name + "%'");
                foreach (DataRow dr1 in dr)
                {
                    empResult.Add(Convert.ToString(dr1["email_id"]));
                }
            }
            return empResult;
            //return Convert.ToString(HttpContext.Current.Session["json"]);
        }


        #endregion

        #region NOTINUSE
        private void SendMails(LocationBO objLocBO)
        {
            EmailDetails objEmail;
            try
            {
                objEmail = new EmailDetails();
                objEmail.toMailId = objLocBO.mailIds;
                if (!string.IsNullOrEmpty(Convert.ToString(objLocBO.line_id)) && objLocBO.line_id != 0)
                {
                    objEmail.subject = "New Line : " + objLocBO.line_name;
                    objEmail.body = "New Line : " + objLocBO.line_name + " is created for location " + objLocBO.location_name;
                }
                else
                {
                    objEmail.subject = "Update for : " + objLocBO.line_name;
                    objEmail.body = "Line : " + objLocBO.line_name + " is updated for location " + objLocBO.location_name;
                }
                objCom.SendMail(objEmail);
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        private void PrepareControlForExport(Control control)
        {
            try
            {
                for (int i = 0; i < control.Controls.Count; i++)
                {
                    Control current = control.Controls[i];
                    if (current is LinkButton)
                    {
                        control.Controls.Remove(current);
                        control.Controls.AddAt(i, new LiteralControl((current as LinkButton).Text));
                    }
                    else if (current is ImageButton)
                    {
                        control.Controls.Remove(current);
                        control.Controls.AddAt(i, new LiteralControl((current as ImageButton).AlternateText));
                    }
                    else if (current is HyperLink)
                    {
                        control.Controls.Remove(current);
                        control.Controls.AddAt(i, new LiteralControl((current as HyperLink).Text));
                    }

                    else if (current is TextBox)
                    {
                        control.Controls.Remove(current);
                        control.Controls.AddAt(i, new LiteralControl((current as TextBox).Text));
                    }
                    else if (current is DropDownList)
                    {
                        control.Controls.Remove(current);
                        //control.Controls.AddAt(i, new LiteralControl((current as DropDownList).SelectedItem.Text));
                    }
                    else if (current is HiddenField)
                    {
                        control.Controls.Remove(current);
                    }
                    else if (current is CheckBox)
                    {
                        control.Controls.Remove(current);
                        // control.Controls.AddAt(i, new LiteralControl((current as CheckBox).Checked ? "True" : "False"));
                    }

                    else if (current is System.Web.UI.WebControls.Image)
                    {
                        control.Controls.Remove(current);
                        // control.Controls.AddAt(i, new LiteralControl((current as CheckBox).Checked ? "True" : "False"));
                    }

                    else if (current is Button)
                    {
                        control.Controls.Remove(current);
                        // control.Controls.AddAt(i, new LiteralControl((current as CheckBox).Checked ? "True" : "False"));
                    }
                    if (current.HasControls())
                    {
                        PrepareControlForExport(current);
                    }
                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }
        [System.Web.Services.WebMethod]
        public static string SetDownloadPath(string strpath)
        {
            Page objp = new Page();
            objp.Session["Print"] = strpath;
            return strpath;
        }

        private Control QRCodeGenerationWithText(string q, string p, int height, int width)
        {
            string code = p;
            System.Web.UI.WebControls.Image imgBarCode = new System.Web.UI.WebControls.Image();
            System.Web.UI.WebControls.Image imgBarCode1 = (System.Web.UI.WebControls.Image)pnlQRCode.FindControl("image");
            if (imgBarCode1 != null)
            {
                imgBarCode1.ID = "";
            }
            System.Web.UI.WebControls.Image imgBarCode2 = (System.Web.UI.WebControls.Image)pnlprintQRCode.FindControl("image");
            if (imgBarCode2 != null)
            {
                imgBarCode2.ID = "";
            }
            try
            {
                QRCodeGenerator qrGenerator = new QRCodeGenerator();
                QRCodeData qrCodeData = qrGenerator.CreateQrCode(code, QRCodeGenerator.ECCLevel.Q);
                QRCode qrCode = new QRCode(qrCodeData);
                //QRCodeGenerator qrGenerator = new QRCodeGenerator();
                //QRCodeGenerator.QRCode qrCode = qrGenerator.CreateQrCode(code, QRCodeGenerator.ECCLevel.Q);
                imgBarCode.Height = height;
                imgBarCode.Width = width;
                imgBarCode.ID = "image";
                Label l = new Label();
                l.Text = q;
                imgBarCode.Controls.Add(l);
                using (Bitmap bitMap = qrCode.GetGraphic(20))
                {
                    using (MemoryStream ms = new MemoryStream())
                    {
                        bitMap.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
                        byte[] byteImage = ms.ToArray();
                        imgBarCode.ImageUrl = "data:image/png;base64," + Convert.ToBase64String(byteImage);
                    }
                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            return imgBarCode;
        }


        #endregion



    }
}