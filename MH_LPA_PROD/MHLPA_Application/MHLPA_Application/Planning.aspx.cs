﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;
using MHLPA_BusinessLogic;
using MHLPA_BusinessObject;

namespace MHLPA_Application
{
    public partial class Planning : System.Web.UI.Page
    {
        #region Global Declaration
        UsersBO objUserBO;
        CommonBL objComBL;
        DataSet dsDropDownData;
        PlanBO objPlanBO;
        CommonFunctions objCom = new CommonFunctions();
        PlanBL objPlanBL;
        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(Convert.ToString(Session["UserId"]))) { Response.Redirect("Login.aspx"); return; }

            if (!IsPostBack)
            {
                try
                {
                    if (!string.IsNullOrEmpty(Convert.ToString(Session["UserId"])))
                        GetMasterDropdown();
                }
                catch (Exception ex)
                {
                    objCom.ErrorLog(ex);
                }
            }

        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                objPlanBO = new PlanBO();
                objPlanBL = new PlanBL();
                if (!string.IsNullOrEmpty(hdnPlanId.Value))
                    objPlanBO.p_audit_plan_id = Convert.ToInt32(hdnPlanId.Value);

                string UIpattern = "dd-MM-yyyy";
                string DBPattern = "MM-dd-yyyy";
                DateTime parsedDate;
                if (DateTime.TryParseExact(txtPlannedStartDate.Text, UIpattern, null, DateTimeStyles.None, out parsedDate))
                {
                    string plannedstartdate = parsedDate.ToString(DBPattern);
                   // objPlanBO.p_planned_start_date = Convert.ToDateTime(plannedstartdate);
                    //objPlanBO.p_planned_start_date = DateTime.Parse(plannedstartdate);
                    objPlanBO.p_planned_start_date = DateTime.ParseExact(plannedstartdate, "MM-dd-yyyy", null);
                    //objPlanBO.p_planned_start_date = DateTime.ParseExact(plannedstartdate, "MM-dd-yyyy", System.Globalization.CultureInfo.InvariantCulture);

                }
                if (DateTime.TryParseExact(txtPlannedEndDate.Text, UIpattern, null, DateTimeStyles.None, out parsedDate))
                {
                    string plannedenddate = parsedDate.ToString(DBPattern);
                   // objPlanBO.p_planned_end_date = Convert.ToDateTime(plannedenddate);
                    //objPlanBO.p_planned_end_date = DateTime.Parse(plannedenddate);
                    objPlanBO.p_planned_end_date = DateTime.ParseExact(plannedenddate, "MM-dd-yyyy", null);
                    //objPlanBO.p_planned_end_date = DateTime.ParseExact(plannedenddate, "MM-dd-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                }
                //objPlanBO.p_shift_no = Convert.ToInt32(ddlShift.SelectedValue);
                //if (rdbOwnerType.Items[0].Selected)
                    objPlanBO.p_user_id = Convert.ToInt32(ddlusers.SelectedValue);
                //else if (rdbOwnerType.Items[1].Selected)
                //    objPlanBO.p_group_id = Convert.ToInt32(ddlGroup.SelectedValue);
                objPlanBO.p_line_id = Convert.ToInt32(ddlLineNumber.SelectedValue);
                //objPlanBO.p_product_id = Convert.ToInt32(ddlProduct.SelectedValue);
                objPlanBO = objPlanBL.SavePlanningBL(objPlanBO);
                if (objPlanBO.error_code == 0)
                {
                    string msg = objPlanBO.error_msg;
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ShowPopup", "alert('" + msg + "'); location.reload();", true);
                   
                    SendInvitation(objPlanBO);
                    //Page_Load(null, null);
                    
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ShowPopup", "alert('" + objPlanBO.error_msg + "');", true);
                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        protected void ddlLocation_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dtLocation = new DataTable();
            DataTable dtLine = new DataTable();
            DataTable dtPart = new DataTable();
            DataTable dtUsers = new DataTable();
            try
            {
                objUserBO = new UsersBO();
                objComBL = new CommonBL();
                Session["SelectedLocation"] = Convert.ToString(ddlLocation.SelectedValue);
                dtLocation = (DataTable)Session["dtLocation"];
                objUserBO.location_id = Convert.ToInt32(ddlLocation.SelectedValue);
                //DataRow drselect = (from DataRow dr in dtLocation.Rows
                //                    where (int)dr["location_id"] == objUserBO.location_id
                //                    select dr).FirstOrDefault();
                //ddlShift.SelectedValue = Convert.ToString(drselect["Shift_no"]);
                dtUsers = objComBL.GetEmployeesBasedOnLocationBL(objUserBO);
                if (dtLocation.Rows.Count > 0)
                {
                    ddlusers.DataSource = dtUsers;
                    ddlusers.DataTextField = "emp_full_name";
                    ddlusers.DataValueField = "user_id";
                    ddlusers.DataBind();
                    Session["SelectedUser"] = Convert.ToString(ddlusers.SelectedItem);
                }
                else
                {
                    ddlusers.Items.Clear();
                    ddlusers.DataSource = null;
                    ddlusers.DataBind();
                }
                dtLine = objComBL.GetLineListDropDownBL(objUserBO);
                if (dtLine.Rows.Count > 0)
                {
                    ddlLineNumber.DataSource = dtLine;
                    ddlLineNumber.DataTextField = "line_name";
                    ddlLineNumber.DataValueField = "line_id";
                    ddlLineNumber.DataBind();
                    Session["dtLine"] = dtLine;
                }
                else
                {
                    ddlLineNumber.Items.Clear();
                    ddlLineNumber.DataSource = null;
                    ddlLineNumber.DataBind();
                }
                //objUserBO.line_id = Convert.ToInt32(ddlLineNumber.SelectedValue);
                //dtPart = objComBL.GetPartBasedOnLineBL(objUserBO);
                //if (dtPart.Rows.Count > 0)
                //{
                //    ddlProduct.DataSource = dtPart;
                //    ddlProduct.DataTextField = "Part_code";
                //    ddlProduct.DataValueField = "Part_id";
                //    ddlProduct.DataBind();
                //    Session["dtPart"] = dtPart;
                //}
                //else
                //{
                //    ddlProduct.Items.Clear();
                //    ddlProduct.DataSource = null;
                //    ddlProduct.DataBind();
                //}
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        protected void ddlRegion_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dtCountry = new DataTable();
            DataTable dtLocation = new DataTable();
            try
            {
                dsDropDownData = new DataSet();
                objUserBO = new UsersBO();
                objUserBO.UserId = Convert.ToInt32(Session["UserId"]);
                objComBL = new CommonBL();

                objUserBO.region_id = Convert.ToInt32(ddlRegion.SelectedValue);
                dtCountry = objComBL.GetCountryBasedOnRegionBL(objUserBO);

                if (dtCountry.Rows.Count > 0)
                {
                    ddlCountry.DataSource = dtCountry;
                    ddlCountry.DataTextField = "country_name";
                    ddlCountry.DataValueField = "country_id";
                    ddlCountry.DataBind();
                    objUserBO.country_id = Convert.ToInt32(ddlCountry.SelectedValue);
                }
                else
                {
                    ddlCountry.Items.Clear();
                    ddlCountry.DataSource = null;
                    ddlCountry.DataBind();
                }

                dtLocation = objComBL.GetLocationBasedOnCountryBL(objUserBO);
                if (dtLocation.Rows.Count > 0)
                {
                    ddlLocation.DataSource = dtLocation;
                    ddlLocation.DataTextField = "location_name";
                    ddlLocation.DataValueField = "location_id";
                    ddlLocation.DataBind();
                    objUserBO.location_id = Convert.ToInt32(ddlLocation.SelectedValue);
                    Session["dtLocation"] = dtLocation;
                    ddlLocation_SelectedIndexChanged(null, null);
                }
                else
                {
                    ddlLocation.Items.Clear();
                    ddlLocation.DataSource = null;
                    ddlLocation.DataBind();
                }

            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        protected void ddlCountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dtCountry = new DataTable();
            DataTable dtLocation = new DataTable();
            try
            {
                dsDropDownData = new DataSet();
                objUserBO = new UsersBO();
                objUserBO.UserId = Convert.ToInt32(Session["UserId"]);
                objComBL = new CommonBL();

                objUserBO.country_id = Convert.ToInt32(ddlCountry.SelectedValue);
                dtLocation = objComBL.GetLocationBasedOnCountryBL(objUserBO);
                if (dtLocation.Rows.Count > 0)
                {
                    ddlLocation.DataSource = dtLocation;
                    ddlLocation.DataTextField = "location_name";
                    ddlLocation.DataValueField = "location_id";
                    ddlLocation.DataBind();
                    objUserBO.location_id = Convert.ToInt32(ddlLocation.SelectedValue);
                    Session["dtLocation"] = dtLocation;
                    ddlLocation_SelectedIndexChanged(null, null);
                }
                else
                {
                    ddlLocation.Items.Clear();
                    ddlLocation.DataSource = null;
                    ddlLocation.DataBind();
                }

            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                objPlanBO = new PlanBO();
                objPlanBL = new PlanBL();
                if (!string.IsNullOrEmpty(hdnPlanId.Value))
                    objPlanBO.p_audit_plan_id = Convert.ToInt32(hdnPlanId.Value);
                objPlanBO = objPlanBL.DeletePlanningBL(objPlanBO);
                if (objPlanBO.error_code == 0)
                {
                    string msg = objPlanBO.error_msg;
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ShowPopup", "alert('" + msg + "'); location.reload();", true);
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ShowPopup", "alert('" + objPlanBO.error_msg + "');", true);
                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        //protected void ddlLineNumber_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    DataTable dtPart = new DataTable();
        //    objUserBO = new UsersBO();
        //    objComBL = new CommonBL();
        //    try
        //    {
        //        objUserBO.line_id = Convert.ToInt32(ddlLineNumber.SelectedValue);
        //        dtPart = objComBL.GetPartBasedOnLineBL(objUserBO);
        //        if (dtPart.Rows.Count > 0)
        //        {
        //            ddlProduct.DataSource = dtPart;
        //            ddlProduct.DataTextField = "Part_code";
        //            ddlProduct.DataValueField = "Part_id";
        //            ddlProduct.DataBind();
        //            Session["dtPart"] = dtPart;
        //        }
        //        else
        //        {
        //            ddlProduct.Items.Clear();
        //            ddlProduct.DataSource = null;
        //            ddlProduct.DataBind();
        //        }
        //        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ShowPopup", " PageRefresh();", true);
        //    }
        //    catch (Exception ex)
        //    {
        //        objCom.ErrorLog(ex);
        //    }
        //}

        #endregion

        #region Methods

        private void GetMasterDropdown()
        {
            DataTable dtCountry = new DataTable();
            DataTable dtLocation = new DataTable();
            DataTable dtLine = new DataTable();
            DataTable dtEmployee = new DataTable();
            DataTable dtPart = new DataTable();
            try
            {
                dsDropDownData = new DataSet();
                objUserBO = new UsersBO();
                objUserBO.UserId = Convert.ToInt32(Session["UserId"]);
                objComBL = new CommonBL();
                dsDropDownData = objComBL.GetAllMasterBL(objUserBO);
                if (dsDropDownData.Tables.Count > 0)
                {
                    if (dsDropDownData.Tables[0].Rows.Count > 0)
                    {
                        ddlRegion.DataSource = dsDropDownData.Tables[0];
                        ddlRegion.DataTextField = "region_name";
                        ddlRegion.DataValueField = "region_id";
                        ddlRegion.DataBind();
                        ddlRegion.SelectedValue = Convert.ToString(Session["LoggedInRegionId"]);
                        if (Convert.ToString(Session["GlobalAdminFlag"]) == "N")
                        {
                       //     ddlRegion.SelectedValue = Convert.ToString(Session["LoggedInRegionId"]);
                            ddlRegion.Attributes["disabled"] = "disabled";
                        }
                        else
                            ddlRegion.Enabled = true;

                       
                    }
                    else
                    {
                        ddlRegion.DataSource = null;
                        ddlRegion.DataBind();
                    }
                }
                if (!string.IsNullOrEmpty(ddlRegion.SelectedValue))
                {
                    objUserBO.region_id = Convert.ToInt32(ddlRegion.SelectedValue);
                    dtCountry = objComBL.GetCountryBasedOnRegionBL(objUserBO);

                    if (dtCountry.Rows.Count > 0)
                    {
                        ddlCountry.DataSource = dtCountry;
                        ddlCountry.DataTextField = "country_name";
                        ddlCountry.DataValueField = "country_id";
                        ddlCountry.DataBind();
                        ddlCountry.SelectedValue = Convert.ToString(Session["LoggedInCountryId"]);
                        if (Convert.ToString(Session["GlobalAdminFlag"]) == "N")
                        {
                            //ddlCountry.SelectedValue = Convert.ToString(Session["LoggedInCountryId"]);
                            ddlCountry.Attributes["disabled"] = "disabled";
                        }
                        else
                            ddlCountry.Enabled = true;

                       
                    }
                    else
                    {
                        ddlCountry.Items.Clear();
                        ddlCountry.DataSource = null;
                        ddlCountry.DataBind();
                    }
                }
                else
                {
                    ddlCountry.Items.Clear();
                    ddlCountry.DataSource = null;
                    ddlCountry.DataBind();
                }
                if (!string.IsNullOrEmpty(ddlCountry.SelectedValue))
                {
                    objUserBO.country_id = Convert.ToInt32(ddlCountry.SelectedValue);
                    dtLocation = objComBL.GetLocationBasedOnCountryBL(objUserBO);
                    if (dtLocation.Rows.Count > 0)
                    {
                        ddlLocation.DataSource = dtLocation;
                        ddlLocation.DataTextField = "location_name";
                        ddlLocation.DataValueField = "location_id";
                        ddlLocation.DataBind();
                        ddlLocation.SelectedValue = Convert.ToString(Session["LocationId"]);
                        if (Convert.ToString(Session["GlobalAdminFlag"]) == "N")
                        {
                            //ddlLocation.SelectedValue = Convert.ToString(Session["LocationId"]);
                            ddlLocation.Attributes["disabled"] = "disabled";
                        }
                        else
                        ddlLocation.Enabled = true;
                       
                        Session["SelectedLocation"] = Convert.ToString(ddlLocation.SelectedValue);
                        Session["dtLocation"] = dtLocation;
                    }
                    else
                    {
                        ddlLocation.Items.Clear();
                        ddlLocation.DataSource = null;
                        ddlLocation.DataBind();
                    }
                }
                else
                {
                    ddlLocation.Items.Clear();
                    ddlLocation.DataSource = null;
                    ddlLocation.DataBind();
                }
                if (!string.IsNullOrEmpty(ddlLocation.SelectedValue))
                {
                    objUserBO.location_id = Convert.ToInt32(ddlLocation.SelectedValue);
                    dtEmployee = objComBL.GetEmployeesBasedOnLocationBL(objUserBO);
                    if (dtLocation.Rows.Count > 0)
                    {
                        ddlusers.DataSource = dtEmployee;
                        ddlusers.DataTextField = "emp_full_name";
                        ddlusers.DataValueField = "user_id";
                        ddlusers.DataBind();
                        Session["SelectedUser"] = Convert.ToString(ddlusers.SelectedItem);
                    }
                    else
                    {
                        ddlusers.Items.Clear();
                        ddlusers.DataSource = null;
                        ddlusers.DataBind();
                    }
                }
                else
                {
                    ddlusers.Items.Clear();
                    ddlusers.DataSource = null;
                    ddlusers.DataBind();
                }
                //if (dsDropDownData.Tables.Count > 0)
                //{
                //    if (dsDropDownData.Tables[7].Rows.Count > 0)
                //    {
                //        //Session["dtusers"] = dsDropDownData.Tables[7];
                //        ddlusers.DataSource = dsDropDownData.Tables[7];
                //        ddlusers.DataTextField = "emp_full_name";
                //        ddlusers.DataValueField = "user_id";
                //        ddlusers.DataBind();
                //    }
                //    else
                //    {
                //        ddlusers.DataSource = null;
                //        ddlusers.DataBind();
                //    }
                //}
                //if (dsDropDownData.Tables.Count > 0)
                //{
                //    if (dsDropDownData.Tables[3].Rows.Count > 0)
                //    {
                //        ddlLineNumber.DataSource = dsDropDownData.Tables[3];
                //        ddlLineNumber.DataTextField = "line_name";
                //        ddlLineNumber.DataValueField = "line_id";
                //        ddlLineNumber.DataBind();
                //    }
                //    else
                //    {
                //        ddlLineNumber.DataSource = null;
                //        ddlLineNumber.DataBind();
                //    }
                //}
                //if (dsDropDownData.Tables.Count > 0)
                //{
                //    if (dsDropDownData.Tables[4].Rows.Count > 0)
                //    {
                //        ddlProduct.DataSource = dsDropDownData.Tables[4];
                //        ddlProduct.DataTextField = "product_name";
                //        ddlProduct.DataValueField = "product_id";
                //        ddlProduct.DataBind();
                //    }
                //    else
                //    {
                //        ddlProduct.DataSource = null;
                //        ddlProduct.DataBind();
                //    }
                //}
                //if (dsDropDownData.Tables.Count > 0)
                //{
                //    if (dsDropDownData.Tables[6].Rows.Count > 0)
                //    {
                //        //Session["dtGroup"] = dsDropDownData.Tables[6];
                //        ddlGroup.DataSource = dsDropDownData.Tables[6];
                //        ddlGroup.DataTextField = "group_name";
                //        ddlGroup.DataValueField = "group_id";
                //        ddlGroup.DataBind();
                //    }
                //    else
                //    {
                //        ddlGroup.DataSource = null;
                //        ddlGroup.DataBind();
                //    }
                //}
                objUserBO.location_id = Convert.ToInt32(ddlLocation.SelectedValue);
                //DataRow drselect = (from DataRow dr in dtLocation.Rows
                //                    where (int)dr["location_id"] == objUserBO.location_id
                //                    select dr).FirstOrDefault();
                //ddlShift.SelectedValue = Convert.ToString(drselect["Shift_no"]);
                dtLine = objComBL.GetLineListDropDownBL(objUserBO);
                if (dtLine.Rows.Count > 0)
                {
                    ddlLineNumber.DataSource = dtLine;
                    ddlLineNumber.DataTextField = "line_name";
                    ddlLineNumber.DataValueField = "line_id";
                    ddlLineNumber.DataBind();
                    Session["dtLine"] = dtLine;
                }
                else
                {
                    ddlLineNumber.DataSource = null;
                    ddlLineNumber.DataBind();
                }
                //objUserBO.line_id = Convert.ToInt32(ddlLineNumber.SelectedValue);
                //dtPart = objComBL.GetPartBasedOnLocBL(objUserBO);
                //if (dtPart.Rows.Count > 0)
                //{
                //    ddlProduct.DataSource = dtPart;
                //    ddlProduct.DataTextField = "Part_code";
                //    ddlProduct.DataValueField = "Part_id";
                //    ddlProduct.DataBind();
                //    Session["dtPart"] = dtPart;
                //}
                //else
                //{
                //    ddlProduct.DataSource = null;
                //    ddlProduct.DataBind();
                //}
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }

        }

        private void SendInvitation(PlanBO objPlanBO)
        {
            eAppointmentMail objApptEmail;
            //EmailDetails objEmail;
            try
            {
                if (!string.IsNullOrEmpty(objPlanBO.invitationMailIds))
                {
                    objApptEmail = new eAppointmentMail();
                    objApptEmail.Body = "Audit for "+Convert.ToString(ddlLineNumber.SelectedItem)+" needs to be performed between " + objPlanBO.p_planned_start_date + " and " + objPlanBO.p_planned_end_date + ". Please contact the LPA Administrator if you need to make any changes.";
                    objApptEmail.Email = objPlanBO.invitationMailIds;
                   // objApptEmail.newEndDate = objPlanBO.p_planned_end_date;
                    objApptEmail.Location = "At your desk";
                    //objApptEmail.newStartDate = objPlanBO.p_planned_start_date;
                    objApptEmail.Subject = "You have an audit planned from " + objPlanBO.p_planned_start_date + " to " + objPlanBO.p_planned_end_date + " for "+Convert.ToString(ddlLineNumber.SelectedItem);
                    objApptEmail.Name = objPlanBO.user_name;
                    objCom.SendInvitation(objApptEmail);
                }
                //if (!string.IsNullOrEmpty(objPlanBO.mailIds))
                //{
                //    objEmail = new EmailDetails();
                //    objEmail.body = "Audit is planned.";
                //    objEmail.subject = "Perform Audit";
                //    objEmail.toMailId = objPlanBO.mailIds;
                //    objCom.SendMail(objEmail);
                //}
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }

        }

        #endregion
    }
}