﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.DataVisualization.Charting;
using System.Web.UI.WebControls;
using MHLPA_BusinessLogic;
using MHLPA_BusinessObject;

namespace MHLPA_Application
{
    public partial class Dashboard : System.Web.UI.Page
    {
        #region Global Declaration
        CommonFunctions objCom = new CommonFunctions();
        DataSet dsDropDownData;
        UsersBO objUserBO;
        DataTable dtAnswer = new DataTable();
        CommonBL objComBL;
        DataTable dtReport;
        DashboardBO objDashBO;
        DashboardBL objDashBL;
        private readonly Random random = new Random(1234);
        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    objDashBO = new DashboardBO();
                    LoadFilterDropDowns(objDashBO);
                    txtDate.Text = String.Format("{0:dd-MM-yyyy}", DateTime.Now);
                    btnFilter_Click(null, null);
                }
                catch (Exception ex)
                {
                    objCom.ErrorLog(ex);
                }
            }
        }

        //protected void ddlLocation_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    DataTable dtLocation = new DataTable();
        //    DataTable dtLine = new DataTable();
        //    DataTable dtPart = new DataTable();
        //    DataTable dtUsers = new DataTable();
        //    try
        //    {
        //        objUserBO = new UsersBO();
        //        objComBL = new CommonBL();
        //        Session["SelectedLocation"] = Convert.ToString(ddlLocation.SelectedValue);
        //        dtLocation = (DataTable)Session["dtLocation"];
        //        objUserBO.location_id = Convert.ToInt32(ddlLocation.SelectedValue);

        //        //objUserBO.location_id = Convert.ToInt32(ddlLocation.SelectedValue);
        //        dtLine = objComBL.GetLineListDropDownBL(objUserBO);
        //        if (dtLine.Rows.Count > 0)
        //        {
        //            ddlLineName.DataSource = dtLine;
        //            ddlLineName.DataTextField = "line_name";
        //            ddlLineName.DataValueField = "line_id";
        //            ddlLineName.DataBind();
        //            ddlLineName.Items.Insert(0, new System.Web.UI.WebControls.ListItem("All", "0"));
        //        }
        //        else
        //        {
        //            ddlLineName.Items.Clear();
        //            ddlLineName.DataSource = null;
        //            ddlLineName.DataBind();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        objCom.ErrorLog(ex);
        //    }
        //}

        //protected void ddlRegion_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    DataTable dtCountry = new DataTable();
        //    DataTable dtLocation = new DataTable();
        //    try
        //    {
        //        dsDropDownData = new DataSet();
        //        objUserBO = new UsersBO();
        //        objUserBO.UserId = Convert.ToInt32(Session["UserId"]);
        //        objComBL = new CommonBL();

        //        objUserBO.region_id = Convert.ToInt32(ddlRegion.SelectedValue);
        //        dtCountry = objComBL.GetCountryBasedOnRegionBL(objUserBO);

        //        if (dtCountry.Rows.Count > 0)
        //        {
        //            ddlCountry.DataSource = dtCountry;
        //            ddlCountry.DataTextField = "country_name";
        //            ddlCountry.DataValueField = "country_id";
        //            ddlCountry.DataBind();
        //            ddlCountry.Items.Insert(0, new System.Web.UI.WebControls.ListItem("All", "0"));
        //            objUserBO.country_id = Convert.ToInt32(ddlCountry.SelectedValue);
        //        }
        //        else
        //        {
        //            ddlCountry.Items.Clear();
        //            ddlCountry.DataSource = null;
        //            ddlCountry.DataBind();
        //        }

        //        dtLocation = objComBL.GetLocationBasedOnCountryBL(objUserBO);
        //        if (dtLocation.Rows.Count > 0)
        //        {
        //            ddlLocation.DataSource = dtLocation;
        //            ddlLocation.DataTextField = "location_name";
        //            ddlLocation.DataValueField = "location_id";
        //            ddlLocation.DataBind();
        //            ddlLocation.Items.Insert(0, new System.Web.UI.WebControls.ListItem("All", "0"));
        //            objUserBO.location_id = Convert.ToInt32(ddlLocation.SelectedValue);
        //            Session["dtLocation"] = dtLocation;
        //            ddlLocation_SelectedIndexChanged(null, null);
        //        }
        //        else
        //        {
        //            ddlLocation.Items.Clear();
        //            ddlLocation.DataSource = null;
        //            ddlLocation.DataBind();
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        objCom.ErrorLog(ex);
        //    }
        //}

        //protected void ddlCountry_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    DataTable dtCountry = new DataTable();
        //    DataTable dtLocation = new DataTable();
        //    try
        //    {
        //        dsDropDownData = new DataSet();
        //        objUserBO = new UsersBO();
        //        objUserBO.UserId = Convert.ToInt32(Session["UserId"]);
        //        objComBL = new CommonBL();

        //        objUserBO.country_id = Convert.ToInt32(ddlCountry.SelectedValue);
        //        dtLocation = objComBL.GetLocationBasedOnCountryBL(objUserBO);
        //        if (dtLocation.Rows.Count > 0)
        //        {
        //            ddlLocation.DataSource = dtLocation;
        //            ddlLocation.DataTextField = "location_name";
        //            ddlLocation.DataValueField = "location_id";
        //            ddlLocation.DataBind();
        //            ddlLocation.Items.Insert(0, new System.Web.UI.WebControls.ListItem("All", "0"));
        //            objUserBO.location_id = Convert.ToInt32(ddlLocation.SelectedValue);
        //            Session["dtLocation"] = dtLocation;
        //            ddlLocation_SelectedIndexChanged(null, null);
        //        }
        //        else
        //        {
        //            ddlLocation.Items.Clear();
        //            ddlLocation.DataSource = null;
        //            ddlLocation.DataBind();
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        objCom.ErrorLog(ex);
        //    }
        //}

        //protected void ddlLineName_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    DataTable dtLine = new DataTable();
        //    try
        //    {
        //        //objUserBO = new UsersBO();
        //        //objComBL = new CommonBL();
        //        //Session["SelectedLine"] = Convert.ToString(ddlLineName.SelectedValue);
        //        //string selectedline = Session["SelectedLine"].ToString();
        //        //objUserBO.line_id = Convert.ToInt32(ddlLineName.SelectedValue);
        //    }
        //    catch (Exception ex)
        //    {
        //        objCom.ErrorLog(ex);
        //    }
        //}

        protected void ddlRegion_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dtLineProduct = new DataTable();
            DataTable dtCountry = new DataTable();
            DataTable dtLocation = new DataTable();
            try
            {
                objDashBO = new DashboardBO();
                objDashBO.region_id = Convert.ToInt32(ddlRegion.SelectedValue);
                //objDashBO.country_id = Convert.ToInt32(ddlCountry.SelectedValue);
                //objDashBO.location_id = Convert.ToInt32(ddlLocation.SelectedValue);
                //objDashBO.line_id = Convert.ToInt32(ddlLineName.SelectedValue);
                objDashBO.selection_flag = "Region";
                // LoadFilterDropDowns(objDashBO);


                objDashBL = new DashboardBL();
                dsDropDownData = new DataSet();
                dsDropDownData = objDashBL.getFilterDataBL(objDashBO);
                if (dsDropDownData.Tables.Count > 0)
                {
                    if (dsDropDownData.Tables[0].Rows.Count > 0)
                    {
                        //ddlRegion.DataSource = dsDropDownData.Tables[0];
                        //ddlRegion.DataTextField = "region_name";
                        //ddlRegion.DataValueField = "region_id";
                        //ddlRegion.DataBind();
                        //ddlRegion.Items.Insert(0, new ListItem("All", "0"));
                        ddlRegion.SelectedValue = Convert.ToString(objDashBO.region_id);
                    }
                    else
                    {
                        ddlRegion.Items.Clear();
                        ddlRegion.DataSource = null;
                        ddlRegion.DataBind();
                    }
                    if (dsDropDownData.Tables[1].Rows.Count > 0)
                    {
                        ddlCountry.DataSource = dsDropDownData.Tables[1];
                        ddlCountry.DataTextField = "country_name";
                        ddlCountry.DataValueField = "country_id";
                        ddlCountry.DataBind();
                        ddlCountry.Items.Insert(0, new ListItem("All", "0"));
                    }
                    else
                    {
                        ddlCountry.Items.Clear();
                        ddlCountry.DataSource = null;
                        ddlCountry.DataBind();
                    }
                    if (dsDropDownData.Tables[2].Rows.Count > 0)
                    {
                        ddlLocation.DataSource = dsDropDownData.Tables[2];
                        ddlLocation.DataTextField = "location_name";
                        ddlLocation.DataValueField = "location_id";
                        ddlLocation.DataBind();
                        ddlLocation.Items.Insert(0, new ListItem("All", "0"));
                    }
                    else
                    {
                        ddlLocation.Items.Clear();
                        ddlLocation.DataSource = null;
                        ddlLocation.DataBind();
                    }
                    if (dsDropDownData.Tables[3].Rows.Count > 0)
                    {
                        ddlLineName.DataSource = dsDropDownData.Tables[3];
                        ddlLineName.DataTextField = "line_name";
                        ddlLineName.DataValueField = "line_id";
                        ddlLineName.DataBind();
                        ddlLineName.Items.Insert(0, new ListItem("All", "0"));
                    }
                    else
                    {
                        ddlLineName.Items.Clear();
                        ddlLineName.DataSource = null;
                        ddlLineName.DataBind();
                    }
                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        protected void ddlCountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dtLineProduct = new DataTable();
            DataTable dtCountry = new DataTable();
            DataTable dtLocation = new DataTable();
            DataSet dsDropDownDataCountry = new DataSet();
            DashboardBO objDashBOCountry = new DashboardBO();
            int region;
            try
            {
                objDashBO = new DashboardBO();
                region = Convert.ToInt32(ddlRegion.SelectedValue);
                objDashBO.country_id = Convert.ToInt32(ddlCountry.SelectedValue);
                if (objDashBO.country_id == 0)
                {
                    objDashBO.region_id = Convert.ToInt32(ddlRegion.SelectedValue);
                }
                //objDashBO.location_id = Convert.ToInt32(ddlLocation.SelectedValue);
                //objDashBO.line_id = Convert.ToInt32(ddlLineName.SelectedValue);
                objDashBO.selection_flag = "Country";
                // LoadFilterDropDowns(objDashBO);

                objDashBL = new DashboardBL();
                dsDropDownData = new DataSet();
                dsDropDownData = objDashBL.getFilterDataBL(objDashBO);
                if (dsDropDownData.Tables.Count > 0)
                {
                    if (dsDropDownData.Tables[0].Rows.Count > 0)
                    {
                        //    ddlRegion.DataSource = dsDropDownData.Tables[0];
                        //    ddlRegion.DataTextField = "region_name";
                        //    ddlRegion.DataValueField = "region_id";
                        //    ddlRegion.DataBind();
                        //    ddlRegion.Items.Insert(0, new ListItem("All", "0"));
                        //    if (ddlRegion.Items.FindByValue(Convert.ToString(region)) != null && region != 0)
                        //    {
                        //        ddlRegion.SelectedValue = Convert.ToString(region);
                        //    }
                        //    else
                        //    {
                        //        ddlRegion.SelectedIndex = 1;
                        //    }
                        if (ddlRegion.Items.FindByValue(Convert.ToString(region)) != null && region != 0)
                        {
                            ddlRegion.SelectedValue = Convert.ToString(region);
                        }
                        else
                        {
                            ddlRegion.SelectedValue = Convert.ToString(dsDropDownData.Tables[0].Rows[0]["region_id"]);
                        }
                    }
                    else
                    {
                        //    ddlRegion.Items.Clear();
                        //    ddlRegion.DataSource = null;
                        //    ddlRegion.DataBind();
                    }
                    if (dsDropDownData.Tables[1].Rows.Count > 0)
                    {
                        if (ddlRegion.Items.FindByValue(Convert.ToString(region)) == null || region == 0)
                        {
                            objDashBOCountry.region_id = Convert.ToInt32(ddlRegion.SelectedValue);
                            dsDropDownDataCountry = objDashBL.getFilterDataBL(objDashBOCountry);
                            if (dsDropDownDataCountry.Tables.Count > 0)
                            {
                                if (dsDropDownDataCountry.Tables[1].Rows.Count > 0)
                                {
                                    ddlCountry.DataSource = dsDropDownDataCountry.Tables[1];
                                    ddlCountry.DataTextField = "country_name";
                                    ddlCountry.DataValueField = "country_id";
                                    ddlCountry.DataBind();
                                    ddlCountry.Items.Insert(0, new ListItem("All", "0"));
                                    ddlCountry.SelectedValue = Convert.ToString(objDashBO.country_id);
                                }
                                else
                                {
                                    ddlCountry.Items.Clear();
                                    ddlCountry.DataSource = null;
                                    ddlCountry.DataBind();
                                }
                            }
                        }
                        else
                        {
                            ddlCountry.SelectedValue = Convert.ToString(objDashBO.country_id);
                        }
                        //else
                        //{
                        //    ddlRegion.SelectedValue = Convert.ToString(dsDropDownData.Tables[0].Rows[0]["region_id"]);
                        //}


                    }
                    else
                    {
                        ddlCountry.Items.Clear();
                        ddlCountry.DataSource = null;
                        ddlCountry.DataBind();
                    }
                    if (dsDropDownData.Tables[2].Rows.Count > 0)
                    {
                        ddlLocation.DataSource = dsDropDownData.Tables[2];
                        ddlLocation.DataTextField = "location_name";
                        ddlLocation.DataValueField = "location_id";
                        ddlLocation.DataBind();
                        ddlLocation.Items.Insert(0, new ListItem("All", "0"));
                    }
                    else
                    {
                        ddlLocation.Items.Clear();
                        ddlLocation.DataSource = null;
                        ddlLocation.DataBind();
                    }
                    if (dsDropDownData.Tables[3].Rows.Count > 0)
                    {
                        ddlLineName.DataSource = dsDropDownData.Tables[3];
                        ddlLineName.DataTextField = "line_name";
                        ddlLineName.DataValueField = "line_id";
                        ddlLineName.DataBind();
                        ddlLineName.Items.Insert(0, new ListItem("All", "0"));
                    }
                    else
                    {
                        ddlLineName.Items.Clear();
                        ddlLineName.DataSource = null;
                        ddlLineName.DataBind();
                    }
                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        protected void ddlLocation_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dtLineProduct = new DataTable();
            DataSet dsDropDownDataCountry = new DataSet();
            DataSet dsDropDownDataLocation = new DataSet();
            DashboardBO objDashBOCountry = new DashboardBO();
            DashboardBO objDashBOlocation = new DashboardBO();
            int region, country;

            try
            {
                objDashBO = new DashboardBO();
                region = Convert.ToInt32(ddlRegion.SelectedValue);
                country = Convert.ToInt32(ddlCountry.SelectedValue);
                objDashBO.location_id = Convert.ToInt32(ddlLocation.SelectedValue);
                if (objDashBO.location_id == 0)
                {
                    objDashBO.country_id = country;
                    objDashBO.region_id = region;
                }
                //objDashBO.region_id = Convert.ToInt32(ddlRegion.SelectedValue);
                //objDashBO.country_id = Convert.ToInt32(ddlCountry.SelectedValue);
                //objDashBO.location_id = Convert.ToInt32(ddlLocation.SelectedValue);
                //objDashBO.line_id = Convert.ToInt32(ddlLineName.SelectedValue);
                objDashBO.selection_flag = "Location";
                // LoadFilterDropDowns(objDashBO);

                objDashBL = new DashboardBL();
                dsDropDownData = new DataSet();
                dsDropDownData = objDashBL.getFilterDataBL(objDashBO);
                if (dsDropDownData.Tables.Count > 0)
                {
                    if (dsDropDownData.Tables[0].Rows.Count > 0)
                    {
                        //    ddlRegion.DataSource = dsDropDownData.Tables[0];
                        //    ddlRegion.DataTextField = "region_name";
                        //    ddlRegion.DataValueField = "region_id";
                        //    ddlRegion.DataBind();
                        //    ddlRegion.Items.Insert(0, new ListItem("All", "0"));
                        //    if (ddlRegion.Items.FindByValue(Convert.ToString(region)) != null && region != 0)
                        //    {
                        //        ddlRegion.SelectedValue = Convert.ToString(region);
                        //    }
                        //    else
                        //    {
                        //        ddlRegion.SelectedIndex = 1;
                        //    }
                        if (ddlRegion.Items.FindByValue(Convert.ToString(region)) != null && region != 0)
                        {
                            ddlRegion.SelectedValue = Convert.ToString(region);
                        }
                        else
                        {
                            ddlRegion.SelectedValue = Convert.ToString(dsDropDownData.Tables[0].Rows[0]["region_id"]);
                        }
                    }
                    //else
                    //{
                    //    ddlRegion.Items.Clear();
                    //    ddlRegion.DataSource = null;
                    //    ddlRegion.DataBind();
                    //}
                    if (dsDropDownData.Tables[1].Rows.Count > 0)
                    {
                        //    ddlCountry.DataSource = dsDropDownData.Tables[1];
                        //    ddlCountry.DataTextField = "country_name";
                        //    ddlCountry.DataValueField = "country_id";
                        //    ddlCountry.DataBind();
                        //    ddlCountry.Items.Insert(0, new ListItem("All", "0"));
                        //    if (ddlCountry.Items.FindByValue(Convert.ToString(country)) != null && country != 0)
                        //    {
                        //        ddlCountry.SelectedValue = Convert.ToString(country);
                        //    }
                        //    else
                        //    {
                        //        ddlCountry.SelectedIndex = 1;
                        //    }

                        objDashBOCountry.region_id = Convert.ToInt32(ddlRegion.SelectedValue);
                        dsDropDownDataCountry = objDashBL.getFilterDataBL(objDashBOCountry);
                        if (dsDropDownDataCountry.Tables.Count > 0)
                        {
                            if (dsDropDownDataCountry.Tables[1].Rows.Count > 0)
                            {
                                ddlCountry.DataSource = dsDropDownDataCountry.Tables[1];
                                ddlCountry.DataTextField = "country_name";
                                ddlCountry.DataValueField = "country_id";
                                ddlCountry.DataBind();
                                ddlCountry.Items.Insert(0, new ListItem("All", "0"));
                                if (ddlCountry.Items.FindByValue(Convert.ToString(country)) != null && country != 0)
                                {
                                    ddlCountry.SelectedValue = Convert.ToString(country);
                                }
                                else
                                {
                                    ddlCountry.SelectedValue = Convert.ToString(dsDropDownData.Tables[1].Rows[0]["country_id"]); ;
                                }
                            }
                            else
                            {
                                ddlCountry.Items.Clear();
                                ddlCountry.DataSource = null;
                                ddlCountry.DataBind();
                            }
                        }
                    }
                    //else
                    //{
                    //    ddlCountry.Items.Clear();
                    //    ddlCountry.DataSource = null;
                    //    ddlCountry.DataBind();
                    //}
                    if (dsDropDownData.Tables[2].Rows.Count > 0)
                    {
                        //ddlLocation.DataSource = dsDropDownData.Tables[2];
                        //ddlLocation.DataTextField = "location_name";
                        //ddlLocation.DataValueField = "location_id";
                        //ddlLocation.DataBind();
                        //ddlLocation.Items.Insert(0, new ListItem("All", "0"));
                        objDashBOlocation.country_id = Convert.ToInt32(ddlCountry.SelectedValue);
                        dsDropDownDataLocation = objDashBL.getFilterDataBL(objDashBOlocation);
                        if (dsDropDownDataLocation.Tables.Count > 0)
                        {
                            if (dsDropDownDataLocation.Tables[2].Rows.Count > 0)
                            {
                                ddlLocation.DataSource = dsDropDownDataLocation.Tables[2];
                                ddlLocation.DataTextField = "location_name";
                                ddlLocation.DataValueField = "location_id";
                                ddlLocation.DataBind();
                                ddlLocation.Items.Insert(0, new ListItem("All", "0"));
                                ddlLocation.SelectedValue = Convert.ToString(objDashBO.location_id);
                            }
                            else
                            {
                                ddlLocation.Items.Clear();
                                ddlLocation.DataSource = null;
                                ddlLocation.DataBind();
                            }

                        }



                    }
                    else
                    {
                        ddlLocation.Items.Clear();
                        ddlLocation.DataSource = null;
                        ddlLocation.DataBind();
                    }
                    DataTable dtBuilding = new DataTable();
                    objUserBO = new UsersBO();
                    objComBL = new CommonBL();
                    objUserBO.UserId = Convert.ToInt32(Session["UserId"]);
                    objUserBO.location_id = Convert.ToInt32(ddlLocation.SelectedValue);
                    dtBuilding = objComBL.GetBuildingListDropDownBL(objUserBO);

                    if (dtBuilding.Rows.Count > 0)
                    {
                        ddlBuilding.DataSource = dtBuilding;
                        ddlBuilding.DataTextField = "building_name";
                        ddlBuilding.DataValueField = "building_id";
                        ddlBuilding.DataBind();
                        ddlBuilding.Items.Insert(0, new ListItem("All", "0"));
                    }
                    else
                    {
                        ddlBuilding.Items.Clear();
                        ddlBuilding.DataSource = null;
                        ddlBuilding.DataBind();
                    }
                    if (dsDropDownData.Tables[3].Rows.Count > 0)
                    {
                        ddlLineName.DataSource = dsDropDownData.Tables[3];
                        ddlLineName.DataTextField = "line_name";
                        ddlLineName.DataValueField = "line_id";
                        ddlLineName.DataBind();
                        ddlLineName.Items.Insert(0, new ListItem("All", "0"));
                    }
                    else
                    {
                        ddlLineName.Items.Clear();
                        ddlLineName.DataSource = null;
                        ddlLineName.DataBind();
                    }
                }

            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        protected void ddlLineName_SelectedIndexChanged(object sender, EventArgs e)
        {
            int region, country, location, building;

            try
            {
                objDashBO = new DashboardBO();
                region = Convert.ToInt32(ddlRegion.SelectedValue);
                country = Convert.ToInt32(ddlCountry.SelectedValue);
                location = Convert.ToInt32(ddlLocation.SelectedValue);
                building = Convert.ToInt32(ddlBuilding.SelectedValue);
                objDashBO.line_id = Convert.ToInt32(ddlLineName.SelectedValue);
                if (objDashBO.line_id == 0)
                {
                    objDashBO.region_id = Convert.ToInt32(ddlRegion.SelectedValue);
                    objDashBO.country_id = Convert.ToInt32(ddlCountry.SelectedValue);
                    objDashBO.location_id = Convert.ToInt32(ddlLocation.SelectedValue);
                    objDashBO.building_id = Convert.ToInt32(ddlBuilding.SelectedValue);
                }
                objDashBO.selection_flag = "Line";
                // LoadFilterDropDowns(objDashBO);

                objDashBL = new DashboardBL();
                dsDropDownData = new DataSet();
                dsDropDownData = objDashBL.getFilterDataBL(objDashBO);
                if (dsDropDownData.Tables.Count > 0)
                {
                    if (dsDropDownData.Tables[0].Rows.Count > 0)
                    {
                        ddlRegion.DataSource = dsDropDownData.Tables[0];
                        ddlRegion.DataTextField = "region_name";
                        ddlRegion.DataValueField = "region_id";
                        ddlRegion.DataBind();
                        ddlRegion.Items.Insert(0, new ListItem("All", "0"));
                        ddlRegion.SelectedValue = Convert.ToString(region);
                    }
                    else
                    {
                        ddlRegion.Items.Clear();
                        ddlRegion.DataSource = null;
                        ddlRegion.DataBind();
                    }
                    if (dsDropDownData.Tables[1].Rows.Count > 0)
                    {
                        ddlCountry.DataSource = dsDropDownData.Tables[1];
                        ddlCountry.DataTextField = "country_name";
                        ddlCountry.DataValueField = "country_id";
                        ddlCountry.DataBind();
                        ddlCountry.Items.Insert(0, new ListItem("All", "0"));
                        ddlCountry.SelectedValue = Convert.ToString(country);
                    }
                    else
                    {
                        ddlCountry.Items.Clear();
                        ddlCountry.DataSource = null;
                        ddlCountry.DataBind();
                    }
                    if (dsDropDownData.Tables[2].Rows.Count > 0)
                    {
                        ddlLocation.DataSource = dsDropDownData.Tables[2];
                        ddlLocation.DataTextField = "location_name";
                        ddlLocation.DataValueField = "location_id";
                        ddlLocation.DataBind();
                        ddlLocation.Items.Insert(0, new ListItem("All", "0"));
                        ddlLocation.SelectedValue = Convert.ToString(location);
                    }
                    else
                    {
                        ddlLocation.Items.Clear();
                        ddlLocation.DataSource = null;
                        ddlLocation.DataBind();
                    }
                    if (dsDropDownData.Tables[3].Rows.Count > 0)
                    {
                        ddlLineName.DataSource = dsDropDownData.Tables[3];
                        ddlLineName.DataTextField = "line_name";
                        ddlLineName.DataValueField = "line_id";
                        ddlLineName.DataBind();
                        ddlLineName.Items.Insert(0, new ListItem("All", "0"));
                        ddlLineName.SelectedValue = Convert.ToString(objDashBO.line_id);
                    }
                    else
                    {
                        ddlLineName.Items.Clear();
                        ddlLineName.DataSource = null;
                        ddlLineName.DataBind();
                    }
                    if (dsDropDownData.Tables[4].Rows.Count > 0)
                    {
                        ddlBuilding.DataSource = dsDropDownData.Tables[4];
                        ddlBuilding.DataTextField = "building_name";
                        ddlBuilding.DataValueField = "building_id";
                        ddlBuilding.DataBind();
                        ddlBuilding.Items.Insert(0, new ListItem("All", "0"));
                        ddlBuilding.SelectedValue = Convert.ToString(building);
                    }
                    else
                    {
                        ddlBuilding.Items.Clear();
                        ddlBuilding.DataSource = null;
                        ddlBuilding.DataBind();
                    }
                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }

        }

        protected void btnFilter_Click(object sender, EventArgs e)
        {
            try
            {
                objDashBO = new DashboardBO();
                objDashBL = new DashboardBL();

                dtReport = new DataTable();
                Session["location"] = Convert.ToInt32(ddlLocation.SelectedValue);
                Session["region"] = Convert.ToInt32(ddlRegion.SelectedValue);
                Session["country"] = Convert.ToInt32(ddlCountry.SelectedValue);
                Session["line"] = Convert.ToInt32(ddlLineName.SelectedValue);
                Session["building"] = Convert.ToInt32(ddlBuilding.SelectedValue);
                Session["date"] = Convert.ToString(txtDate.Text);
                Page.ClientScript.RegisterStartupScript(this.GetType(), "LoadChart", "LoadCharts()", true);
                objDashBO.location_id = Convert.ToInt32(ddlLocation.SelectedValue);
                objDashBO.region_id = Convert.ToInt32(ddlRegion.SelectedValue);
                objDashBO.country_id = Convert.ToInt32(ddlCountry.SelectedValue);
                objDashBO.line_id = Convert.ToInt32(ddlLineName.SelectedValue);
                string UIpattern = "dd-MM-yyyy";
                string DBPattern = "MM-dd-yyyy";
                DateTime parsedDate;
                if (DateTime.TryParseExact(txtDate.Text, UIpattern, null, DateTimeStyles.None, out parsedDate))
                {
                    string reportdate = parsedDate.ToString(DBPattern);
                    // objPlanBO.p_planned_start_date = Convert.ToDateTime(plannedstartdate);
                    // objDashBO.reportdate = DateTime.Parse(reportdate);
                    objDashBO.reportdate = DateTime.ParseExact(reportdate, "MM-dd-yyyy", null);
                }
                LoadSecLabels();
                #region CommentedChartCode

                //#region LPA Result

                //dtReport = objDashBL.getDataforLPAResultsReportBL(objDashBO);
                //if (dtReport.Rows.Count > 0)
                //{
                //    pnlReport.Visible = true;

                //    DataTable ChartData = dtReport;
                //    string[] XPointMember = new string[ChartData.Rows.Count];
                //    int[] YPointMember = new int[ChartData.Rows.Count];

                //    for (int count = 0; count < ChartData.Rows.Count; count++)
                //    {
                //        //storing Values for X axis  
                //        XPointMember[count] = ChartData.Rows[count]["audit_period"].ToString();
                //        //storing values for Y Axis  
                //        YPointMember[count] = Convert.ToInt32(Convert.ToDecimal(ChartData.Rows[count]["average_score"]) * 100);
                //    }
                //    Chart1.Series[1].Points.DataBindXY(XPointMember, YPointMember);
                //    Chart1.Series[1].ChartType = SeriesChartType.Column;
                //    Chart1.Series[1].Color = Color.DarkGreen;

                //    for (int count = 0; count < ChartData.Rows.Count; count++)
                //    {
                //        //storing Values for X axis  
                //        XPointMember[count] = ChartData.Rows[count]["audit_period"].ToString();
                //        //storing values for Y Axis  
                //        YPointMember[count] = Convert.ToInt32(Convert.ToDecimal(ChartData.Rows[count]["previous_score"]) * 100);
                //    }
                //    Chart1.Series[0].Points.DataBindXY(XPointMember, YPointMember);
                //    Chart1.Series[0].ChartType = SeriesChartType.Area;
                //    Chart1.Series[0].Color = Color.LightGray;

                //    Chart1.Series[1].IsValueShownAsLabel = true;
                //    Chart1.ChartAreas[0].AxisX.MajorGrid.LineWidth = 0;
                //    Chart1.ChartAreas[0].AxisY.MajorGrid.LineWidth = 0;
                //    Chart1.ChartAreas[0].AxisX.Interval = 1;
                //    Chart1.ChartAreas[0].AxisY.Interval = 25;
                //    Chart1.Series[0].LabelFormat = "{0} %";
                //    Chart1.Series[1].LabelFormat = "{0} %";
                //    Chart1.ChartAreas[0].AxisY.LabelStyle.Format = "{0} %";
                //    Chart1.Titles[0].Text = "LPA Result";
                //    Chart1.ChartAreas[0].AxisY.Maximum = 100;
                //}
                //#endregion

                //#region LPA Result Per Section


                //dtReport = new DataTable();
                //dtReport = objDashBL.getLPAResultPerSectionBL(objDashBO);
                //if (dtReport.Rows.Count > 0)
                //{
                //    pnlReport.Visible = true;

                //    DataTable ChartData = dtReport;
                //    sec1.Text = Convert.ToString(ChartData.Rows[0]["Sec_name_abbr"]) + " - " + Convert.ToString(ChartData.Rows[0]["Section_name"]);
                //    sec2.Text = Convert.ToString(ChartData.Rows[1]["Sec_name_abbr"]) + " - " + Convert.ToString(ChartData.Rows[1]["Section_name"]);
                //    sec3.Text = Convert.ToString(ChartData.Rows[2]["Sec_name_abbr"]) + " - " + Convert.ToString(ChartData.Rows[2]["Section_name"]);
                //    sec4.Text = Convert.ToString(ChartData.Rows[3]["Sec_name_abbr"]) + " - " + Convert.ToString(ChartData.Rows[3]["Section_name"]);
                //    sec5.Text = Convert.ToString(ChartData.Rows[4]["Sec_name_abbr"]) + " - " + Convert.ToString(ChartData.Rows[4]["Section_name"]);
                //    sec6.Text = Convert.ToString(ChartData.Rows[5]["Sec_name_abbr"]) + " - " + Convert.ToString(ChartData.Rows[5]["Section_name"]);
                //    string[] XPointMember = new string[ChartData.Rows.Count];
                //    int[] YPointMember = new int[ChartData.Rows.Count];

                //    for (int count = 0; count < ChartData.Rows.Count; count++)
                //    {
                //        //storing Values for X axis  
                //        XPointMember[count] = ChartData.Rows[count]["Sec_name_abbr"].ToString();
                //        //storing values for Y Axis  
                //        YPointMember[count] = Convert.ToInt32(Convert.ToDecimal(ChartData.Rows[count]["Score"]) * 100);
                //        //ChartPerSection.Series[0].Name = column.ColumnName;

                //    }

                //    ChartPerSection.Series[0].Points.DataBindXY(XPointMember, YPointMember);
                //    ChartPerSection.Series[0].ChartType = SeriesChartType.Radar;
                //    ChartPerSection.Series[0]["RadarDrawingStyle"] = "Area";
                //    ChartPerSection.Series[0]["AreaDrawingStyle"] = "Polygon";
                //    ChartPerSection.Series[0].Color = Color.DarkGreen;

                //    ChartPerSection.Titles[0].Text = "LPA Result Per Section";
                //    ChartPerSection.Series[0].IsValueShownAsLabel = true;
                //    ChartPerSection.ChartAreas[0].AxisX.MajorGrid.LineWidth = 0;
                //    ChartPerSection.ChartAreas[0].AxisY.MajorGrid.LineWidth = 0;
                //    ChartPerSection.ChartAreas[0].AxisX.Interval = 1;
                //    ChartPerSection.ChartAreas[0].AxisY.Interval = 25;
                //    ChartPerSection.Series[0].LabelFormat = "{0} %";
                //    ChartPerSection.ChartAreas[0].AxisY.LabelStyle.Format = "{0} %";
                //    ChartPerSection.ChartAreas[0].AxisY.Maximum = 100;
                //    // Axis[] xaxis;
                //    //Axis axis;
                //    // for (int count = 0; count < ChartData.Rows.Count; count++)
                //    // {
                //    //     //string axis = Convert.ToString(XPointMember[count]);
                //    //     axis = new Axis(ChartPerSection.ChartAreas[0], ChartPerSection.ChartAreas[0].AxisX.AxisName);
                //    //     xaxis = new Axis(axis);
                //    // }
                //    //ChartPerSection.ChartAreas[0].Axes = xaxis;
                //}
                //#endregion

                //#region LPA Result Per Section Monthly
                //dtReport = new DataTable();
                //dtReport = objDashBL.getLPAResultPerSectionMonthlyBL(objDashBO);
                //if (dtReport.Rows.Count > 0)
                //{
                //    pnlReport.Visible = true;

                //    DataTable ChartData = dtReport;
                //    string[] XPointMember = new string[ChartData.Rows.Count];
                //    int[] YPointMember = new int[ChartData.Rows.Count];

                //    for (int count = 0; count < ChartData.Rows.Count; count++)
                //    {
                //        //storing Values for X axis  
                //        XPointMember[count] = ChartData.Rows[count]["audit_period"].ToString();
                //        //storing values for Y Axis  
                //        YPointMember[count] = Convert.ToInt32(Convert.ToDecimal(ChartData.Rows[count]["GP"]) * 100);


                //    }
                //    ChartPerSectionMonthly.Series[0].Points.DataBindXY(XPointMember, YPointMember);
                //    ChartPerSectionMonthly.Series[0].ChartType = SeriesChartType.Line;
                //    ChartPerSectionMonthly.Series[0].Color = Color.DarkGreen;
                //    ChartPerSectionMonthly.Series[0].IsValueShownAsLabel = true;
                //    ChartPerSectionMonthly.Series[0].IsVisibleInLegend = true;
                //    ChartPerSectionMonthly.Series[0].LabelFormat = "{0} %";
                //    for (int count = 0; count < ChartData.Rows.Count; count++)
                //    {
                //        //storing Values for X axis  
                //        XPointMember[count] = ChartData.Rows[count]["audit_period"].ToString();
                //        //storing values for Y Axis  
                //        YPointMember[count] = Convert.ToInt32(Convert.ToDecimal(ChartData.Rows[count]["PD"]) * 100);


                //    }
                //    ChartPerSectionMonthly.Series[1].Points.DataBindXY(XPointMember, YPointMember);
                //    ChartPerSectionMonthly.Series[1].ChartType = SeriesChartType.Line;
                //    ChartPerSectionMonthly.Series[1].Color = Color.LightGreen;
                //    ChartPerSectionMonthly.Series[1].IsValueShownAsLabel = true;
                //    ChartPerSectionMonthly.Series[1].IsVisibleInLegend = true;
                //    ChartPerSectionMonthly.Series[1].LabelFormat = "{0} %";
                //    for (int count = 0; count < ChartData.Rows.Count; count++)
                //    {
                //        //storing Values for X axis  
                //        XPointMember[count] = ChartData.Rows[count]["audit_period"].ToString();
                //        //storing values for Y Axis  
                //        YPointMember[count] = Convert.ToInt32(Convert.ToDecimal(ChartData.Rows[count]["SOCWA"]) * 100);


                //    }
                //    ChartPerSectionMonthly.Series[2].Points.DataBindXY(XPointMember, YPointMember);
                //    ChartPerSectionMonthly.Series[2].ChartType = SeriesChartType.Line;
                //    ChartPerSectionMonthly.Series[2].Color = Color.Orange;
                //    ChartPerSectionMonthly.Series[2].IsValueShownAsLabel = true;
                //    ChartPerSectionMonthly.Series[2].IsVisibleInLegend = true;
                //    ChartPerSectionMonthly.Series[2].LabelFormat = "{0} %";
                //    for (int count = 0; count < ChartData.Rows.Count; count++)
                //    {
                //        //storing Values for X axis  
                //        XPointMember[count] = ChartData.Rows[count]["audit_period"].ToString();
                //        //storing values for Y Axis  
                //        YPointMember[count] = Convert.ToInt32(Convert.ToDecimal(ChartData.Rows[count]["RPE"]) * 100);


                //    }
                //    ChartPerSectionMonthly.Series[3].Points.DataBindXY(XPointMember, YPointMember);
                //    ChartPerSectionMonthly.Series[3].ChartType = SeriesChartType.Line;
                //    ChartPerSectionMonthly.Series[3].Color = Color.OrangeRed;
                //    ChartPerSectionMonthly.Series[3].IsValueShownAsLabel = true;
                //    ChartPerSectionMonthly.Series[3].IsVisibleInLegend = true;
                //    ChartPerSectionMonthly.Series[3].LabelFormat = "{0} %";
                //    for (int count = 0; count < ChartData.Rows.Count; count++)
                //    {
                //        //storing Values for X axis  
                //        XPointMember[count] = ChartData.Rows[count]["audit_period"].ToString();
                //        //storing values for Y Axis  
                //        YPointMember[count] = Convert.ToInt32(Convert.ToDecimal(ChartData.Rows[count]["SW"]) * 100);


                //    }
                //    ChartPerSectionMonthly.Series[4].Points.DataBindXY(XPointMember, YPointMember);
                //    ChartPerSectionMonthly.Series[4].ChartType = SeriesChartType.Line;
                //    ChartPerSectionMonthly.Series[4].Color = Color.DarkGray;
                //    ChartPerSectionMonthly.Series[4].IsValueShownAsLabel = true;
                //    ChartPerSectionMonthly.Series[4].IsVisibleInLegend = true;
                //    ChartPerSectionMonthly.Series[4].LabelFormat = "{0} %";
                //    for (int count = 0; count < ChartData.Rows.Count; count++)
                //    {
                //        //storing Values for X axis  
                //        XPointMember[count] = ChartData.Rows[count]["audit_period"].ToString();
                //        //storing values for Y Axis  
                //        YPointMember[count] = Convert.ToInt32(Convert.ToDecimal(ChartData.Rows[count]["RPS"]) * 100);


                //    }
                //    ChartPerSectionMonthly.Series[5].Points.DataBindXY(XPointMember, YPointMember);
                //    ChartPerSectionMonthly.Series[5].ChartType = SeriesChartType.Line;
                //    ChartPerSectionMonthly.Series[5].Color = Color.LightGray;
                //    ChartPerSectionMonthly.Series[5].IsValueShownAsLabel = true;
                //    ChartPerSectionMonthly.Series[5].IsVisibleInLegend = true;
                //    ChartPerSectionMonthly.ChartAreas[0].AxisX.MajorGrid.LineWidth = 0;
                //    ChartPerSectionMonthly.ChartAreas[0].AxisY.MajorGrid.LineWidth = 0;
                //    ChartPerSectionMonthly.ChartAreas[0].AxisX.Interval = 1;
                //    ChartPerSectionMonthly.Series[5].LabelFormat = "{0} %";
                //    ChartPerSectionMonthly.ChartAreas[0].AxisY.LabelStyle.Format = "{0} %";
                //    ChartPerSectionMonthly.ChartAreas[0].AxisY.Interval = 25;
                //    ChartPerSectionMonthly.ChartAreas[0].AxisY.Maximum = 100;
                //    ChartPerSectionMonthly.Titles[0].Text = "LPA Result Per Section Monthly";
                //    int i = 0;
                //    foreach (DataColumn column in ChartData.Columns)
                //    {
                //        if (column.ColumnName != "audit_period" && column.ColumnName != "audit_date_format")
                //        {
                //            ChartPerSectionMonthly.Series[i].Name = column.ColumnName;
                //            i++;
                //        }
                //    }
                //}
                //#endregion

                //#region LPA Result Per Line
                //dtReport = new DataTable();
                //dtReport = objDashBL.getLPAResultByLineBL(objDashBO);
                //if (dtReport.Rows.Count > 0)
                //{
                //    pnlReport.Visible = true;
                //    DataTable ChartData = new DataTable();
                //    ChartData = dtReport;
                //    string[] XPointMember = new string[ChartData.Rows.Count];
                //    int[] YPointMember = new int[ChartData.Rows.Count];

                //    for (int count = 0; count < ChartData.Rows.Count; count++)
                //    {
                //        XPointMember[count] = ChartData.Rows[count]["line_name"].ToString();
                //        YPointMember[count] = Convert.ToInt32(Convert.ToDecimal(ChartData.Rows[count]["average_score"]) * 100);
                //    }
                //    ChartPerLine.Series[0].Points.DataBindXY(XPointMember, YPointMember);
                //    ChartPerLine.Series[0].ChartType = SeriesChartType.Column;
                //    ChartPerLine.Series[0].Color = Color.DarkGreen;
                //    ChartPerLine.Titles[0].Text = "LPA Result per Line(pareto)";

                //    ChartPerLine.Series[0].IsValueShownAsLabel = true;
                //    ChartPerLine.ChartAreas[0].AxisX.MajorGrid.LineWidth = 0;
                //    ChartPerLine.ChartAreas[0].AxisY.MajorGrid.LineWidth = 0;
                //    ChartPerLine.ChartAreas[0].AxisX.Interval = 1;
                //    ChartPerLine.Series[0].LabelFormat = "{0} %";
                //    ChartPerLine.ChartAreas[0].AxisY.LabelStyle.Format = "{0} %";
                //    ChartPerLine.ChartAreas[0].AxisY.Interval = 25;
                //    ChartPerLine.ChartAreas[0].AxisY.Maximum = 100;

                //}
                //#endregion

                //#region LPA Result Per Questions
                //dtReport = new DataTable();
                //dtReport = objDashBL.getLPAResultByQuestionBL(objDashBO);
                //if (dtReport.Rows.Count > 0)
                //{
                //    pnlReport.Visible = true;

                //    DataTable ChartData = new DataTable();
                //    ChartData = dtReport;
                //    string[] XPointMember = new string[ChartData.Rows.Count];
                //    int[] YPointMember = new int[ChartData.Rows.Count];

                //    for (int count = 0; count < ChartData.Rows.Count; count++)
                //    {
                //        //storing Values for X axis  
                //        XPointMember[count] = Convert.ToString(ChartData.Rows[count]["question_id"]).Substring(1, Convert.ToString(ChartData.Rows[count]["question_id"]).Length - 1);
                //        //storing values for Y Axis  
                //        YPointMember[count] = Convert.ToInt32(Convert.ToDecimal(ChartData.Rows[count]["score"]) * 100);


                //    }
                //    ChartPerQuestion.Series[0].Points.DataBindXY(XPointMember, YPointMember);
                //    ChartPerQuestion.Series[0].ChartType = SeriesChartType.Column;
                //    ChartPerQuestion.Series[0].Color = Color.DarkGreen;
                //    ChartPerQuestion.Series[0].IsValueShownAsLabel = true;
                //    ChartPerQuestion.ChartAreas[0].AxisX.MajorGrid.LineWidth = 0;
                //    ChartPerQuestion.ChartAreas[0].AxisY.MajorGrid.LineWidth = 0;
                //    ChartPerQuestion.ChartAreas[0].AxisX.Interval = 1;
                //    ChartPerQuestion.Titles[0].Text = "LPA Result per Question";
                //    ChartPerQuestion.Series[0].LabelFormat = "{0} %";
                //    ChartPerQuestion.ChartAreas[0].AxisY.LabelStyle.Format = "{0} %";
                //    ChartPerQuestion.ChartAreas[0].AxisY.Interval = 25;
                //    ChartPerQuestion.ChartAreas[0].AxisY.Maximum = 100;
                //}
                //#endregion

                //#region LPA Completion Rate

                //dtReport = objDashBL.getDataforLPAComparisionReportBL(objDashBO);
                //if (dtReport.Rows.Count > 0)
                //{
                //    pnlReport.Visible = true;

                //    DataTable ChartData = dtReport;
                //    string[] XPointMember = new string[ChartData.Rows.Count];
                //    int[] YPointMember = new int[ChartData.Rows.Count];

                //    for (int count = 0; count < ChartData.Rows.Count; count++)
                //    {
                //        //storing Values for X axis  
                //        XPointMember[count] = ChartData.Rows[count]["audit_period"].ToString();
                //        //storing values for Y Axis  
                //        YPointMember[count] = Convert.ToInt32(Convert.ToDecimal(ChartData.Rows[count]["average_score"]) * 100);
                //    }
                //    chartComparision.Series[1].Points.DataBindXY(XPointMember, YPointMember);
                //    chartComparision.Series[1].ChartType = SeriesChartType.Column;
                //    chartComparision.Series[1].Color = Color.DarkGreen;

                //    for (int count = 0; count < ChartData.Rows.Count; count++)
                //    {
                //        //storing Values for X axis  
                //        XPointMember[count] = ChartData.Rows[count]["audit_period"].ToString();
                //        //storing values for Y Axis  
                //        YPointMember[count] = Convert.ToInt32(Convert.ToDecimal(ChartData.Rows[count]["previous_score"]) * 100);


                //    }
                //    chartComparision.Series[0].Points.DataBindXY(XPointMember, YPointMember);
                //    chartComparision.Series[0].ChartType = SeriesChartType.Area;
                //    chartComparision.Series[0].Color = Color.LightGray;


                //    chartComparision.Series[1].IsValueShownAsLabel = true;
                //    chartComparision.ChartAreas[0].AxisX.MajorGrid.LineWidth = 0;
                //    chartComparision.ChartAreas[0].AxisY.MajorGrid.LineWidth = 0;
                //    chartComparision.ChartAreas[0].AxisX.Interval = 1;
                //    chartComparision.ChartAreas[0].AxisY.Interval = 25;
                //    chartComparision.Series[0].LabelFormat = "{0} %";
                //    chartComparision.Series[1].LabelFormat = "{0} %";
                //    chartComparision.ChartAreas[0].AxisY.LabelStyle.Format = "{0} %";
                //    chartComparision.Titles[0].Text = "LPA Completion Rate";
                //    chartComparision.ChartAreas[0].AxisY.Maximum = 100;
                //}
                //#endregion
                #endregion
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        protected void btnClear_Click(object sender, EventArgs e)
        {
            try
            {
                objDashBO = new DashboardBO();
                LoadFilterDropDowns(objDashBO);
                txtDate.Text = String.Format("{0:dd-MM-yyyy}", DateTime.Now);
                pnlReport.Visible = false;
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        protected void ddlBuilding_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dtLineProduct = new DataTable();
            DataSet dsDropDownDataCountry = new DataSet();
            DataSet dsDropDownDataLocation = new DataSet();
            DashboardBO objDashBOCountry = new DashboardBO();
            DashboardBO objDashBOlocation = new DashboardBO();
            int region, country, location;

            try
            {
                objDashBO = new DashboardBO();
                region = Convert.ToInt32(ddlRegion.SelectedValue);
                country = Convert.ToInt32(ddlCountry.SelectedValue);
                location = Convert.ToInt32(ddlLocation.SelectedValue);
                objDashBO.building_id = Convert.ToInt32(ddlBuilding.SelectedValue);
                //if (objDashBO.building_id == 0)
                //{
                    objDashBO.location_id = location;
                    objDashBO.country_id = country;
                    objDashBO.region_id = region;
                //}
                objDashBO.selection_flag = "Location";

                objDashBL = new DashboardBL();
                dsDropDownData = new DataSet();
                dsDropDownData = objDashBL.getFilterDataBL(objDashBO);
                if (dsDropDownData.Tables.Count > 0)
                {
                    if (dsDropDownData.Tables[0].Rows.Count > 0)
                    {
                        if (ddlRegion.Items.FindByValue(Convert.ToString(region)) != null && region != 0)
                        {
                            ddlRegion.SelectedValue = Convert.ToString(region);
                        }
                        else
                        {
                            ddlRegion.SelectedValue = Convert.ToString(dsDropDownData.Tables[0].Rows[0]["region_id"]);
                        }
                    }
                    if (dsDropDownData.Tables[1].Rows.Count > 0)
                    {
                        objDashBOCountry.region_id = Convert.ToInt32(ddlRegion.SelectedValue);
                        dsDropDownDataCountry = objDashBL.getFilterDataBL(objDashBOCountry);
                        if (dsDropDownDataCountry.Tables.Count > 0)
                        {
                            if (dsDropDownDataCountry.Tables[1].Rows.Count > 0)
                            {
                                ddlCountry.DataSource = dsDropDownDataCountry.Tables[1];
                                ddlCountry.DataTextField = "country_name";
                                ddlCountry.DataValueField = "country_id";
                                ddlCountry.DataBind();
                                ddlCountry.Items.Insert(0, new ListItem("All", "0"));
                                if (ddlCountry.Items.FindByValue(Convert.ToString(country)) != null && country != 0)
                                {
                                    ddlCountry.SelectedValue = Convert.ToString(country);
                                }
                                else
                                {
                                    ddlCountry.SelectedValue = Convert.ToString(dsDropDownData.Tables[1].Rows[0]["country_id"]); ;
                                }
                            }
                            else
                            {
                                ddlCountry.Items.Clear();
                                ddlCountry.DataSource = null;
                                ddlCountry.DataBind();
                            }
                        }
                    }
                    if (dsDropDownData.Tables[2].Rows.Count > 0)
                    {
                        objDashBOlocation.country_id = Convert.ToInt32(ddlCountry.SelectedValue);
                        dsDropDownDataLocation = objDashBL.getFilterDataBL(objDashBOlocation);
                        if (dsDropDownDataLocation.Tables.Count > 0)
                        {
                            if (dsDropDownDataLocation.Tables[2].Rows.Count > 0)
                            {
                                ddlLocation.DataSource = dsDropDownDataLocation.Tables[2];
                                ddlLocation.DataTextField = "location_name";
                                ddlLocation.DataValueField = "location_id";
                                ddlLocation.DataBind();
                                ddlLocation.Items.Insert(0, new ListItem("All", "0"));
                                ddlLocation.SelectedValue = Convert.ToString(location);
                            }
                            else
                            {
                                ddlLocation.Items.Clear();
                                ddlLocation.DataSource = null;
                                ddlLocation.DataBind();
                            }

                        }



                    }
                    else
                    {
                        ddlLocation.Items.Clear();
                        ddlLocation.DataSource = null;
                        ddlLocation.DataBind();
                    }
                    if (dsDropDownData.Tables[3].Rows.Count > 0)
                    {
                        ddlLineName.DataSource = dsDropDownData.Tables[3];
                        ddlLineName.DataTextField = "line_name";
                        ddlLineName.DataValueField = "line_id";
                        ddlLineName.DataBind();
                        ddlLineName.Items.Insert(0, new ListItem("All", "0"));
                    }
                    else
                    {
                        ddlLineName.Items.Clear();
                        ddlLineName.DataSource = null;
                        ddlLineName.DataBind();
                    }
                    //if (dsDropDownData.Tables[4].Rows.Count > 0)
                    //{
                    //    ddlBuilding.DataSource = dsDropDownData.Tables[4];
                    //    ddlBuilding.DataTextField = "building_name";
                    //    ddlBuilding.DataValueField = "building_id";
                    //    ddlBuilding.DataBind();
                    //    ddlBuilding.Items.Insert(0, new ListItem("All", "0"));
                    //    ddlBuilding.SelectedValue = Convert.ToString(objDashBO.building_id);
                    //}
                    //else
                    //{
                    //    ddlBuilding.Items.Clear();
                    //    ddlBuilding.DataSource = null;
                    //    ddlBuilding.DataBind();
                    //}
                }

            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        #endregion

        #region Methods
        private void LoadFilterDropDowns(DashboardBO objDashBO)
        {
            try
            {
                objDashBL = new DashboardBL();
                dsDropDownData = new DataSet();
                dsDropDownData = objDashBL.getFilterDataBL(objDashBO);
                if (dsDropDownData.Tables.Count > 0)
                {
                    if (dsDropDownData.Tables[0].Rows.Count > 0)
                    {
                        ddlRegion.DataSource = dsDropDownData.Tables[0];
                        ddlRegion.DataTextField = "region_name";
                        ddlRegion.DataValueField = "region_id";
                        ddlRegion.DataBind();
                        ddlRegion.Items.Insert(0, new ListItem("All", "0"));
                        ddlRegion.SelectedValue = Convert.ToString(Session["LoggedInRegionId"]);
                    }
                    else
                    {
                        ddlRegion.Items.Clear();
                        ddlRegion.DataSource = null;
                        ddlRegion.DataBind();
                    }
                }
                objDashBO.region_id = Convert.ToInt32(ddlRegion.SelectedValue);
                dsDropDownData = objDashBL.getFilterDataBL(objDashBO);
                if (dsDropDownData.Tables.Count > 0)
                {
                    if (dsDropDownData.Tables[1].Rows.Count > 0)
                    {
                        ddlCountry.DataSource = dsDropDownData.Tables[1];
                        ddlCountry.DataTextField = "country_name";
                        ddlCountry.DataValueField = "country_id";
                        ddlCountry.DataBind();
                        ddlCountry.Items.Insert(0, new ListItem("All", "0"));
                        ddlCountry.SelectedValue = Convert.ToString(Session["LoggedInCountryId"]);
                    }
                    else
                    {
                        ddlCountry.Items.Clear();
                        ddlCountry.DataSource = null;
                        ddlCountry.DataBind();
                    }
                }
                objDashBO.country_id = Convert.ToInt32(ddlCountry.SelectedValue);
                dsDropDownData = objDashBL.getFilterDataBL(objDashBO);
                if (dsDropDownData.Tables.Count > 0)
                {
                    if (dsDropDownData.Tables[2].Rows.Count > 0)
                    {
                        ddlLocation.DataSource = dsDropDownData.Tables[2];
                        ddlLocation.DataTextField = "location_name";
                        ddlLocation.DataValueField = "location_id";
                        ddlLocation.DataBind();
                        ddlLocation.Items.Insert(0, new ListItem("All", "0"));
                        ddlLocation.SelectedValue = Convert.ToString(Session["LocationId"]);
                    }
                    else
                    {
                        ddlLocation.Items.Clear();
                        ddlLocation.DataSource = null;
                        ddlLocation.DataBind();
                    }
                    DataTable dtBuilding = new DataTable();
                    objUserBO = new UsersBO();
                    objComBL = new CommonBL();
                    objUserBO.UserId = Convert.ToInt32(Session["UserId"]);
                    objUserBO.location_id = Convert.ToInt32(ddlLocation.SelectedValue);
                    dtBuilding = objComBL.GetBuildingListDropDownBL(objUserBO);

                    if (dtBuilding.Rows.Count > 0)
                    {
                        ddlBuilding.DataSource = dtBuilding;
                        ddlBuilding.DataTextField = "building_name";
                        ddlBuilding.DataValueField = "building_id";
                        ddlBuilding.DataBind();
                        ddlBuilding.Items.Insert(0, new ListItem("All", "0"));
                    }
                    else
                    {
                        ddlBuilding.Items.Clear();
                        ddlBuilding.DataSource = null;
                        ddlBuilding.DataBind();
                    }
                }

                objDashBO.location_id = Convert.ToInt32(ddlLocation.SelectedValue);
                dsDropDownData = objDashBL.getFilterDataBL(objDashBO);
                if (dsDropDownData.Tables.Count > 0)
                {

                    if (dsDropDownData.Tables[3].Rows.Count > 0)
                    {
                        ddlLineName.DataSource = dsDropDownData.Tables[3];
                        ddlLineName.DataTextField = "line_name";
                        ddlLineName.DataValueField = "line_id";
                        ddlLineName.DataBind();
                        ddlLineName.Items.Insert(0, new ListItem("All", "0"));
                    }
                    else
                    {
                        ddlLineName.Items.Clear();
                        ddlLineName.DataSource = null;
                        ddlLineName.DataBind();
                    }
                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        private void LoadDropDowns()
        {
            DataTable dtLine = new DataTable();
            DataTable dtCountry = new DataTable();
            DataTable dtLocation = new DataTable();

            try
            {
                dsDropDownData = new DataSet();
                objUserBO = new UsersBO();
                objUserBO.UserId = Convert.ToInt32(Session["UserId"]);
                objComBL = new CommonBL();
                dsDropDownData = objComBL.GetAllMasterBL(objUserBO);

                if (dsDropDownData.Tables.Count > 0)
                {
                    if (dsDropDownData.Tables[0].Rows.Count > 0)
                    {
                        ddlRegion.DataSource = dsDropDownData.Tables[0];
                        ddlRegion.DataTextField = "region_name";
                        ddlRegion.DataValueField = "region_id";
                        ddlRegion.DataBind();
                        ddlRegion.Items.Insert(0, new ListItem("All", "0"));
                    }
                    else
                    {
                        ddlRegion.Items.Clear();
                        ddlRegion.DataSource = null;
                        ddlRegion.DataBind();
                    }
                    if (dsDropDownData.Tables[1].Rows.Count > 0)
                    {
                        ddlCountry.DataSource = dsDropDownData.Tables[1];
                        ddlCountry.DataTextField = "country_name";
                        ddlCountry.DataValueField = "country_id";
                        ddlCountry.DataBind();
                        ddlCountry.Items.Insert(0, new ListItem("All", "0"));
                    }
                    else
                    {
                        ddlCountry.DataSource = null;
                        ddlCountry.DataBind();
                    }
                    if (dsDropDownData.Tables[2].Rows.Count > 0)
                    {
                        ddlLocation.DataSource = dsDropDownData.Tables[2];
                        ddlLocation.DataTextField = "location_name";
                        ddlLocation.DataValueField = "location_id";
                        ddlLocation.DataBind();
                        ddlLocation.Items.Insert(0, new ListItem("All", "0"));
                    }
                    else
                    {
                        ddlLocation.DataSource = null;
                        ddlLocation.DataBind();
                    }
                }
                //if (!string.IsNullOrEmpty(Convert.ToString(ddlRegion.SelectedValue)))
                //{
                //    objUserBO.region_id = Convert.ToInt32(ddlRegion.SelectedValue);
                //    dtCountry = objComBL.GetCountryBasedOnRegionBL(objUserBO);

                //    if (dtCountry.Rows.Count > 0)
                //    {
                //        ddlCountry.DataSource = dtCountry;
                //        ddlCountry.DataTextField = "country_name";
                //        ddlCountry.DataValueField = "country_id";
                //        ddlCountry.DataBind();
                //        ddlCountry.Items.Insert(0,new ListItem("All",""));
                //    }
                //    else
                //    {
                //        ddlCountry.Items.Clear();
                //        ddlCountry.DataSource = null;
                //        ddlCountry.DataBind();
                //    }
                //}
                //else
                //{
                //    ddlCountry.Items.Clear();
                //    ddlCountry.DataSource = null;
                //    ddlCountry.DataBind();
                //}
                //if (!string.IsNullOrEmpty(Convert.ToString(ddlCountry.SelectedValue)))
                //{
                //    objUserBO.country_id = Convert.ToInt32(ddlCountry.SelectedValue);
                //    dtLocation = objComBL.GetLocationBasedOnCountryBL(objUserBO);
                //    if (dtLocation.Rows.Count > 0)
                //    {
                //        ddlLocation.DataSource = dtLocation;
                //        ddlLocation.DataTextField = "location_name";
                //        ddlLocation.DataValueField = "location_id";
                //        ddlLocation.DataBind();
                //        ddlLocation.Items.Insert(0, new ListItem("All", ""));
                //    }
                //    else
                //    {
                //        ddlLocation.Items.Clear();
                //        ddlLocation.DataSource = null;
                //        ddlLocation.DataBind();
                //    }
                //}
                //else
                //{
                //    ddlLocation.Items.Clear();
                //    ddlLocation.DataSource = null;
                //    ddlLocation.DataBind();
                //}
                if (!string.IsNullOrEmpty(Convert.ToString(ddlLocation.SelectedValue)))
                {
                    objUserBO.location_id = Convert.ToInt32(ddlLocation.SelectedValue);
                    dtLine = objComBL.GetLineListDropDownBL(objUserBO);
                    if (dtLine.Rows.Count > 0)
                    {
                        ddlLineName.DataSource = dtLine;
                        ddlLineName.DataTextField = "line_name";
                        ddlLineName.DataValueField = "line_id";
                        ddlLineName.DataBind();
                        ddlLineName.Items.Insert(0, new ListItem("All", "0"));
                        Session["dtLine"] = dtLine;
                    }
                    else
                    {
                        ddlLineName.Items.Clear();
                        ddlLineName.DataSource = null;
                        ddlLineName.DataBind();
                    }
                }
                else
                {
                    objUserBO.location_id = Convert.ToInt32(ddlLocation.SelectedIndex);
                    dtLine = objComBL.GetLineListDropDownBL(objUserBO);
                    if (dtLine.Rows.Count > 0)
                    {
                        ddlLineName.DataSource = dtLine;
                        ddlLineName.DataTextField = "line_name";
                        ddlLineName.DataValueField = "line_id";
                        ddlLineName.DataBind();
                        ddlLineName.Items.Insert(0, new ListItem("All", "0"));
                        Session["dtLine"] = dtLine;
                    }
                    else
                    {
                        ddlLineName.Items.Clear();
                        ddlLineName.DataSource = null;
                        ddlLineName.DataBind();
                    }
                }

            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }


        [WebMethod]
        public static string returnChartLPAResult()
        {
            DataTable dtReport;
            CommonFunctions objCom = new CommonFunctions();
            DashboardBO objDashBO;
            DashboardBL objDashBL;
            string output = string.Empty;
            try
            {
                objDashBO = new DashboardBO();
                objDashBL = new DashboardBL();

                dtReport = new DataTable();
                objDashBO.building_id = Convert.ToInt32(HttpContext.Current.Session["building"]);
                objDashBO.location_id = Convert.ToInt32(HttpContext.Current.Session["location"]);
                objDashBO.region_id = Convert.ToInt32(HttpContext.Current.Session["region"]);
                objDashBO.country_id = Convert.ToInt32(HttpContext.Current.Session["country"]);
                objDashBO.line_id = Convert.ToInt32(HttpContext.Current.Session["line"]);
                string UIpattern = "dd-MM-yyyy";
                string DBPattern = "MM-dd-yyyy";
                DateTime parsedDate;
                if (DateTime.TryParseExact(Convert.ToString(HttpContext.Current.Session["date"]), UIpattern, null, DateTimeStyles.None, out parsedDate))
                {
                    string reportdate = parsedDate.ToString(DBPattern);
                    // objPlanBO.p_planned_start_date = Convert.ToDateTime(plannedstartdate);
                    // objDashBO.reportdate = DateTime.Parse(reportdate);
                    objDashBO.reportdate = DateTime.ParseExact(reportdate, "MM-dd-yyyy", null);
                }

                dtReport = objDashBL.getDataforLPAResultsReportBL(objDashBO);
                dtReport.Columns.Add("color");
                for (int i = 0; i < dtReport.Rows.Count; i++)
                {
                    dtReport.Rows[i]["color"] = "#00732d";
                    dtReport.Rows[i]["average_score"] = Convert.ToInt32(Convert.ToDecimal(dtReport.Rows[i]["average_score"]) * 100);
                    dtReport.Rows[i]["previous_score"] = Convert.ToInt32(Convert.ToDecimal(dtReport.Rows[i]["previous_score"]) * 100);
                }
                output = DataTableToJSONWithStringBuilder(dtReport);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            return output;
        }

        [WebMethod]
        public static string returnData()
        {
            DataTable dtReport;
            CommonFunctions objCom = new CommonFunctions();
            DashboardBO objDashBO;
            DashboardBL objDashBL;
            string output = string.Empty;
            try
            {
                objDashBO = new DashboardBO();
                objDashBL = new DashboardBL();

                dtReport = new DataTable();
                objDashBO.building_id = Convert.ToInt32(HttpContext.Current.Session["building"]);
                objDashBO.location_id = Convert.ToInt32(HttpContext.Current.Session["location"]);
                objDashBO.region_id = Convert.ToInt32(HttpContext.Current.Session["region"]);
                objDashBO.country_id = Convert.ToInt32(HttpContext.Current.Session["country"]);
                objDashBO.line_id = Convert.ToInt32(HttpContext.Current.Session["line"]);
                string UIpattern = "dd-MM-yyyy";
                string DBPattern = "MM-dd-yyyy";
                DateTime parsedDate;
                if (DateTime.TryParseExact(Convert.ToString(HttpContext.Current.Session["date"]), UIpattern, null, DateTimeStyles.None, out parsedDate))
                {
                    string reportdate = parsedDate.ToString(DBPattern);
                    // objPlanBO.p_planned_start_date = Convert.ToDateTime(plannedstartdate);
                    // objDashBO.reportdate = DateTime.Parse(reportdate);
                    objDashBO.reportdate = DateTime.ParseExact(reportdate, "MM-dd-yyyy", null);
                }

                dtReport = objDashBL.getLPAResultPerSectionBL(objDashBO);
                dtReport.Columns.Add("color");
                for (int i = 0; i < dtReport.Rows.Count; i++)
                {
                    dtReport.Rows[i]["color"] = "#00732d";
                    dtReport.Rows[i]["Score"] = Convert.ToInt32(Convert.ToDecimal(dtReport.Rows[i]["Score"]) * 100);

                }
                output = DataTableToJSONWithStringBuilder(dtReport);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            return output;
        }

        [WebMethod]
        public static string returnChartPerSectionMonthly()
        {
            DataTable dtReport;
            CommonFunctions objCom = new CommonFunctions();
            DashboardBO objDashBO;
            DashboardBL objDashBL;
            string output = string.Empty;
            try
            {
                objDashBO = new DashboardBO();
                objDashBL = new DashboardBL();

                dtReport = new DataTable();
                objDashBO.building_id = Convert.ToInt32(HttpContext.Current.Session["building"]);
                objDashBO.location_id = Convert.ToInt32(HttpContext.Current.Session["location"]);
                objDashBO.region_id = Convert.ToInt32(HttpContext.Current.Session["region"]);
                objDashBO.country_id = Convert.ToInt32(HttpContext.Current.Session["country"]);
                objDashBO.line_id = Convert.ToInt32(HttpContext.Current.Session["line"]);
                string UIpattern = "dd-MM-yyyy";
                string DBPattern = "MM-dd-yyyy";
                DateTime parsedDate;
                if (DateTime.TryParseExact(Convert.ToString(HttpContext.Current.Session["date"]), UIpattern, null, DateTimeStyles.None, out parsedDate))
                {
                    string reportdate = parsedDate.ToString(DBPattern);
                    // objPlanBO.p_planned_start_date = Convert.ToDateTime(plannedstartdate);
                    // objDashBO.reportdate = DateTime.Parse(reportdate);
                    objDashBO.reportdate = DateTime.ParseExact(reportdate, "MM-dd-yyyy", null);
                }

                dtReport = objDashBL.getLPAResultPerSectionMonthlyBL(objDashBO);
                for (int i = 0; i < dtReport.Rows.Count; i++)
                {

                    dtReport.Rows[i]["GT"] = Convert.ToInt32(Convert.ToDecimal(dtReport.Rows[i]["GT"]) * 100);
                    dtReport.Rows[i]["A"] = Convert.ToInt32(Convert.ToDecimal(dtReport.Rows[i]["A"]) * 100);
                    dtReport.Rows[i]["B"] = Convert.ToInt32(Convert.ToDecimal(dtReport.Rows[i]["B"]) * 100);
                    dtReport.Rows[i]["C"] = Convert.ToInt32(Convert.ToDecimal(dtReport.Rows[i]["C"]) * 100);
                    dtReport.Rows[i]["D"] = Convert.ToInt32(Convert.ToDecimal(dtReport.Rows[i]["D"]) * 100);
                    dtReport.Rows[i]["E"] = Convert.ToInt32(Convert.ToDecimal(dtReport.Rows[i]["E"]) * 100);
                }
                output = DataTableToJSONWithStringBuilder(dtReport);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            return output;
        }

        [WebMethod]
        public static string returnChartPerLine()
        {
            DataTable dtReport;
            CommonFunctions objCom = new CommonFunctions();
            DashboardBO objDashBO;
            DashboardBL objDashBL;
            string output = string.Empty;
            try
            {
                objDashBO = new DashboardBO();
                objDashBL = new DashboardBL();

                dtReport = new DataTable();
                objDashBO.building_id = Convert.ToInt32(HttpContext.Current.Session["building"]);
                objDashBO.location_id = Convert.ToInt32(HttpContext.Current.Session["location"]);
                objDashBO.region_id = Convert.ToInt32(HttpContext.Current.Session["region"]);
                objDashBO.country_id = Convert.ToInt32(HttpContext.Current.Session["country"]);
                objDashBO.line_id = Convert.ToInt32(HttpContext.Current.Session["line"]);
                string UIpattern = "dd-MM-yyyy";
                string DBPattern = "MM-dd-yyyy";
                DateTime parsedDate;
                if (DateTime.TryParseExact(Convert.ToString(HttpContext.Current.Session["date"]), UIpattern, null, DateTimeStyles.None, out parsedDate))
                {
                    string reportdate = parsedDate.ToString(DBPattern);
                    // objPlanBO.p_planned_start_date = Convert.ToDateTime(plannedstartdate);
                    // objDashBO.reportdate = DateTime.Parse(reportdate);
                    objDashBO.reportdate = DateTime.ParseExact(reportdate, "MM-dd-yyyy", null);
                }

                dtReport = objDashBL.getLPAResultByLineBL(objDashBO);
                dtReport.Columns.Add("color");
                for (int i = 0; i < dtReport.Rows.Count; i++)
                {
                    dtReport.Rows[i]["color"] = "#00732d";
                    dtReport.Rows[i]["average_score"] = Convert.ToInt32(Convert.ToDecimal(dtReport.Rows[i]["average_score"]) * 100);
                }
                output = DataTableToJSONWithStringBuilder(dtReport);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            return output;
        }

        [WebMethod]
        public static string returnChartPerQuestion()
        {
            DataTable dtReport;
            CommonFunctions objCom = new CommonFunctions();
            DashboardBO objDashBO;
            DashboardBL objDashBL;
            string output = string.Empty;
            try
            {
                objDashBO = new DashboardBO();
                objDashBL = new DashboardBL();

                dtReport = new DataTable();
                objDashBO.building_id = Convert.ToInt32(HttpContext.Current.Session["building"]);
                objDashBO.location_id = Convert.ToInt32(HttpContext.Current.Session["location"]);
                objDashBO.region_id = Convert.ToInt32(HttpContext.Current.Session["region"]);
                objDashBO.country_id = Convert.ToInt32(HttpContext.Current.Session["country"]);
                objDashBO.line_id = Convert.ToInt32(HttpContext.Current.Session["line"]);
                string UIpattern = "dd-MM-yyyy";
                string DBPattern = "MM-dd-yyyy";
                DateTime parsedDate;
                if (DateTime.TryParseExact(Convert.ToString(HttpContext.Current.Session["date"]), UIpattern, null, DateTimeStyles.None, out parsedDate))
                {
                    string reportdate = parsedDate.ToString(DBPattern);
                    // objPlanBO.p_planned_start_date = Convert.ToDateTime(plannedstartdate);
                    // objDashBO.reportdate = DateTime.Parse(reportdate);
                    objDashBO.reportdate = DateTime.ParseExact(reportdate, "MM-dd-yyyy", null);
                }

                dtReport = objDashBL.getLPAResultByQuestionBL(objDashBO);
                dtReport.Columns.Add("color");
                dtReport.Rows[0]["color"] = "#00732d";
                dtReport.Rows[1]["color"] = "#3ca014";
                dtReport.Rows[2]["color"] = "#3ca014";
                dtReport.Rows[3]["color"] = "#3ca014";
                dtReport.Rows[4]["color"] = "#eb690f";
                dtReport.Rows[5]["color"] = "#eb690f";
                dtReport.Rows[6]["color"] = "#eb690f";
                dtReport.Rows[7]["color"] = "#eb690f";
                dtReport.Rows[8]["color"] = "#eb690f";
                dtReport.Rows[9]["color"] = "#eb690f";
                dtReport.Rows[10]["color"] = "#be001e";
                dtReport.Rows[11]["color"] = "#be001e";
                dtReport.Rows[12]["color"] = "#7f7f7f";
                dtReport.Rows[13]["color"] = "#7f7f7f";
                dtReport.Rows[14]["color"] = "#7f7f7f";
                dtReport.Rows[15]["color"] = "#7f7f7f";
                dtReport.Rows[16]["color"] = "#7f7f7f";
                dtReport.Rows[17]["color"] = "#7f7f7f";
                dtReport.Rows[18]["color"] = "#7f7f7f";
                dtReport.Rows[19]["color"] = "#7f7f7f";
                dtReport.Rows[20]["color"] = "#7f7f7f";
                dtReport.Rows[21]["color"] = "#7f7f7f";
                dtReport.Rows[22]["color"] = "#d9d9d9";
                dtReport.Rows[23]["color"] = "#d9d9d9";
                dtReport.Rows[24]["color"] = "#d9d9d9";

                for (int i = 0; i < dtReport.Rows.Count; i++)
                {
                    dtReport.Rows[i]["score"] = Convert.ToInt32(Convert.ToDecimal(dtReport.Rows[i]["score"]) * 100);
                    dtReport.Rows[i]["question_id"] = Convert.ToString(dtReport.Rows[i]["question_id"]).Replace("q", "");
                }
                output = DataTableToJSONWithStringBuilder(dtReport);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            return output;
        }

        [WebMethod]
        public static string returnchartComparision()
        {
            DataTable dtReport;
            CommonFunctions objCom = new CommonFunctions();
            DashboardBO objDashBO;
            DashboardBL objDashBL;
            string output = string.Empty;
            try
            {
                objDashBO = new DashboardBO();
                objDashBL = new DashboardBL();

                dtReport = new DataTable();
                objDashBO.building_id = Convert.ToInt32(HttpContext.Current.Session["building"]);
                objDashBO.location_id = Convert.ToInt32(HttpContext.Current.Session["location"]);
                objDashBO.region_id = Convert.ToInt32(HttpContext.Current.Session["region"]);
                objDashBO.country_id = Convert.ToInt32(HttpContext.Current.Session["country"]);
                objDashBO.line_id = Convert.ToInt32(HttpContext.Current.Session["line"]);
                string UIpattern = "dd-MM-yyyy";
                string DBPattern = "MM-dd-yyyy";
                DateTime parsedDate;
                if (DateTime.TryParseExact(Convert.ToString(HttpContext.Current.Session["date"]), UIpattern, null, DateTimeStyles.None, out parsedDate))
                {
                    string reportdate = parsedDate.ToString(DBPattern);
                    // objPlanBO.p_planned_start_date = Convert.ToDateTime(plannedstartdate);
                    // objDashBO.reportdate = DateTime.Parse(reportdate);
                    objDashBO.reportdate = DateTime.ParseExact(reportdate, "MM-dd-yyyy", null);
                }

                dtReport = objDashBL.getDataforLPAComparisionReportBL(objDashBO);
                dtReport.Columns.Add("color");
                for (int i = 0; i < dtReport.Rows.Count; i++)
                {
                    dtReport.Rows[i]["color"] = "#00732d";
                    dtReport.Rows[i]["cy_perform_rate"] = Convert.ToInt32(Convert.ToDecimal(dtReport.Rows[i]["cy_perform_rate"]) * 1);
                    dtReport.Rows[i]["ly_perform_rate"] = Convert.ToInt32(Convert.ToDecimal(dtReport.Rows[i]["ly_perform_rate"]) * 1);
                }
                output = DataTableToJSONWithStringBuilder(dtReport);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            return output;
        }

        public static string DataTableToJSONWithStringBuilder(DataTable table)
        {
            var JSONString = new StringBuilder();
            if (table.Rows.Count > 0)
            {
                JSONString.Append("[");
                for (int i = 0; i < table.Rows.Count; i++)
                {
                    JSONString.Append("{");
                    for (int j = 0; j < table.Columns.Count; j++)
                    {
                        if (j < table.Columns.Count - 1)
                        {
                            JSONString.Append("\"" + table.Columns[j].ColumnName.ToString() + "\":" + "\"" + table.Rows[i][j].ToString() + "\",");
                        }
                        else if (j == table.Columns.Count - 1)
                        {
                            JSONString.Append("\"" + table.Columns[j].ColumnName.ToString() + "\":" + "\"" + table.Rows[i][j].ToString() + "\"");
                        }
                    }
                    if (i == table.Rows.Count - 1)
                    {
                        JSONString.Append("}");
                    }
                    else
                    {
                        JSONString.Append("},");
                    }
                }
                JSONString.Append("]");
            }
            return JSONString.ToString();
        }

        public void LoadSecLabels()
        {
            CommonFunctions objCom = new CommonFunctions();
            DashboardBO objDashBO;
            DashboardBL objDashBL;
            DataTable dtReport1 = new DataTable();
            string output = string.Empty;

            try
            {
                objDashBO = new DashboardBO();
                objDashBL = new DashboardBL();

                dtReport = new DataTable();
                objDashBO.building_id = Convert.ToInt32(HttpContext.Current.Session["building"]);
                objDashBO.location_id = Convert.ToInt32(HttpContext.Current.Session["location"]);
                objDashBO.region_id = Convert.ToInt32(HttpContext.Current.Session["region"]);
                objDashBO.country_id = Convert.ToInt32(HttpContext.Current.Session["country"]);
                objDashBO.line_id = Convert.ToInt32(HttpContext.Current.Session["line"]);
                string UIpattern = "dd-MM-yyyy";
                string DBPattern = "MM-dd-yyyy";
                DateTime parsedDate;
                if (DateTime.TryParseExact(Convert.ToString(HttpContext.Current.Session["date"]), UIpattern, null, DateTimeStyles.None, out parsedDate))
                {
                    string reportdate = parsedDate.ToString(DBPattern);
                    // objPlanBO.p_planned_start_date = Convert.ToDateTime(plannedstartdate);
                    // objDashBO.reportdate = DateTime.Parse(reportdate);
                    objDashBO.reportdate = DateTime.ParseExact(reportdate, "MM-dd-yyyy", null);
                }
                dtReport1 = objDashBL.getLPAResultPerSectionBL(objDashBO);

                if (dtReport1.Rows.Count > 0)
                {
                    sec1.Text = Convert.ToString(dtReport1.Rows[0]["Sec_name_abbr"]) + " - " + Convert.ToString(dtReport1.Rows[0]["section_name"]);
                    sec2.Text = Convert.ToString(dtReport1.Rows[1]["Sec_name_abbr"]) + " - " + Convert.ToString(dtReport1.Rows[1]["section_name"]);
                    sec3.Text = Convert.ToString(dtReport1.Rows[2]["Sec_name_abbr"]) + " - " + Convert.ToString(dtReport1.Rows[2]["section_name"]);
                    sec4.Text = Convert.ToString(dtReport1.Rows[3]["Sec_name_abbr"]) + " - " + Convert.ToString(dtReport1.Rows[3]["section_name"]);
                    sec5.Text = Convert.ToString(dtReport1.Rows[4]["Sec_name_abbr"]) + " - " + Convert.ToString(dtReport1.Rows[4]["section_name"]);
                    sec6.Text = Convert.ToString(dtReport1.Rows[5]["Sec_name_abbr"]) + " - " + Convert.ToString(dtReport1.Rows[5]["section_name"]);
                }
                else
                {
                    sec1.Text = "";
                    sec2.Text = "";
                    sec3.Text = "";
                    sec4.Text = "";
                    sec5.Text = "";
                    sec6.Text = "";
                }

            }

            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        [WebMethod]
        public static string returnChartPerLineBottom()
        {
            DataTable dtReport;
            CommonFunctions objCom = new CommonFunctions();
            DashboardBO objDashBO;
            DashboardBL objDashBL;
            string output = string.Empty;
            try
            {
                objDashBO = new DashboardBO();
                objDashBL = new DashboardBL();

                dtReport = new DataTable();
                objDashBO.building_id = Convert.ToInt32(HttpContext.Current.Session["building"]);
                objDashBO.location_id = Convert.ToInt32(HttpContext.Current.Session["location"]);
                objDashBO.region_id = Convert.ToInt32(HttpContext.Current.Session["region"]);
                objDashBO.country_id = Convert.ToInt32(HttpContext.Current.Session["country"]);
                objDashBO.line_id = Convert.ToInt32(HttpContext.Current.Session["line"]);
                string UIpattern = "dd-MM-yyyy";
                string DBPattern = "MM-dd-yyyy";
                DateTime parsedDate;
                if (DateTime.TryParseExact(Convert.ToString(HttpContext.Current.Session["date"]), UIpattern, null, DateTimeStyles.None, out parsedDate))
                {
                    string reportdate = parsedDate.ToString(DBPattern);
                    // objPlanBO.p_planned_start_date = Convert.ToDateTime(plannedstartdate);
                    // objDashBO.reportdate = DateTime.Parse(reportdate);
                    objDashBO.reportdate = DateTime.ParseExact(reportdate, "MM-dd-yyyy", null);
                }

                dtReport = objDashBL.getLPAResultByLineBottomBL(objDashBO);
                dtReport.Columns.Add("color");
                for (int i = 0; i < dtReport.Rows.Count; i++)
                {
                    dtReport.Rows[i]["color"] = "#00732d";
                    dtReport.Rows[i]["average_score"] = Convert.ToInt32(Convert.ToDecimal(dtReport.Rows[i]["average_score"]) * 100);
                }
                output = DataTableToJSONWithStringBuilder(dtReport);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            return output;
        }
        #endregion


    }
}