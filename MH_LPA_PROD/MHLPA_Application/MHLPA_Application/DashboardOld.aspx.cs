﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MHLPA_BusinessObject;
using MHLPA_BusinessLogic;
using System.Globalization;
using System.Web.UI.DataVisualization.Charting;
using System.Drawing;

namespace MHLPA_Application
{
    public partial class DashboardOld : System.Web.UI.Page
    {
        #region Global Declaration
        CommonFunctions objCom = new CommonFunctions();
        DataSet dsDropDownData;
        UsersBO objUserBO;
        DataTable dtAnswer = new DataTable();
        CommonBL objComBL;
        DataTable dtReport;
        DashboardBO objDashBO;
        DashboardBL objDashBL;
        private readonly Random random = new Random(1234);
        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    objDashBO = new DashboardBO();
                    //string reportserver = ConfigurationManager.AppSettings["ReportServer"].ToString();
                    //rvSiteMapping.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Remote;
                    ////rvSiteMapping.ServerReport.ReportServerCredentials=new report
                    //rvSiteMapping.ServerReport.ReportServerUrl = new Uri(reportserver); // Add the Reporting Server URL 
                    //rvSiteMapping.ServerReport.ReportPath = String.Format("/{0}/{1}", "MHLPA_Reports", "Dashboard");
                    //rvSiteMapping.ServerReport.Refresh();
                   // LoadDropDowns();
                    LoadFilterDropDowns(objDashBO);
                }
                catch (Exception ex)
                {
                    objCom.ErrorLog(ex);
                }
            }
        }

        protected void ddlRegion_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dtLineProduct = new DataTable();
            DataTable dtCountry = new DataTable();
            DataTable dtLocation = new DataTable();
            try
            {
                objDashBO = new DashboardBO();
                objDashBO.region_id = Convert.ToInt32(ddlRegion.SelectedValue);
                objDashBO.country_id = Convert.ToInt32(ddlCountry.SelectedValue);
                objDashBO.location_id = Convert.ToInt32(ddlLocation.SelectedValue);
                objDashBO.line_id = Convert.ToInt32(ddlLineName.SelectedValue);
                objDashBO.selection_flag = "Region";
               // LoadFilterDropDowns(objDashBO);


                objDashBL = new DashboardBL();
                dsDropDownData = new DataSet();
                dsDropDownData = objDashBL.getFilterDataBL(objDashBO);
                if (dsDropDownData.Tables.Count > 0)
                {
                    if (dsDropDownData.Tables[0].Rows.Count > 0)
                    {
                        ddlRegion.DataSource = dsDropDownData.Tables[0];
                        ddlRegion.DataTextField = "region_name";
                        ddlRegion.DataValueField = "region_id";
                        ddlRegion.DataBind();
                    }
                    else
                    {
                        ddlRegion.Items.Clear();
                        ddlRegion.DataSource = null;
                        ddlRegion.DataBind();
                    }
                    if (dsDropDownData.Tables[1].Rows.Count > 0)
                    {
                        ddlCountry.DataSource = dsDropDownData.Tables[1];
                        ddlCountry.DataTextField = "country_name";
                        ddlCountry.DataValueField = "country_id";
                        ddlCountry.DataBind();
                        ddlCountry.Items.Insert(0, new ListItem("All", "0"));
                    }
                    else
                    {
                        ddlCountry.Items.Clear();
                        ddlCountry.DataSource = null;
                        ddlCountry.DataBind();
                    }
                    if (dsDropDownData.Tables[2].Rows.Count > 0)
                    {
                        ddlLocation.DataSource = dsDropDownData.Tables[2];
                        ddlLocation.DataTextField = "location_name";
                        ddlLocation.DataValueField = "location_id";
                        ddlLocation.DataBind();
                        ddlLocation.Items.Insert(0, new ListItem("All", "0"));
                    }
                    else
                    {
                        ddlLocation.Items.Clear();
                        ddlLocation.DataSource = null;
                        ddlLocation.DataBind();
                    }
                    if (dsDropDownData.Tables[3].Rows.Count > 0)
                    {
                        ddlLineName.DataSource = dsDropDownData.Tables[3];
                        ddlLineName.DataTextField = "line_name";
                        ddlLineName.DataValueField = "line_id";
                        ddlLineName.DataBind();
                        ddlLineName.Items.Insert(0, new ListItem("All", "0"));
                    }
                    else
                    {
                        ddlLineName.Items.Clear();
                        ddlLineName.DataSource = null;
                        ddlLineName.DataBind();
                    }
                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        protected void ddlCountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dtLineProduct = new DataTable();
            DataTable dtCountry = new DataTable();
            DataTable dtLocation = new DataTable();
            try
            {
                objDashBO = new DashboardBO();
                objDashBO.region_id = Convert.ToInt32(ddlRegion.SelectedValue);
                objDashBO.country_id = Convert.ToInt32(ddlCountry.SelectedValue);
                objDashBO.location_id = Convert.ToInt32(ddlLocation.SelectedValue);
                objDashBO.line_id = Convert.ToInt32(ddlLineName.SelectedValue);
                objDashBO.selection_flag = "Country";
               // LoadFilterDropDowns(objDashBO);

                objDashBL = new DashboardBL();
                dsDropDownData = new DataSet();
                dsDropDownData = objDashBL.getFilterDataBL(objDashBO);
                if (dsDropDownData.Tables.Count > 0)
                {
                    if (dsDropDownData.Tables[0].Rows.Count > 0)
                    {
                        ddlRegion.DataSource = dsDropDownData.Tables[0];
                        ddlRegion.DataTextField = "region_name";
                        ddlRegion.DataValueField = "region_id";
                        ddlRegion.DataBind();
                    }
                    else
                    {
                        ddlRegion.Items.Clear();
                        ddlRegion.DataSource = null;
                        ddlRegion.DataBind();
                    }
                    if (dsDropDownData.Tables[1].Rows.Count > 0)
                    {
                        ddlCountry.DataSource = dsDropDownData.Tables[1];
                        ddlCountry.DataTextField = "country_name";
                        ddlCountry.DataValueField = "country_id";
                        ddlCountry.DataBind();
                    }
                    else
                    {
                        ddlCountry.Items.Clear();
                        ddlCountry.DataSource = null;
                        ddlCountry.DataBind();
                    }
                    if (dsDropDownData.Tables[2].Rows.Count > 0)
                    {
                        ddlLocation.DataSource = dsDropDownData.Tables[2];
                        ddlLocation.DataTextField = "location_name";
                        ddlLocation.DataValueField = "location_id";
                        ddlLocation.DataBind();
                        ddlLocation.Items.Insert(0, new ListItem("All", "0"));
                    }
                    else
                    {
                        ddlLocation.Items.Clear();
                        ddlLocation.DataSource = null;
                        ddlLocation.DataBind();
                    }
                    if (dsDropDownData.Tables[3].Rows.Count > 0)
                    {
                        ddlLineName.DataSource = dsDropDownData.Tables[3];
                        ddlLineName.DataTextField = "line_name";
                        ddlLineName.DataValueField = "line_id";
                        ddlLineName.DataBind();
                        ddlLineName.Items.Insert(0, new ListItem("All", "0"));
                    }
                    else
                    {
                        ddlLineName.Items.Clear();
                        ddlLineName.DataSource = null;
                        ddlLineName.DataBind();
                    }
                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        protected void ddlLocation_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dtLineProduct = new DataTable();
            try
            {
                objDashBO = new DashboardBO();
                objDashBO.region_id = Convert.ToInt32(ddlRegion.SelectedValue);
                objDashBO.country_id = Convert.ToInt32(ddlCountry.SelectedValue);
                objDashBO.location_id = Convert.ToInt32(ddlLocation.SelectedValue);
                objDashBO.line_id = Convert.ToInt32(ddlLineName.SelectedValue);
                objDashBO.selection_flag = "Location";
               // LoadFilterDropDowns(objDashBO);

                objDashBL = new DashboardBL();
                dsDropDownData = new DataSet();
                dsDropDownData = objDashBL.getFilterDataBL(objDashBO);
                if (dsDropDownData.Tables.Count > 0)
                {
                    if (dsDropDownData.Tables[0].Rows.Count > 0)
                    {
                        ddlRegion.DataSource = dsDropDownData.Tables[0];
                        ddlRegion.DataTextField = "region_name";
                        ddlRegion.DataValueField = "region_id";
                        ddlRegion.DataBind();
                    }
                    else
                    {
                        ddlRegion.Items.Clear();
                        ddlRegion.DataSource = null;
                        ddlRegion.DataBind();
                    }
                    if (dsDropDownData.Tables[1].Rows.Count > 0)
                    {
                        ddlCountry.DataSource = dsDropDownData.Tables[1];
                        ddlCountry.DataTextField = "country_name";
                        ddlCountry.DataValueField = "country_id";
                        ddlCountry.DataBind();
                    }
                    else
                    {
                        ddlCountry.Items.Clear();
                        ddlCountry.DataSource = null;
                        ddlCountry.DataBind();
                    }
                    if (dsDropDownData.Tables[2].Rows.Count > 0)
                    {
                        ddlLocation.DataSource = dsDropDownData.Tables[2];
                        ddlLocation.DataTextField = "location_name";
                        ddlLocation.DataValueField = "location_id";
                        ddlLocation.DataBind();
                    }
                    else
                    {
                        ddlLocation.Items.Clear();
                        ddlLocation.DataSource = null;
                        ddlLocation.DataBind();
                    }
                    if (dsDropDownData.Tables[3].Rows.Count > 0)
                    {
                        ddlLineName.DataSource = dsDropDownData.Tables[3];
                        ddlLineName.DataTextField = "line_name";
                        ddlLineName.DataValueField = "line_id";
                        ddlLineName.DataBind();
                        ddlLineName.Items.Insert(0, new ListItem("All", "0"));
                    }
                    else
                    {
                        ddlLineName.Items.Clear();
                        ddlLineName.DataSource = null;
                        ddlLineName.DataBind();
                    }
                }

            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        protected void btnFilter_Click(object sender, EventArgs e)
        {
            try
            {
                objDashBO = new DashboardBO();
                objDashBL = new DashboardBL();

                dtReport = new DataTable();
                objDashBO.location_id = Convert.ToInt32(ddlLocation.SelectedValue);
                objDashBO.region_id = Convert.ToInt32(ddlRegion.SelectedValue);
                objDashBO.country_id = Convert.ToInt32(ddlCountry.SelectedValue);
                objDashBO.line_id = Convert.ToInt32(ddlLineName.SelectedValue);
                string UIpattern = "dd-MM-yyyy";
                string DBPattern = "MM-dd-yyyy";
                DateTime parsedDate;
                if (DateTime.TryParseExact(txtDate.Text, UIpattern, null, DateTimeStyles.None, out parsedDate))
                {
                    string reportdate = parsedDate.ToString(DBPattern);
                    // objPlanBO.p_planned_start_date = Convert.ToDateTime(plannedstartdate);
                    // objDashBO.reportdate = DateTime.Parse(reportdate);
                    objDashBO.reportdate = DateTime.ParseExact(reportdate, "MM-dd-yyyy", null);
                }
                #region LPA Result

                dtReport = objDashBL.getDataforLPAResultsReportBL(objDashBO);
                if (dtReport.Rows.Count > 0)
                {
                    pnlReport.Visible = true;

                    DataTable ChartData = dtReport;
                    string[] XPointMember = new string[ChartData.Rows.Count];
                    int[] YPointMember = new int[ChartData.Rows.Count];

                    for (int count = 0; count < ChartData.Rows.Count; count++)
                    {
                        //storing Values for X axis  
                        XPointMember[count] = ChartData.Rows[count]["audit_period"].ToString();
                        //storing values for Y Axis  
                        YPointMember[count] = Convert.ToInt32(Convert.ToDecimal(ChartData.Rows[count]["average_score"]) * 100);
                    }
                    Chart1.Series[1].Points.DataBindXY(XPointMember, YPointMember);
                    Chart1.Series[1].ChartType = SeriesChartType.Column;
                    Chart1.Series[1].Color = Color.DarkGreen;

                    for (int count = 0; count < ChartData.Rows.Count; count++)
                    {
                        //storing Values for X axis  
                        XPointMember[count] = ChartData.Rows[count]["audit_period"].ToString();
                        //storing values for Y Axis  
                        YPointMember[count] = Convert.ToInt32(Convert.ToDecimal(ChartData.Rows[count]["previous_score"]) * 100);


                    }
                    Chart1.Series[0].Points.DataBindXY(XPointMember, YPointMember);
                    Chart1.Series[0].ChartType = SeriesChartType.Area;
                    Chart1.Series[0].Color = Color.LightGray;

                    
                    Chart1.Series[1].IsValueShownAsLabel = true;
                    Chart1.ChartAreas[0].AxisX.MajorGrid.LineWidth = 0;
                    Chart1.ChartAreas[0].AxisY.MajorGrid.LineWidth = 0;
                    Chart1.ChartAreas[0].AxisX.Interval = 1;
                    Chart1.ChartAreas[0].AxisY.Interval = 25;
                    Chart1.Series[0].LabelFormat = "{0} %";
                    Chart1.Series[1].LabelFormat = "{0} %";
                    Chart1.ChartAreas[0].AxisY.LabelStyle.Format = "{0} %";
                    Chart1.Titles[0].Text = "LPA Result";
                    Chart1.ChartAreas[0].AxisY.Maximum = 100;
                }
                #endregion

                #region LPA Result Per Section

               
                dtReport = new DataTable();
                dtReport = objDashBL.getLPAResultPerSectionBL(objDashBO);
                if (dtReport.Rows.Count > 0)
                {
                    pnlReport.Visible = true;
                                     
                    DataTable ChartData = dtReport;
                    sec1.Text = Convert.ToString(ChartData.Rows[0]["Sec_name_abbr"]) + " - " + Convert.ToString(ChartData.Rows[0]["Section_name"]);
                    sec2.Text = Convert.ToString(ChartData.Rows[1]["Sec_name_abbr"]) + " - " + Convert.ToString(ChartData.Rows[1]["Section_name"]);
                    sec3.Text = Convert.ToString(ChartData.Rows[2]["Sec_name_abbr"]) + " - " + Convert.ToString(ChartData.Rows[2]["Section_name"]);
                    sec4.Text = Convert.ToString(ChartData.Rows[3]["Sec_name_abbr"]) + " - " + Convert.ToString(ChartData.Rows[3]["Section_name"]);
                    sec5.Text = Convert.ToString(ChartData.Rows[4]["Sec_name_abbr"]) + " - " + Convert.ToString(ChartData.Rows[4]["Section_name"]);
                    sec6.Text = Convert.ToString(ChartData.Rows[5]["Sec_name_abbr"]) + " - " + Convert.ToString(ChartData.Rows[5]["Section_name"]);
                    string[] XPointMember = new string[ChartData.Rows.Count];
                    int[] YPointMember = new int[ChartData.Rows.Count];

                    for (int count = 0; count < ChartData.Rows.Count; count++)
                    {
                        //storing Values for X axis  
                        XPointMember[count] = ChartData.Rows[count]["Sec_name_abbr"].ToString();
                        //storing values for Y Axis  
                        YPointMember[count] = Convert.ToInt32(Convert.ToDecimal(ChartData.Rows[count]["Score"]) * 100);
                        //ChartPerSection.Series[0].Name = column.ColumnName;

                    }
                   
                    ChartPerSection.Series[0].Points.DataBindXY(XPointMember, YPointMember);
                    ChartPerSection.Series[0].ChartType = SeriesChartType.Radar;
                    ChartPerSection.Series[0]["RadarDrawingStyle"] = "Area";
                    ChartPerSection.Series[0]["AreaDrawingStyle"] = "Polygon";
                    ChartPerSection.Series[0].Color = Color.DarkGreen;

                    ChartPerSection.Titles[0].Text = "LPA Result Per Section";
                    ChartPerSection.Series[0].IsValueShownAsLabel = true;
                    ChartPerSection.ChartAreas[0].AxisX.MajorGrid.LineWidth = 0;
                    ChartPerSection.ChartAreas[0].AxisY.MajorGrid.LineWidth = 0;
                    ChartPerSection.ChartAreas[0].AxisX.Interval = 1;
                    ChartPerSection.ChartAreas[0].AxisY.Interval = 25;
                    ChartPerSection.Series[0].LabelFormat = "{0} %";
                    ChartPerSection.ChartAreas[0].AxisY.LabelStyle.Format = "{0} %";
                    ChartPerSection.ChartAreas[0].AxisY.Maximum = 100;
                    // Axis[] xaxis;
                    //Axis axis;
                    // for (int count = 0; count < ChartData.Rows.Count; count++)
                    // {
                    //     //string axis = Convert.ToString(XPointMember[count]);
                    //     axis = new Axis(ChartPerSection.ChartAreas[0], ChartPerSection.ChartAreas[0].AxisX.AxisName);
                    //     xaxis = new Axis(axis);
                    // }
                    //ChartPerSection.ChartAreas[0].Axes = xaxis;
                }
                #endregion

                #region LPA Result Per Section Monthly
                dtReport = new DataTable();
                dtReport = objDashBL.getLPAResultPerSectionMonthlyBL(objDashBO);
                if (dtReport.Rows.Count > 0)
                {
                    pnlReport.Visible = true;

                    DataTable ChartData = dtReport;
                    string[] XPointMember = new string[ChartData.Rows.Count];
                    int[] YPointMember = new int[ChartData.Rows.Count];

                    for (int count = 0; count < ChartData.Rows.Count; count++)
                    {
                        //storing Values for X axis  
                        XPointMember[count] = ChartData.Rows[count]["audit_period"].ToString();
                        //storing values for Y Axis  
                        YPointMember[count] = Convert.ToInt32(Convert.ToDecimal(ChartData.Rows[count]["GP"]) * 100);


                    }
                    ChartPerSectionMonthly.Series[0].Points.DataBindXY(XPointMember, YPointMember);
                    ChartPerSectionMonthly.Series[0].ChartType = SeriesChartType.Line;
                    ChartPerSectionMonthly.Series[0].Color = Color.DarkGreen;
                    ChartPerSectionMonthly.Series[0].IsValueShownAsLabel = true;
                    ChartPerSectionMonthly.Series[0].IsVisibleInLegend = true;
                    ChartPerSectionMonthly.Series[0].LabelFormat = "{0} %";
                    for (int count = 0; count < ChartData.Rows.Count; count++)
                    {
                        //storing Values for X axis  
                        XPointMember[count] = ChartData.Rows[count]["audit_period"].ToString();
                        //storing values for Y Axis  
                        YPointMember[count] = Convert.ToInt32(Convert.ToDecimal(ChartData.Rows[count]["PD"]) * 100);


                    }
                    ChartPerSectionMonthly.Series[1].Points.DataBindXY(XPointMember, YPointMember);
                    ChartPerSectionMonthly.Series[1].ChartType = SeriesChartType.Line;
                    ChartPerSectionMonthly.Series[1].Color = Color.LightGreen;
                    ChartPerSectionMonthly.Series[1].IsValueShownAsLabel = true;
                    ChartPerSectionMonthly.Series[1].IsVisibleInLegend = true;
                    ChartPerSectionMonthly.Series[1].LabelFormat = "{0} %";
                    for (int count = 0; count < ChartData.Rows.Count; count++)
                    {
                        //storing Values for X axis  
                        XPointMember[count] = ChartData.Rows[count]["audit_period"].ToString();
                        //storing values for Y Axis  
                        YPointMember[count] = Convert.ToInt32(Convert.ToDecimal(ChartData.Rows[count]["SOCWA"]) * 100);


                    }
                    ChartPerSectionMonthly.Series[2].Points.DataBindXY(XPointMember, YPointMember);
                    ChartPerSectionMonthly.Series[2].ChartType = SeriesChartType.Line;
                    ChartPerSectionMonthly.Series[2].Color = Color.Orange;
                    ChartPerSectionMonthly.Series[2].IsValueShownAsLabel = true;
                    ChartPerSectionMonthly.Series[2].IsVisibleInLegend = true;
                    ChartPerSectionMonthly.Series[2].LabelFormat = "{0} %";
                    for (int count = 0; count < ChartData.Rows.Count; count++)
                    {
                        //storing Values for X axis  
                        XPointMember[count] = ChartData.Rows[count]["audit_period"].ToString();
                        //storing values for Y Axis  
                        YPointMember[count] = Convert.ToInt32(Convert.ToDecimal(ChartData.Rows[count]["RPE"]) * 100);


                    }
                    ChartPerSectionMonthly.Series[3].Points.DataBindXY(XPointMember, YPointMember);
                    ChartPerSectionMonthly.Series[3].ChartType = SeriesChartType.Line;
                    ChartPerSectionMonthly.Series[3].Color = Color.OrangeRed;
                    ChartPerSectionMonthly.Series[3].IsValueShownAsLabel = true;
                    ChartPerSectionMonthly.Series[3].IsVisibleInLegend = true;
                    ChartPerSectionMonthly.Series[3].LabelFormat = "{0} %";
                    for (int count = 0; count < ChartData.Rows.Count; count++)
                    {
                        //storing Values for X axis  
                        XPointMember[count] = ChartData.Rows[count]["audit_period"].ToString();
                        //storing values for Y Axis  
                        YPointMember[count] = Convert.ToInt32(Convert.ToDecimal(ChartData.Rows[count]["SW"]) * 100);


                    }
                    ChartPerSectionMonthly.Series[4].Points.DataBindXY(XPointMember, YPointMember);
                    ChartPerSectionMonthly.Series[4].ChartType = SeriesChartType.Line;
                    ChartPerSectionMonthly.Series[4].Color = Color.DarkGray;
                    ChartPerSectionMonthly.Series[4].IsValueShownAsLabel = true;
                    ChartPerSectionMonthly.Series[4].IsVisibleInLegend = true;
                    ChartPerSectionMonthly.Series[4].LabelFormat = "{0} %";
                    for (int count = 0; count < ChartData.Rows.Count; count++)
                    {
                        //storing Values for X axis  
                        XPointMember[count] = ChartData.Rows[count]["audit_period"].ToString();
                        //storing values for Y Axis  
                        YPointMember[count] = Convert.ToInt32(Convert.ToDecimal(ChartData.Rows[count]["RPS"]) * 100);


                    }
                    ChartPerSectionMonthly.Series[5].Points.DataBindXY(XPointMember, YPointMember);
                    ChartPerSectionMonthly.Series[5].ChartType = SeriesChartType.Line;
                    ChartPerSectionMonthly.Series[5].Color = Color.LightGray;
                    ChartPerSectionMonthly.Series[5].IsValueShownAsLabel = true;
                    ChartPerSectionMonthly.Series[5].IsVisibleInLegend = true;
                    ChartPerSectionMonthly.ChartAreas[0].AxisX.MajorGrid.LineWidth = 0;
                    ChartPerSectionMonthly.ChartAreas[0].AxisY.MajorGrid.LineWidth = 0;
                    ChartPerSectionMonthly.ChartAreas[0].AxisX.Interval = 1;
                    ChartPerSectionMonthly.Series[5].LabelFormat = "{0} %";
                    ChartPerSectionMonthly.ChartAreas[0].AxisY.LabelStyle.Format = "{0} %";
                    ChartPerSectionMonthly.ChartAreas[0].AxisY.Interval = 25;
                    ChartPerSectionMonthly.ChartAreas[0].AxisY.Maximum = 100;
                    ChartPerSectionMonthly.Titles[0].Text = "LPA Result Per Section Monthly";
                    int i = 0;
                    foreach (DataColumn column in ChartData.Columns)
                    {
                        if (column.ColumnName != "audit_period" && column.ColumnName != "audit_date_format")
                        {
                            ChartPerSectionMonthly.Series[i].Name = column.ColumnName;
                            i++;
                        }
                    }
                }
                #endregion

                #region LPA Result Per Line
                dtReport = new DataTable();
                dtReport = objDashBL.getLPAResultByLineBL(objDashBO);
                if (dtReport.Rows.Count > 0)
                {
                    pnlReport.Visible = true;
                    DataTable ChartData = new DataTable();
                    ChartData = dtReport;
                    string[] XPointMember = new string[ChartData.Rows.Count];
                    int[] YPointMember = new int[ChartData.Rows.Count];

                    for (int count = 0; count < ChartData.Rows.Count; count++)
                    {
                        XPointMember[count] = ChartData.Rows[count]["line_name"].ToString();
                        YPointMember[count] = Convert.ToInt32(Convert.ToDecimal(ChartData.Rows[count]["average_score"]) * 100);
                    }
                    ChartPerLine.Series[0].Points.DataBindXY(XPointMember, YPointMember);
                    ChartPerLine.Series[0].ChartType = SeriesChartType.Column;
                    ChartPerLine.Series[0].Color = Color.DarkGreen;
                    ChartPerLine.Titles[0].Text = "LPA Result per Line(pareto)";

                    ChartPerLine.Series[0].IsValueShownAsLabel = true;
                    ChartPerLine.ChartAreas[0].AxisX.MajorGrid.LineWidth = 0;
                    ChartPerLine.ChartAreas[0].AxisY.MajorGrid.LineWidth = 0;
                    ChartPerLine.ChartAreas[0].AxisX.Interval = 1;
                    ChartPerLine.Series[0].LabelFormat = "{0} %";
                    ChartPerLine.ChartAreas[0].AxisY.LabelStyle.Format = "{0} %";
                    ChartPerLine.ChartAreas[0].AxisY.Interval = 25;
                    ChartPerLine.ChartAreas[0].AxisY.Maximum = 100;

                }
                #endregion

                #region LPA Result Per Questions
                dtReport = new DataTable();
                dtReport = objDashBL.getLPAResultByQuestionBL(objDashBO);
                if (dtReport.Rows.Count > 0)
                {
                    pnlReport.Visible = true;

                    DataTable ChartData = new DataTable();
                    ChartData = dtReport;
                    string[] XPointMember = new string[ChartData.Rows.Count];
                    int[] YPointMember = new int[ChartData.Rows.Count];

                    for (int count = 0; count < ChartData.Rows.Count; count++)
                    {
                        //storing Values for X axis  
                        XPointMember[count] = Convert.ToString(ChartData.Rows[count]["question_id"]).Substring(1,Convert.ToString(ChartData.Rows[count]["question_id"]).Length-1);
                        //storing values for Y Axis  
                        YPointMember[count] = Convert.ToInt32(Convert.ToDecimal(ChartData.Rows[count]["score"]) * 100);


                    }
                    ChartPerQuestion.Series[0].Points.DataBindXY(XPointMember, YPointMember);
                    ChartPerQuestion.Series[0].ChartType = SeriesChartType.Column;
                    ChartPerQuestion.Series[0].Color = Color.DarkGreen;
                    ChartPerQuestion.Series[0].IsValueShownAsLabel = true;
                    ChartPerQuestion.ChartAreas[0].AxisX.MajorGrid.LineWidth = 0;
                    ChartPerQuestion.ChartAreas[0].AxisY.MajorGrid.LineWidth = 0;
                    ChartPerQuestion.ChartAreas[0].AxisX.Interval = 1;
                    ChartPerQuestion.Titles[0].Text = "LPA Result per Question";
                    ChartPerQuestion.Series[0].LabelFormat = "{0} %";
                    ChartPerQuestion.ChartAreas[0].AxisY.LabelStyle.Format = "{0} %";
                    ChartPerQuestion.ChartAreas[0].AxisY.Interval = 25;
                    ChartPerQuestion.ChartAreas[0].AxisY.Maximum = 100;
                }
                #endregion

                #region LPA Completion Rate

                dtReport = objDashBL.getDataforLPAComparisionReportBL(objDashBO);
                if (dtReport.Rows.Count > 0)
                {
                    pnlReport.Visible = true;

                    DataTable ChartData = dtReport;
                    string[] XPointMember = new string[ChartData.Rows.Count];
                    int[] YPointMember = new int[ChartData.Rows.Count];

                    for (int count = 0; count < ChartData.Rows.Count; count++)
                    {
                        //storing Values for X axis  
                        XPointMember[count] = ChartData.Rows[count]["audit_period"].ToString();
                        //storing values for Y Axis  
                        YPointMember[count] = Convert.ToInt32(Convert.ToDecimal(ChartData.Rows[count]["cy_perform_rate"]));
                    }
                    chartComparision.Series[1].Points.DataBindXY(XPointMember, YPointMember);
                    chartComparision.Series[1].ChartType = SeriesChartType.Column;
                    chartComparision.Series[1].Color = Color.DarkGreen;

                    for (int count = 0; count < ChartData.Rows.Count; count++)
                    {
                        //storing Values for X axis  
                        XPointMember[count] = ChartData.Rows[count]["audit_period"].ToString();
                        //storing values for Y Axis  
                        YPointMember[count] = Convert.ToInt32(Convert.ToDecimal(ChartData.Rows[count]["ly_perform_rate"]));


                    }
                    chartComparision.Series[0].Points.DataBindXY(XPointMember, YPointMember);
                    chartComparision.Series[0].ChartType = SeriesChartType.Area;
                    chartComparision.Series[0].Color = Color.LightGray;


                    chartComparision.Series[1].IsValueShownAsLabel = true;
                    chartComparision.ChartAreas[0].AxisX.MajorGrid.LineWidth = 0;
                    chartComparision.ChartAreas[0].AxisY.MajorGrid.LineWidth = 0;
                    chartComparision.ChartAreas[0].AxisX.Interval = 1;
                    chartComparision.ChartAreas[0].AxisY.Interval = 25;
                    chartComparision.Series[0].LabelFormat = "{0} %";
                    chartComparision.Series[1].LabelFormat = "{0} %";
                    chartComparision.ChartAreas[0].AxisY.LabelStyle.Format = "{0} %";
                    chartComparision.Titles[0].Text = "LPA Completion Rate";
                    chartComparision.ChartAreas[0].AxisY.Maximum = 100;
                }
                #endregion

            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        protected void ddlLineName_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                objDashBO = new DashboardBO();
                objDashBO.region_id = Convert.ToInt32(ddlRegion.SelectedValue);
                objDashBO.country_id = Convert.ToInt32(ddlCountry.SelectedValue);
                objDashBO.location_id = Convert.ToInt32(ddlLocation.SelectedValue);
                objDashBO.line_id = Convert.ToInt32(ddlLineName.SelectedValue);
                objDashBO.selection_flag = "Line";
               // LoadFilterDropDowns(objDashBO);

                objDashBL = new DashboardBL();
                dsDropDownData = new DataSet();
                dsDropDownData = objDashBL.getFilterDataBL(objDashBO);
                if (dsDropDownData.Tables.Count > 0)
                {
                    if (dsDropDownData.Tables[0].Rows.Count > 0)
                    {
                        ddlRegion.DataSource = dsDropDownData.Tables[0];
                        ddlRegion.DataTextField = "region_name";
                        ddlRegion.DataValueField = "region_id";
                        ddlRegion.DataBind();
                    }
                    else
                    {
                        ddlRegion.Items.Clear();
                        ddlRegion.DataSource = null;
                        ddlRegion.DataBind();
                    }
                    if (dsDropDownData.Tables[1].Rows.Count > 0)
                    {
                        ddlCountry.DataSource = dsDropDownData.Tables[1];
                        ddlCountry.DataTextField = "country_name";
                        ddlCountry.DataValueField = "country_id";
                        ddlCountry.DataBind();
                    }
                    else
                    {
                        ddlCountry.Items.Clear();
                        ddlCountry.DataSource = null;
                        ddlCountry.DataBind();
                    }
                    if (dsDropDownData.Tables[2].Rows.Count > 0)
                    {
                        ddlLocation.DataSource = dsDropDownData.Tables[2];
                        ddlLocation.DataTextField = "location_name";
                        ddlLocation.DataValueField = "location_id";
                        ddlLocation.DataBind();
                    }
                    else
                    {
                        ddlLocation.Items.Clear();
                        ddlLocation.DataSource = null;
                        ddlLocation.DataBind();
                    }
                    if (dsDropDownData.Tables[3].Rows.Count > 0)
                    {
                        ddlLineName.DataSource = dsDropDownData.Tables[3];
                        ddlLineName.DataTextField = "line_name";
                        ddlLineName.DataValueField = "line_id";
                        ddlLineName.DataBind();
                    }
                    else
                    {
                        ddlLineName.Items.Clear();
                        ddlLineName.DataSource = null;
                        ddlLineName.DataBind();
                    }
                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }

        }

        protected void btnClear_Click(object sender, EventArgs e)
        {
            try
            {
                objDashBO = new DashboardBO();
                LoadFilterDropDowns(objDashBO);
                pnlReport.Visible = false;
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }
        #endregion

        #region Methods
        private void LoadFilterDropDowns(DashboardBO objDashBO)
        {
            try
            {
                objDashBL=new DashboardBL();
                dsDropDownData = new DataSet();
                dsDropDownData = objDashBL.getFilterDataBL(objDashBO);
                if (dsDropDownData.Tables.Count > 0)
                {
                    if (dsDropDownData.Tables[0].Rows.Count > 0)
                    {
                        ddlRegion.DataSource = dsDropDownData.Tables[0];
                        ddlRegion.DataTextField = "region_name";
                        ddlRegion.DataValueField = "region_id";
                        ddlRegion.DataBind();
                        ddlRegion.Items.Insert(0, new ListItem("All", "0"));
                    }
                    else
                    {
                        ddlRegion.Items.Clear();
                        ddlRegion.DataSource = null;
                        ddlRegion.DataBind();
                    }
                    if (dsDropDownData.Tables[1].Rows.Count > 0)
                    {
                        ddlCountry.DataSource = dsDropDownData.Tables[1];
                        ddlCountry.DataTextField = "country_name";
                        ddlCountry.DataValueField = "country_id";
                        ddlCountry.DataBind();
                        ddlCountry.Items.Insert(0, new ListItem("All", "0"));
                    }
                    else
                    {
                        ddlCountry.Items.Clear();
                        ddlCountry.DataSource = null;
                        ddlCountry.DataBind();
                    }
                    if (dsDropDownData.Tables[2].Rows.Count > 0)
                    {
                        ddlLocation.DataSource = dsDropDownData.Tables[2];
                        ddlLocation.DataTextField = "location_name";
                        ddlLocation.DataValueField = "location_id";
                        ddlLocation.DataBind();
                        ddlLocation.Items.Insert(0, new ListItem("All", "0"));
                    }
                    else
                    {
                        ddlLocation.Items.Clear();
                        ddlLocation.DataSource = null;
                        ddlLocation.DataBind();
                    }
                    if (dsDropDownData.Tables[3].Rows.Count > 0)
                    {
                        ddlLineName.DataSource = dsDropDownData.Tables[3];
                        ddlLineName.DataTextField = "line_name";
                        ddlLineName.DataValueField = "line_id";
                        ddlLineName.DataBind();
                        ddlLineName.Items.Insert(0, new ListItem("All", "0"));
                    }
                    else
                    {
                        ddlLineName.Items.Clear();
                        ddlLineName.DataSource = null;
                        ddlLineName.DataBind();
                    }
                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        private void LoadDropDowns()
        {
            DataTable dtLine = new DataTable();
            DataTable dtCountry = new DataTable();
            DataTable dtLocation = new DataTable();

            try
            {
                dsDropDownData = new DataSet();
                objUserBO = new UsersBO();
                objUserBO.UserId = Convert.ToInt32(Session["UserId"]);
                objComBL = new CommonBL();
                dsDropDownData = objComBL.GetAllMasterBL(objUserBO);

                if (dsDropDownData.Tables.Count > 0)
                {
                    if (dsDropDownData.Tables[0].Rows.Count > 0)
                    {
                        ddlRegion.DataSource = dsDropDownData.Tables[0];
                        ddlRegion.DataTextField = "region_name";
                        ddlRegion.DataValueField = "region_id";
                        ddlRegion.DataBind();
                        ddlRegion.Items.Insert(0, new ListItem("All", "0"));
                    }
                    else
                    {
                        ddlRegion.Items.Clear();
                        ddlRegion.DataSource = null;
                        ddlRegion.DataBind();
                    }
                    if (dsDropDownData.Tables[1].Rows.Count > 0)
                    {
                        ddlCountry.DataSource = dsDropDownData.Tables[1];
                        ddlCountry.DataTextField = "country_name";
                        ddlCountry.DataValueField = "country_id";
                        ddlCountry.DataBind();
                        ddlCountry.Items.Insert(0, new ListItem("All", "0"));
                    }
                    else
                    {
                        ddlCountry.DataSource = null;
                        ddlCountry.DataBind();
                    }
                    if (dsDropDownData.Tables[2].Rows.Count > 0)
                    {
                        ddlLocation.DataSource = dsDropDownData.Tables[2];
                        ddlLocation.DataTextField = "location_name";
                        ddlLocation.DataValueField = "location_id";
                        ddlLocation.DataBind();
                        ddlLocation.Items.Insert(0, new ListItem("All", "0"));
                    }
                    else
                    {
                        ddlLocation.DataSource = null;
                        ddlLocation.DataBind();
                    }
                }
                //if (!string.IsNullOrEmpty(Convert.ToString(ddlRegion.SelectedValue)))
                //{
                //    objUserBO.region_id = Convert.ToInt32(ddlRegion.SelectedValue);
                //    dtCountry = objComBL.GetCountryBasedOnRegionBL(objUserBO);

                //    if (dtCountry.Rows.Count > 0)
                //    {
                //        ddlCountry.DataSource = dtCountry;
                //        ddlCountry.DataTextField = "country_name";
                //        ddlCountry.DataValueField = "country_id";
                //        ddlCountry.DataBind();
                //        ddlCountry.Items.Insert(0,new ListItem("All",""));
                //    }
                //    else
                //    {
                //        ddlCountry.Items.Clear();
                //        ddlCountry.DataSource = null;
                //        ddlCountry.DataBind();
                //    }
                //}
                //else
                //{
                //    ddlCountry.Items.Clear();
                //    ddlCountry.DataSource = null;
                //    ddlCountry.DataBind();
                //}
                //if (!string.IsNullOrEmpty(Convert.ToString(ddlCountry.SelectedValue)))
                //{
                //    objUserBO.country_id = Convert.ToInt32(ddlCountry.SelectedValue);
                //    dtLocation = objComBL.GetLocationBasedOnCountryBL(objUserBO);
                //    if (dtLocation.Rows.Count > 0)
                //    {
                //        ddlLocation.DataSource = dtLocation;
                //        ddlLocation.DataTextField = "location_name";
                //        ddlLocation.DataValueField = "location_id";
                //        ddlLocation.DataBind();
                //        ddlLocation.Items.Insert(0, new ListItem("All", ""));
                //    }
                //    else
                //    {
                //        ddlLocation.Items.Clear();
                //        ddlLocation.DataSource = null;
                //        ddlLocation.DataBind();
                //    }
                //}
                //else
                //{
                //    ddlLocation.Items.Clear();
                //    ddlLocation.DataSource = null;
                //    ddlLocation.DataBind();
                //}
                if (!string.IsNullOrEmpty(Convert.ToString(ddlLocation.SelectedValue)))
                {
                    objUserBO.location_id = Convert.ToInt32(ddlLocation.SelectedValue);
                    dtLine = objComBL.GetLineListDropDownBL(objUserBO);
                    if (dtLine.Rows.Count > 0)
                    {
                        ddlLineName.DataSource = dtLine;
                        ddlLineName.DataTextField = "line_name";
                        ddlLineName.DataValueField = "line_id";
                        ddlLineName.DataBind();
                        ddlLineName.Items.Insert(0, new ListItem("All", "0"));
                        Session["dtLine"] = dtLine;
                    }
                    else
                    {
                        ddlLineName.Items.Clear();
                        ddlLineName.DataSource = null;
                        ddlLineName.DataBind();
                    }
                }
                else
                {
                    objUserBO.location_id = Convert.ToInt32(ddlLocation.SelectedIndex);
                    dtLine = objComBL.GetLineListDropDownBL(objUserBO);
                    if (dtLine.Rows.Count > 0)
                    {
                        ddlLineName.DataSource = dtLine;
                        ddlLineName.DataTextField = "line_name";
                        ddlLineName.DataValueField = "line_id";
                        ddlLineName.DataBind();
                        ddlLineName.Items.Insert(0, new ListItem("All", "0"));
                        Session["dtLine"] = dtLine;
                    }
                    else
                    {
                        ddlLineName.Items.Clear();
                        ddlLineName.DataSource = null;
                        ddlLineName.DataBind();
                    }
                }

            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        #endregion
        
    }
}