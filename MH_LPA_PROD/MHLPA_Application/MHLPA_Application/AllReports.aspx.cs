﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MHLPA_BusinessLogic;
using MHLPA_BusinessObject;

namespace MHLPA_Application
{
    public partial class AllReports : System.Web.UI.Page
    {
        #region Global Declaration
        CommonFunctions objCom = new CommonFunctions();
        DataSet dsDropDownData;
        UsersBO objUserBO;
        DataTable dtAnswer = new DataTable();
        CommonBL objComBL;
        DataTable dtReport;
        DashboardBO objDashBO;
        DashboardBL objDashBL;
        #endregion

        #region Events
        /// <summary>
        /// Author : Neha(KNS)
        /// Date : 30 Jan 2020
        /// Desc : when the page loads, all the drop down values are loaded in this event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(Convert.ToString(Session["UserId"]))) { Response.Redirect("Login.aspx"); return; }

                if (!IsPostBack)
                {
                    // divreports.Visible = false;
                    objDashBO = new DashboardBO();
                    LoadFilterDropDowns(objDashBO);
                   // Session["AuditScheduleReports"] = dtReport;
                    btnFilter_Click(null, null);
                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        /// <summary>
        /// Author : Neha(KNS)
        /// Date : 30 Jan 2020
        /// Desc : Based on selection of region value, country, location will be loaded accordinglyby calling getFilterDataBL method.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlRegion_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dtCountry = new DataTable();
            DataTable dtLocation = new DataTable();
            try
            {
                objDashBO = new DashboardBO();
                objDashBO.region_id = Convert.ToInt32(ddlRegion.SelectedValue);
                objDashBO.selection_flag = "Region";

                objDashBL = new DashboardBL();
                dsDropDownData = new DataSet();
                dsDropDownData = objDashBL.getFilterDataBL(objDashBO);
                if (dsDropDownData.Tables.Count > 0)
                {
                    if (dsDropDownData.Tables[0].Rows.Count > 0)
                    {
                        //ddlRegion.DataSource = dsDropDownData.Tables[0];
                        //ddlRegion.DataTextField = "region_name";
                        //ddlRegion.DataValueField = "region_id";
                        //ddlRegion.DataBind();
                        //ddlRegion.Items.Insert(0, new ListItem("All", "0"));
                        ddlRegion.SelectedValue = Convert.ToString(objDashBO.region_id);
                    }
                    else
                    {
                        ddlRegion.Items.Clear();
                        ddlRegion.DataSource = null;
                        ddlRegion.DataBind();
                    }
                    if (dsDropDownData.Tables[1].Rows.Count > 0)
                    {
                        ddlCountry.DataSource = dsDropDownData.Tables[1];
                        ddlCountry.DataTextField = "country_name";
                        ddlCountry.DataValueField = "country_id";
                        ddlCountry.DataBind();
                        ddlCountry.Items.Insert(0, new ListItem("All", "0"));
                    }
                    else
                    {
                        ddlCountry.Items.Clear();
                        ddlCountry.DataSource = null;
                        ddlCountry.DataBind();
                    }
                    if (dsDropDownData.Tables[2].Rows.Count > 0)
                    {
                        ddlLocation.DataSource = dsDropDownData.Tables[2];
                        ddlLocation.DataTextField = "location_name";
                        ddlLocation.DataValueField = "location_id";
                        ddlLocation.DataBind();
                        ddlLocation.Items.Insert(0, new ListItem("All", "0"));
                    }
                    else
                    {
                        ddlLocation.Items.Clear();
                        ddlLocation.DataSource = null;
                        ddlLocation.DataBind();
                    }
                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }
        /// <summary>
        /// Author : Neha(KNS)
        /// Date : 30 Jan 2020
        /// Desc : Based on selection of country value, region, location will be loaded accordinglyby calling getFilterDataBL method.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlCountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dtCountry = new DataTable();
            DataTable dtLocation = new DataTable();
            DataSet dsDropDownDataCountry = new DataSet();
            DashboardBO objDashBOCountry = new DashboardBO();
            int region;
            try
            {
                objDashBO = new DashboardBO();
                region = Convert.ToInt32(ddlRegion.SelectedValue);
                objDashBO.country_id = Convert.ToInt32(ddlCountry.SelectedValue);
                if (objDashBO.country_id == 0)
                {
                    objDashBO.region_id = Convert.ToInt32(ddlRegion.SelectedValue);
                }
                objDashBO.selection_flag = "Country";
                objDashBL = new DashboardBL();
                dsDropDownData = new DataSet();
                dsDropDownData = objDashBL.getFilterDataBL(objDashBO);
                if (dsDropDownData.Tables.Count > 0)
                {
                    if (dsDropDownData.Tables[0].Rows.Count > 0)
                    {
                        if (ddlRegion.Items.FindByValue(Convert.ToString(region)) != null && region != 0)
                        {
                            ddlRegion.SelectedValue = Convert.ToString(region);
                        }
                        else
                        {
                            ddlRegion.SelectedValue = Convert.ToString(dsDropDownData.Tables[0].Rows[0]["region_id"]);
                        }
                    }
                    else
                    {
                        //    ddlRegion.Items.Clear();
                        //    ddlRegion.DataSource = null;
                        //    ddlRegion.DataBind();
                    }
                    if (dsDropDownData.Tables[1].Rows.Count > 0)
                    {
                        if (ddlRegion.Items.FindByValue(Convert.ToString(region)) == null || region == 0)
                        {
                            objDashBOCountry.region_id = Convert.ToInt32(ddlRegion.SelectedValue);
                            dsDropDownDataCountry = objDashBL.getFilterDataBL(objDashBOCountry);
                            if (dsDropDownDataCountry.Tables.Count > 0)
                            {
                                if (dsDropDownDataCountry.Tables[1].Rows.Count > 0)
                                {
                                    ddlCountry.DataSource = dsDropDownDataCountry.Tables[1];
                                    ddlCountry.DataTextField = "country_name";
                                    ddlCountry.DataValueField = "country_id";
                                    ddlCountry.DataBind();
                                    ddlCountry.Items.Insert(0, new ListItem("All", "0"));
                                    ddlCountry.SelectedValue = Convert.ToString(objDashBO.country_id);
                                }
                                else
                                {
                                    ddlCountry.Items.Clear();
                                    ddlCountry.DataSource = null;
                                    ddlCountry.DataBind();
                                }
                            }
                        }
                        else
                        {
                            ddlCountry.SelectedValue = Convert.ToString(objDashBO.country_id);
                        }
                    }
                    else
                    {
                        ddlCountry.Items.Clear();
                        ddlCountry.DataSource = null;
                        ddlCountry.DataBind();
                    }
                    if (dsDropDownData.Tables[2].Rows.Count > 0)
                    {
                        ddlLocation.DataSource = dsDropDownData.Tables[2];
                        ddlLocation.DataTextField = "location_name";
                        ddlLocation.DataValueField = "location_id";
                        ddlLocation.DataBind();
                        ddlLocation.Items.Insert(0, new ListItem("All", "0"));
                    }
                    else
                    {
                        ddlLocation.Items.Clear();
                        ddlLocation.DataSource = null;
                        ddlLocation.DataBind();
                    }

                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }
        /// <summary>
        /// Author : Neha(KNS)
        /// Date : 30 Jan 2020
        /// Desc : Based on selection of location value, region, country will be loaded accordinglyby calling getFilterDataBL method.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlLocation_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dtLineProduct = new DataTable();
            DataSet dsDropDownDataCountry = new DataSet();
            DataSet dsDropDownDataLocation = new DataSet();
            DashboardBO objDashBOCountry = new DashboardBO();
            DashboardBO objDashBOlocation = new DashboardBO();
            int region, country;

            try
            {
                objDashBO = new DashboardBO();
                region = Convert.ToInt32(ddlRegion.SelectedValue);
                country = Convert.ToInt32(ddlCountry.SelectedValue);
                objDashBO.location_id = Convert.ToInt32(ddlLocation.SelectedValue);
                if (objDashBO.location_id == 0)
                {
                    objDashBO.country_id = country;
                    objDashBO.region_id = region;
                }
                objDashBO.selection_flag = "Location";
                objDashBL = new DashboardBL();
                dsDropDownData = new DataSet();
                dsDropDownData = objDashBL.getFilterDataBL(objDashBO);
                if (dsDropDownData.Tables.Count > 0)
                {
                    if (dsDropDownData.Tables[0].Rows.Count > 0)
                    {
                        if (ddlRegion.Items.FindByValue(Convert.ToString(region)) != null && region != 0)
                        {
                            ddlRegion.SelectedValue = Convert.ToString(region);
                        }
                        else
                        {
                            ddlRegion.SelectedValue = Convert.ToString(dsDropDownData.Tables[0].Rows[0]["region_id"]);
                        }
                    }

                    if (dsDropDownData.Tables[1].Rows.Count > 0)
                    {
                        objDashBOCountry.region_id = Convert.ToInt32(ddlRegion.SelectedValue);
                        dsDropDownDataCountry = objDashBL.getFilterDataBL(objDashBOCountry);
                        if (dsDropDownDataCountry.Tables.Count > 0)
                        {
                            if (dsDropDownDataCountry.Tables[1].Rows.Count > 0)
                            {
                                ddlCountry.DataSource = dsDropDownDataCountry.Tables[1];
                                ddlCountry.DataTextField = "country_name";
                                ddlCountry.DataValueField = "country_id";
                                ddlCountry.DataBind();
                                ddlCountry.Items.Insert(0, new ListItem("All", "0"));
                                if (ddlCountry.Items.FindByValue(Convert.ToString(country)) != null && country != 0)
                                {
                                    ddlCountry.SelectedValue = Convert.ToString(country);
                                }
                                else
                                {
                                    ddlCountry.SelectedValue = Convert.ToString(dsDropDownData.Tables[1].Rows[0]["country_id"]); ;
                                }
                            }
                            else
                            {
                                ddlCountry.Items.Clear();
                                ddlCountry.DataSource = null;
                                ddlCountry.DataBind();
                            }
                        }
                    }

                    if (dsDropDownData.Tables[2].Rows.Count > 0)
                    {
                        objDashBOlocation.country_id = Convert.ToInt32(ddlCountry.SelectedValue);
                        dsDropDownDataLocation = objDashBL.getFilterDataBL(objDashBOlocation);
                        if (dsDropDownDataLocation.Tables.Count > 0)
                        {
                            if (dsDropDownDataLocation.Tables[2].Rows.Count > 0)
                            {
                                ddlLocation.DataSource = dsDropDownDataLocation.Tables[2];
                                ddlLocation.DataTextField = "location_name";
                                ddlLocation.DataValueField = "location_id";
                                ddlLocation.DataBind();
                                ddlLocation.Items.Insert(0, new ListItem("All", "0"));
                                ddlLocation.SelectedValue = Convert.ToString(objDashBO.location_id);
                            }
                            else
                            {
                                ddlLocation.Items.Clear();
                                ddlLocation.DataSource = null;
                                ddlLocation.DataBind();
                            }
                        }
                    }
                    else
                    {
                        ddlLocation.Items.Clear();
                        ddlLocation.DataSource = null;
                        ddlLocation.DataBind();
                    }

                }

            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }
        /// <summary>
        /// Author : Neha(KNS)
        /// Date : 30 Jan 2020
        /// Desc :
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void lnkauditschedule_Click(object sender, EventArgs e)
        {
            try
            {
                divreports.Visible = true;
                LoadAuditSchedule();
                
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }
        /// <summary>
        /// Author : Neha(KNS)
        /// Date : 30 Jan 2020
        /// Desc :
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void lnkauditplan_Click(object sender, EventArgs e)
        {
            try
            {
                divreports.Visible = true;
                LoadAuditPlanReport();
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }
        /// <summary>
        /// Author : Neha(KNS)
        /// Date : 30 Jan 2020
        /// Desc : 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void lnkworkcnt_Click(object sender, EventArgs e)
        {
            try
            {
                divreports.Visible = true;

            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }
        /// <summary>
        /// Author : Neha(KNS)
        /// Date : 30 Jan 2020
        /// Desc :
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void lnkplantcoverage_Click(object sender, EventArgs e)
        {
            try
            {
                divreports.Visible = true;

            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }
        /// <summary>
        /// Author : Neha(KNS)
        /// Date : 30 Jan 2020
        /// Desc : 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void lnksqdclpa_Click(object sender, EventArgs e)
        {
            try
            {
                divreports.Visible = true;

            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }

        }
        /// <summary>
        /// Author : Neha(KNS)
        /// Date : 30 Jan 2020
        /// Desc : 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void lnkregionalfindings_Click(object sender, EventArgs e)
        {
            try
            {
                divreports.Visible = true;

            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }
        /// <summary>
        /// Author : Neha(KNS)
        /// Date : 30 Jan 2020
        /// Desc : 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void lnkfindingsummary_Click(object sender, EventArgs e)
        {
            try
            {
                divreports.Visible = true;

            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }
        /// <summary>
        /// Author : Neha(KNS)
        /// Date : 30 Jan 2020
        /// Desc :
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void lnkopenfindingdetails_Click(object sender, EventArgs e)
        {
            try
            {
                divreports.Visible = true;

            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }
        /// <summary>
        /// Author : Neha(KNS)
        /// Date : 30 Jan 2020
        /// Desc :
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void lnkorphanedfindings_Click(object sender, EventArgs e)
        {
            try
            {
                divreports.Visible = true;

            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }

        }
        /// <summary>
        /// Author : Neha(KNS)
        /// Date : 30 Jan 2020
        /// Desc : 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void lnkuserole_Click(object sender, EventArgs e)
        {
            try
            {
                divreports.Visible = true;

            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }
        /// <summary>
        /// Author:Neha(Kns)
        /// Date  :31 Jan 2020
        /// Desc  : It calls GetAuditResultsForDownloadBL by passing all the filter selection and load result to datatable. It calls ExporttoExcel to download the result as excel.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btndownloadreport_Click(object sender, EventArgs e)
        {
            try
            {
                btnFilter_Click(null, null);
                DataTable dtAuditReports = objDashBL.GetAuditScheduleReportBL(objDashBO);
                if (dtAuditReports != null)
                {
                    ExporttoExcel(dtAuditReports, "AuditScheduleReport");

                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }

        }
        /// <summary>
        /// Author:Neha(Kns)
        /// Date  :31 Jan 2020
        /// Desc  :It calls GetAuditScheduleReportBL function to fetch  all tasks based on filter selection region, country and location 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnFilter_Click(object sender, EventArgs e)
        {
            try
            {
                objDashBO = new DashboardBO();
                objDashBO.region_id = Convert.ToInt32(ddlRegion.SelectedValue);
                objDashBO.report_flag = "Audit Schedule";
                LoadAuditSchedule();
                objDashBO.report_flag = "Audit Plan";
                LoadAuditPlanReport();
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }
        /// <summary>
        /// Author : Neha(KNS)
        /// Date : 30 Jan 2020
        /// Desc :  On click of Clear button, it clears all the dropdown selection to default by calling LoadFilterDropDowns method.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnClear_Click(object sender, EventArgs e)
        {
            try
            {
                objDashBO = new DashboardBO();
                LoadFilterDropDowns(objDashBO);
                grdAuditSchedule.Visible = false;
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Author : Neha(KNS)
        /// Date : 30 Jan 2020
        /// Desc : It converts the datatable to excel file with filename passed in parameter and download.
        /// </summary>
        /// <param name="objDashBO"></param>
        private void LoadFilterDropDowns(DashboardBO objDashBO)
        {
            try
            {
                objDashBL = new DashboardBL();
                dsDropDownData = new DataSet();
                dsDropDownData = objDashBL.getFilterDataBL(objDashBO);
                if (dsDropDownData.Tables.Count > 0)
                {
                    if (dsDropDownData.Tables[0].Rows.Count > 0)
                    {
                        ddlRegion.DataSource = dsDropDownData.Tables[0];
                        ddlRegion.DataTextField = "region_name";
                        ddlRegion.DataValueField = "region_id";
                        ddlRegion.DataBind();
                        ddlRegion.Items.Insert(0, new ListItem("All", "0"));
                        //if (Convert.ToString(Session["GlobalAdminFlag"]) == "N")
                        //{
                        ddlRegion.SelectedValue = Convert.ToString(Session["LoggedInRegionId"]);
                        //    ddlRegion.Attributes["disabled"] = "disabled";
                        //}
                        //else
                        //    ddlRegion.Enabled = true;
                    }
                    else
                    {
                        ddlRegion.Items.Clear();
                        ddlRegion.DataSource = null;
                        ddlRegion.DataBind();
                    }

                }
                objDashBO.region_id = Convert.ToInt32(ddlRegion.SelectedValue);
                dsDropDownData = objDashBL.getFilterDataBL(objDashBO);
                if (dsDropDownData.Tables.Count > 0)
                {
                    if (dsDropDownData.Tables[1].Rows.Count > 0)
                    {
                        ddlCountry.DataSource = dsDropDownData.Tables[1];
                        ddlCountry.DataTextField = "country_name";
                        ddlCountry.DataValueField = "country_id";
                        ddlCountry.DataBind();
                        ddlCountry.Items.Insert(0, new ListItem("All", "0"));
                        ddlCountry.SelectedValue = Convert.ToString(Session["LoggedInCountryId"]);
                    }
                    else
                    {
                        ddlCountry.Items.Clear();
                        ddlCountry.DataSource = null;
                        ddlCountry.DataBind();
                    }
                }
                objDashBO.country_id = Convert.ToInt32(ddlCountry.SelectedValue);
                dsDropDownData = objDashBL.getFilterDataBL(objDashBO);
                if (dsDropDownData.Tables.Count > 0)
                {
                    if (dsDropDownData.Tables[2].Rows.Count > 0)
                    {
                        ddlLocation.DataSource = dsDropDownData.Tables[2];
                        ddlLocation.DataTextField = "location_name";
                        ddlLocation.DataValueField = "location_id";
                        ddlLocation.DataBind();
                        ddlLocation.Items.Insert(0, new ListItem("All", "0"));
                        ddlLocation.SelectedValue = Convert.ToString(Session["LocationId"]);
                    }
                    else
                    {
                        ddlLocation.Items.Clear();
                        ddlLocation.DataSource = null;
                        ddlLocation.DataBind();
                    }

                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        private void ExporttoExcel(DataTable table, string filename)
        {

            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.ClearHeaders();
            HttpContext.Current.Response.Buffer = true;
            HttpContext.Current.Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            HttpContext.Current.Response.Write(@"<!DOCTYPE HTML PUBLIC ""-//W3C//DTD HTML 12.0 Transitional//EN"">");
            HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=" + filename + ".xls");

            HttpContext.Current.Response.Charset = "utf-8";
            HttpContext.Current.Response.ContentEncoding = System.Text.Encoding.GetEncoding("windows-1250");
            //sets font
            HttpContext.Current.Response.Write("<font style='font-size:10.0pt; font-family:Calibri;'>");
            HttpContext.Current.Response.Write("<BR><BR><BR>");
            //sets the table border, cell spacing, border color, font of the text, background, foreground, font height
            HttpContext.Current.Response.Write("<Table border='1' bgColor='#ffffff' " +
              "borderColor='#000000' cellSpacing='0' cellPadding='0' " +
              "style='font-size:10.0pt; font-family:Calibri; background:white;'> <TR>");
            //am getting my grid's column headers
            int columnscount = table.Columns.Count;

            for (int j = 0; j < columnscount; j++)
            {      //write in new column
                HttpContext.Current.Response.Write("<Td>");
                //Get column headers  and make it as bold in excel columns
                HttpContext.Current.Response.Write("<B>");
                HttpContext.Current.Response.Write(table.Columns[j].ToString());
                HttpContext.Current.Response.Write("</B>");
                HttpContext.Current.Response.Write("</Td>");
            }
            HttpContext.Current.Response.Write("</TR>");
            foreach (DataRow row in table.Rows)
            {//write in new row
                HttpContext.Current.Response.Write("<TR>");
                for (int i = 0; i < table.Columns.Count; i++)
                {
                    HttpContext.Current.Response.Write("<Td>");
                    HttpContext.Current.Response.Write(row[i].ToString());
                    HttpContext.Current.Response.Write("</Td>");
                }

                HttpContext.Current.Response.Write("</TR>");
            }
            HttpContext.Current.Response.Write("</Table>");
            HttpContext.Current.Response.Write("</font>");
            HttpContext.Current.Response.Flush();
            HttpContext.Current.Response.End();


        }

        private void LoadAuditSchedule()
        {
            DataTable dtAuditReports;
            try
            {
              
                objDashBO = new DashboardBO();
                objDashBL = new DashboardBL();
                dtAuditReports = new DataTable();
                objDashBO.location_id = Convert.ToInt32(ddlLocation.SelectedValue);
                objDashBO.region_id = Convert.ToInt32(ddlRegion.SelectedValue);
                objDashBO.country_id = Convert.ToInt32(ddlCountry.SelectedValue);

                string selectedDate = reportrange.Text;
                if (string.IsNullOrEmpty(selectedDate))
                {

                    selectedDate = String.Format("{0:dd/MM/yyyy}", DateTime.Now) + "-" + String.Format("{0:dd/MM/yyyy}", DateTime.Now);
                }
                if (selectedDate.Contains("/"))
                {
                    string DBPattern = "dd-MM-yyyy";
                    string UIpattern = "dd/MM/yyyy";
                    DateTime startDate;
                    DateTime endDate;
                    string[] splittedDates = selectedDate.Split("-".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                    if (DateTime.TryParseExact(Convert.ToString(splittedDates[0].TrimEnd().Trim()), UIpattern, null, DateTimeStyles.None, out startDate))
                    //if (DateTime.TryParse(splittedDates[0], out startDate))
                    {
                        string frmdate = startDate.ToString(DBPattern);
                        objDashBO.startdate = frmdate;
                        objDashBO.l_st_date = startDate;
                    }
                    if (DateTime.TryParseExact(Convert.ToString(splittedDates[1].TrimStart().Trim()), UIpattern, null, DateTimeStyles.None, out endDate))
                    //if (DateTime.TryParse(splittedDates[1], out endDate))
                    {
                        string Todate = endDate.ToString(DBPattern);
                        objDashBO.enddate = Todate;
                        objDashBO.l_end_date = endDate;
                    }
                }

                dtAuditReports = objDashBL.GetAuditScheduleReportBL(objDashBO);
                grdAuditSchedule.DataSource = dtAuditReports;
                grdAuditSchedule.DataBind();
                Session["AuditScheduleReports"] = dtAuditReports;

            }

            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }

        }

        private void LoadAuditPlanReport()
        {
            DataTable dtAuditReports;
            try
            {

                objDashBO = new DashboardBO();
                objDashBL = new DashboardBL();
                dtAuditReports = new DataTable();
                objDashBO.location_id = Convert.ToInt32(ddlLocation.SelectedValue);
                objDashBO.region_id = Convert.ToInt32(ddlRegion.SelectedValue);
                objDashBO.country_id = Convert.ToInt32(ddlCountry.SelectedValue);

                string selectedDate = reportrange.Text;
                if (string.IsNullOrEmpty(selectedDate))
                {

                    selectedDate = String.Format("{0:dd/MM/yyyy}", DateTime.Now) + "-" + String.Format("{0:dd/MM/yyyy}", DateTime.Now);
                }
                if (selectedDate.Contains("/"))
                {
                    string DBPattern = "dd-MM-yyyy";
                    string UIpattern = "dd/MM/yyyy";
                    DateTime startDate;
                    DateTime endDate;
                    string[] splittedDates = selectedDate.Split("-".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                    if (DateTime.TryParseExact(Convert.ToString(splittedDates[0].TrimEnd().Trim()), UIpattern, null, DateTimeStyles.None, out startDate))
                    //if (DateTime.TryParse(splittedDates[0], out startDate))
                    {
                        string frmdate = startDate.ToString(DBPattern);
                        objDashBO.startdate = frmdate;
                        objDashBO.l_st_date = startDate;
                    }
                    if (DateTime.TryParseExact(Convert.ToString(splittedDates[1].TrimStart().Trim()), UIpattern, null, DateTimeStyles.None, out endDate))
                    //if (DateTime.TryParse(splittedDates[1], out endDate))
                    {
                        string Todate = endDate.ToString(DBPattern);
                        objDashBO.enddate = Todate;
                        objDashBO.l_end_date = endDate;
                    }
                }

                dtAuditReports = objDashBL.GetAuditPlanReportBL(objDashBO);
                grdAuditPlan.DataSource = dtAuditReports;
                grdAuditPlan.DataBind();
                Session["AuditScheduleReports"] = dtAuditReports;

            }

            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }

        }
        #endregion


    }
}