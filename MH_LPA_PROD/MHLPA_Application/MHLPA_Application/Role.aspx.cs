﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MHLPA_BusinessLogic;
using MHLPA_BusinessObject;

namespace MHLPA_Application
{
    public partial class Role : System.Web.UI.Page
    {
        #region GlobalDeclaration
        RoleBO objRoleBO;
        CommonBL objComBL;
        DataSet dsDropDownData;
        RoleBL objRoleBL;
        UsersBO objUserBO;
        CommonFunctions objCom = new CommonFunctions();

        #endregion

        #region Events
        /// <summary>
        /// Author: Anamika(KNS)
        /// Date : Feb 26,2018
        /// Desc: Check for login session and load the login page if session timeout, if not, load all the role details in a grid and the view panel with first row data.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(Convert.ToString(Session["UserId"]))) { Response.Redirect("Login.aspx"); return; }

            int vwroleid;

            if (!IsPostBack)
            {
                if (!string.IsNullOrEmpty(Convert.ToString(Session["UserId"])))
                {
                    try
                    {
                        GridBind();
                        DataTable dtGrid = new DataTable();
                        dtGrid = (DataTable)Session["dtRole"];
                        vwroleid = Convert.ToInt32(dtGrid.Rows[0]["role_id"]);
                        DataRow drfirst = selectedRow(vwroleid);
                        LoadViewpanel(drfirst);
                    }
                    catch (Exception ex)
                    {
                        objCom.ErrorLog(ex);
                    }
                }
            }
        }

        /// <summary>
        /// Author: Anamika(KNS)
        /// Date : Feb 26,2018
        /// Desc: On click of Add button, it will clear all the field from edit panel and display Add panel.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                Clear();
                addpanl.Visible = true;
                viewpanel.Visible = false;
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }


        }

        /// <summary>
        /// Author: Anamika(KNS)
        /// Date : Feb 26,2018
        /// Desc: On click of Save button, it will call the SaveRoleBL with all the details in edit panel to save to database.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            RoleBO Output;
            try
            {
                Output = new RoleBO();
                objRoleBO = new RoleBO();
                objRoleBL = new RoleBL();
                if (!string.IsNullOrEmpty(Convert.ToString(Session["SelectedRoleId"])))
                    objRoleBO.role_id = Convert.ToInt32(Session["SelectedRoleId"]);
                objRoleBO.rolename = Convert.ToString(ddlRole.Text);
                objRoleBO.audits_no = Convert.ToInt32(txtaudits.Text);
                objRoleBO.frequency = Convert.ToString(ddlfrequency.SelectedValue);


                Output = objRoleBL.SaveRoleBL(objRoleBO);

                if (Output.error_code == 0)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "alert('" + Output.error_msg + "');", true);
                    GridBind();
                    DataTable dtGrid = new DataTable();
                    dtGrid = (DataTable)Session["dtRole"];
                    DataRow drfirst = selectedRow(Convert.ToInt32(dtGrid.Rows[0]["role_id"]));
                    LoadViewpanel(drfirst);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "alert('There is some error, please try again.');", true);
                }

            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        /// <summary>
        /// Author: Anamika(KNS)
        /// Date : Feb 26,2018
        /// Desc: On click of any row in gridview, the selected row details will be loaded in view panel by calling LoadViewpanel.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Select(object sender, EventArgs e)
        {
            try
            {
                int roleid = Convert.ToInt32((sender as LinkButton).CommandArgument);
                DataRow drselect = selectedRow(roleid);
                LoadViewpanel(drselect);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        /// <summary>
        /// Author: Anamika(KNS)
        /// Date : Feb 26,2018
        /// Desc:On click of any row in gridview, the selected row details will be loaded in edit panel by calling LoadEditPanel.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Edit(object sender, EventArgs e)
        {
            try
            {
                int roleid = Convert.ToInt32((sender as ImageButton).CommandArgument);
                DataRow drselect = selectedRow(roleid);
                LoadEditPanel(drselect);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        /// <summary>
        /// Author: Anamika(KNS)
        /// Date : Feb 26,2018
        /// Desc: On click of Delete button, it will call the DeleteRoleBL with all the details selected row to delete from database.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Delete(object sender, EventArgs e)
        {
            try
            {
                objRoleBO = new RoleBO();
                objRoleBL=new RoleBL();
                int roleid = Convert.ToInt32((sender as ImageButton).CommandArgument);
                objRoleBO.role_id = roleid;
                objRoleBO=objRoleBL.DeleteRoleBL(objRoleBO);

                if (objRoleBO.error_code == 0)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "alert('" + objRoleBO.error_msg + "');", true);
                    GridBind();
                    DataTable dtGrid = new DataTable();
                    dtGrid = (DataTable)Session["dtRole"];
                    DataRow drfirst = selectedRow(Convert.ToInt32(dtGrid.Rows[0]["role_id"]));
                    LoadViewpanel(drfirst);
                }
                else if (objRoleBO.error_code == 100)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "alert('" + objRoleBO.error_msg + "');", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "alert('There is some error in delete Role, please try again.');", true);
                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        /// <summary>
        /// Author: Anamika(KNS)
        /// Date : Feb 26,2018
        /// Desc: On click of pages in gridview, the selected page will be loaded.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void grdRoles_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdRoles.PageIndex = e.NewPageIndex;
                grdRoles.DataSource = (DataTable)Session["dtRole"];
                grdRoles.DataBind();
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        #endregion

        #region Methods
        /// <summary>
        /// Author: Anamika(KNS)
        /// Date : Feb 26,2018
        /// Desc : Calling GetRoleBL function to fetch datable and load gridview with the datatable.
        /// </summary>
        /// <param name="UserId"></param>
        private void GridBind()
        {
            DataTable dtRoles;
            try
            {
                dtRoles = new DataTable();
                objRoleBL = new RoleBL();
                dtRoles = objRoleBL.GetRoleBL();
                if (dtRoles.Rows.Count > 0)
                {
                    Session["dtRole"] = dtRoles;
                    grdRoles.DataSource = dtRoles;
                }
                else
                {
                    grdRoles.DataSource = null;
                }
                grdRoles.DataBind();
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        /// <summary>
        /// Author: Anamika(KNS)
        /// Date : Feb 26,2018
        /// Desc : Display the add panel and load all the information with respective controls.
        /// </summary>
        /// <param name="drselect">Selected Row from the dridview</param>
        private void LoadEditPanel(DataRow drselect)
        {
            try
            {
                btnAdd_Click(null, null);
                Session["SelectedRoleId"] = Convert.ToString(drselect["role_id"]);
                ddlRole.Text = Convert.ToString(drselect["rolename"]);
                ddlfrequency.SelectedValue = Convert.ToString(drselect["frequency"]);
                txtaudits.Text = Convert.ToString(drselect["no_of_audits_required"]);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        /// <summary>
        /// Author: Anamika(KNS)
        /// Date : Feb 26,2018
        /// Desc : To fetch the complete row details with roleid
        /// </summary>
        /// <param name="userid">Selected Row Role id</param>
        /// <returns>Selected Row details</returns>
        private DataRow selectedRow(int roleid)
        {
            DataTable dtGrid = (DataTable)Session["dtRole"];
            DataRow drselect = (from DataRow dr in dtGrid.Rows
                                where (int)dr["role_id"] == roleid
                                select dr).FirstOrDefault();

            return drselect;
        }

        /// <summary>
        /// Author: Anamika(KNS)
        /// Date : Feb 26,2018
        /// Desc : Display the view panel and load all the information with respective controls.
        /// </summary>
        /// <param name="drselect">Selected Row from the dridview</param>
        public void LoadViewpanel(DataRow drselect)
        {
            try
            {
                addpanl.Visible = false;
                viewpanel.Visible = true;
                lblrole.Text = Convert.ToString(drselect["rolename"]);
                lblaudits.Text = Convert.ToString(drselect["no_of_audits_required"]);
                lblmeasurement.Text = Convert.ToString(drselect["frequency"]);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        /// <summary>
        /// Author: Anamika(KNS)
        /// Date : Feb 26,2018
        /// Desc : Clears all the session related to the page and fields
        /// </summary>
        private void Clear()
        {
            ddlRole.Text = "";
            txtaudits.Text = "";
            ddlfrequency.SelectedValue = "Per Shift";
            Session["SelectedRoleId"] = "";
        }
        #endregion

    }
}