﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DeleteAudit.aspx.cs" Inherits="MHLPA_Application.DeleteAudit" MasterPageFile="~/Site.Master" EnableEventValidation="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">

    <script type="text/javascript">
      
        function Confirm() {
            debugger;
            var confirm_value = document.createElement("INPUT");
            confirm_value.type = "hidden";
            confirm_value.name = "confirm_value";
            if (confirm("Do you want to delete the  Record?")) {
                confirm_value.value = "Yes";
            } else {
                confirm_value.value = "No";
            }
            window.location.reload(true);
            document.forms[0].appendChild(confirm_value);
        }
    </script>
    <style>
        table, td, th {
            border: 1px solid black;
        }

        table {
            border-collapse: collapse;
        }

        th {
            height: 50px;
        }

        td, th {
            border: 1px solid #b3dec8;
            text-align: center;
            padding: 8px 0;
            background:#fff
        }

        .color {
            background-color: #cdead2;
        }

        #MainContent_tblAuditResult {
            border-collapse: collapse;
            height: 250px;
            text-align: center;
            width:100%
        }

        #MainContent_pnlaudit {
            margin: 0 auto;
            width:50%
        }
        /*.mn_body_main {
            margin: 20px 0 0;
        }*/
        td:first-child {background: #cdead2}
        td:nth-child(2n+3) {background: #cdead2}
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:ScriptManager runat="server" EnablePageMethods="true"></asp:ScriptManager>
     <div>
        <div class='block-web' style='float: left; width: 100%; height: 36px;'>
            <div class="header">
                <div class="crumbs">
                    <!-- Start : Breadcrumbs -->
                    <ul id="breadcrumbs" class="breadcrumb">
                        <li>
                            <a class="mn_breadcrumb">SetUp</a>
                        </li>
                        <li class="current">Delete Audit</li>

                    </ul>
                </div>
            </div>
        </div>
    </div>
            <asp:UpdatePanel runat="server" ID="updpnlDelete">
                <ContentTemplate>
    <div class="row">
        <div>
            <div class="clearfix"></div>
            <div class="filter_panel">
                <div>
                    <div class="col-md-4 filter_label" style="width: 14%">
                        <p>Audit Number* : </p>
                    </div>
                    <div class="col-md-4" style="width: 20%">
                        <asp:TextBox class="form-control1 mn_inp control3 filter_select_control" runat="server" ID="txtAuditID" AutoPostBack="true"></asp:TextBox>
                    </div>

                    <div class="col-md-4" style="width: 20%">
                        <asp:Button runat="server" ID="btnSubmit" CssClass="btn-add" Text="Submit" OnClick="btnSubmit_Click" />
                        <asp:Button runat="server" ID="btncancel" CssClass="btn-add" Text="Cancel" OnClick="btnCancel_Click" />
                    </div>
                </div>
            </div>
        </div>
    </div>
    <br />
    <asp:Panel runat="server" ID="pnlaudit">
        <asp:Label runat="server" ID="lblError" ForeColor="Red"></asp:Label>
        <table border="1" id="tblAuditResult" runat="server">
            <tr>
                <td>AuditID</td>
                <td>
                    <asp:Label runat="server" ID="lblaud_id"></asp:Label></td>
                <td>Location_Name</td>
                <td>
                    <asp:Label ID="lblocation_name" runat="server"></asp:Label></td>
                <td>Product</td>
                <td>
                    <asp:Label ID="lblproduct" runat="server"></asp:Label></td>
            </tr>
            <tr>
                <td>Conducted On</td>
                <td>
                    <asp:Label ID="lblconducted_on" runat="server"></asp:Label></td>
                <td>Conducted By</td>
                <td>
                    <asp:Label ID="lblconducted_by" runat="server"></asp:Label></td>
                <td>Area</td>
                <td>
                    <asp:Label ID="lblBuilding_name" runat="server"></asp:Label></td>
            </tr>
            <tr>
                <td>Line Name</td>
                <td>
                    <asp:Label ID="lblinename" runat="server"></asp:Label></td>
                <td>Score</td>
                <td>
                    <asp:Label ID="lblscore" runat="server"></asp:Label></td>
                <td>No Of Shift</td>
                <td>
                    <asp:Label ID="lblnoshifts" runat="server"></asp:Label></td>
            </tr>
        </table>
        <asp:Button runat="server" ID="btnDelete" CssClass="btn-add" Text="Delete" OnClientClick="Confirm()" OnClick="btnDelete_Click" />
        <asp:Button runat="server" ID="btnclear" CssClass="btn-add" Text="Clear" OnClick="btnClear_Click" />
    </asp:Panel>
    </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="btnSubmit" EventName="Click" />
                    <asp:AsyncPostBackTrigger ControlID="btncancel" EventName="Click" />
                    <asp:AsyncPostBackTrigger ControlID="btnDelete" EventName="Click" />
                    <asp:AsyncPostBackTrigger ControlID="btnclear" EventName="Click" />
                </Triggers>

            </asp:UpdatePanel>
</asp:Content>
