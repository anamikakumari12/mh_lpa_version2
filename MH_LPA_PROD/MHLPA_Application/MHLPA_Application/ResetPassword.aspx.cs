﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MHLPA_BusinessLogic;
using MHLPA_BusinessObject;

namespace MHLPA_Application
{
    public partial class ResetPassword : System.Web.UI.Page
    {
        #region GlobalDeclaration
        UsersBO objUserBO;
        CommonBL objComBL;
        UsersBL objUsersBL;
        CommonFunctions objCom = new CommonFunctions();
        LoginAuthenticateBL objAuth;
        #endregion

        #region Events
     
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                int UserId;
                if (string.IsNullOrEmpty(Convert.ToString(Session["UserId"]))) { Response.Redirect("Login.aspx"); return; }
                else
                {
                    UserId = Convert.ToInt32(Session["UserId"]);
                }
                if (!IsPostBack)
                {
                    txtConfirm.Text = "";
                    txtNew.Text = "";
                    txtOld.Text = "";
                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            try
            {
                objUserBO = new UsersBO();
                objAuth = new LoginAuthenticateBL();
                objUsersBL = new UsersBL();
                objUserBO.UserName = Convert.ToString(Session["Username"]);
                objUserBO.Password = objAuth.Encrypt(Convert.ToString(txtNew.Text));
                string output = objUsersBL.ChangePasswordBL(objUserBO);
                if (!string.IsNullOrEmpty(output))
                {
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ShowPopup", "alert('Password has been updated successfully.');", true);
                    txtConfirm.Text = "";
                    txtNew.Text = "";
                    txtOld.Text = "";
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ShowPopup", "alert('There is some error in updating password. Please try again.');", true);
                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        #endregion
    }
}