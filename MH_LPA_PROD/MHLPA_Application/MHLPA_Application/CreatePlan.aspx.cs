﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using MHLPA_BusinessLogic;
using MHLPA_BusinessObject;

namespace MHLPA_Application
{
    public partial class CreatePlan : System.Web.UI.Page
    {
        #region Global Declaration
        UsersBO objUserBO;
        CommonBL objComBL;
        DataSet dsDropDownData;
        PlanBO objPlanBO;
        CommonFunctions objCom = new CommonFunctions();
        PlanBL objPlanBL;
        int j = 0;
        #endregion

        #region Events
        /// <summary>
        /// Author : Anamika(KNS)
        /// Date : 23 Oct 2017
        /// Desc :
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(Convert.ToString(Session["UserId"]))) { Response.Redirect("Login.aspx"); return; }

            if (!IsPostBack)
            {
                try
                {
                    if (!string.IsNullOrEmpty(Convert.ToString(Session["UserId"])))
                        GetMasterDropdown();

                }
                catch (Exception ex)
                {
                    objCom.ErrorLog(ex);
                }
            }
        }

        /// <summary>
        /// Author : Anamika(KNS)
        /// Date : 23 Oct 2017
        /// Desc :
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlRegion_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dtCountry = new DataTable();
            DataTable dtLocation = new DataTable();
            try
            {
                dsDropDownData = new DataSet();
                objUserBO = new UsersBO();
                objUserBO.UserId = Convert.ToInt32(Session["UserId"]);
                objComBL = new CommonBL();

                objUserBO.region_id = Convert.ToInt32(ddlRegion.SelectedValue);
                dtCountry = objComBL.GetCountryBasedOnRegionBL(objUserBO);

                if (dtCountry.Rows.Count > 0)
                {
                    ddlCountry.DataSource = dtCountry;
                    ddlCountry.DataTextField = "country_name";
                    ddlCountry.DataValueField = "country_id";
                    ddlCountry.DataBind();
                    objUserBO.country_id = Convert.ToInt32(ddlCountry.SelectedValue);
                }
                else
                {
                    ddlCountry.Items.Clear();
                    ddlCountry.DataSource = null;
                    ddlCountry.DataBind();
                }

                dtLocation = objComBL.GetLocationBasedOnCountryBL(objUserBO);
                if (dtLocation.Rows.Count > 0)
                {
                    ddlLocation.DataSource = dtLocation;
                    ddlLocation.DataTextField = "location_name";
                    ddlLocation.DataValueField = "location_id";
                    ddlLocation.DataBind();
                    objUserBO.location_id = Convert.ToInt32(ddlLocation.SelectedValue);
                    Session["dtLocation"] = dtLocation;
                    ddlLocation_SelectedIndexChanged(null, null);
                }
                else
                {
                    ddlLocation.Items.Clear();
                    ddlLocation.DataSource = null;
                    ddlLocation.DataBind();
                }

            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        /// <summary>
        /// Author : Anamika(KNS)
        /// Date : 23 Oct 2017
        /// Desc :
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlCountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dtCountry = new DataTable();
            DataTable dtLocation = new DataTable();
            try
            {
                dsDropDownData = new DataSet();
                objUserBO = new UsersBO();
                objUserBO.UserId = Convert.ToInt32(Session["UserId"]);
                objComBL = new CommonBL();

                objUserBO.country_id = Convert.ToInt32(ddlCountry.SelectedValue);
                dtLocation = objComBL.GetLocationBasedOnCountryBL(objUserBO);
                if (dtLocation.Rows.Count > 0)
                {
                    ddlLocation.DataSource = dtLocation;
                    ddlLocation.DataTextField = "location_name";
                    ddlLocation.DataValueField = "location_id";
                    ddlLocation.DataBind();
                    objUserBO.location_id = Convert.ToInt32(ddlLocation.SelectedValue);
                    Session["dtLocation"] = dtLocation;
                    ddlLocation_SelectedIndexChanged(null, null);
                }
                else
                {
                    ddlLocation.Items.Clear();
                    ddlLocation.DataSource = null;
                    ddlLocation.DataBind();
                }

            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        /// <summary>
        /// Author : Anamika(KNS)
        /// Date : 23 Oct 2017
        /// Desc :
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlLocation_SelectedIndexChanged(object sender, EventArgs e)
        {

            DataTable dtEmployeeusers = new DataTable();
            try
            {
                objUserBO = new UsersBO();
                objComBL = new CommonBL();
                objUserBO.location_id = Convert.ToInt32(ddlLocation.SelectedValue);
                dtEmployeeusers = objComBL.GetEmployeesBasedOnLocationBL(objUserBO);
                Session["dtEmployeeusers"] = dtEmployeeusers;
                if (dtEmployeeusers.Rows.Count > 0)
                {
                    rblEmployee.DataSource = dtEmployeeusers;
                    rblEmployee.DataTextField = "emp_full_name";
                    rblEmployee.DataValueField = "user_id";
                    rblEmployee.DataBind();
                    rblEmployee.Items.Insert(0, new ListItem("None", "0"));
                }

                DataTable dtBuilding = new DataTable();
                objUserBO = new UsersBO();
                objUserBO.UserId = Convert.ToInt32(Session["UserId"]);
                objUserBO.location_id = Convert.ToInt32(ddlLocation.SelectedValue);
                dtBuilding = objComBL.GetBuildingListDropDownBL(objUserBO);

                if (dtBuilding.Rows.Count > 0)
                {
                    ddlBuilding.DataSource = dtBuilding;
                    ddlBuilding.DataTextField = "building_name";
                    ddlBuilding.DataValueField = "building_id";
                    ddlBuilding.DataBind();
                    DataTable dtLine = objComBL.GetLineListDropDownBL(objUserBO);
                    if (dtLine.Rows.Count < 20)
                    {
                        if (dtBuilding.Rows.Count > 1)
                            ddlBuilding.Items.Insert(ddlBuilding.Items.Capacity, new ListItem("All", "0"));
                    }
                    if (Convert.ToString(ddlLocation.SelectedValue) == "10")
                    {
                        if (dtLine.Rows.Count <= 100)
                        {
                            if (dtBuilding.Rows.Count > 1)
                                ddlBuilding.Items.Insert(ddlBuilding.Items.Capacity, new ListItem("All", "0"));
                        }
                    }
                }
                else
                {
                    ddlBuilding.Items.Clear();
                    ddlBuilding.DataSource = null;
                    ddlBuilding.DataBind();
                }


                CreateplanGrid.Visible = false;
                btnSave.Visible = false;
                lblMessage.Text = "Please click on Filter button after your selection.";
                //j = 0;
                //objUserBO = new UsersBO();
                //objComBL = new CommonBL();
                //Session["SelectedLocation"] = Convert.ToString(ddlLocation.SelectedValue);
                //dtLocation = (DataTable)Session["dtLocation"];
                //objUserBO.location_id = Convert.ToInt32(ddlLocation.SelectedValue);
                //dtLine = objComBL.GetLineListDropDownBL(objUserBO);
                //BindData();
                //if (dtLine.Rows.Count > 0)
                //{
                //    CreateplanGrid.DataSource = dtLine;
                //    CreateplanGrid.DataBind();
                //    Session["dtLine"] = dtLine;
                //    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ShowPopup", "LoadGrid();", true);
                //}
                //else
                //{
                //    CreateplanGrid.DataSource = null;
                //    CreateplanGrid.DataBind();
                //}

            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        /// <summary>
        /// Author : Anamika(KNS)
        /// Date : 23 Oct 2017
        /// Desc :
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            DataTable dtEmployeeusers = new DataTable();
            DataTable dtUsers = new DataTable();
            int currentYear = Convert.ToInt32(ddlYear.SelectedValue);
            int currentyearweeks = GetWeeksInYear(currentYear);
            int weeks = currentyearweeks - 1;
            int currentweek = GetIso8601WeekOfYear(DateTime.Now);
            int disabledweek = currentweek-1;
            Button ddlusers1;
            HiddenField hdnusers1, hdnLineWeek;
            string id, hdnid, hdnlwid;
            dtUsers = (DataTable)Session["dtOutput"];
            try
            {
                if (currentyearweeks < 53)
                {
                    if (e.Row.Cells.Count > 54)
                    {
                        e.Row.Cells[54].Visible = false;
                    }
                }
                //if (e.Row.RowType == DataControlRowType.Header)
                //{
                //    objUserBO.location_id = Convert.ToInt32(ddlLocation.SelectedValue);
                //    dtEmployeeusers = objComBL.GetEmployeesBasedOnLocationBL(objUserBO);
                //    Session["dtEmployeeusers"] = dtEmployeeusers;
                //    if(dtEmployeeusers.Rows.Count>0)
                //    {
                //        rblEmployee.DataSource = dtEmployeeusers;
                //        rblEmployee.DataTextField = "emp_full_name";
                //        rblEmployee.DataValueField = "user_id";
                //        rblEmployee.DataBind();
                //    }
                //}
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label line = e.Row.FindControl("LbllineNoId") as Label;
                    Label linename = e.Row.FindControl("LblLineNo") as Label;

                    //Find the DropDownList in the Row             
                    //objUserBO.location_id = Convert.ToInt32(ddlLocation.SelectedValue);
                    //dtEmployeeusers = (DataTable)Session["dtEmployeeusers"];

                    for (int i = 1; i <= 53; i++)
                    {
                        id = "ddlusers" + i;
                        hdnid = "hdnusers" + i;
                        hdnlwid = "hdnLineWeek" + i;
                        //objCom.TestLog(id);
                        ddlusers1 = (e.Row.FindControl(id) as Button);
                        hdnusers1 = (e.Row.FindControl(hdnid) as HiddenField);
                        hdnLineWeek = (e.Row.FindControl(hdnlwid) as HiddenField);
                        hdnLineWeek.Value = Convert.ToString(linename.Text) + "(Week" + Convert.ToString(i) + ")";


                        if ((i <= disabledweek && currentYear == Convert.ToInt32(DateTime.Now.Year)) || currentYear < Convert.ToInt32(DateTime.Now.Year))
                        {
                            ddlusers1.Enabled = false;
                        }
                        else
                        {
                            ddlusers1.Enabled = true;
                        }
                        //if (dtUsers != null)
                        //{
                        //    if (dtUsers.Rows.Count > 0)
                        //    {
                        //        if (Convert.ToString(dtUsers.Rows[j][1]) == Convert.ToString(line.Text))
                        //        {
                        //            if (Convert.ToString(dtUsers.Rows[j][i + 3]) == "0")
                        //            {
                        //                if (i <= disabledweek && currentYear == Convert.ToInt32(DateTime.Now.Year))
                        //                {
                        //                    ddlusers1.Text = "";
                        //                }
                        //                else
                        //                {
                        //                    ddlusers1.Text = "Pick Name";
                        //                }
                        //            }
                        //            else
                        //            {
                        //                ddlusers1.Text = Convert.ToString(dtUsers.Rows[j]["wkn" + i]);
                        //                hdnusers1.Value = Convert.ToString(dtUsers.Rows[j][i + 3]);
                        //                //DataRow[] dr = dtEmployeeusers.Select("user_id = " + Convert.ToString(dtUsers.Rows[j][i + 3]));
                        //                //if (dr.Length > 0)
                        //                //{
                        //                //    ddlusers1.Text = Convert.ToString(dr[0]["emp_full_name"]);
                        //                //    hdnusers1.Value = Convert.ToString(dtUsers.Rows[j][i + 3]);
                        //                //}
                        //                //else
                        //                //{
                        //                //    ddlusers1.Text = "Pick Name";
                        //                //}
                        //            }
                        //        }
                        //    }
                        //}
                    }

                    j++;
                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }

        }

        /// <summary>
        /// Author : Anamika(KNS)
        /// Date : 23 Oct 2017
        /// Desc :
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            int currentYear = Convert.ToInt32(ddlYear.SelectedValue);
            int currentyearweeks = GetWeeksInYear(currentYear);
            objCom.TestLog("Week Number :" + Convert.ToString(currentyearweeks));
            int weeks = currentyearweeks - 1;
            objCom.TestLog("Week No :" + Convert.ToString(weeks));
            objPlanBL = new PlanBL();
            objPlanBO = new PlanBO();
            DataTable SelectedUsers = new DataTable();
            SelectedUsers.Columns.Add("location_id");
            SelectedUsers.Columns.Add("line_id");
            SelectedUsers.Columns.Add("line_name");
            SelectedUsers.Columns.Add("year");
            for (int i = 1; i <= 53; i++)
            {
                SelectedUsers.Columns.Add("wk" + i);
            }
            SelectedUsers.Columns.Add("building_id");
            DataSet dtMailDetails = new DataSet();
            try
            {
                if (CreateplanGrid.Rows.Count != 0)
                {
                    HiddenField Username;
                    DataRow dr;
                    Label linename;
                    Label linenum;
                    string id;
                    foreach (GridViewRow row in CreateplanGrid.Rows)
                    {
                        linename = (row.FindControl("LblLineNo") as Label);
                        linenum = (row.FindControl("LbllineNoId") as Label);
                        dr = SelectedUsers.NewRow();
                        dr["location_id"] = Convert.ToString(ddlLocation.SelectedValue);
                        dr["line_id"] = Convert.ToString(linenum.Text);
                        dr["line_name"] = Convert.ToString(linename.Text);
                        dr["year"] = Convert.ToInt32(ddlYear.SelectedValue);
                        for (int i = 1; i <= 53; i++)
                        {
                            id = "hdnusers" + i;
                            Username = (row.FindControl(id) as HiddenField);
                            dr["wk" + i] = Convert.ToString(Username.Value);
                            objCom.TestLog(Convert.ToString(linename.Text) + ", week" + i + " : " + Convert.ToString(Username.Value));
                        }
                        dr["building_id"]= Convert.ToInt32(ddlBuilding.SelectedValue);
                        SelectedUsers.Rows.Add(dr);

                    }
                    dtMailDetails = objPlanBL.SaveBulkPlanningBL(SelectedUsers);
                    if (dtMailDetails != null)
                    {
                        if (dtMailDetails.Tables[0] != null)
                        {
                            if (dtMailDetails.Tables[0].Rows.Count > 0)
                            {
                                SendInvitation(dtMailDetails.Tables[0]);
                                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ShowPopup", "alert('Plans are created and mails are sent to respective employees.'); LoadGrid();", true);
                                //  BindData();
                                btnFilter_Click(null, null);
                            }
                            else
                            {
                                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ShowPopup", "alert('There is no plan saved or there is error in saving. Please try again.'); LoadGrid();", true);
                            }
                        }
                        if (dtMailDetails.Tables[1] != null)
                        {
                            if (dtMailDetails.Tables[1].Rows.Count > 0)
                            {
                                CancelInvitation(dtMailDetails.Tables[1]);
                                //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ShowPopup", "alert('Plans are created and mails are sent to respective employees.');", true);
                                //  BindData();
                            }
                            //else
                            //{
                            //    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ShowPopup", "alert('There is no plan saved or there is error in saving. Please try again.');", true);
                            //}
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        /// <summary>
        /// Author : Anamika(KNS)
        /// Date : 23 Oct 2017
        /// Desc :
        /// </summary>
        /// <param name="dtMailDetails"></param>
        private void CancelInvitation(DataTable dtMailDetails)
        {
            eAppointmentMail objApptEmail;
            try
            {
                for (int i = 0; i < dtMailDetails.Rows.Count; i++)
                {
                    if (!string.IsNullOrEmpty(Convert.ToString(dtMailDetails.Rows[i]["email_id"])))
                    {
                        objApptEmail = new eAppointmentMail();
                        objApptEmail.Body = "Audit for " + Convert.ToString(dtMailDetails.Rows[i]["line_name"]) + " between " + Convert.ToString(dtMailDetails.Rows[i]["start_date"]) + " and " + Convert.ToString(dtMailDetails.Rows[i]["end_date"]) + " is cancelled. Please contact the LPA Administrator if you need to make any changes.";
                        objApptEmail.Email = Convert.ToString(dtMailDetails.Rows[i]["email_id"]);
                        //objApptEmail.EndDate = Convert.ToDateTime(dtMailDetails.Rows[i]["end_date"]);
                        objApptEmail.Location = Convert.ToString(dtMailDetails.Rows[i]["line_name"]) + ", " + Convert.ToString(dtMailDetails.Rows[i]["location_name"]);
                        // objApptEmail.StartDate = Convert.ToDateTime(dtMailDetails.Rows[i]["start_date"]);
                        objApptEmail.Subject = "Your audit planned from " + Convert.ToString(dtMailDetails.Rows[i]["start_date"]) + " to " + Convert.ToString(dtMailDetails.Rows[i]["end_date"]) + " for " + Convert.ToString(dtMailDetails.Rows[i]["line_name"]) + " is cancelled";
                        objApptEmail.Name = Convert.ToString(dtMailDetails.Rows[i]["email_id"]);
                        objApptEmail.newStartDate = Convert.ToString(dtMailDetails.Rows[i]["start_date_formatted"]);
                        objApptEmail.newEndDate = Convert.ToString(dtMailDetails.Rows[i]["end_date_formatted"]);
                        objCom.CancelInvitation(objApptEmail);
                    }
                }

            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        /// <summary>
        /// Author : Anamika(KNS)
        /// Date : 23 Oct 2017
        /// Desc : It will call SendInvitation function by passing email body, subject, location for sending mails to respective users. The details are available in datatable passed in parameter.
        /// </summary>
        /// <param name="dtMailDetails">Details for sending mail</param>
        private void SendInvitation(DataTable dtMailDetails)
        {
            eAppointmentMail objApptEmail;
            try
            {
                for (int i = 0; i < dtMailDetails.Rows.Count; i++)
                {
                    if (!string.IsNullOrEmpty(Convert.ToString(dtMailDetails.Rows[i]["email_id"])))
                    {
                        objApptEmail = new eAppointmentMail();
                        objApptEmail.Body = "Audit for " + Convert.ToString(dtMailDetails.Rows[i]["line_name"]) + " needs to be performed between " + Convert.ToString(dtMailDetails.Rows[i]["start_date"]) + " and " + Convert.ToString(dtMailDetails.Rows[i]["end_date"]) + ". Please contact the LPA Administrator if you need to make any changes.";
                        objApptEmail.Email = Convert.ToString(dtMailDetails.Rows[i]["email_id"]);
                        // objApptEmail.EndDate = Convert.ToDateTime(dtMailDetails.Rows[i]["end_date"]);
                        objApptEmail.Location = Convert.ToString(dtMailDetails.Rows[i]["line_name"]) + ", " + Convert.ToString(dtMailDetails.Rows[i]["location_name"]);
                        // objApptEmail.StartDate = Convert.ToDateTime(dtMailDetails.Rows[i]["start_date"]);
                        objApptEmail.Subject = "You have an audit planned from " + Convert.ToString(dtMailDetails.Rows[i]["start_date"]) + " to " + Convert.ToString(dtMailDetails.Rows[i]["end_date"]) + " for " + Convert.ToString(dtMailDetails.Rows[i]["line_name"]);
                        objApptEmail.Name = Convert.ToString(dtMailDetails.Rows[i]["email_id"]);
                        objApptEmail.newStartDate = Convert.ToString(dtMailDetails.Rows[i]["start_date_formatted"]);
                        objApptEmail.newEndDate = Convert.ToString(dtMailDetails.Rows[i]["end_date_formatted"]);
                        objCom.SendInvitation(objApptEmail);
                    }
                }

            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        /// <summary>
        /// Author : Anamika(KNS)
        /// Date : 23 Oct 2017
        /// Desc : On changing the year value, it calls ddlLocation_SelectedIndexChanged
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlYear_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlLocation_SelectedIndexChanged(null, null);
        }

        #endregion

        #region Methods
        /// <summary>
        /// Author : Anamika(KNS)
        /// Date : 23 Oct 2017
        /// Desc : It calls GetAllMasterBL to fetch details of region,c ountry and location and it binds with the dropdowns.
        /// </summary>
        private void GetMasterDropdown()
        {
            DataTable dtCountry = new DataTable();
            DataTable dtLocation = new DataTable();
            DataTable dtLine = new DataTable();
            DataTable dtEmployee = new DataTable();
            DataTable dtPart = new DataTable();
            try
            {
                dsDropDownData = new DataSet();
                objUserBO = new UsersBO();
                objUserBO.UserId = Convert.ToInt32(Session["UserId"]);
                objComBL = new CommonBL();
                dsDropDownData = objComBL.GetAllMasterBL(objUserBO);
                if (dsDropDownData.Tables.Count > 0)
                {
                    if (dsDropDownData.Tables[0].Rows.Count > 0)
                    {
                        ddlRegion.DataSource = dsDropDownData.Tables[0];
                        ddlRegion.DataTextField = "region_name";
                        ddlRegion.DataValueField = "region_id";
                        ddlRegion.DataBind();
                        ddlRegion.SelectedValue = Convert.ToString(Session["LoggedInRegionId"]);
                        if (Convert.ToString(Session["GlobalAdminFlag"]) == "N")
                        {
                            //     ddlRegion.SelectedValue = Convert.ToString(Session["LoggedInRegionId"]);
                            ddlRegion.Attributes["disabled"] = "disabled";
                        }
                        else
                            ddlRegion.Enabled = true;
                    }
                    else
                    {
                        ddlRegion.DataSource = null;
                        ddlRegion.DataBind();
                    }
                }
                if (!string.IsNullOrEmpty(ddlRegion.SelectedValue))
                {
                    objUserBO.region_id = Convert.ToInt32(ddlRegion.SelectedValue);
                    dtCountry = objComBL.GetCountryBasedOnRegionBL(objUserBO);

                    if (dtCountry.Rows.Count > 0)
                    {
                        ddlCountry.DataSource = dtCountry;
                        ddlCountry.DataTextField = "country_name";
                        ddlCountry.DataValueField = "country_id";
                        ddlCountry.DataBind();
                        ddlCountry.SelectedValue = Convert.ToString(Session["LoggedInCountryId"]);
                        if (Convert.ToString(Session["GlobalAdminFlag"]) == "N")
                        {
                            //ddlCountry.SelectedValue = Convert.ToString(Session["LoggedInCountryId"]);
                            ddlCountry.Attributes["disabled"] = "disabled";
                        }
                        else
                            ddlCountry.Enabled = true;
                    }
                    else
                    {
                        ddlCountry.Items.Clear();
                        ddlCountry.DataSource = null;
                        ddlCountry.DataBind();
                    }
                }
                else
                {
                    ddlCountry.Items.Clear();
                    ddlCountry.DataSource = null;
                    ddlCountry.DataBind();
                }
                if (!string.IsNullOrEmpty(ddlCountry.SelectedValue))
                {
                    objUserBO.country_id = Convert.ToInt32(ddlCountry.SelectedValue);
                    dtLocation = objComBL.GetLocationBasedOnCountryBL(objUserBO);
                    if (dtLocation.Rows.Count > 0)
                    {
                        ddlLocation.DataSource = dtLocation;
                        ddlLocation.DataTextField = "location_name";
                        ddlLocation.DataValueField = "location_id";
                        ddlLocation.DataBind();
                        ddlLocation.SelectedValue = Convert.ToString(Session["LocationId"]);
                        if (Convert.ToString(Session["GlobalAdminFlag"]) == "N")
                        {
                            //ddlLocation.SelectedValue = Convert.ToString(Session["LocationId"]);
                            ddlLocation.Attributes["disabled"] = "disabled";
                        }
                        else
                            ddlLocation.Enabled = true;
                        Session["SelectedLocation"] = Convert.ToString(ddlLocation.SelectedValue);
                        Session["dtLocation"] = dtLocation;
                    }
                    else
                    {
                        ddlLocation.Items.Clear();
                        ddlLocation.DataSource = null;
                        ddlLocation.DataBind();
                    }
                }
                else
                {
                    ddlLocation.Items.Clear();
                    ddlLocation.DataSource = null;
                    ddlLocation.DataBind();
                }

                DataTable dtBuilding = new DataTable();
                objUserBO = new UsersBO();
                objUserBO.UserId = Convert.ToInt32(Session["UserId"]);
                objUserBO.location_id = Convert.ToInt32(ddlLocation.SelectedValue);
                dtBuilding = objComBL.GetBuildingListDropDownBL(objUserBO);

                if (dtBuilding.Rows.Count > 0)
                {
                    ddlBuilding.DataSource = dtBuilding;
                    ddlBuilding.DataTextField = "building_name";
                    ddlBuilding.DataValueField = "building_id";
                    ddlBuilding.DataBind();
                }
                else
                {
                    ddlBuilding.Items.Clear();
                    ddlBuilding.DataSource = null;
                    ddlBuilding.DataBind();
                }

                ddlYear.Items.Insert(0, new ListItem(Convert.ToString(DateTime.Now.Year), Convert.ToString(DateTime.Now.Year)));
                ddlYear.Items.Insert(1, new ListItem(Convert.ToString(Convert.ToInt32(DateTime.Now.Year) + 1), Convert.ToString(Convert.ToInt32(DateTime.Now.Year) + 1)));
                ddlYear.Items.Insert(1, new ListItem(Convert.ToString(Convert.ToInt32(DateTime.Now.Year) - 1), Convert.ToString(Convert.ToInt32(DateTime.Now.Year) - 1)));
                BindData();

                objUserBO.location_id = Convert.ToInt32(ddlLocation.SelectedValue);
                DataTable dtEmployeeusers = new DataTable();
                dtEmployeeusers = objComBL.GetEmployeesBasedOnLocationBL(objUserBO);
                Session["dtEmployeeusers"] = dtEmployeeusers;
                if (dtEmployeeusers.Rows.Count > 0)
                {
                    rblEmployee.DataSource = dtEmployeeusers;
                    rblEmployee.DataTextField = "emp_full_name";
                    rblEmployee.DataValueField = "user_id";
                    rblEmployee.DataBind();
                    rblEmployee.Items.Insert(0, new ListItem("None", "0"));
                }
                dtLine = objComBL.GetLineListDropDownBL(objUserBO);
                if (dtLine.Rows.Count < 20)
                {
                    if (dtBuilding.Rows.Count > 1)
                        ddlBuilding.Items.Insert(ddlBuilding.Items.Capacity, new ListItem("All", "0"));
                }
                if(Convert.ToString(ddlLocation.SelectedValue)=="10")
                {
                    if(dtLine.Rows.Count<=100)
                    {
                        if (dtBuilding.Rows.Count > 1)
                            ddlBuilding.Items.Insert(ddlBuilding.Items.Capacity, new ListItem("All", "0"));
                    }
                }
                dtLine = (DataTable)Session["dtOutput"];
                if (dtLine.Rows.Count > 0)
                {
                    CreateplanGrid.DataSource = dtLine;
                    CreateplanGrid.DataBind();
                    Session["dtLine"] = dtLine;
                }
                else
                {
                    CreateplanGrid.DataSource = null;
                    CreateplanGrid.DataBind();
                }

            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }

        }

        /// <summary>
        /// Author : Anamika(KNS)
        /// Date : 23 Oct 2017
        /// Desc : It calls GetBulkPlansBL function based on location and year and the output is stored in session.
        /// </summary>
        private void BindData()
        {
            try
            {
                objPlanBO = new PlanBO();
                objPlanBL = new PlanBL();
                DataTable dtOutput = new DataTable();
                objPlanBO.location_id = Convert.ToInt32(ddlLocation.SelectedValue);
                objPlanBO.building_id = Convert.ToInt32(ddlBuilding.SelectedValue);
                objPlanBO.p_year = Convert.ToInt32(ddlYear.SelectedValue);
                dtOutput = objPlanBL.GetBulkPlansBL(objPlanBO);
                Session["dtOutput"] = dtOutput;
                int currentweek = GetIso8601WeekOfYear(DateTime.Now);

                hdnWeekNumer.Value = Convert.ToString(currentweek);
                string col_name = "wk" + Convert.ToString(currentweek);

                var query = from row in dtOutput.AsEnumerable()
                            where row.Field<int>(col_name) != 0
                            select new
                            {
                                p1 = row[col_name]
                            };


                var myColumn = dtOutput.Columns.Cast<DataColumn>().SingleOrDefault(col => col.ColumnName == "wk" + Convert.ToString(currentweek));
                if (myColumn != null)
                {
                    var results = from myRow in dtOutput.AsEnumerable()
                                  where myRow.Field<int>(myColumn) != 0
                                  select myRow;
                    if (results != null)
                    {
                        int count = results.Count();
                        hdnPlanCount.Value = Convert.ToString(count);
                    }
                    else
                    {
                        hdnPlanCount.Value = "0";
                    }
                    //DataRow[] dr = dtOutput.Select(myColumn+" <> 0");
                    //if (dr.Length > 0)
                    //{
                    //    int count = dr.Count();
                    //    hdnPlanCount.Value = Convert.ToString(count);
                    //}
                    //else
                    //{
                    //    hdnPlanCount.Value = "0";
                    //}
                    //// just some roww
                    //var tableRow = dtOutput.AsEnumerable().First();
                    //var myData = tableRow.Field<string>(myColumn);
                    //// or if above does not work
                    //myData = tableRow.Field<string>(table.Columns.IndexOf(myColumn));
                }
                //var row = from dt in dtOutput.AsEnumerable()
                //          where (dt.GetType().GetProperty("wkn" + Convert.ToString(currentweek + 1)).GetValue(dt) as int) != 0
                //          select dt;



                ////from dt in dtOutput.AsEnumerable()
                ////        where dt.Field<int>("wkn" + Convert.ToString(currentweek+1)) != 0
                ////        select dt;


            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        //public static Expression<Func<General, bool>> CreatePredicate(string columnName, object searchValue)
        //{
        //    var xType = typeof(General);
        //    var x = Expression.Parameter(xType, "x");
        //    var column = xType.GetProperties().FirstOrDefault(p => p.Name == columnName);

        //    var body = column == null
        //        ? (Expression)Expression.Constant(true)
        //        : Expression.Equal(
        //            Expression.PropertyOrField(x, columnName),
        //            Expression.Constant(searchValue));

        //    return Expression.Lambda<Func<General, bool>>(body, x);
        //}
        /// <summary>
        /// Author : Anamika(KNS)
        /// Date : 23 Oct 2017
        /// Desc : It will return the week count for year passed in parameter
        /// </summary>
        /// <param name="year">year</param>
        /// <returns>week count</returns>
        public int GetWeeksInYear(int year)
        {
            //DateTimeFormatInfo dfi = DateTimeFormatInfo.CurrentInfo;
            //DateTime date1 = new DateTime(year, 01, 01);
            //System.Globalization.Calendar cal = dfi.Calendar;
            //return cal.GetWeekOfYear(date1, CalendarWeekRule.FirstDay,
            //                                    DayOfWeek.Monday);
            string day = new DateTime(year, 01, 01).DayOfWeek.ToString();
            if ((year%4==0 && day=="Wednesday")||day=="Thursday")
            {
                return 53;
            }
            else
            return 52;
        }

        /// <summary>
        /// Author : Anamika(KNS)
        /// Date : 23 Oct 2017
        /// Desc : This function returns the week for the passed datetime.
        /// </summary>
        /// <param name="time"></param>
        /// <returns>week number</returns>
        public static int GetIso8601WeekOfYear(DateTime time)
        {
            // Seriously cheat.  If its Monday, Tuesday or Wednesday, then it'll 
            // be the same week# as whatever Thursday, Friday or Saturday are,
            // and we always get those right
            DayOfWeek day = CultureInfo.InvariantCulture.Calendar.GetDayOfWeek(time);
            if (day >= DayOfWeek.Monday && day <= DayOfWeek.Wednesday)
            {
                time = time.AddDays(3);
            }

            // Return the week of our adjusted day
            return CultureInfo.InvariantCulture.Calendar.GetWeekOfYear(time, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);
        }

        #endregion

        protected void btnFilter_Click(object sender, EventArgs e)
        {
            objCom.TestLog("start");
            objCom.TestLog(Convert.ToString(DateTime.Now));
            DataTable dtLocation = new DataTable();
            DataTable dtLine = new DataTable();
            DataTable dtPart = new DataTable();
            DataTable dtUsers = new DataTable();
            try
            {
                j = 0;
                objUserBO = new UsersBO();
                objComBL = new CommonBL();
                Session["SelectedLocation"] = Convert.ToString(ddlLocation.SelectedValue);
                dtLocation = (DataTable)Session["dtLocation"];
                objUserBO.location_id = Convert.ToInt32(ddlLocation.SelectedValue);
                //dtLine = objComBL.GetLineListDropDownBL(objUserBO);
                //dtLine = (DataTable)Session["dtOutput"];
                BindData();
                dtLine = (DataTable)Session["dtOutput"];
                if (dtLine.Rows.Count > 0)
                {
                    CreateplanGrid.DataSource = null;
                    CreateplanGrid.DataBind();
                    CreateplanGrid.DataSource = dtLine;
                    CreateplanGrid.DataBind();
                    Session["dtLine"] = dtLine;
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ShowPopup", "LoadGrid();", true);

                    CreateplanGrid.Visible = true;
                    lblMessage.Text = "";
                    btnSave.Visible = true;
                }
                else
                {
                    CreateplanGrid.DataSource = null;
                    CreateplanGrid.DataBind();
                    CreateplanGrid.Visible = false;
                    lblMessage.Text = "There are no lines for selection. Please change the loacation selection and try again.";
                    btnSave.Visible = false;
                }

            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        protected void btnTemplate_Click(object sender, EventArgs e)
        {
            var workbook = new Microsoft.Office.Interop.Excel.Workbook();
            var worksheet = workbook.Worksheets.Add("AuditPlan");
            worksheet.Cell(1, 1).Value = "A1";

            //Input cell value by cell name
            worksheet.Cell("B1").Value = "B1";

            //Input cell value by row id and column name
            worksheet.Cell(1, "C").Value = "C1";
            workbook.Save();
        }
        
    }
}
