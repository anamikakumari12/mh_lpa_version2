﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AllReports.aspx.cs" Inherits="MHLPA_Application.AllReports" MasterPageFile="~/Site.Master" %>

<%@ Register Assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" Namespace="System.Web.UI.DataVisualization.Charting" TagPrefix="asp" %>


<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script src="js/datatables_export_lib.min.js"></script>
    <script src="js/dataTables.bootstrap.min.js"></script>
    <link href="css/datatables_lib_export.min.css" rel="stylesheet" />
    <link href="css/dataTables.bootstrap.css" rel="stylesheet" />
    <script src="js/moment.js"></script>
    <script src="js/daterangepicker.js"></script>
    <link href="css/daterangepicker.css" rel="stylesheet" />
    <script type="text/javascript">

        $(document).ready(function () {

            var head_content = $('#MainContent_grdAuditSchedule tr:first').html();
            $('#MainContent_grdAuditSchedule').prepend('<thead></thead>')
            $('#MainContent_grdAuditSchedule thead').html('<tr>' + head_content + '</tr>');
            $('#MainContent_grdAuditSchedule tbody tr:first').hide();

            $('#MainContent_grdAuditSchedule').dataTable({
                scrollY: '500px',
                scrollX: '100%',
                sScrollXInner: "100%",
                "columnDefs": [
                 { "width": "200px", "targets": 0 },
                ],
                "fixedColumns": true,
                "bInfo": false,
            });


            var head_content = $('#MainContent_grdAuditPlan tr:first').html();
            $('#MainContent_grdAuditPlan').prepend('<thead></thead>')
            $('#MainContent_grdAuditPlan thead').html('<tr>' + head_content + '</tr>');
            $('#MainContent_grdAuditPlan tbody tr:first').hide();

            $('#MainContent_grdAuditPlan').dataTable({
                scrollY: '500px',
                scrollX: '100%',
                sScrollXInner: "100%",
                "columnDefs": [
                 { "width": "200px", "targets": 0 },
                ],
                "fixedColumns": true,
                "bInfo": false,
            });


        });


        $(function () {
            $("#<%= reportrange.ClientID %>").attr("readonly", "readonly");
            $("#<%= reportrange.ClientID %>").attr("unselectable", "on");
            $("#<%= reportrange.ClientID %>").daterangepicker({
                locale: {
                    format: 'DD/MM/YYYY'
                },
                ranges: {
                    'Today': [moment(), moment()],
                    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                },
            });
        });


        function validateControls() {
            var err_flag = 0;
            if ($('#MainContent_ddlRegion').val() == "" || $('#MainContent_ddlRegion').val() == null) {
                $('#MainContent_ddlRegion').css('border-color', 'red');
                err_flag = 1;
            }
            else {
                $('#MainContent_ddlRegion').css('border-color', '');
            }
            if ($('#MainContent_ddlCountry').val() == "" || $('#MainContent_ddlCountry').val() == null) {
                $('#MainContent_ddlCountry').css('border-color', 'red');

                err_flag = 1;
            }
            else {
                $('#MainContent_ddlCountry').css('border-color', '');
            }

            if ($('#MainContent_ddlLocation').val() == "" || $('#MainContent_ddlLocation').val() == null) {
                $('#MainContent_ddlLocation').css('border-color', 'red');

                err_flag = 1;
            }
            else {
                $('#MainContent_ddlLocation').css('border-color', '');
            }


            if (err_flag == 0) {
                $('#MainContent_lblError').text('');
                return setmin();

            }
            else {
                $('#MainContent_lblError').text('Please enter all the mandatory fields.');
                return false;
            }
        }
    </script>
    <style>
        .number {
            font-style: italic;
            font-weight: 600;
            color: gray;
        }

        ._panel {
            background-color: #CDEAD2;
            padding: 10px;
            -moz-box-shadow: 0px 1px 10px rgba(10, 10, 10, 0.25);
            box-shadow: 0px 1px 10px rgba(10, 10, 10, 0.25);
            border-radius: 5px !important;
            float: left;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePageMethods="true"></asp:ScriptManager>
    <div class="col-md-12">
        <div class='block-web' style='float: left; width: 100%; height: 36px;'>
            <div class="header">
                <div class="crumbs">
                    <!-- Start : Breadcrumbs -->
                    <ul id="breadcrumbs" class="breadcrumb">
                        <li>
                            <a class="mn_breadcrumb">Reports</a>
                        </li>
                        <li class="current">All Reports</li>

                    </ul>
                </div>
            </div>

        </div>
    </div>
    <asp:Panel runat="server">
        <div class="col-md-12">
            <div class="col-md-2 nopad">
                <div class="panel panel-default panel-table">
                    <div class="panel-group tool-tips widget-shadow" id="accordion" role="tablist" aria-multiselectable="true">
                        <asp:Label ID="lblnum1" runat="server" CssClass="number">01</asp:Label>
                        <asp:LinkButton ID="lnkauditschedule" runat="server" OnClick="lnkauditschedule_Click"> AUDIT SCHEDULE</asp:LinkButton>
                        <%--<div class="col-md-12 nopad mn_table">                                      
                                   </div>--%>
                        <br />
                        <br />

                        <asp:Label ID="lblnum2" runat="server" CssClass="number">02</asp:Label>
                        <asp:LinkButton ID="lnkauditplan" runat="server" OnClick="lnkauditplan_Click"> AUDIT PLAN REPORT</asp:LinkButton>
                        <br />
                        <br />

                        <asp:Label ID="lblnum3" runat="server" CssClass="number">03</asp:Label>
                        <asp:LinkButton ID="lnkworkcnt" runat="server" OnClick="lnkworkcnt_Click"> WORKCENTER REPORT</asp:LinkButton>
                        <br />
                        <br />

                        <asp:Label ID="lblnum4" runat="server" CssClass="number">04</asp:Label>
                        <asp:LinkButton ID="lnkplantcoverage" runat="server" OnClick="lnkplantcoverage_Click"> PLANT COVERAGE REPORT</asp:LinkButton>
                        <br />
                        <br />

                        <asp:Label ID="lblnum5" runat="server" CssClass="number">05</asp:Label>
                        <asp:LinkButton ID="lnksqdclpa" runat="server" OnClick="lnksqdclpa_Click"> SQDCLPA REPORT</asp:LinkButton>
                        <br />
                        <br />

                        <asp:Label ID="lblnum6" runat="server" CssClass="number">06</asp:Label>
                        <asp:LinkButton ID="lnkregionalfindings" runat="server" OnClick="lnkregionalfindings_Click"> REGIONAL FINDINGS DASHBOARD</asp:LinkButton>
                        <br />
                        <br />

                        <asp:Label ID="lblnum7" runat="server" CssClass="number">07</asp:Label>
                        <asp:LinkButton ID="lnkfindingsummary" runat="server" OnClick="lnkfindingsummary_Click"> FINDINGS SUMMARY REPORT</asp:LinkButton>
                        <br />
                        <br />

                        <asp:Label ID="lblnum8" runat="server" CssClass="number">08</asp:Label>
                        <asp:LinkButton ID="lnkopenfindingdetails" runat="server" OnClick="lnkopenfindingdetails_Click"> OPEN FINDINGS DETAIL</asp:LinkButton>
                        <br />
                        <br />

                        <asp:Label ID="lblnum9" runat="server" CssClass="number">09</asp:Label>
                        <asp:LinkButton ID="lnkorphanedfindings" runat="server" OnClick="lnkorphanedfindings_Click"> ORPHANED FINDINGS REPORT</asp:LinkButton>
                        <br />
                        <br />

                        <asp:Label ID="lblnum10" runat="server" CssClass="number">10</asp:Label>
                        <asp:LinkButton ID="lnkuserole" runat="server" OnClick="lnkuserole_Click"> USER ROLES</asp:LinkButton>
                    </div>
                </div>
            </div>
            <div class="col-md-10 nopad" runat="server" id="divreports">
                <div class="_panel">
                    <div class="col-md-2 control">
                        <label class="filter_label"><b>Region* : </b></label>
                        <asp:DropDownList class="control_dropdown" runat="server" ID="ddlRegion" OnSelectedIndexChanged="ddlRegion_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                    </div>

                    <div class="col-md-2 control">
                        <label class="filter_label"><b>Country* : </b></label>
                        <asp:DropDownList class="control_dropdown" runat="server" ID="ddlCountry" OnSelectedIndexChanged="ddlCountry_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                    </div>

                    <div class="col-md-2 control">
                        <label class="filter_label"><b>Location* : </b></label>
                        <asp:DropDownList class="control_dropdown" runat="server" ID="ddlLocation" OnSelectedIndexChanged="ddlLocation_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                    </div>

                    <div class="col-md-2 control">
                        <label class="filter_label"><b>Date* : </b></label>
                        <br />
                        <asp:TextBox ID="reportrange" class="control_dropdown" runat="server"></asp:TextBox>
                    </div>

                    <div class="col-md-4 control">
                        <asp:Label runat="server" ID="lblError" ForeColor="Red"></asp:Label>
                        <asp:Button runat="server" CssClass="btn-add" ID="btnSubmit" Style="margin-left: 9px; float: right; margin-top: 17px;" Text="Submit" OnClick="btnFilter_Click" OnClientClick="return validateControls();" />
                        <asp:Button runat="server" CssClass="btn-add" ID="btndownloadreport" Style="margin-left: 9px; float: right; margin-top: 17px;" Text="Export" OnClick="btndownloadreport_Click" />
                        <asp:Button runat="server" CssClass="btn-add" ID="btnClear" Style="margin-left: 9px; float: right; margin-top: 17px;" Text="Clear" OnClick="btnClear_Click" />

                    </div>
                    <%--  <div class="col-md-12 nopad mn_table" runat="server" id="divgrd">--%>
                    <div class="clearfix" style="height: 5px;"></div>
                    <div class="result_panel_1">
                        <asp:GridView ID="grdAuditSchedule" runat="server" AutoGenerateColumns="False" Width="100%" CssClass="mn_th">
                            <Columns>
                                <asp:TemplateField HeaderText="Location" HeaderStyle-ForeColor="#ffffff">
                                    <ItemTemplate>
                                        <asp:Label Style="text-align: center;" ID="lbl_location" runat="server" Text='<%# Eval("location_name") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Line Name" HeaderStyle-ForeColor="#ffffff">
                                    <ItemTemplate>
                                        <asp:Label Style="text-align: center;" ID="lbl_line" runat="server" Text='<%# Eval("line_name") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Employee Full Name" HeaderStyle-ForeColor="#ffffff">
                                    <ItemTemplate>
                                        <asp:Label Style="text-align: center;" ID="lbl_emp_fullname" runat="server" Text='<%# Eval("emp_full_name") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Role" HeaderStyle-ForeColor="#ffffff">
                                    <ItemTemplate>
                                        <asp:Label Style="text-align: center;" ID="LabelRole" runat="server" Text='<%# Eval("Role") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Planned Date" HeaderStyle-ForeColor="#ffffff">
                                    <ItemTemplate>
                                        <asp:Label Style="text-align: center;" ID="lbl_plannedate" runat="server" Text='<%# Eval("planned_date") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Planned Date End" HeaderStyle-ForeColor="#ffffff">
                                    <ItemTemplate>
                                        <asp:Label Style="text-align: center;" ID="Labelplanned_date_end" runat="server" Text='<%# Eval("planned_date_end") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                              
                            </Columns>
                        </asp:GridView>

                         <asp:GridView ID="grdAuditPlan" runat="server" AutoGenerateColumns="False" Width="100%" CssClass="mn_th">
                            <Columns>
                                <asp:TemplateField HeaderText="Location" HeaderStyle-ForeColor="#ffffff">
                                    <ItemTemplate>
                                        <asp:Label Style="text-align: center;" ID="lbl_location" runat="server" Text='<%# Eval("location_name") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Line Name" HeaderStyle-ForeColor="#ffffff">
                                    <ItemTemplate>
                                        <asp:Label Style="text-align: center;" ID="lbl_line" runat="server" Text='<%# Eval("line_name") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Employee Full Name" HeaderStyle-ForeColor="#ffffff">
                                    <ItemTemplate>
                                        <asp:Label Style="text-align: center;" ID="lbl_emp_fullname" runat="server" Text='<%# Eval("emp_full_name") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Role" HeaderStyle-ForeColor="#ffffff">
                                    <ItemTemplate>
                                        <asp:Label Style="text-align: center;" ID="LabelRole" runat="server" Text='<%# Eval("Role") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Planned Date" HeaderStyle-ForeColor="#ffffff">
                                    <ItemTemplate>
                                        <asp:Label Style="text-align: center;" ID="lbl_plannedate" runat="server" Text='<%# Eval("planned_date") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Planned Date End" HeaderStyle-ForeColor="#ffffff">
                                    <ItemTemplate>
                                        <asp:Label Style="text-align: center;" ID="Labelplanned_date_end" runat="server" Text='<%# Eval("planned_date_end") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                 <asp:TemplateField HeaderText="Auditor"  HeaderStyle-ForeColor="#ffffff">
                                    <ItemTemplate>
                                        <asp:Label Style="text-align: center;" ID="lblAuditor" runat="server" Text='<%# Eval("Auditor") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                 <asp:TemplateField HeaderText="Audit Date" HeaderStyle-ForeColor="#ffffff">
                                    <ItemTemplate>
                                        <asp:Label Style="text-align: center;" ID="lbl_audit_date" runat="server" Text='<%# Eval("audit_date") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                 <asp:TemplateField HeaderText="Shift No" HeaderStyle-ForeColor="#ffffff">
                                    <ItemTemplate>
                                        <asp:Label Style="text-align: center;" ID="lbl_shift_no" runat="server" Text='<%# Eval("Shift_no") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                 <asp:TemplateField HeaderText="Score" HeaderStyle-ForeColor="#ffffff">
                                    <ItemTemplate>
                                        <asp:Label Style="text-align: center;" ID="lbl_score" runat="server" Text='<%# Eval("Score") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </div>

                    <%--</div>--%>
                </div>
            </div>
        </div>
    </asp:Panel>
</asp:Content>
