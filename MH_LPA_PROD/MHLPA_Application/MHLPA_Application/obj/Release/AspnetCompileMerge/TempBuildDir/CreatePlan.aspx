﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CreatePlan.aspx.cs" Inherits="MHLPA_Application.CreatePlan" MasterPageFile="~/Site.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">

    <%--   <script src="js/datatables_export_lib.min.js"></script>
   <script src="js/dataTables.bootstrap.min.js"></script>
    
   <link href="css/datatables_lib_export.min.css" rel="stylesheet" />
    <link href="css/dataTables.bootstrap.css" rel="stylesheet" />
    <script src="js/DatatblefixedColumns.js"></script>
    <script src="https://cdn.datatables.net/scroller/1.4.4/js/dataTables.scroller.min.js"></script>--%>
    <script src="js/gridviewScroll.min.js"></script>

    <link href="css/GridviewScroll.css" rel="stylesheet" />

    <style>
        .control_dropdown
        {
            width:150px;
        }
        .b_pickname[disabled]
        {
            background:#afb3b0;
        }
        #MainContent_CreateplanGridPanelItemContent
        {
            /*padding: 50px;*/
        }
        .linewidth
        {
            width: 285px;
        }
        /*td .GridCellDiv
        {
            
        }*/
        .GridCellDiv
        {
            /*width:120px!important;*/
            min-width: 120px!important;
        }

        .FixedHeader
        {
            /*position: relative;
             top: expression(document.getElementById("divGrid").scrollTop-2);
            /*top: expression(this.offsetParent.scrollTop);*/
            /*z-index: 10;*/
            /*position: absolute;
            font-weight: bold;*/ 
        }

        .col-css
        {
            width:100px!important;
        }

        .width80
        {
            /*width: 83%!important;*/
        }

        .b_pickname
        {
            background: #e9f5eb;
            width: 100%;
            text-align: left;
            float: left;
        }

            .b_pickname:hover
            {
                background: #00592f;
                color: #fff;
            }

        .b_picknameactive
        {
            width: 100%;
            background: #00592f;
            color: #fff;
            text-align: left;
        }

            .b_picknameactive:hover
            {
                color: #fff;
            }

        .mhd
        {
            background: #fff;
            box-shadow: 0px 0px 5px #ddd;
            /*width: 100%;*/
            float: left;
        }

            .mhd h3
            {
                color: #317838;
                font-size: 16px;
                text-align: center;
                padding: 15px 0;
                margin: 0;
                border-bottom: solid 1px #ddd;
            }

        .mn_radio
        {
            height: 400px;
            overflow: auto;
        }

        .radio
        {
            display: block;
            position: relative;
            padding-left: 45px;
            margin-bottom: 12px;
            cursor: pointer;
            font-size: 14px;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            font-weight: normal;
        }

            .radio input
            {
                position: absolute;
                opacity: 0;
                cursor: pointer;
            }

        .checkround
        {
            position: absolute;
            top: 0px;
            left: 15px;
            height: 18px;
            width: 18px;
            background-color: #fff;
            border-color: #317838;
            border-style: solid;
            border-width: 2px;
            border-radius: 50%;
        }

        .radio input:checked ~ .checkround
        {
            background-color: #fff;
        }

        .checkround:after
        {
            content: "";
            position: absolute;
            display: none;
        }

        .radio input:checked ~ .checkround:after
        {
            display: block;
        }

        .radio .checkround:after
        {
            left: 2px;
            top: 2px;
            width: 10px;
            height: 10px;
            border-radius: 50%;
            background: #317838;
        }

        #MainContent_CreateplanGridWrapper
        {
            float: left;
        }

        #MainContent_CreateplanGridWrapper, #MainContent_CreateplanGridPanelHeader, #MainContent_CreateplanGridPanelHeaderContent, #MainContent_CreateplanGridPanelItemContent, #MainContent_CreateplanGridPanelItem
        {
            width: 100% !important;
        }

            #MainContent_CreateplanGridWrapper th, #MainContent_CreateplanGridWrapper td
            {
                /*padding: 5px 15px;*/
                font-size: 13px;
            }

        .ma_table
        {
            margin: 10px 0 0 0;
            width: 100%;
            float: left;
        }

        #MainContent_CreateplanGridPanelHeaderContent
        {
            background: none !important;
        }

        #MainContent_CreateplanGridPanelHeader, #MainContent_CreateplanGridPanelHeaderContentFreeze
        {
            background: #008244 !important;
        }

        #MainContent_CreateplanGridFreeze
        {
            background: #008244;
            color: #ffffff;
        }

        .DTFC_Clone
        {
            background: #008244;
            color: #ffffff;
        }

        div.DTFC_LeftHeadWrapper table, div.DTFC_LeftFootWrapper table, div.DTFC_RightHeadWrapper table, div.DTFC_RightFootWrapper table, table.DTFC_Cloned tr.even
        {
            background: #008244;
            color: #ffffff;
            padding: 0 5px;
        }

        div.DTFC_LeftHeadWrapper table, div.DTFC_LeftFootWrapper table, div.DTFC_RightHeadWrapper table, div.DTFC_RightFootWrapper table, table.DTFC_Cloned tr.odd
        {
            background: #008244;
            color: #ffffff;
            padding: 0 5px;
        }

        table.dataTable thead .sorting
        {
            border: none;
        }

        th, td
        {
            white-space: nowrap;
        }

        table.DTFC_Cloned thead, table.DTFC_Cloned tfoot
        {
            background-color: white;
        }

        div.DTFC_Blocker
        {
            background-color: white;
        }

        div.DTFC_LeftWrapper table.dataTable, div.DTFC_RightWrapper table.dataTable
        {
            margin-bottom: 0;
            z-index: 2;
        }

            div.DTFC_LeftWrapper table.dataTable.no-footer, div.DTFC_RightWrapper table.dataTable.no-footer
            {
                border-bottom: none;
            }

        .divWaiting
        {
            position: absolute;
            background-color: #FAFAFA;
            z-index: 2147483647 !important;
            opacity: 0.8;
            overflow: hidden;
            text-align: center;
            top: 0;
            left: 0;
            height: 100%;
            width: 100%;
            padding-top: 20%;
        }

        #MainContent_CreateplanGridHeaderCopy .linewidth div
        {
            min-width:283px!important;
            width:283px!important;
        }

    </style>
    <script type="text/javascript">

         function LoadGrid() {
            console.log("ready2");
            $('#MainContent_CreateplanGrid').gridviewScroll({
                width: $(window).width() - 50,
                height: 500,
                railcolor: "#F0F0F0",
                barcolor: "#606060",
                barhovercolor: "#606060",
                bgcolor: "#F0F0F0",
                freezesize: 1,
                arrowsize: 30,
                freezeColumn: true,
                //varrowtopimg: "Images/arrowvt.png",
                //varrowbottomimg: "Images/arrowvb.png",
                //harrowleftimg: "Images/arrowhl.png",
                //harrowrightimg: "Images/arrowhr.png",
                headerrowcount: 1,
                railsize: 16,
                barsize: 14,
                verticalbar: "auto",
                horizontalbar: "auto",
                overflow: 'none',
                wheelstep: 1,
            });
            $('.linewidth').get(0).children[0].style["width"] = "282px!important";
            $('.col-css').get(0).children[0].style["width"] = "";
            //var head_content = $('#MainContent_CreateplanGrid tr:first').html();
            //$('#MainContent_CreateplanGrid').prepend('<thead></thead>')
            //$('#MainContent_CreateplanGrid thead').html('<tr>' + head_content + '</tr>');
            //$('#MainContent_CreateplanGrid tbody tr:first').hide();

            //var table = $('#MainContent_CreateplanGrid').dataTable({
            //    scrollY: '500px',
            //    scrollX: '100%',
            //    sScrollXInner: "100%",
            //    paging: false,
            //    sorting: false,
            //    fixedColumns: {
            //        leftColumns: 1

            //    },
            //    columnDefs: [
            //      { "width": "200px", "targets": 0 },
            //    ],
            //    "deferRender": true
            //    // "lengthMenu": [[25, 50, 100, -1], [25, 50, 100, "All"]]
            //});
        }
        $(document).ready(function () {
           

            gridView1 = $('#MainContent_CreateplanGrid').gridviewScroll({
                width: $(window).width() - 50,
                height: 500,
                railcolor: "#F0F0F0",
                barcolor: "#606060",
                barhovercolor: "#606060",
                bgcolor: "#F0F0F0",
                freezesize: 1,
                arrowsize: 30,
                freezeColumn : true,
                //varrowtopimg: "Images/arrowvt.png",
                //varrowbottomimg: "Images/arrowvb.png",
                //harrowleftimg: "Images/arrowhl.png",
                //harrowrightimg: "Images/arrowhr.png",
                headerrowcount: 1,
                railsize: 16,
                barsize: 14,
                verticalbar: "auto",
                horizontalbar: "auto",
                overflow: 'none',
                wheelstep: 1,
            });
            $('.linewidth').get(0).children[0].style["width"] = "282px!important";
            $('.col-css').get(0).children[0].style["width"] = "";
            //gridviewScroll();
            debugger;
            //Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
            console.log("ready");
            //if ($.fn.dataTable.isDataTable('#MainContent_CreateplanGrid')) {

            //}
            //else {
            //    var head_content = $('#MainContent_CreateplanGrid tr:first').html();
            //    $('#MainContent_CreateplanGrid').prepend('<thead></thead>')
            //    $('#MainContent_CreateplanGrid thead').html('<tr>' + head_content + '</tr>');
            //    $('#MainContent_CreateplanGrid tbody tr:first').hide();
            //    var table = $('#MainContent_CreateplanGrid').dataTable({
            //        scrollY: '500px',
            //        scrollX: '100%',
            //        sScrollXInner: "100%",
            //        paging: false,
            //        sorting: false,
            //        fixedColumns: {
            //            leftColumns: 1

            //        },
            //        columnDefs: [
            //          { "width": "200px", "targets": 0 },
            //        ],
            //        "deferRender": true
            //        // "lengthMenu": [[25, 50, 100, -1], [25, 50, 100, "All"]]
            //    });
            //}

            //$('#MainContent_CreateplanGrid').scroll(5);
            //var left = $('#MainContent_CreateplanGrid').scrollLeft;
            //$('#MainContent_CreateplanGrid').scrollLeft = 1000;
        });

        function ShowEmployeeList(obj, e) {
            e.preventDefault();
            debugger;
            console.log(obj.id);

            $('#MainContent_hdnEmpName').val(obj.id);

            var divEmp = document.getElementById('divEmp');
            var divGrid = document.getElementById('divGrid');
            divEmp.style.display = "block";
            $('#divGrid').removeClass('col-md-12');
            $('#divGrid').addClass('col-md-10 width80');
            var EmpName_id = obj.id;
            var EmpNum_id = EmpName_id.replace('ddlusers', 'hdnusers');
            var LineWeek_id = EmpName_id.replace('ddlusers', 'hdnLineWeek');
            var lineweek = $('#' + LineWeek_id).val();
            var EmpNum = $('#' + EmpNum_id).val();
            var EmpName = $('#' + EmpName_id).val();
            $('#MainContent_lblEmpSelect').text(lineweek);

            sessionStorage.setItem("btn", obj.id);
            sessionStorage.setItem("previoushdn", EmpNum);
            sessionStorage.setItem("previousbtn", EmpName);

            var rb = document.getElementById("<%=rblEmployee.ClientID%>");
            var radio = rb.getElementsByTagName("input");
            var label = rb.getElementsByTagName("label");
            if (!IsnullOrEmpty(EmpNum)) {

                for (var i = 0; i < radio.length; i++) {
                    if (radio[i].value == EmpNum) {
                        radio[i].checked = true;
                        break;
                    }
                }
            }
            else {
                for (var i = 0; i < radio.length; i++) {
                    radio[i].checked = false;
                }
            }

            console.log("end");

        }
        function IsnullOrEmpty(val) {
            if (val != '' && val != undefined && val != '--Select--')
                return false;
            else
                return true;
        }
        function LoadCellname(obj, e) {
            debugger;
            console.log(obj);
            var stext;
            var sval;
            var EmpName_id = $('#MainContent_hdnEmpName').val();
            var EmpNum_id = EmpName_id.replace('ddlusers', 'hdnusers');
            var radio = obj.getElementsByTagName("input");
            var label = obj.getElementsByTagName("label");
            for (var i = 0; i < radio.length; i++) {
                if (radio[i].checked) {
                    stext = label[i].innerHTML;
                    sval = radio[i].value;
                    break;
                }
            }
            $('#' + EmpName_id).val(stext);
            $('#' + EmpName_id).addClass("b_picknameactive");
            $('#' + EmpNum_id).val(sval);
        }

        $('#MainContent_CreateplanGrid tbody').on('click', 'td', function () {
            debugger;
            alert('Data:' + $(this).html().trim());
            alert('Row:' + $(this).parent().find('td').html().trim());
            alert('Column:' + $('#example thead tr th').eq($(this).index()).html().trim());
        });
        function divVisibility(e) {
            e.preventDefault();
            debugger;
            var divEmp = document.getElementById('divEmp');
            divEmp.style.display = "none";
            $('#divGrid').removeClass('col-md-10 width80');
            $('#divGrid').addClass('col-md-12');
        }

        function CellReset(e) {
            e.preventDefault();
            debugger;
            var obj = sessionStorage.getItem("btn");
            var stext = sessionStorage.getItem("previousbtn");;
            var sval = sessionStorage.getItem("previoushdn");
            var EmpNum_id = obj.replace('ddlusers', 'hdnusers');
            $('#' + obj).val(stext);
            $('#' + EmpNum_id).val(sval);
            var divEmp = document.getElementById('divEmp');
            divEmp.style.display = "none";
            $('#divGrid').removeClass('col-md-10 width80');
            $('#divGrid').addClass('col-md-12');
        }
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:ScriptManager runat="server" EnablePageMethods="true"></asp:ScriptManager>

    <div>
        <div class='block-web' style='float: left; width: 100%; height: 36px;'>
            <div class="header">
                <div class="crumbs">
                    <!-- Start : Breadcrumbs -->
                    <ul id="breadcrumbs" class="breadcrumb">
                        <li>
                            <a class="mn_breadcrumb">Audit</a>
                        </li>
                        <li class="current">Create Plan</li>

                    </ul>
                </div>
            </div>

        </div>
    </div>
    <div>
        <!-- main content start-->

        <div class="md_main">
            <%-- <asp:UpdateProgress ID="updateProgress" runat="server">
                <ProgressTemplate>
                    <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                        <span style="border-width: 0px; position: fixed; padding: 50px; background-color: #FFFFFF; font-size: 36px; left: 40%; top: 40%;">Loading ...</span>
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>--%>
            <asp:UpdatePanel runat="server" ID="updpnlGrid">
                <ContentTemplate>
                    <div class="row">
                        <div>
                            <div class="clearfix"></div>
                            <div class="filter_panel">
                                    <div class="col-md-2 control">
                                        <p>Region* : </p>
                                        <asp:DropDownList class="control_dropdown" runat="server" ID="ddlRegion" OnSelectedIndexChanged="ddlRegion_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                    </div>
                                    <div class="col-md-2 control">
                                        <p>Country* : </p>
                                        <asp:DropDownList class="control_dropdown" runat="server" ID="ddlCountry" OnSelectedIndexChanged="ddlCountry_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                    </div>
                                    <div class="col-md-2 control">
                                        <p>Location* : </p>
                                        <asp:DropDownList class="control_dropdown" runat="server" ID="ddlLocation" OnSelectedIndexChanged="ddlLocation_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                    </div>
                                <div class="col-md-2 control">
                                        <p>Building* : </p>
                                        <asp:DropDownList class="control_dropdown" runat="server" ID="ddlBuilding" OnSelectedIndexChanged="ddlLocation_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                    </div>
                                    <div class="col-md-2 control">
                                        <p>Year* : </p>
                                        <asp:DropDownList class="control_dropdown" runat="server" ID="ddlYear" OnSelectedIndexChanged="ddlYear_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>

                                    </div>
                                    <div class="col-md-2 control">
                                        <div id="save">
                                            <asp:Button ID="btnFilter" runat="server" CssClass="btn-add" Text="Filter" OnClick="btnFilter_Click" />
                                            <asp:Button runat="server" CssClass="btn-add" ID="btnSave" Text="SAVE" OnClick="btnSave_Click" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix" style="height: 5px;"></div>


                            <asp:UpdateProgress ID="UpdateProgress" runat="server">
                                <ProgressTemplate>
                                    <div class="divWaiting">
                                        <asp:Image ID="Image1" ImageUrl="images/loader.gif" AlternateText="Please Wait....."
                                            runat="server" />
                                    </div>
                                </ProgressTemplate>

                            </asp:UpdateProgress>

                            <div class="result_panel_1">
                                <div id="divGrid" class="col-md-12" style="height: 500px; overflow: auto">
                                    <asp:Label ID="lblMessage" runat="server"></asp:Label>
                                    <%--<asp:SqlDataSource ConnectionString="<%$ ConnectionStrings:csMHLPA %>" SelectCommand="select user_id, emp_full_name from mh_lpa_user_master" ID="SqlDataSource1" runat="server"></asp:SqlDataSource>--%>
                                    <asp:GridView ID="CreateplanGrid" runat="server" AutoGenerateColumns="false" Width="100%" OnRowDataBound="OnRowDataBound">
                                        <Columns>
                                            <asp:TemplateField HeaderStyle-CssClass="linewidth" HeaderText="Line Name/No" HeaderStyle-ForeColor="#ffffff" HeaderStyle-BackColor="#00834b" ItemStyle-BackColor="#00834b" ItemStyle-ForeColor="#ffffff">
                                                <ItemTemplate>
                                                    <%-- <asp:HiddenField ID="LbllineNoId" runat="server" Value='<%# Eval("line_id") %>'/>--%>
                                                    <asp:Label ID="LbllineNoId" style="min-width:100px!important;" runat="server" Text='<%# Eval("line_id") %>' Visible="false"></asp:Label>
                                                    <asp:Label ID="LblLineNo" runat="server" Text='<%# Eval("line_name") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderStyle-CssClass="col-css" ItemStyle-CssClass="col-css" HeaderText="Week 1" HeaderStyle-ForeColor="#ffffff" HeaderStyle-BackColor="#00834b">
                                                <ItemTemplate>
                                                    <asp:Button CssClass="b_pickname" ID="ddlusers1" runat="server" Text='<%# Eval("wkn1") %>' OnClientClick="ShowEmployeeList(this, event);"></asp:Button>
                                                    <asp:HiddenField ID="hdnusers1" runat="server" Value='<%# Eval("wk1") %>' />
                                                    <asp:HiddenField ID="hdnLineWeek1" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderStyle-CssClass="col-css" ItemStyle-CssClass="col-css" HeaderText="Week 2" HeaderStyle-ForeColor="#ffffff" HeaderStyle-BackColor="#00834b">
                                                <ItemTemplate>
                                                    <asp:Button CssClass="b_pickname" ID="ddlusers2" runat="server" Text='<%# Eval("wkn2") %>' OnClientClick="ShowEmployeeList(this, event);"></asp:Button>
                                                    <asp:HiddenField ID="hdnusers2" runat="server" Value='<%# Eval("wk2") %>' />
                                                    <asp:HiddenField ID="hdnLineWeek2" runat="server" Value='<%# Eval("line_name") %>' />

                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderStyle-CssClass="col-css" ItemStyle-CssClass="col-css" HeaderText="Week 3" HeaderStyle-ForeColor="#ffffff" HeaderStyle-BackColor="#00834b">
                                                <ItemTemplate>
                                                    <asp:Button CssClass="b_pickname" ID="ddlusers3" runat="server" Text='<%# Eval("wkn3") %>' OnClientClick="ShowEmployeeList(this, event);"></asp:Button><asp:HiddenField ID="hdnusers3" runat="server" Value='<%# Eval("wk3") %>' />
                                                    <asp:HiddenField ID="hdnLineWeek3" runat="server" Value='<%# Eval("line_name") %>' />

                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderStyle-CssClass="col-css" ItemStyle-CssClass="col-css" HeaderText="Week 4" HeaderStyle-ForeColor="#ffffff" HeaderStyle-BackColor="#00834b">
                                                <ItemTemplate>
                                                    <asp:Button CssClass="b_pickname" ID="ddlusers4" runat="server" Text='<%# Eval("wkn4") %>' OnClientClick="ShowEmployeeList(this, event);"></asp:Button><asp:HiddenField ID="hdnusers4" runat="server" Value='<%# Eval("wk4") %>' />
                                                    <asp:HiddenField ID="hdnLineWeek4" runat="server" Value='<%# Eval("line_name") %>' />

                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderStyle-CssClass="col-css" ItemStyle-CssClass="col-css" HeaderText="Week 5" HeaderStyle-ForeColor="#ffffff" HeaderStyle-BackColor="#00834b">
                                                <ItemTemplate>
                                                    <asp:Button CssClass="b_pickname" ID="ddlusers5" runat="server" Text='<%# Eval("wkn5") %>' OnClientClick="ShowEmployeeList(this, event);"></asp:Button><asp:HiddenField ID="hdnusers5" runat="server" Value='<%# Eval("wk5") %>' />
                                                    <asp:HiddenField ID="hdnLineWeek5" runat="server" Value='<%# Eval("line_name") %>' />

                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderStyle-CssClass="col-css" ItemStyle-CssClass="col-css" HeaderText="Week 6" HeaderStyle-ForeColor="#ffffff" HeaderStyle-BackColor="#00834b">
                                                <ItemTemplate>
                                                    <asp:Button CssClass="b_pickname" ID="ddlusers6" runat="server" Text='<%# Eval("wkn6") %>' OnClientClick="ShowEmployeeList(this, event);"></asp:Button><asp:HiddenField ID="hdnusers6" runat="server" Value='<%# Eval("wk6") %>' />
                                                    <asp:HiddenField ID="hdnLineWeek6" runat="server" Value='<%# Eval("line_name") %>' />

                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderStyle-CssClass="col-css" ItemStyle-CssClass="col-css" HeaderText="Week 7" HeaderStyle-ForeColor="#ffffff" HeaderStyle-BackColor="#00834b">
                                                <ItemTemplate>
                                                    <asp:Button CssClass="b_pickname" ID="ddlusers7" runat="server" Text='<%# Eval("wkn7") %>' OnClientClick="ShowEmployeeList(this, event);"></asp:Button><asp:HiddenField ID="hdnusers7" runat="server" Value='<%# Eval("wk7") %>' />
                                                    <asp:HiddenField ID="hdnLineWeek7" runat="server" Value='<%# Eval("line_name") %>' />

                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderStyle-CssClass="col-css" ItemStyle-CssClass="col-css" HeaderText="Week 8" HeaderStyle-ForeColor="#ffffff" HeaderStyle-BackColor="#00834b">
                                                <ItemTemplate>
                                                    <asp:Button CssClass="b_pickname" ID="ddlusers8" runat="server" Text='<%# Eval("wkn8") %>' OnClientClick="ShowEmployeeList(this, event);"></asp:Button><asp:HiddenField ID="hdnusers8" runat="server" Value='<%# Eval("wk8") %>' />
                                                    <asp:HiddenField ID="hdnLineWeek8" runat="server" Value='<%# Eval("line_name") %>' />

                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderStyle-CssClass="col-css" ItemStyle-CssClass="col-css" HeaderText="Week 9" HeaderStyle-ForeColor="#ffffff" HeaderStyle-BackColor="#00834b">
                                                <ItemTemplate>
                                                    <asp:Button CssClass="b_pickname" ID="ddlusers9" runat="server" Text='<%# Eval("wkn9") %>' OnClientClick="ShowEmployeeList(this, event);"></asp:Button><asp:HiddenField ID="hdnusers9" runat="server" Value='<%# Eval("wk9") %>' />
                                                    <asp:HiddenField ID="hdnLineWeek9" runat="server" Value='<%# Eval("line_name") %>' />

                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderStyle-CssClass="col-css" ItemStyle-CssClass="col-css" HeaderText="Week 10" HeaderStyle-ForeColor="#ffffff" HeaderStyle-BackColor="#00834b">
                                                <ItemTemplate>
                                                    <asp:Button CssClass="b_pickname" ID="ddlusers10" runat="server" Text='<%# Eval("wkn10") %>' OnClientClick="ShowEmployeeList(this, event);"></asp:Button><asp:HiddenField ID="hdnusers10" runat="server" Value='<%# Eval("wk10") %>' />
                                                    <asp:HiddenField ID="hdnLineWeek10" runat="server" Value='<%# Eval("line_name") %>' />

                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderStyle-CssClass="col-css" ItemStyle-CssClass="col-css" HeaderText="Week 11" HeaderStyle-ForeColor="#ffffff" HeaderStyle-BackColor="#00834b">
                                                <ItemTemplate>
                                                    <asp:Button CssClass="b_pickname" ID="ddlusers11" runat="server" Text='<%# Eval("wkn11") %>' OnClientClick="ShowEmployeeList(this, event);"></asp:Button><asp:HiddenField ID="hdnusers11" runat="server" Value='<%# Eval("wk11") %>' />
                                                    <asp:HiddenField ID="hdnLineWeek11" runat="server" Value='<%# Eval("line_name") %>' />

                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderStyle-CssClass="col-css" ItemStyle-CssClass="col-css" HeaderText="Week 12" HeaderStyle-ForeColor="#ffffff" HeaderStyle-BackColor="#00834b">
                                                <ItemTemplate>
                                                    <asp:Button CssClass="b_pickname" ID="ddlusers12" runat="server" Text='<%# Eval("wkn12") %>' OnClientClick="ShowEmployeeList(this, event);"></asp:Button><asp:HiddenField ID="hdnusers12" runat="server" Value='<%# Eval("wk12") %>' />
                                                    <asp:HiddenField ID="hdnLineWeek12" runat="server" Value='<%# Eval("line_name") %>' />

                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderStyle-CssClass="col-css" ItemStyle-CssClass="col-css" HeaderText="Week 13" HeaderStyle-ForeColor="#ffffff" HeaderStyle-BackColor="#00834b">
                                                <ItemTemplate>
                                                    <asp:Button CssClass="b_pickname" ID="ddlusers13" runat="server" Text='<%# Eval("wkn13") %>' OnClientClick="ShowEmployeeList(this, event);"></asp:Button><asp:HiddenField ID="hdnusers13" runat="server" Value='<%# Eval("wk13") %>' />
                                                    <asp:HiddenField ID="hdnLineWeek13" runat="server" Value='<%# Eval("line_name") %>' />

                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderStyle-CssClass="col-css" ItemStyle-CssClass="col-css" HeaderText="Week 14" HeaderStyle-ForeColor="#ffffff" HeaderStyle-BackColor="#00834b">
                                                <ItemTemplate>
                                                    <asp:Button CssClass="b_pickname" ID="ddlusers14" runat="server" Text='<%# Eval("wkn14") %>' OnClientClick="ShowEmployeeList(this, event);"></asp:Button><asp:HiddenField ID="hdnusers14" runat="server" Value='<%# Eval("wk14") %>' />
                                                    <asp:HiddenField ID="hdnLineWeek14" runat="server" Value='<%# Eval("line_name") %>' />

                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderStyle-CssClass="col-css" ItemStyle-CssClass="col-css" HeaderText="Week 15" HeaderStyle-ForeColor="#ffffff" HeaderStyle-BackColor="#00834b">
                                                <ItemTemplate>
                                                    <asp:Button CssClass="b_pickname" ID="ddlusers15" runat="server" Text='<%# Eval("wkn15") %>' OnClientClick="ShowEmployeeList(this, event);"></asp:Button><asp:HiddenField ID="hdnusers15" runat="server" Value='<%# Eval("wk15") %>' />
                                                    <asp:HiddenField ID="hdnLineWeek15" runat="server" Value='<%# Eval("line_name") %>' />

                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderStyle-CssClass="col-css" ItemStyle-CssClass="col-css" HeaderText="Week 16" HeaderStyle-ForeColor="#ffffff" HeaderStyle-BackColor="#00834b">
                                                <ItemTemplate>
                                                    <asp:Button CssClass="b_pickname" ID="ddlusers16" runat="server" Text='<%# Eval("wkn16") %>' OnClientClick="ShowEmployeeList(this, event);"></asp:Button><asp:HiddenField ID="hdnusers16" runat="server" Value='<%# Eval("wk16") %>' />
                                                    <asp:HiddenField ID="hdnLineWeek16" runat="server" Value='<%# Eval("line_name") %>' />

                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderStyle-CssClass="col-css" ItemStyle-CssClass="col-css" HeaderText="Week 17" HeaderStyle-ForeColor="#ffffff" HeaderStyle-BackColor="#00834b">
                                                <ItemTemplate>
                                                    <asp:Button CssClass="b_pickname" ID="ddlusers17" runat="server" Text='<%# Eval("wkn17") %>' OnClientClick="ShowEmployeeList(this, event);"></asp:Button><asp:HiddenField ID="hdnusers17" runat="server" Value='<%# Eval("wk17") %>' />
                                                    <asp:HiddenField ID="hdnLineWeek17" runat="server" Value='<%# Eval("line_name") %>' />

                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderStyle-CssClass="col-css" ItemStyle-CssClass="col-css" HeaderText="Week 18" HeaderStyle-ForeColor="#ffffff" HeaderStyle-BackColor="#00834b">
                                                <ItemTemplate>
                                                    <asp:Button CssClass="b_pickname" ID="ddlusers18" runat="server" Text='<%# Eval("wkn18") %>' OnClientClick="ShowEmployeeList(this, event);"></asp:Button><asp:HiddenField ID="hdnusers18" runat="server" Value='<%# Eval("wk18") %>' />
                                                    <asp:HiddenField ID="hdnLineWeek18" runat="server" Value='<%# Eval("line_name") %>' />

                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderStyle-CssClass="col-css" ItemStyle-CssClass="col-css" HeaderText="Week 19" HeaderStyle-ForeColor="#ffffff" HeaderStyle-BackColor="#00834b">
                                                <ItemTemplate>
                                                    <asp:Button CssClass="b_pickname" ID="ddlusers19" runat="server" Text='<%# Eval("wkn19") %>' OnClientClick="ShowEmployeeList(this, event);"></asp:Button><asp:HiddenField ID="hdnusers19" runat="server" Value='<%# Eval("wk19") %>' />
                                                    <asp:HiddenField ID="hdnLineWeek19" runat="server" Value='<%# Eval("line_name") %>' />

                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderStyle-CssClass="col-css" ItemStyle-CssClass="col-css" HeaderText="Week 20" HeaderStyle-ForeColor="#ffffff" HeaderStyle-BackColor="#00834b">
                                                <ItemTemplate>
                                                    <asp:Button CssClass="b_pickname" ID="ddlusers20" runat="server" Text='<%# Eval("wkn20") %>' OnClientClick="ShowEmployeeList(this, event);"></asp:Button><asp:HiddenField ID="hdnusers20" runat="server" Value='<%# Eval("wk20") %>' />
                                                    <asp:HiddenField ID="hdnLineWeek20" runat="server" Value='<%# Eval("line_name") %>' />

                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderStyle-CssClass="col-css" ItemStyle-CssClass="col-css" HeaderText="Week 21" HeaderStyle-ForeColor="#ffffff" HeaderStyle-BackColor="#00834b">
                                                <ItemTemplate>
                                                    <asp:Button CssClass="b_pickname" ID="ddlusers21" runat="server" Text='<%# Eval("wkn21") %>' OnClientClick="ShowEmployeeList(this, event);"></asp:Button><asp:HiddenField ID="hdnusers21" runat="server" Value='<%# Eval("wk21") %>' />
                                                    <asp:HiddenField ID="hdnLineWeek21" runat="server" Value='<%# Eval("line_name") %>' />

                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderStyle-CssClass="col-css" ItemStyle-CssClass="col-css" HeaderText="Week 22" HeaderStyle-ForeColor="#ffffff" HeaderStyle-BackColor="#00834b">
                                                <ItemTemplate>
                                                    <asp:Button CssClass="b_pickname" ID="ddlusers22" runat="server" Text='<%# Eval("wkn22") %>' OnClientClick="ShowEmployeeList(this, event);"></asp:Button><asp:HiddenField ID="hdnusers22" runat="server" Value='<%# Eval("wk22") %>' />
                                                    <asp:HiddenField ID="hdnLineWeek22" runat="server" Value='<%# Eval("line_name") %>' />

                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderStyle-CssClass="col-css" ItemStyle-CssClass="col-css" HeaderText="Week 23" HeaderStyle-ForeColor="#ffffff" HeaderStyle-BackColor="#00834b">
                                                <ItemTemplate>
                                                    <asp:Button CssClass="b_pickname" ID="ddlusers23" runat="server" Text='<%# Eval("wkn23") %>' OnClientClick="ShowEmployeeList(this, event);"></asp:Button><asp:HiddenField ID="hdnusers23" runat="server" Value='<%# Eval("wk23") %>' />
                                                    <asp:HiddenField ID="hdnLineWeek23" runat="server" Value='<%# Eval("line_name") %>' />

                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderStyle-CssClass="col-css" ItemStyle-CssClass="col-css" HeaderText="Week 24" HeaderStyle-ForeColor="#ffffff" HeaderStyle-BackColor="#00834b">
                                                <ItemTemplate>
                                                    <asp:Button CssClass="b_pickname" ID="ddlusers24" runat="server" Text='<%# Eval("wkn24") %>' OnClientClick="ShowEmployeeList(this, event);"></asp:Button><asp:HiddenField ID="hdnusers24" runat="server" Value='<%# Eval("wk24") %>' />
                                                    <asp:HiddenField ID="hdnLineWeek24" runat="server" Value='<%# Eval("line_name") %>' />

                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderStyle-CssClass="col-css" ItemStyle-CssClass="col-css" HeaderText="Week 25" HeaderStyle-ForeColor="#ffffff" HeaderStyle-BackColor="#00834b">
                                                <ItemTemplate>
                                                    <asp:Button CssClass="b_pickname" ID="ddlusers25" runat="server" Text='<%# Eval("wkn25") %>' OnClientClick="ShowEmployeeList(this, event);"></asp:Button><asp:HiddenField ID="hdnusers25" runat="server" Value='<%# Eval("wk25") %>' />
                                                    <asp:HiddenField ID="hdnLineWeek25" runat="server" Value='<%# Eval("line_name") %>' />

                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderStyle-CssClass="col-css" ItemStyle-CssClass="col-css" HeaderText="Week 26" HeaderStyle-ForeColor="#ffffff" HeaderStyle-BackColor="#00834b">
                                                <ItemTemplate>
                                                    <asp:Button CssClass="b_pickname" ID="ddlusers26" runat="server" Text='<%# Eval("wkn26") %>' OnClientClick="ShowEmployeeList(this, event);"></asp:Button><asp:HiddenField ID="hdnusers26" runat="server" Value='<%# Eval("wk26") %>' />
                                                    <asp:HiddenField ID="hdnLineWeek26" runat="server" Value='<%# Eval("line_name") %>' />

                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderStyle-CssClass="col-css" ItemStyle-CssClass="col-css" HeaderText="Week 27" HeaderStyle-ForeColor="#ffffff" HeaderStyle-BackColor="#00834b">
                                                <ItemTemplate>
                                                    <asp:Button CssClass="b_pickname" ID="ddlusers27" runat="server" Text='<%# Eval("wkn27") %>' OnClientClick="ShowEmployeeList(this, event);"></asp:Button><asp:HiddenField ID="hdnusers27" runat="server" Value='<%# Eval("wk27") %>' />
                                                    <asp:HiddenField ID="hdnLineWeek27" runat="server" Value='<%# Eval("line_name") %>' />

                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderStyle-CssClass="col-css" ItemStyle-CssClass="col-css" HeaderText="Week 28" HeaderStyle-ForeColor="#ffffff" HeaderStyle-BackColor="#00834b">
                                                <ItemTemplate>
                                                    <asp:Button CssClass="b_pickname" ID="ddlusers28" runat="server" Text='<%# Eval("wkn28") %>' OnClientClick="ShowEmployeeList(this, event);"></asp:Button><asp:HiddenField ID="hdnusers28" runat="server" Value='<%# Eval("wk28") %>' />
                                                    <asp:HiddenField ID="hdnLineWeek28" runat="server" Value='<%# Eval("line_name") %>' />

                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderStyle-CssClass="col-css" ItemStyle-CssClass="col-css" HeaderText="Week 29" HeaderStyle-ForeColor="#ffffff" HeaderStyle-BackColor="#00834b">
                                                <ItemTemplate>
                                                    <asp:Button CssClass="b_pickname" ID="ddlusers29" runat="server" Text='<%# Eval("wkn29") %>' OnClientClick="ShowEmployeeList(this, event);"></asp:Button><asp:HiddenField ID="hdnusers29" runat="server" Value='<%# Eval("wk29") %>' />
                                                    <asp:HiddenField ID="hdnLineWeek29" runat="server" Value='<%# Eval("line_name") %>' />

                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderStyle-CssClass="col-css" ItemStyle-CssClass="col-css" HeaderText="Week 30" HeaderStyle-ForeColor="#ffffff" HeaderStyle-BackColor="#00834b">
                                                <ItemTemplate>
                                                    <asp:Button CssClass="b_pickname" ID="ddlusers30" runat="server" Text='<%# Eval("wkn30") %>' OnClientClick="ShowEmployeeList(this, event);"></asp:Button><asp:HiddenField ID="hdnusers30" runat="server" Value='<%# Eval("wk30") %>' />
                                                    <asp:HiddenField ID="hdnLineWeek30" runat="server" Value='<%# Eval("line_name") %>' />

                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderStyle-CssClass="col-css" ItemStyle-CssClass="col-css" HeaderText="Week 31" HeaderStyle-ForeColor="#ffffff" HeaderStyle-BackColor="#00834b">
                                                <ItemTemplate>
                                                    <asp:Button CssClass="b_pickname" ID="ddlusers31" runat="server" Text='<%# Eval("wkn31") %>' OnClientClick="ShowEmployeeList(this, event);"></asp:Button><asp:HiddenField ID="hdnusers31" runat="server" Value='<%# Eval("wk31") %>' />
                                                    <asp:HiddenField ID="hdnLineWeek31" runat="server" Value='<%# Eval("line_name") %>' />

                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderStyle-CssClass="col-css" ItemStyle-CssClass="col-css" HeaderText="Week 32" HeaderStyle-ForeColor="#ffffff" HeaderStyle-BackColor="#00834b">
                                                <ItemTemplate>
                                                    <asp:Button CssClass="b_pickname" ID="ddlusers32" runat="server" Text='<%# Eval("wkn32") %>' OnClientClick="ShowEmployeeList(this, event);"></asp:Button><asp:HiddenField ID="hdnusers32" runat="server" Value='<%# Eval("wk32") %>' />
                                                    <asp:HiddenField ID="hdnLineWeek32" runat="server" Value='<%# Eval("line_name") %>' />

                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderStyle-CssClass="col-css" ItemStyle-CssClass="col-css" HeaderText="Week 33" HeaderStyle-ForeColor="#ffffff" HeaderStyle-BackColor="#00834b">
                                                <ItemTemplate>
                                                    <asp:Button CssClass="b_pickname" ID="ddlusers33" runat="server" Text='<%# Eval("wkn33") %>' OnClientClick="ShowEmployeeList(this, event);"></asp:Button><asp:HiddenField ID="hdnusers33" runat="server" Value='<%# Eval("wk33") %>' />
                                                    <asp:HiddenField ID="hdnLineWeek33" runat="server" Value='<%# Eval("line_name") %>' />

                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderStyle-CssClass="col-css" ItemStyle-CssClass="col-css" HeaderText="Week 34" HeaderStyle-ForeColor="#ffffff" HeaderStyle-BackColor="#00834b">
                                                <ItemTemplate>
                                                    <asp:Button CssClass="b_pickname" ID="ddlusers34" runat="server" Text='<%# Eval("wkn34") %>' OnClientClick="ShowEmployeeList(this, event);"></asp:Button><asp:HiddenField ID="hdnusers34" runat="server" Value='<%# Eval("wk34") %>' />
                                                    <asp:HiddenField ID="hdnLineWeek34" runat="server" Value='<%# Eval("line_name") %>' />

                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderStyle-CssClass="col-css" ItemStyle-CssClass="col-css" HeaderText="Week 35" HeaderStyle-ForeColor="#ffffff" HeaderStyle-BackColor="#00834b">
                                                <ItemTemplate>
                                                    <asp:Button CssClass="b_pickname" ID="ddlusers35" runat="server" Text='<%# Eval("wkn35") %>' OnClientClick="ShowEmployeeList(this, event);"></asp:Button><asp:HiddenField ID="hdnusers35" runat="server" Value='<%# Eval("wk35") %>' />
                                                    <asp:HiddenField ID="hdnLineWeek35" runat="server" Value='<%# Eval("line_name") %>' />

                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderStyle-CssClass="col-css" ItemStyle-CssClass="col-css" HeaderText="Week 36" HeaderStyle-ForeColor="#ffffff" HeaderStyle-BackColor="#00834b">
                                                <ItemTemplate>
                                                    <asp:Button CssClass="b_pickname" ID="ddlusers36" runat="server" Text='<%# Eval("wkn36") %>' OnClientClick="ShowEmployeeList(this, event);"></asp:Button><asp:HiddenField ID="hdnusers36" runat="server" Value='<%# Eval("wk36") %>' />
                                                    <asp:HiddenField ID="hdnLineWeek36" runat="server" Value='<%# Eval("line_name") %>' />

                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderStyle-CssClass="col-css" ItemStyle-CssClass="col-css" HeaderText="Week 37" HeaderStyle-ForeColor="#ffffff" HeaderStyle-BackColor="#00834b">
                                                <ItemTemplate>
                                                    <asp:Button CssClass="b_pickname" ID="ddlusers37" runat="server" Text='<%# Eval("wkn37") %>' OnClientClick="ShowEmployeeList(this, event);"></asp:Button><asp:HiddenField ID="hdnusers37" runat="server" Value='<%# Eval("wk37") %>' />
                                                    <asp:HiddenField ID="hdnLineWeek37" runat="server" Value='<%# Eval("line_name") %>' />

                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderStyle-CssClass="col-css" ItemStyle-CssClass="col-css" HeaderText="Week 38" HeaderStyle-ForeColor="#ffffff" HeaderStyle-BackColor="#00834b">
                                                <ItemTemplate>
                                                    <asp:Button CssClass="b_pickname" ID="ddlusers38" runat="server" Text='<%# Eval("wkn38") %>' OnClientClick="ShowEmployeeList(this, event);"></asp:Button><asp:HiddenField ID="hdnusers38" runat="server" Value='<%# Eval("wk38") %>' />
                                                    <asp:HiddenField ID="hdnLineWeek38" runat="server" Value='<%# Eval("line_name") %>' />

                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderStyle-CssClass="col-css" ItemStyle-CssClass="col-css" HeaderText="Week 39" HeaderStyle-ForeColor="#ffffff" HeaderStyle-BackColor="#00834b">
                                                <ItemTemplate>
                                                    <asp:Button CssClass="b_pickname" ID="ddlusers39" runat="server" Text='<%# Eval("wkn39") %>' OnClientClick="ShowEmployeeList(this, event);"></asp:Button><asp:HiddenField ID="hdnusers39" runat="server" Value='<%# Eval("wk39") %>' />
                                                    <asp:HiddenField ID="hdnLineWeek39" runat="server" Value='<%# Eval("line_name") %>' />

                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderStyle-CssClass="col-css" ItemStyle-CssClass="col-css" HeaderText="Week 40" HeaderStyle-ForeColor="#ffffff" HeaderStyle-BackColor="#00834b">
                                                <ItemTemplate>
                                                    <asp:Button CssClass="b_pickname" ID="ddlusers40" runat="server" Text='<%# Eval("wkn40") %>' OnClientClick="ShowEmployeeList(this, event);"></asp:Button><asp:HiddenField ID="hdnusers40" runat="server" Value='<%# Eval("wk40") %>' />
                                                    <asp:HiddenField ID="hdnLineWeek40" runat="server" Value='<%# Eval("line_name") %>' />

                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderStyle-CssClass="col-css" ItemStyle-CssClass="col-css" HeaderText="Week 41" HeaderStyle-ForeColor="#ffffff" HeaderStyle-BackColor="#00834b">
                                                <ItemTemplate>
                                                    <asp:Button CssClass="b_pickname" ID="ddlusers41" runat="server" Text='<%# Eval("wkn41") %>' OnClientClick="ShowEmployeeList(this, event);"></asp:Button><asp:HiddenField ID="hdnusers41" runat="server" Value='<%# Eval("wk41") %>' />
                                                    <asp:HiddenField ID="hdnLineWeek41" runat="server" Value='<%# Eval("line_name") %>' />

                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderStyle-CssClass="col-css" ItemStyle-CssClass="col-css" HeaderText="Week 42" HeaderStyle-ForeColor="#ffffff" HeaderStyle-BackColor="#00834b">
                                                <ItemTemplate>
                                                    <asp:Button CssClass="b_pickname" ID="ddlusers42" runat="server" Text='<%# Eval("wkn42") %>' OnClientClick="ShowEmployeeList(this, event);"></asp:Button><asp:HiddenField ID="hdnusers42" runat="server" Value='<%# Eval("wk42") %>' />
                                                    <asp:HiddenField ID="hdnLineWeek42" runat="server" Value='<%# Eval("line_name") %>' />

                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderStyle-CssClass="col-css" ItemStyle-CssClass="col-css" HeaderText="Week 43" HeaderStyle-ForeColor="#ffffff" HeaderStyle-BackColor="#00834b">
                                                <ItemTemplate>
                                                    <asp:Button CssClass="b_pickname" ID="ddlusers43" runat="server" Text='<%# Eval("wkn43") %>' OnClientClick="ShowEmployeeList(this, event);"></asp:Button><asp:HiddenField ID="hdnusers43" runat="server" Value='<%# Eval("wk43") %>' />
                                                    <asp:HiddenField ID="hdnLineWeek43" runat="server" Value='<%# Eval("line_name") %>' />

                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderStyle-CssClass="col-css" ItemStyle-CssClass="col-css" HeaderText="Week 44" HeaderStyle-ForeColor="#ffffff" HeaderStyle-BackColor="#00834b">
                                                <ItemTemplate>
                                                    <asp:Button CssClass="b_pickname" ID="ddlusers44" runat="server" Text='<%# Eval("wkn44") %>' OnClientClick="ShowEmployeeList(this, event);"></asp:Button><asp:HiddenField ID="hdnusers44" runat="server" Value='<%# Eval("wk44") %>' />
                                                    <asp:HiddenField ID="hdnLineWeek44" runat="server" Value='<%# Eval("line_name") %>' />

                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderStyle-CssClass="col-css" ItemStyle-CssClass="col-css" HeaderText="Week 45" HeaderStyle-ForeColor="#ffffff" HeaderStyle-BackColor="#00834b">
                                                <ItemTemplate>
                                                    <asp:Button CssClass="b_pickname" ID="ddlusers45" runat="server" Text='<%# Eval("wkn45") %>' OnClientClick="ShowEmployeeList(this, event);"></asp:Button><asp:HiddenField ID="hdnusers45" runat="server" Value='<%# Eval("wk45") %>' />
                                                    <asp:HiddenField ID="hdnLineWeek45" runat="server" Value='<%# Eval("line_name") %>' />

                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderStyle-CssClass="col-css" ItemStyle-CssClass="col-css" HeaderText="Week 46" HeaderStyle-ForeColor="#ffffff" HeaderStyle-BackColor="#00834b">
                                                <ItemTemplate>
                                                    <asp:Button CssClass="b_pickname" ID="ddlusers46" runat="server" Text='<%# Eval("wkn46") %>' OnClientClick="ShowEmployeeList(this, event);"></asp:Button><asp:HiddenField ID="hdnusers46" runat="server" Value='<%# Eval("wk46") %>' />
                                                    <asp:HiddenField ID="hdnLineWeek46" runat="server" Value='<%# Eval("line_name") %>' />

                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderStyle-CssClass="col-css" ItemStyle-CssClass="col-css" HeaderText="Week 47" HeaderStyle-ForeColor="#ffffff" HeaderStyle-BackColor="#00834b">
                                                <ItemTemplate>
                                                    <asp:Button CssClass="b_pickname" ID="ddlusers47" runat="server" Text='<%# Eval("wkn47") %>' OnClientClick="ShowEmployeeList(this, event);"></asp:Button><asp:HiddenField ID="hdnusers47" runat="server" Value='<%# Eval("wk47") %>' />
                                                    <asp:HiddenField ID="hdnLineWeek47" runat="server" Value='<%# Eval("line_name") %>' />

                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderStyle-CssClass="col-css" ItemStyle-CssClass="col-css" HeaderText="Week 48" HeaderStyle-ForeColor="#ffffff" HeaderStyle-BackColor="#00834b">
                                                <ItemTemplate>
                                                    <asp:Button CssClass="b_pickname" ID="ddlusers48" runat="server" Text='<%# Eval("wkn48") %>' OnClientClick="ShowEmployeeList(this, event);"></asp:Button><asp:HiddenField ID="hdnusers48" runat="server" Value='<%# Eval("wk48") %>' />
                                                    <asp:HiddenField ID="hdnLineWeek48" runat="server" Value='<%# Eval("line_name") %>' />

                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderStyle-CssClass="col-css" ItemStyle-CssClass="col-css" HeaderText="Week 49" HeaderStyle-ForeColor="#ffffff" HeaderStyle-BackColor="#00834b">
                                                <ItemTemplate>
                                                    <asp:Button CssClass="b_pickname" ID="ddlusers49" runat="server" Text='<%# Eval("wkn49") %>' OnClientClick="ShowEmployeeList(this, event);"></asp:Button><asp:HiddenField ID="hdnusers49" runat="server" Value='<%# Eval("wk49") %>' />
                                                    <asp:HiddenField ID="hdnLineWeek49" runat="server" Value='<%# Eval("line_name") %>' />

                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderStyle-CssClass="col-css" ItemStyle-CssClass="col-css" HeaderText="Week 50" HeaderStyle-ForeColor="#ffffff" HeaderStyle-BackColor="#00834b">
                                                <ItemTemplate>
                                                    <asp:Button CssClass="b_pickname" ID="ddlusers50" runat="server" Text='<%# Eval("wkn50") %>' OnClientClick="ShowEmployeeList(this, event);"></asp:Button><asp:HiddenField ID="hdnusers50" runat="server" Value='<%# Eval("wk50") %>' />
                                                    <asp:HiddenField ID="hdnLineWeek50" runat="server" Value='<%# Eval("line_name") %>' />

                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderStyle-CssClass="col-css" ItemStyle-CssClass="col-css" HeaderText="Week 51" HeaderStyle-ForeColor="#ffffff" HeaderStyle-BackColor="#00834b">
                                                <ItemTemplate>
                                                    <asp:Button CssClass="b_pickname" ID="ddlusers51" runat="server" Text='<%# Eval("wkn51") %>' OnClientClick="ShowEmployeeList(this, event);"></asp:Button><asp:HiddenField ID="hdnusers51" runat="server" Value='<%# Eval("wk51") %>' />
                                                    <asp:HiddenField ID="hdnLineWeek51" runat="server" Value='<%# Eval("line_name") %>' />

                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderStyle-CssClass="col-css" ItemStyle-CssClass="col-css" HeaderText="Week 52" HeaderStyle-ForeColor="#ffffff" HeaderStyle-BackColor="#00834b">
                                                <ItemTemplate>
                                                    <asp:Button CssClass="b_pickname" ID="ddlusers52" runat="server" Text='<%# Eval("wkn52") %>' OnClientClick="ShowEmployeeList(this, event);"></asp:Button>
                                                    <asp:HiddenField ID="hdnusers52" runat="server" Value='<%# Eval("wk52") %>' />
                                                    <asp:HiddenField ID="hdnLineWeek52" runat="server" Value='<%# Eval("line_name") %>' />

                                                </ItemTemplate>
                                            </asp:TemplateField>

                                        </Columns>
                                    </asp:GridView>
                                </div>
                                <div class="col-md-2" id="divEmp" style="display: none;">
                                    <div class="mhd">
                                        <h3>
                                            <asp:Label ID="lblEmpSelect" runat="server"></asp:Label></h3>
                                        <div class="mn_radio">

                                            <asp:RadioButtonList ID="rblEmployee" runat="server" onchange="LoadCellname(this,event);">
                                                <asp:ListItem Text="None" Value="0"></asp:ListItem>
                                            </asp:RadioButtonList>

                                            <asp:HiddenField ID="hdnEmpName" runat="server" />
                                        </div>
                                        <div class="col-md-6">
                                            <asp:Button ID="btnOk" CssClass="btn-add" Text="Ok" runat="server" OnClientClick="divVisibility(event);" />
                                        </div>
                                        <div class="col-md-6">
                                            <asp:Button ID="btnCancel" CssClass="btn-add" Text="Cancel" runat="server" OnClientClick="CellReset(event);" />
                                        </div>
                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="ddlRegion" EventName="SelectedIndexChanged" />
                    <asp:AsyncPostBackTrigger ControlID="ddlCountry" EventName="SelectedIndexChanged" />
                    <asp:AsyncPostBackTrigger ControlID="ddlLocation" EventName="SelectedIndexChanged" />
                    <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="Click" />
                </Triggers>

            </asp:UpdatePanel>


        </div>
    </div>
    <%--</ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="ddlRegion" />
            <asp:PostBackTrigger ControlID="ddlCountry" />
            <asp:PostBackTrigger ControlID="ddlRegion" />
        </Triggers>
</asp:UpdatePanel>--%>
</asp:Content>
