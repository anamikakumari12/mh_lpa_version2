﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ComparisonReportOld.aspx.cs" Inherits="MHLPA_Application.ComparisonReportOld" MasterPageFile="~/Site.Master" %>

<%@ Register Assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"  Namespace="System.Web.UI.DataVisualization.Charting" TagPrefix="asp" %>   

<asp:Content ID="Content1"  ContentPlaceHolderID="HeadContent" runat="server">
    <link href="css/calender.css" rel="stylesheet" />
    <script src="js/calender.js"></script>
    <style>
        .mn_mrg_left
        {
            padding-left:0;
            box-shadow:0px 0px 3px #aaa;
            background:#fff;
        }
        .mn_mrg_right
        {
            padding-right:0;
            box-shadow:0px 0px 3px #aaa;
            background:#fff;
        }
        .mn_mrg
        {
            padding: 0;
            box-shadow:0px 0px 3px #aaa;
            background:#fff;
        }
        .mn_fetch .mn_mrg_left, .mn_fetch .mn_mrg, .mn_fetch .mn_mrg_right 
        {
            box-shadow:none;
            padding: 5px;
        }
        .mn_fetch, .ma_lpa_result
        {
            width: 100%;
            float: left;
            background: #fff;
            border: solid 1px #ddd;
        }
        .ma_lpa_result
        {
            margin:10px 0;
        }
        #MainContent_pnlReport
        {
            width:100%;
            float:left
        }
    </style>
     <script type="text/javascript">

         $(document).ready(function () {
             $("#MainContent_txtDate").datepicker({
                 showOn: "both",
                 buttonImageOnly: true,
                 buttonText: "",
                 changeYear: true,
                 changeMonth: true,
                 yearRange: "c-20:c+50",
                 // minDate: new Date(),
                 dateFormat: 'dd-mm-yy',
                 //buttonImage: "images/calander_icon.png",
             });
         });
         $("#MainContent_txtDate").datepicker({
             showOn: "both",
             buttonImageOnly: true,
             buttonText: "",
             changeYear: true,
             changeMonth: true,
             yearRange: "c-20:c+50",
             // minDate: new Date(),
             dateFormat: 'dd-mm-yy',
             //buttonImage: "images/calander_icon.png",
         });
         function ValidateControls() {
             var err_flag = 0;
             if ($('#MainContent_ddlRegion1').val() == "" || $('#MainContent_ddlRegion1').val() == null) {
                 $('#MainContent_ddlRegion1').css('border-color', 'red');
                 err_flag = 1;
             }
             else {
                 $('#MainContent_ddlRegion1').css('border-color', '');
             }
             if ($('#MainContent_ddlCountry1').val() == "" || $('#MainContent_ddlCountry1').val() == null) {
                 $('#MainContent_ddlCountry1').css('border-color', 'red');

                 err_flag = 1;
             }
             else {
                 $('#MainContent_ddlCountry1').css('border-color', '');
             }

             if ($('#MainContent_ddlLocation1').val() == "" || $('#MainContent_ddlLocation1').val() == null) {
                 $('#MainContent_ddlLocation1').css('border-color', 'red');

                 err_flag = 1;
             }
             else {
                 $('#MainContent_ddlLocation1').css('border-color', '');
             }

             if ($('#MainContent_ddlLine1').val() == "" || $('#MainContent_ddlLine1').val() == null) {
                 $('#MainContent_ddlLine1').css('border-color', 'red');

                 err_flag = 1;
             }
             else {
                 $('#MainContent_ddlLine1').css('border-color', '');
             }
             if ($('#MainContent_ddlRegion2').val() == "" || $('#MainContent_ddlRegion2').val() == null) {
                 $('#MainContent_ddlRegion2').css('border-color', 'red');
                 err_flag = 1;
             }
             else {
                 $('#MainContent_ddlRegion2').css('border-color', '');
             }
             if ($('#MainContent_ddlCountry2').val() == "" || $('#MainContent_ddlCountry2').val() == null) {
                 $('#MainContent_ddlCountry2').css('border-color', 'red');

                 err_flag = 1;
             }
             else {
                 $('#MainContent_ddlCountry2').css('border-color', '');
             }

             if ($('#MainContent_ddlLocation2').val() == "" || $('#MainContent_ddlLocation2').val() == null) {
                 $('#MainContent_ddlLocation2').css('border-color', 'red');

                 err_flag = 1;
             }
             else {
                 $('#MainContent_ddlLocation2').css('border-color', '');
             }

             if ($('#MainContent_ddlLine2').val() == "" || $('#MainContent_ddlLine2').val() == null) {
                 $('#MainContent_ddlLine2').css('border-color', 'red');

                 err_flag = 1;
             }
             else {
                 $('#MainContent_ddlLine2').css('border-color', '');
             }
             if ($('#MainContent_ddlRegion3').val() == "" || $('#MainContent_ddlRegion3').val() == null) {
                 $('#MainContent_ddlRegion3').css('border-color', 'red');
                 err_flag = 1;
             }
             else {
                 $('#MainContent_ddlRegion3').css('border-color', '');
             }
             if ($('#MainContent_ddlCountry3').val() == "" || $('#MainContent_ddlCountry3').val() == null) {
                 $('#MainContent_ddlCountry3').css('border-color', 'red');

                 err_flag = 1;
             }
             else {
                 $('#MainContent_ddlCountry3').css('border-color', '');
             }

             if ($('#MainContent_ddlLocation3').val() == "" || $('#MainContent_ddlLocation3').val() == null) {
                 $('#MainContent_ddlLocation3').css('border-color', 'red');

                 err_flag = 1;
             }
             else {
                 $('#MainContent_ddlLocation3').css('border-color', '');
             }

             if ($('#MainContent_ddlLine3').val() == "" || $('#MainContent_ddlLine3').val() == null) {
                 $('#MainContent_ddlLine3').css('border-color', 'red');

                 err_flag = 1;
             }
             else {
                 $('#MainContent_ddlLine3').css('border-color', '');
             }

             if ($('#MainContent_txtDate').val() == "") {
                 $('#MainContent_txtDate').css('border-color', 'red');
                 err_flag = 1;
             }
             else {

                 $('#MainContent_txtDate').css('border-color', '');
             }

             if (err_flag == 0) {
                 $('#MainContent_lblError').text('');
                 return setmin();

             }
             else {
                 $('#MainContent_lblError').text('Please enter all the mandatory fields.');
                 return false;
             }
         }
    </script>
    </asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
     <div class="col-md-12">
                            <div class='block-web' style='float: left; width: 100%; height:36px;'>
                                <div class="header">
                                    <div class="crumbs">
                                        <ul id="breadcrumbs" class="breadcrumb">
                                            <li>
                                                <a class="mn_breadcrumb">Reports</a>
                                            </li>
                                            <li class="current">Comparison Report</li>

                                        </ul>
                                    </div>
                                </div>

                            </div>
                        </div>
    <%--from--%>
        <div class="md_main">
            <div class="row">
                <div class="mannHummel" id="mannHummelId">
                    <div class="clearfix" style="width:40px;"></div>
                     <asp:Panel runat="server" ID="pnlFilters">
                        <div class="filter_panel">

                            <div>
                                <div class="col-md-5" style="width:20%"></div>
                                <div class="col-md-5 filter_label" style="width:20%"><p>Region* : </p></div>
                                 <div class="col-md-5 filter_label" style="width:20%"><p>Country* : </p></div> 
                                <div class="col-md-5 filter_label" style="width:20%"><p>Location* : </p></div> 
                                <div class="col-md-5 filter_label" style="width:20%"><p>Line Name/No* : </p></div> 
                            </div>

                            <div>
                                <div class="col-md-4 filter_label" style="width:20%;">
                                <p style="float:right;">Select 1st Comparison* : </p>
                            </div>
                                <div class="col-md-4" style="width:20%">
                                <asp:DropDownList class="form-control1 mn_inp control3 filter_select_control" runat="server" ID="ddlRegion1" OnSelectedIndexChanged="ddlRegion1_SelectedIndexChanged"  AutoPostBack="true"></asp:DropDownList>
                            </div>
                                <div class="col-md-4" style="width:20%">
                                <asp:DropDownList class="form-control1 mn_inp control3 filter_select_control" runat="server" ID="ddlCountry1" OnSelectedIndexChanged="ddlCountry1_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                            </div>
                                <div class="col-md-4" style="width:20%">
                                <asp:DropDownList class="form-control1 mn_inp control3 filter_select_control" runat="server" ID="ddlLocation1" OnSelectedIndexChanged="ddlLocation1_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                            </div>
                                <div class="col-md-4" style="width:20%">
                                    <asp:DropDownList class="form-control1 mn_inp control3 filter_select_control" runat="server" ID="ddlLine1" OnSelectedIndexChanged="ddlLine1_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>

                            </div>
                            </div>

                            <div>
                                <div class="col-md-4 filter_label" style="width:20%;">
                                <p style="float:right;">Select 2nd Comparison* : </p>
                            </div>
                                <div class="col-md-4" style="width:20%">
                                <asp:DropDownList class="form-control1 mn_inp control3 filter_select_control" runat="server" ID="ddlRegion2" OnSelectedIndexChanged="ddlRegion2_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                            </div>
                                <div class="col-md-4" style="width:20%">
                                <asp:DropDownList class="form-control1 mn_inp control3 filter_select_control" runat="server" ID="ddlCountry2" OnSelectedIndexChanged="ddlCountry2_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                            </div>
                                <div class="col-md-4" style="width:20%">
                                <asp:DropDownList class="form-control1 mn_inp control3 filter_select_control" runat="server" ID="ddlLocation2" OnSelectedIndexChanged="ddlLocation2_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                            </div>
                                <div class="col-md-4" style="width:20%">
                                    <asp:DropDownList class="form-control1 mn_inp control3 filter_select_control" runat="server" ID="ddlLine2" OnSelectedIndexChanged="ddlLine2_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>

                            </div>
                            </div>

                            <div>
                                <div class="col-md-4 filter_label" style="width:20%;">
                                <p style="float:right;">Select 3rd Comparison* : </p>
                            </div>
                                <div class="col-md-4" style="width:20%">
                                <asp:DropDownList class="form-control1 mn_inp control3 filter_select_control" runat="server" ID="ddlRegion3" OnSelectedIndexChanged="ddlRegion3_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                            </div>
                                <div class="col-md-4" style="width:20%">
                                <asp:DropDownList class="form-control1 mn_inp control3 filter_select_control" runat="server" ID="ddlCountry3" OnSelectedIndexChanged="ddlCountry3_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                            </div>
                                <div class="col-md-4" style="width:20%">
                                <asp:DropDownList class="form-control1 mn_inp control3 filter_select_control" runat="server" ID="ddlLocation3" OnSelectedIndexChanged="ddlLocation3_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                            </div>
                                <div class="col-md-4" style="width:20%">
                                    <asp:DropDownList class="form-control1 mn_inp control3 filter_select_control" runat="server" ID="ddlLine3" OnSelectedIndexChanged="ddlLine3_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>

                            </div>
                            </div>
                           
                               <div>
                            <div class="col-md-4 filter_label" style="width:20%;"><p style="float:right;">Date* : </p></div> 
                            <div class="col-md-4" style="width:20%">
                                <asp:TextBox class="form-control1 mn_inp control3 filter_select_control" runat="server" ID="txtDate"></asp:TextBox>
                            </div>

                                <div class="col-md-4 filter_label" style="width:12%"> <asp:Label runat="server" ID="lblError" ForeColor="Red"></asp:Label></div> 
                            <div class="col-md-4"  style="width:21%">
                                <div class="col-md-5" style="float:left">
                                <asp:Button runat="server" CssClass="btn-add" ID="btnClear" style="margin-left: 4px; float:right;" OnClick="btnClear_Click" Text="Clear" />
                                </div><div class="col-md-7">
                                <asp:Button runat="server" CssClass="btn-add" ID="btnFilter" style="margin-left: 4px; float:right;" Text="Fetch Report" OnClick="btnFilter_Click" OnClientClick="return ValidateControls();"/>
                            </div></div>
                                </div>
                            
                            </div>
                            
                            
                      
                    </asp:Panel>
                    <div class="clearfix" style="height:5px;"></div>
                    <asp:Panel runat="server" Visible="false" ID="pnlReport">
                        <div class="ma_lpa_result">
                        <asp:Chart ID="Chart1" runat="server" style="margin:0 auto; display:block; height:auto;"
        BackGradientStyle="LeftRight" Palette="None"   
        PaletteCustomColors="green" Width="1000px">
                            <Titles>
                                <asp:Title Font="16pt, style=Bold" Name="LPA Result"></asp:Title>
                            </Titles>
                            <Series>
                                <asp:Series ChartType="Area"></asp:Series>
                                 <asp:Series ChartType="Area"></asp:Series>
                                <asp:Series ChartType="Area"></asp:Series>
                            </Series>
                            <Legends>
                                <asp:Legend  ShadowColor="DarkGreen"></asp:Legend>
                                <asp:Legend ShadowColor="DarkOrange"></asp:Legend>
                                <asp:Legend ShadowColor="Gray"></asp:Legend>
                            </Legends>
                            <ChartAreas>
                                <asp:ChartArea Name="cA1"></asp:ChartArea>
                            </ChartAreas>

                        </asp:Chart>
                        </div>
                         <div class="clearfix" style="height:5px;"></div>
                    <div class="col-md-4 mn_mrg_left">
                        <asp:Chart ID="ChartPerSection1" runat="server" style="border-width: 0px;margin:0 auto; display:block;"
        BackGradientStyle="LeftRight" Palette="None"    
        PaletteCustomColors="green">
                            <Titles>
                                <asp:Title Font="16pt, style=Bold" Name="t2"></asp:Title>
                            </Titles>
                         

                            <Series>
                                <asp:Series Name="Series1"  ChartType="Radar"></asp:Series>
                            </Series>
                            <ChartAreas>
                                <asp:ChartArea Name="ca2"></asp:ChartArea>
                            </ChartAreas>
                        </asp:Chart>
                       </div>
                        <div class="col-md-4 mn_mrg">
                        <asp:Chart ID="ChartPerSection2" runat="server" style="border-width: 0px;margin:0 auto; display:block;"
        BackGradientStyle="LeftRight" Palette="None"   
        PaletteCustomColors="green">
                            <Titles>
                                <asp:Title Font="16pt, style=Bold" Name="t2"></asp:Title>
                            </Titles>
                            <Series>
                                <asp:Series Name="Series1"  ChartType="Radar"></asp:Series>
                            </Series>
                            <ChartAreas>
                                <asp:ChartArea Name="ca2"></asp:ChartArea>
                            </ChartAreas>
                        </asp:Chart>
                            </div>
                        <div class="col-md-4 mn_mrg_right">
                        <asp:Chart ID="ChartPerSection3" runat="server" style="border-width: 0px;margin:0 auto; display:block;"
        BackGradientStyle="LeftRight" Palette="None"   
        PaletteCustomColors="green">
                            <Titles>
                                <asp:Title Font="16pt, style=Bold" Name="t2"></asp:Title>
                            </Titles>
                            <Series>
                                <asp:Series Name="Series1"  ChartType="Radar"></asp:Series>
                            </Series>
                            <ChartAreas>
                                <asp:ChartArea Name="ca2"></asp:ChartArea>
                            </ChartAreas>
                        </asp:Chart>
                        </div>
                       <div class="mn_fetch">
                            <div class="col-md-4 mn_mrg_left" style="background-color:white;">
                               <asp:Label runat="server" ID="sec1"></asp:Label>
                            </div>
                            <div class="col-md-4 mn_mrg" style="background-color:white;">
                               <asp:Label runat="server" ID="sec2"></asp:Label>
                            </div>
                            <div class="col-md-4 mn_mrg_right" style="background-color:white;">
                               <asp:Label runat="server" ID="sec3"></asp:Label>
                            </div>
                            <div class="col-md-4 mn_mrg_left" style="background-color:white;">
                               <asp:Label runat="server" ID="sec4"></asp:Label>
                            </div>
                            <div class="col-md-4 mn_mrg" style="background-color:white;">
                               <asp:Label runat="server" ID="sec5"></asp:Label>
                            </div>
                            <div class="col-md-4 mn_mrg_right" style="background-color:white;">
                               <asp:Label runat="server" ID="sec6"></asp:Label>
                            </div>
                        </div>
                         <div class="clearfix" style="height:5px;"></div>

                      
                        <div class="ma_lpa_result">
                        <asp:Chart ID="ChartPerQuestion" runat="server" style="margin:0 auto; display:block; height:auto;"
        BackGradientStyle="LeftRight" Palette="None"   
        PaletteCustomColors="green" Width="1000px">
                            <Titles>
                                <asp:Title Font="16pt, style=Bold" Name="t5"></asp:Title>
                            </Titles>
                            <Series>
                                <asp:Series ChartType="Column" Name="Category1"></asp:Series>
                                <asp:Series ChartType="Column" Name="Category2"></asp:Series>
                                <asp:Series ChartType="Column" Name="Category3"></asp:Series>
                            </Series>
                            <ChartAreas>
                                <asp:ChartArea Name="ca5"></asp:ChartArea>
                            </ChartAreas>
                             <Legends>
                                <asp:Legend Alignment="Far" HeaderSeparator="Line" Name="category1" ShadowColor="DarkGreen"></asp:Legend>
                                <asp:Legend Alignment="Center" Name="category2" ShadowColor="DarkOrange"></asp:Legend>
                                <asp:Legend Alignment="Center" Name="category3" ShadowColor="Gray"></asp:Legend>
                            </Legends>
                        </asp:Chart>
                    </div>
                    </asp:Panel>
</div>
                </div>
               
    </div> 
     </asp:Content>
