﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Dashboard.aspx.cs" Inherits="MHLPA_Application.Dashboard" MasterPageFile="~/Site.Master" %>

<%@ Register Assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" Namespace="System.Web.UI.DataVisualization.Charting" TagPrefix="asp" %>


<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <link href="css/calender.css" rel="stylesheet" />
    <script src="js/calender.js"></script>
    <%-- <script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>--%>
    <script src="js/amcharts.js"></script>
    <script src="js/radar.js"></script>
    <script src="js/serial.js"></script>
    <script src="js/export.min.js"></script>
    <link href="css/export.css" rel="stylesheet" />
    <script src="js/light.js"></script>
    <script src="js/FileSaver.js"></script>
    <script src="js/fabric.min.js"></script>
    <script src="js/pdfmake.min.js"></script>
    <script src="js/dataloader.min.js"></script>
    <script type="text/javascript">

        $(document).ready(function () {
            $("#MainContent_txtDate").datepicker({
                showOn: "both",
                buttonImageOnly: true,
                buttonText: "",
                changeYear: true,
                changeMonth: true,
                yearRange: "c-20:c+50",
                // minDate: new Date(),
                dateFormat: 'dd-mm-yy',
                //buttonImage: "images/calander_icon.png",
            });
        });
        $("#MainContent_txtDate").datepicker({
            showOn: "both",
            buttonImageOnly: true,
            buttonText: "",
            changeYear: true,
            changeMonth: true,
            yearRange: "c-20:c+50",
            // minDate: new Date(),
            dateFormat: 'dd-mm-yy',
            //buttonImage: "images/calander_icon.png",
        });
        function ValidateControls() {
            var err_flag = 0;
            if ($('#MainContent_ddlRegion').val() == "" || $('#MainContent_ddlRegion').val() == null) {
                $('#MainContent_ddlRegion').css('border-color', 'red');
                err_flag = 1;
            }
            else {
                $('#MainContent_ddlRegion').css('border-color', '');
            }
            if ($('#MainContent_ddlCountry').val() == "" || $('#MainContent_ddlCountry').val() == null) {
                $('#MainContent_ddlCountry').css('border-color', 'red');

                err_flag = 1;
            }
            else {
                $('#MainContent_ddlCountry').css('border-color', '');
            }

            if ($('#MainContent_ddlLocation').val() == "" || $('#MainContent_ddlLocation').val() == null) {
                $('#MainContent_ddlLocation').css('border-color', 'red');
                err_flag = 1;
            }
            else {
                $('#MainContent_ddlLocation').css('border-color', '');
            }

            if ($('#MainContent_ddlLineName').val() == "" || $('#MainContent_ddlLineName').val() == null) {
                $('#MainContent_ddlLineName').css('border-color', 'red');

                err_flag = 1;
            }
            else {
                $('#MainContent_ddlLineName').css('border-color', '');
            }

            if ($('#MainContent_txtDate').val() == "") {
                $('#MainContent_txtDate').css('border-color', 'red');
                err_flag = 1;
            }
            else {

                $('#MainContent_txtDate').css('border-color', '');
            }

            if (err_flag == 0) {
                $('#MainContent_lblError').text('');
                return true;
            }
            else {
                $('#MainContent_lblError').text('Please enter all the mandatory fields.');
                return false;
            }
        }
        function LoadCharts() {
            LoadChartLPAResult();
            LoadChartPerSection();
            LoadChartPerSectionMonthly();
            LoadChartPerLine();
            LoadChartPerQuestion();
            LoadchartComparision();
        }
        function LoadChartLPAResult() {
            var return_first = function () {
                var tmp = null;
                $.ajax({
                    type: "POST",
                    url: 'Dashboard.aspx/returnChartLPAResult',
                    data: "",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (msg) {
                        tmp = msg.d;
                        //tmp = [{"Section_number":"1","section_name":"General Topics","Score":"0.65893470790378","Sec_name_abbr":"GP"},{"Section_number":"2","section_name":"A. People Development","Score":"0.954602368866329","Sec_name_abbr":"PD"},{"Section_number":"3","section_name":"B. Safe, Organized, Clean Work Area","Score":"0.704974619289341","Sec_name_abbr":"SOCWA"},{"Section_number":"4","section_name":"C. Robust Processes and Equipment","Score":"0.918781725888325","Sec_name_abbr":"RPE"},{"Section_number":"5","section_name":"D. Standardized Work","Score":"0.922944162436547","Sec_name_abbr":"SW"},{"Section_number":"6","section_name":"E. Rapid Problem Solving / 8D","Score":"0.82186440677966","Sec_name_abbr":"RPS"}];
                        var chart = AmCharts.makeChart("divChartLPAResult", {
                            "type": "serial",
                            "theme": "light",

                            "titles": [{
                                "text": "LPA Results"
                            }, {
                                "text": "(12 months average)",
                                "bold": false,
                                "size": 10
                            }],
                            "dataProvider": tmp,
                            "valueAxes": [{
                                "gridColor": "#FFFFFF",
                                "gridAlpha": 0,
                                "dashLength": 0,
                                "minimum": 0,
                                "maximum": 100,
                                "minMaxMultiplier": 1.5,
                                "gridCount": 25
                            }],
                            "gridAboveGraphs": true,
                            "startDuration": 1,
                            "graphs": [{
                                "type": "column",
                                "title": "Actual Year",
                                "balloonText": "[[title]]: <b>[[value]]</b>",
                                "bullet": "round",
                                "bulletSize": 10,
                                "bulletBorderColor": "#ffffff",
                                "fillAlphas": 1,
                                "lineAlpha": 0.2,
                                "fillColorsField": "color",
                                "bulletBorderAlpha": 1,
                                "bulletBorderThickness": 2,
                                "labelText": "[[value]]%",
                                "labelPosition": "left",
                                "valueField": "average_score"
                            },

                            {
                                "fillAlphas": 0.4,
                                "title": "Previous Year",
                                "balloonText": "[[title]]: <b>[[value]]</b>",
                                "bullet": "round",
                                "bulletSize": 10,
                                "bulletBorderColor": "#ffffff",
                                "bulletBorderAlpha": 1,
                                "bulletBorderThickness": 2,
                                "valueField": "previous_score",
                                "selectedBackgroundAlpha": 0.1,
                                "selectedBackgroundColor": "#888888",
                                "graphFillAlpha": 0,
                                "graphLineAlpha": 0.5,
                                "selectedGraphFillAlpha": 0,
                                "selectedGraphLineAlpha": 1
                            }],
                            "balloon": {
                                "fillAlpha": 1
                            },
                            //"chartCursor": {
                            //    "categoryBalloonEnabled": false,
                            //    "cursorAlpha": 0,
                            //    "zoomable": false
                            //},
                            "categoryField": "audit_period",
                            "categoryAxis": {
                                "gridPosition": "start",
                                "gridAlpha": 0
                            },
                            "legend": {
                                "position": "absolute",
                                "top": "30px",
                                "left": 600,
                                //"align":"right",
                                "useGraphSettings": true
                            },
                            "export": {
                                "enabled": true
                            }

                        });
                        chart.dataProvider = AmCharts.parseJSON(tmp);
                        chart.validateData();
                    },
                    error: function (e) {
                        //console(e);
                    }
                });
                return tmp;
            }();
        }
        function LoadChartPerSection() {
            var return_first = function () {
                var tmp = null;
                $.ajax({
                    type: "POST",
                    url: 'Dashboard.aspx/returnData',
                    data: "",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (msg) {
                        tmp = msg.d;
                        console.log("tmp_radar:" + tmp);
                        //tmp = [{"Section_number":"1","section_name":"General Topics","Score":"0.65893470790378","Sec_name_abbr":"GP"},{"Section_number":"2","section_name":"A. People Development","Score":"0.954602368866329","Sec_name_abbr":"PD"},{"Section_number":"3","section_name":"B. Safe, Organized, Clean Work Area","Score":"0.704974619289341","Sec_name_abbr":"SOCWA"},{"Section_number":"4","section_name":"C. Robust Processes and Equipment","Score":"0.918781725888325","Sec_name_abbr":"RPE"},{"Section_number":"5","section_name":"D. Standardized Work","Score":"0.922944162436547","Sec_name_abbr":"SW"},{"Section_number":"6","section_name":"E. Rapid Problem Solving / 8D","Score":"0.82186440677966","Sec_name_abbr":"RPS"}];
                        var chart = AmCharts.makeChart("divChartPerSection", {
                            "type": "radar",
                            "theme": "light",
                            "titles": [{
                                "text": "LPA Results Per Section"
                            }, {
                                "text": "(12 months average)",
                                "bold": false,
                                "size": 10
                            }],
                            "dataProvider": tmp,
                            "valueAxes": [{
                                "axisTitleOffset": 20,
                                "minimum": 0,
                                "axisAlpha": 0.15
                            }],
                            "balloon": {
                                "fillAlpha": 1
                            },
                            "startDuration": 2,
                            "graphs": [{
                                "balloonText": "[[category]]: <b>[[value]]</b>",
                                "bullet": "round",
                                "lineThickness": 2,
                                "valueField": "Score"
                            }],
                            "categoryField": "Sec_name_abbr",
                            "export": {
                                "enabled": true
                            }
                        });
                        //var chart = AmCharts.makeChart("divChartPerSection", {
                        //    "type": "radar",
                        //    "theme": "light",
                        //    "titles": [{
                        //        "text": "LPA Result Per Section"
                        //    }],
                        //    "dataProvider": tmp,
                        //    "valueAxes": [{
                        //        "axisTitleOffset": 20,
                        //        "minimum": 0,
                        //        "axisFrequency":25,
                        //        "axisAlpha": 0.15
                        //    }],
                        //    "startDuration": 2,
                        //    "graphs": [{
                        //        "balloonText": "[[category]]: <b>[[value]]</b>",
                        //        "bullet": "round",
                        //        "lineThickness": 2,
                        //        "bulletBorderColor": "#ffffff",
                        //        "valueField": "Score"
                        //    }],
                        //    "categoryField": "Sec_name_abbr",

                        //    "export": {
                        //        "enabled": true
                        //    }
                        //});
                        chart.dataProvider = AmCharts.parseJSON(tmp);
                        chart.validateData();
                    },
                    error: function (e) {
                        //console(e);
                    }
                });
                console.log("tmp2 : " + tmp);
                return tmp;
            }();
            console.log("return_first : " + return_first);

        }
        function LoadChartPerSectionMonthly() {
            var return_first = function () {
                var tmp = null;
                $.ajax({
                    type: "POST",
                    url: 'Dashboard.aspx/returnChartPerSectionMonthly',
                    data: "",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (msg) {
                        tmp = msg.d;
                        //tmp = [{"Section_number":"1","section_name":"General Topics","Score":"0.65893470790378","Sec_name_abbr":"GP"},{"Section_number":"2","section_name":"A. People Development","Score":"0.954602368866329","Sec_name_abbr":"PD"},{"Section_number":"3","section_name":"B. Safe, Organized, Clean Work Area","Score":"0.704974619289341","Sec_name_abbr":"SOCWA"},{"Section_number":"4","section_name":"C. Robust Processes and Equipment","Score":"0.918781725888325","Sec_name_abbr":"RPE"},{"Section_number":"5","section_name":"D. Standardized Work","Score":"0.922944162436547","Sec_name_abbr":"SW"},{"Section_number":"6","section_name":"E. Rapid Problem Solving / 8D","Score":"0.82186440677966","Sec_name_abbr":"RPS"}];
                        var chart = AmCharts.makeChart("divChartPerSectionMonthly", {
                            "type": "serial",
                            "theme": "light",
                            "titles": [{
                                "text": "LPA Results Per Section"
                            }, {
                                "text": "(average)",
                                "bold": false,
                                "size": 10
                            }],
                            "dataProvider": tmp,
                            "valueAxes": [{
                                "gridColor": "#FFFFFF",
                                "gridAlpha": 0.2,
                                "dashLength": 0,
                                "minimum": 0,
                                "maximum": 100
                            }],
                            "gridAboveGraphs": true,
                            "startDuration": 1,
                            "graphs": [{
                                "title": "GT - General Topics",
                                "balloonText": "[[title]]: <b>[[value]]</b>",
                                "bullet": "round",
                                "bulletSize": 10,
                                "bulletBorderColor": "#ffffff",
                                "bulletBorderAlpha": 1,
                                "bulletBorderThickness": 2,
                                "valueField": "GT"
                            },
                            {
                                "title": "A - People Development",
                                "balloonText": "[[title]]: <b>[[value]]</b>",
                                "bullet": "round",
                                "bulletSize": 10,
                                "bulletBorderColor": "#ffffff",
                                "bulletBorderAlpha": 1,
                                "bulletBorderThickness": 2,
                                "valueField": "A"
                            }, {
                                "title": "B - Safe, Organized, Clean Work Area",
                                "balloonText": "[[title]]: <b>[[value]]</b>",
                                "bullet": "round",
                                "bulletSize": 10,
                                "bulletBorderColor": "#ffffff",
                                "bulletBorderAlpha": 1,
                                "bulletBorderThickness": 2,
                                "valueField": "B"
                            },
                            {
                                "title": "C - Robust Processes and Equipment",
                                "balloonText": "[[title]]: <b>[[value]]</b>",
                                "bullet": "round",
                                "bulletSize": 10,
                                "bulletBorderColor": "#ffffff",
                                "bulletBorderAlpha": 1,
                                "bulletBorderThickness": 2,
                                "valueField": "C"
                            },
                            {
                                "title": "D - Standardized Work",
                                "balloonText": "[[title]]: <b>[[value]]</b>",
                                "bullet": "round",
                                "bulletSize": 10,
                                "bulletBorderColor": "#ffffff",
                                "bulletBorderAlpha": 1,
                                "bulletBorderThickness": 2,
                                "valueField": "D"
                            },
                            {
                                "title": "E - Rapid Problem Solving / 8D",
                                "balloonText": "[[title]]: <b>[[value]]</b>",
                                "bullet": "round",
                                "bulletSize": 10,
                                "bulletBorderColor": "#ffffff",
                                "bulletBorderAlpha": 1,
                                "bulletBorderThickness": 2,
                                "valueField": "E"
                            }],
                            "balloon": {
                                "fillAlpha": 1
                            },
                            //"chartCursor": {
                            //    "categoryBalloonEnabled": false,
                            //    "cursorAlpha": 0,
                            //    "zoomable": false
                            //},
                            "categoryField": "audit_period",
                            "categoryAxis": {
                                "gridPosition": "start",
                                "gridAlpha": 0
                            },
                            "legend": {
                                "top": "0 !important"
                            },
                            "export": {
                                "enabled": true
                            }
                        });
                        chart.dataProvider = AmCharts.parseJSON(tmp);
                        chart.validateData();
                    },
                    error: function (e) {
                        //console(e);
                    }
                });
                return tmp;
            }();

        }
        function LoadChartPerLine() {
            var return_first = function () {
                var tmp = null;
                $.ajax({
                    type: "POST",
                    url: 'Dashboard.aspx/returnChartPerLine',
                    data: "",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (msg) {
                        tmp = msg.d;
                        //tmp = [{"Section_number":"1","section_name":"General Topics","Score":"0.65893470790378","Sec_name_abbr":"GP"},{"Section_number":"2","section_name":"A. People Development","Score":"0.954602368866329","Sec_name_abbr":"PD"},{"Section_number":"3","section_name":"B. Safe, Organized, Clean Work Area","Score":"0.704974619289341","Sec_name_abbr":"SOCWA"},{"Section_number":"4","section_name":"C. Robust Processes and Equipment","Score":"0.918781725888325","Sec_name_abbr":"RPE"},{"Section_number":"5","section_name":"D. Standardized Work","Score":"0.922944162436547","Sec_name_abbr":"SW"},{"Section_number":"6","section_name":"E. Rapid Problem Solving / 8D","Score":"0.82186440677966","Sec_name_abbr":"RPS"}];
                        var chart = AmCharts.makeChart("divChartPerLine", {
                            "type": "serial",
                            "theme": "light",
                            "titles": [{
                                "text": "LPA Results per Line(pareto)"
                            }, {
                                "text": "(12 months average)",
                                "bold": false,
                                "size": 10
                            }],
                            "dataProvider": tmp,
                            "valueAxes": [{
                                "gridColor": "#FFFFFF",
                                "gridAlpha": 0,
                                "dashLength": 0,
                                "minimum": 0,
                                "maximum": 100
                            }],
                            "gridAboveGraphs": true,
                            "startDuration": 1,
                            "graphs": [{
                                "type": "column",
                                "title": "Average Score",
                                "balloonText": "[[title]]: <b>[[value]]</b>",
                                "bullet": "round",
                                "bulletSize": 10,
                                "bulletBorderColor": "#ffffff",
                                "fillAlphas": 1,
                                "lineAlpha": 0.2,
                                "fillColorsField": "color",
                                "bulletBorderAlpha": 1,
                                "bulletBorderThickness": 2,
                                "labelText": "[[value]]%",
                                "labelPosition": "left",
                                "valueField": "average_score"
                            }],
                            "balloon": {
                                "fillAlpha": 1
                            },
                            "categoryField": "line_name",
                            "categoryAxis": {
                                "gridPosition": "start",
                                "gridAlpha": 0,
                                "labelRotation": 60
                            },
                            "export": {
                                "enabled": true
                            }
                        });
                        chart.dataProvider = AmCharts.parseJSON(tmp);
                        chart.validateData();
                    },
                    error: function (e) {
                        //console(e);
                    }
                });
                return tmp;
            }();
        }


        function LoadChartPerQuestion() {
            debugger;
            var return_first = function () {
                var tmp = null;
                $.ajax({
                    type: "POST",
                    url: 'Dashboard.aspx/returnChartPerQuestion',
                    data: "",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (msg) {
                        debugger;
                        tmp = msg.d;
                        console.log(msg.d);
                        //tmp = [{"question_id":"0","question":"Are all actions of previous layered audits closed or documented on line RPS?","score":"71","color":"#00732d"},{"question_id":"1","question":"Is the qualification matrix and the validation sheet correctly filled and properly used? Is a first aid responder listed and present?","score":"84","color":"#3ca014"},{"question_id":"2","question":"Are all employees in the work area trained properly to perform the required activities, incl. emergencies?","score":"93","color":"#3ca014"},{"question_id":"3","question":"Do operators understand the quality and safety criteria for their workstation (regarding supplier parts, manufacturing process at the station and the result)?","score":"96","color":"#3ca014"},{"question_id":"4","question":"Are the workplaces clean, orderly, in safe conditions with sufficient lighting? ","score":"59","color":"#eb690f"},{"question_id":"5","question":"Is there a place defined for everything? Is everything on its place incl. emergency equipment?","score":"53","color":"#eb690f"},{"question_id":"6","question":"Are all containers and boxes for components, products, chemicals and waste properly labelled and clean? ","score":"72","color":"#eb690f"},{"question_id":"7","question":"Is the FIFO principle applied for all materials. Are maximum stock levels in the line and in the racks not exceeded?","score":"89","color":"#eb690f"},{"question_id":"8","question":"Are floor and emergency markings used in the right way, not damaged and free of obstacles? ","score":"65","color":"#eb690f"},{"question_id":"9","question":"Are the safety rules followed? Is personal protection equipment listed on the work instruction and properly used? Is safety equipment working (not disabled) and part of the maintenance plan (TPM)?","score":"89","color":"#eb690f"},{"question_id":"10","question":"TPM: Are defined maintenance activities and safety inspection executed according to the plan? Is this documented?","score":"75","color":"#be001e"},{"question_id":"11","question":"Are test equipment, poka yoke devices, gages and fixtures verified? Is the result documented?","score":"91","color":"#be001e"},{"question_id":"12","question":"Was the startup for this part or this shift according to standard (the sheet filled-out, corrective actions documented)? Was the last-off part validated (if required)?","score":"90","color":"#7f7f7f"},{"question_id":"13","question":"Are startup parts properly identified and in the defined location?","score":"92","color":"#7f7f7f"},{"question_id":"14","question":"Do the conditions in the line comply to the requirements in the Standardized Work Chart? Are operators following standardized work chart / visual work instruction?","score":"86","color":"#7f7f7f"},{"question_id":"15","question":"Is the real cycle time according to the standardized work chart? Is the line well balanced? Is the no. of operators acc. to Standardized Work Chart?","score":"87","color":"#7f7f7f"},{"question_id":"16","question":"Is the reaction plan known by the operator / Line leader (when and how to react with failures, how to react with breakdowns or how to react when identifying a problem during startup)?","score":"96","color":"#7f7f7f"},{"question_id":"17","question":"Are defined boundary samples or a catalog available for all quality checks and used according the visual work instruction?","score":"94","color":"#7f7f7f"},{"question_id":"18","question":"Are packaging instructions available and followed?","score":"94","color":"#7f7f7f"},{"question_id":"19","question":"Are nonconforming or suspect products identified and placed in a designated area? Scrap parts in red bins; Suspect parts in yellow bins.","score":"88","color":"#7f7f7f"},{"question_id":"20","question":"In case of rework: is rework executed according to the standard (outside of the line, workstation according to standard, material re-entering the line at same place it was sorted out)","score":"92","color":"#7f7f7f"},{"question_id":"21","question":"In case of any deviation to the standard (e.g. regarding product, process, maintenance etc.): is there a valid, written and approved deviation permit and are all conditions in this permit applied?","score":"94","color":"#7f7f7f"},{"question_id":"22","question":"Is the Line RPS used properly and up-to-date?","score":"71","color":"#d9d9d9"},{"question_id":"23","question":"Are there current customer complaints at the line / for this product? Are the operators aware of the complaint and the corrective action? Is an 8D available for every complaint and up-to-date?","score":"93","color":"#d9d9d9"},{"question_id":"24","question":"Is there a visualization of the performance of the line available? Is Line RPS used when the target is not reached (Downtime, Scrap, etc.)","score":"84","color":"#d9d9d9"}];
                        var chart = AmCharts.makeChart("divChartPerQuestion", {
                            "type": "serial",
                            "theme": "light",
                            "titles": [{
                                "text": "LPA Results per Question"
                            }, {
                                "text": "(12 months average)",
                                "bold": false,
                                "size": 10
                            }],
                            "dataProvider": tmp,
                            "valueAxes": [{
                                "gridColor": "#FFFFFF",
                                "gridAlpha": 0,
                                "dashLength": 0,
                                "minimum": 0,
                                "maximum": 100,
                                "minMaxMultiplier": 1.05,
                            }],
                            "gridAboveGraphs": true,
                            "startDuration": 1,
                            "graphs": [{
                                "type": "column",
                                "title": "score",
                                "balloonText": "[[question]]",
                                "balloonAlpha": 1,
                                "bullet": "round",
                                "bulletSize": 10,
                                "bulletBorderColor": "#ffffff",
                                "bulletBorderAlpha": 1,
                                "bulletBorderThickness": 2,
                                "fillAlphas": 1,
                                "lineAlpha": 0.2,
                                "fillColorsField": "color",
                                "balloonText": "[[question]]",
                                "labelText": "[[value]]%",
                                "valueField": "score",
                                "labelPosition": "left",
                                "balloonPosition": "bottom"

                            }],
                            "balloon": {
                                "fillAlpha": 1
                            },
                            "categoryField": "question_id",
                            "categoryAxis": {
                                "gridPosition": "start",
                                "gridAlpha": 0
                            },
                            "export": {
                                "enabled": true
                            }
                        });
                        chart.dataProvider = AmCharts.parseJSON(tmp);
                        chart.validateData();
                    },
                    error: function (e) {
                        console.log(e);
                    }
                });
                return tmp;
            }();
        }
        function LoadchartComparision() {
            var return_first = function () {
                var tmp = null;
                $.ajax({
                    type: "POST",
                    url: 'Dashboard.aspx/returnchartComparision',
                    data: "",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (msg) {
                        tmp = msg.d;
                        console.log("tmp1 : " + tmp);
                        //tmp = [{"Section_number":"1","section_name":"General Topics","Score":"0.65893470790378","Sec_name_abbr":"GP"},{"Section_number":"2","section_name":"A. People Development","Score":"0.954602368866329","Sec_name_abbr":"PD"},{"Section_number":"3","section_name":"B. Safe, Organized, Clean Work Area","Score":"0.704974619289341","Sec_name_abbr":"SOCWA"},{"Section_number":"4","section_name":"C. Robust Processes and Equipment","Score":"0.918781725888325","Sec_name_abbr":"RPE"},{"Section_number":"5","section_name":"D. Standardized Work","Score":"0.922944162436547","Sec_name_abbr":"SW"},{"Section_number":"6","section_name":"E. Rapid Problem Solving / 8D","Score":"0.82186440677966","Sec_name_abbr":"RPS"}];
                        var chart = AmCharts.makeChart("divchartComparision", {
                            "type": "serial",
                            "theme": "light",
                            "titles": [{
                                "text": "LPA Completion Rate"
                            }, {
                                "text": "(average)",
                                "bold": false,
                                "size": 10
                            }],
                            "dataProvider": tmp,
                            "valueAxes": [{
                                "gridColor": "#FFFFFF",
                                "gridAlpha": 0,
                                "dashLength": 0,
                                "minimum": 0

                            }],
                            "gridAboveGraphs": true,
                            "startDuration": 1,
                            "graphs": [{
                                "type": "column",
                                "title": "",
                                "balloonText": "<b>[[value]]</b>",
                                "bullet": "round",
                                "bulletSize": 10,
                                "bulletBorderColor": "#ffffff",
                                "bulletBorderAlpha": 1,
                                "bulletBorderThickness": 2,
                                "fillAlphas": 1,
                                "lineAlpha": 0.2,
                                "fillColorsField": "color",
                                "labelText": "[[value]]%",
                                "labelPosition": "left",
                                "valueField": "cy_perform_rate"
                            },
                            {
                                "fillAlphas": 0.4,
                                "title": "",
                                "balloonText": "<b>[[value]]</b>",
                                "bullet": "round",
                                "bulletSize": 10,
                                "bulletBorderColor": "#ffffff",
                                "bulletBorderAlpha": 1,
                                "bulletBorderThickness": 2,
                                "valueField": "ly_perform_rate",
                                "selectedBackgroundAlpha": 0.1,
                                "selectedBackgroundColor": "#888888",
                                "graphFillAlpha": 0,
                                "graphLineAlpha": 0.5,
                                "selectedGraphFillAlpha": 0,
                                "labelPosition": "left",
                                "selectedGraphLineAlpha": 1
                            }],
                            "balloon": {
                                "fillAlpha": 1
                            },
                            //"chartCursor": {
                            //    "categoryBalloonEnabled": false,
                            //    "cursorAlpha": 0,
                            //    "zoomable": false
                            //},
                            "categoryField": "audit_period",
                            "categoryAxis": {
                                "gridPosition": "start",
                                "gridAlpha": 0
                            },
                            //"legend": {
                            //    "position": "absolute",
                            //    "top": 15,
                            //    "left": 750,
                            //    //"align":"right",
                            //    "useGraphSettings": true
                            //},
                            "export": {
                                "enabled": true
                            }
                        });
                        chart.dataProvider = AmCharts.parseJSON(tmp);
                        chart.validateData();
                    },
                    error: function (e) {
                        //console(e);
                    }
                });
                return tmp;
            }();
        }

    </script>
    <style>
        #divchartComparision
        {
            width: 100%;
            height: 500px;
        }

        #divChartPerQuestion
        {
            width: 100%;
            height: 500px;
        }

        #divChartPerLine
        {
            width: 100%;
            height: 500px;
        }

        #divChartLPAResult
        {
            width: 100%;
            height: 500px;
        }

        #divChartPerSection
        {
            height: 375px!important;
        }

        .amcharts-chart-div a
        {
            display: none;
        }

        #divChartPerSectionMonthly
        {
            height: 500px;
        }

        .mn_mrg_left
        {
            padding-left: 0;
            box-shadow: 0px 0px 3px #aaa;
            background: #fff;
        }

        .mn_mrg_right
        {
            padding-right: 0;
            box-shadow: 0px 0px 3px #aaa;
            background: #fff;
        }

        .mn_mrg
        {
            padding: 0;
            box-shadow: 0px 0px 3px #aaa;
            background: #fff;
        }

        .mn_fetch .mn_mrg_left, .mn_fetch .mn_mrg, .mn_fetch .mn_mrg_right
        {
            box-shadow: none;
            padding: 5px;
        }

        .mn_fetch, .ma_lpa_result
        {
            width: 100%;
            float: left;
            background: #fff;
            border: solid 1px #ddd;
        }

        .ma_lpa_result
        {
            margin: 10px 0;
        }

        .mn_none_mar
        {
            margin: 0 0 10px 0;
        }

        #MainContent_pnlReport
        {
            width: 100%;
            float: left;
        }

        .amcharts-legend-div
        {
            top: 30px;
            /*position:static!important;*/
        }

        .secLabels
        {
            font-size: 11px;
        }
        /*.amChartsLegend amcharts-legend-div
        {
            position:static!important;
        }*/
        .mn_mrg_right .amcharts-legend-div
        {
            top: 0;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePageMethods="true"></asp:ScriptManager>

    <div class="col-md-12">
        <div class='block-web' style='float: left; width: 100%; height: 36px;'>
            <div class="header">
                <div class="crumbs">
                    <!-- Start : Breadcrumbs -->
                    <ul id="breadcrumbs" class="breadcrumb">
                        <li>
                            <a class="mn_breadcrumb">Reports</a>
                        </li>
                        <li class="current">Dashboard</li>

                    </ul>
                </div>
            </div>

        </div>
    </div>
    <%--from--%>
    <div class="md_main">
        <div class="row">
            <div class="mannHummel" id="mannHummelId">
                <div class="clearfix" style="width: 40px;"></div>
                <asp:Panel runat="server" ID="pnlFilters">
                    <div class="filter_panel">
                        <div>
                            <div class="col-md-3">
                                <div class="col-md-6 control">
                                    <label class="filter_label"><b>Region* : </b></label>
                                    <asp:DropDownList class="control_dropdown" runat="server" ID="ddlRegion" OnSelectedIndexChanged="ddlRegion_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                </div>
                                <div class="col-md-6 control">
                                    <label class="filter_label"><b>Country* : </b></label>
                                    <asp:DropDownList class="control_dropdown" runat="server" ID="ddlCountry" OnSelectedIndexChanged="ddlCountry_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="col-md-6 control">
                                    <label class="filter_label"><b>Location* : </b></label>
                                    <asp:DropDownList class="control_dropdown" runat="server" ID="ddlLocation" OnSelectedIndexChanged="ddlLocation_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                </div>
                                <div class="col-md-6 control">
                                    <label class="filter_label"><b>Building* : </b></label>
                                    <asp:DropDownList class="control_dropdown" runat="server" ID="ddlBuilding" OnSelectedIndexChanged="ddlBuilding_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-6 control">
                                <div class="col-md-3 control">
                                    <label class="filter_label"><b>Line Name/No* : </b></label>
                                    <asp:DropDownList class="control_dropdown" runat="server" ID="ddlLineName" AutoPostBack="true" OnSelectedIndexChanged="ddlLineName_SelectedIndexChanged"></asp:DropDownList>
                                </div>
                                <div class="col-md-3 control">
                                    <label class="filter_label"><b>Date* : </b></label>
                                    <asp:TextBox class="control_dropdown" runat="server" ID="txtDate"></asp:TextBox>
                                </div>
                                <div class="col-md-2" style="float: right">
                                    <asp:Button runat="server" CssClass="btn-add" ID="btnClear" Style="margin-left: 4px; float: right;" Text="Clear" OnClick="btnClear_Click" />
                                </div>
                                <div class="col-md-4">
                                    <asp:Button runat="server" CssClass="btn-add" ID="btnFilter" Style="margin-left: 4px; float: right;" Text="Fetch Report" OnClick="btnFilter_Click" OnClientClick="return ValidateControls();" />
                                </div>
                                <asp:Label runat="server" ID="lblError" ForeColor="Red"></asp:Label>
                            </div>
                            <%--<div class="col-md-4 filter_label" style="width: 14%">
                                <p>Region* : </p>
                            </div>
                            <div class="col-md-4" style="width: 20%">
                                <asp:DropDownList class="form-control1 mn_inp control3 filter_select_control" runat="server" ID="ddlRegion" OnSelectedIndexChanged="ddlRegion_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                            </div>
                            <div class="col-md-4 filter_label" style="width: 13%">
                                <p>Country* : </p>
                            </div>
                            <div class="col-md-4" style="width: 20%">
                                <asp:DropDownList class="form-control1 mn_inp control3 filter_select_control" runat="server" ID="ddlCountry" OnSelectedIndexChanged="ddlCountry_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                            </div>
                            <div class="col-md-4 filter_label" style="width: 13%">
                                <p>Location* : </p>
                            </div>
                            <div class="col-md-4" style="width: 20%">
                                <asp:DropDownList class="form-control1 mn_inp control3 filter_select_control" runat="server" ID="ddlLocation" OnSelectedIndexChanged="ddlLocation_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                            </div>
                        </div>
                        <div>
                            <div class="col-md-4 filter_label" style="width: 14%">
                                <p>Line Name/No* :</p>
                            </div>
                            <div class="col-md-4" style="width: 20%">
                                <asp:DropDownList class="form-control1 mn_inp control3 filter_select_control" runat="server" ID="ddlLineName" AutoPostBack="true" OnSelectedIndexChanged="ddlLineName_SelectedIndexChanged"></asp:DropDownList>

                            </div>

                            <div class="col-md-4 filter_label" style="width: 13%">
                                <p>Date* : </p>
                            </div>
                            <div class="col-md-4" style="width: 20%">
                                <asp:TextBox class="form-control1 mn_inp control3 filter_select_control" runat="server" ID="txtDate"></asp:TextBox>
                            </div>
                            
                            <div class="col-md-4 filter_label" style="width: 12%">
                                <asp:Label runat="server" ID="lblError" ForeColor="Red"></asp:Label>
                            </div>
                            <div class="col-md-4" style="width: 21%">
                                <div class="col-md-5" style="float: left">
                                    <asp:Button runat="server" CssClass="btn-add" ID="btnClear" Style="margin-left: 4px; float: right;" Text="Clear" OnClick="btnClear_Click" />
                                </div>
                                <div class="col-md-7">
                                    <asp:Button runat="server" CssClass="btn-add" ID="btnFilter" Style="margin-left: 4px; float: right;" Text="Fetch Report" OnClick="btnFilter_Click" OnClientClick="return ValidateControls();" />
                                </div>
                            </div>--%>
                        </div>

                    </div>



                </asp:Panel>
                <div class="clearfix" style="height: 5px;"></div>
                <div class="ma_lpa_result">
                    <div class="col-md-10 col-xs-offset-1">
                        <div id="divChartLPAResult"></div>
                    </div>
                </div>
                <div class="col-md-4 mn_mrg_left" style="height: 500px;">
                    <div class="col-md-12 mn_mrg_left" id="divChartPerSection"></div>
                    <div class="mn_fetch">
                        <div class="col-md-6 mn_mrg_left" style="background-color: white;">
                            <asp:Label CssClass="secLabels" runat="server" ID="sec1"></asp:Label>
                        </div>
                        <div class="col-md-6 mn_mrg" style="background-color: white;">
                            <asp:Label CssClass="secLabels" runat="server" ID="sec2"></asp:Label>
                        </div>
                        <div class="col-md-6 mn_mrg_right" style="background-color: white;">
                            <asp:Label CssClass="secLabels" runat="server" ID="sec3"></asp:Label>
                        </div>
                        <div class="col-md-6 mn_mrg_left" style="background-color: white;">
                            <asp:Label CssClass="secLabels" runat="server" ID="sec4"></asp:Label>
                        </div>
                        <div class="col-md-6 mn_mrg" style="background-color: white;">
                            <asp:Label CssClass="secLabels" runat="server" ID="sec5"></asp:Label>
                        </div>
                        <div class="col-md-6 mn_mrg_right" style="background-color: white;">
                            <asp:Label CssClass="secLabels" runat="server" ID="sec6"></asp:Label>
                        </div>
                    </div>
                </div>

                <div class="col-md-8 mn_mrg_right" id="divChartPerSectionMonthly"></div>

                <div class="ma_lpa_result mn_none_mar" id="divChartPerQuestion"></div>
                <div class="ma_lpa_result">
                    <div class="col-md-10 col-xs-offset-1">
                        <div id="divChartPerLine"></div>
                    </div>
                </div>

                <div class="ma_lpa_result" id="divchartComparision"></div>
                <asp:Panel ID="reportpnl" runat="server" Visible="false">
                    <asp:Panel runat="server" Visible="false" ID="pnlReport">
                        <div class="ma_lpa_result">
                            <asp:Chart ID="Chart1" runat="server" Style="margin: 0 auto; display: block; height: auto"
                                BackGradientStyle="LeftRight" Palette="None"
                                PaletteCustomColors="green" Width="1000px">
                                <Titles>
                                    <asp:Title Font="16pt, style=Bold" Name="LPA Result"></asp:Title>
                                </Titles>

                                <Series>

                                    <asp:Series ChartType="Area" Name="Previous Year"></asp:Series>
                                    <asp:Series ChartType="Column" Name="Actual Year"></asp:Series>
                                </Series>
                                <Legends>
                                    <asp:Legend Name="Actual year" ShadowColor="DarkGreen"></asp:Legend>
                                    <asp:Legend Name="Previous Year" ShadowColor="LightGray"></asp:Legend>
                                </Legends>
                                <ChartAreas>
                                    <asp:ChartArea Name="cA1"></asp:ChartArea>
                                </ChartAreas>

                            </asp:Chart>
                        </div>
                        <div class="clearfix" style="height: 5px;"></div>
                        <div class="col-md-4 mn_mrg_left">
                            <asp:Chart ID="ChartPerSection" runat="server" Style="margin: 0 auto; display: block"
                                BackGradientStyle="LeftRight" Palette="None"
                                PaletteCustomColors="green" Width="400px">
                                <Titles>
                                    <asp:Title Font="16pt, style=Bold" Name="t2"></asp:Title>
                                </Titles>
                                <Series>
                                    <asp:Series Name="Series1" ChartType="Radar"></asp:Series>
                                </Series>
                                <ChartAreas>
                                    <asp:ChartArea Name="ca2"></asp:ChartArea>
                                </ChartAreas>
                            </asp:Chart>
                        </div>
                        <div class="col-md-8 mn_mrg_right">
                            <asp:Chart ID="ChartPerSectionMonthly" runat="server" Style="margin: 0 auto; display: block"
                                BackGradientStyle="LeftRight" Palette="None"
                                PaletteCustomColors="green" Width="800px">
                                <Titles>
                                    <asp:Title Font="16pt, style=Bold" Name="t3"></asp:Title>
                                </Titles>
                                <Series>
                                    <asp:Series ChartType="Line"></asp:Series>
                                    <asp:Series ChartType="Line"></asp:Series>
                                    <asp:Series ChartType="Line"></asp:Series>
                                    <asp:Series ChartType="Line"></asp:Series>
                                    <asp:Series ChartType="Line"></asp:Series>
                                    <asp:Series ChartType="Line"></asp:Series>
                                </Series>
                                <ChartAreas>
                                    <asp:ChartArea Name="ca3"></asp:ChartArea>
                                </ChartAreas>
                                <Legends>

                                    <asp:Legend Alignment="Center" Docking="Bottom" IsTextAutoFit="False" LegendStyle="Row" />

                                </Legends>
                            </asp:Chart>
                        </div>
                        <%-- <div class="mn_fetch">
                            <div class="col-md-4 mn_mrg_left" style="background-color: white;">
                                <asp:Label runat="server" ID="sec1"></asp:Label>
                            </div>
                            <div class="col-md-4 mn_mrg" style="background-color: white;">
                                <asp:Label runat="server" ID="sec2"></asp:Label>
                            </div>
                            <div class="col-md-4 mn_mrg_right" style="background-color: white;">
                                <asp:Label runat="server" ID="sec3"></asp:Label>
                            </div>
                            <div class="col-md-4 mn_mrg_left" style="background-color: white;">
                                <asp:Label runat="server" ID="sec4"></asp:Label>
                            </div>
                            <div class="col-md-4 mn_mrg" style="background-color: white;">
                                <asp:Label runat="server" ID="sec5"></asp:Label>
                            </div>
                            <div class="col-md-4 mn_mrg_right" style="background-color: white;">
                                <asp:Label runat="server" ID="sec6"></asp:Label>
                            </div>
                        </div>--%>
                        <div class="clearfix" style="height: 5px;"></div>
                        <div class="ma_lpa_result">
                            <asp:Chart ID="ChartPerLine" runat="server" Style="margin: 0 auto; display: block"
                                BackGradientStyle="LeftRight" Palette="None"
                                PaletteCustomColors="green" Width="1000px">
                                <Titles>
                                    <asp:Title Font="16pt, style=Bold" Name="t4"></asp:Title>
                                </Titles>
                                <Series>
                                    <asp:Series ChartType="Column" Name="Average Score"></asp:Series>
                                </Series>
                                <ChartAreas>
                                    <asp:ChartArea Name="ca4"></asp:ChartArea>
                                </ChartAreas>
                                <Legends>
                                    <asp:Legend Name="Average Score" ShadowColor="DarkGreen"></asp:Legend>
                                </Legends>
                            </asp:Chart>
                        </div>
                        <div class="clearfix"></div>
                        <div class="ma_lpa_result mn_none_mar">
                            <asp:Chart ID="ChartPerQuestion" runat="server" Style="margin: 0 auto; display: block"
                                BackGradientStyle="LeftRight" Palette="None"
                                PaletteCustomColors="green" Width="1000px">
                                <Titles>
                                    <asp:Title Font="16pt, style=Bold" Name="t5"></asp:Title>
                                </Titles>
                                <Series>
                                    <asp:Series ChartType="Column" Name="Average Score"></asp:Series>
                                </Series>
                                <ChartAreas>
                                    <asp:ChartArea Name="ca5"></asp:ChartArea>
                                </ChartAreas>
                                <%--<Legends>
                                 <asp:Legend Name="Average Score" ShadowColor="DarkGreen"></asp:Legend>
                            </Legends>--%>
                            </asp:Chart>
                        </div>
                        <div class="clearfix"></div>
                        <div class="ma_lpa_result">
                            <asp:Chart ID="chartComparision" runat="server" Style="margin: 0 auto; display: block; height: auto"
                                BackGradientStyle="LeftRight" Palette="None"
                                PaletteCustomColors="green" Width="1000px">
                                <Titles>
                                    <asp:Title Font="16pt, style=Bold" Name="LPA Result"></asp:Title>
                                </Titles>
                                <Series>

                                    <asp:Series ChartType="Area" Name="Previous Year"></asp:Series>
                                    <asp:Series ChartType="Column" Name="Actual Year"></asp:Series>
                                </Series>
                                <Legends>
                                    <asp:Legend Name="Actual year" ShadowColor="DarkGreen"></asp:Legend>
                                    <asp:Legend Name="Previous Year" ShadowColor="LightGray"></asp:Legend>
                                </Legends>
                                <ChartAreas>
                                    <asp:ChartArea Name="cA1"></asp:ChartArea>
                                </ChartAreas>

                            </asp:Chart>
                        </div>
                    </asp:Panel>
                </asp:Panel>
            </div>
        </div>
        <%--end--%>
        <%-- <asp:ScriptManager ID="scriptManagerReport" runat="server"> 
         </asp:ScriptManager> 
  
        <rsweb:ReportViewer  runat ="server" ProcessingMode="Remote" Width="99.9%" Height="100%"  id="rvSiteMapping" > 
            <ServerReport DisplayName="MainReport" Timeout="600000" />
        </rsweb:ReportViewer>  --%>
    </div>
</asp:Content>
