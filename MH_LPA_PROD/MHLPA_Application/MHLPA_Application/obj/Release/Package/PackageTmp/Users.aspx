﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Users.aspx.cs" Inherits="MHLPA_Application.Users" MasterPageFile="~/Site.Master" ValidateRequest="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript">
        $(document).ready(function () {

            //$("#chkInActive").on("click", function () {
            //    $.ajax({
            //        type: "POST",
            //        contentType: "application/json; charset=utf-8",
            //        url: "Users.aspx/LoadGrid",
            //        data: "{'Inactive':'1'}",
            //        dataType: "json",
            //        success: function (data) {
            //            console.log(data);
            //            console.log(data.d);
            //        },
            //        error: function (result) {
            //            console.log("No Match");
            //        }
            //    });

            //});

            $("#MainContent_txtStartDate").datepicker({
                showOn: "both",
                buttonImageOnly: true,
                buttonText: "",
                changeYear: true,
                changeMonth: true,
                yearRange: "c-20:c+50",
                // minDate: new Date(),
                dateFormat: 'dd-mm-yy',
                //buttonImage: "../images/calander-icon.png",
            });
            $("#MainContent_txtEndDate").datepicker({
                showOn: "both",
                buttonImageOnly: true,
                buttonText: "",
                changeYear: true,
                changeMonth: true,
                yearRange: "c-20:c+50",
                // minDate: new Date(),
                dateFormat: 'dd-mm-yy',
                //buttonImage: "../images/calander-icon.png",
            });
        });
        function validateEmail(email) {
            var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(email);
        }
        function validateControls() {
            //debugger
            var err_flag = 0;
            if ($('#MainContent_txtUserID').val() == "") {
                $('#MainContent_txtUserID').css('border-color', 'red');
                err_flag = 1;
            }
            else {
                $('#MainContent_txtUserID').css('border-color', '');
            }
            if ($('#MainContent_txtLastName').val() == "") {
                $('#MainContent_txtLastName').css('border-color', 'red');

                err_flag = 1;
            }
            else {
                $('#MainContent_txtLastName').css('border-color', '');
            }
            if ($('#MainContent_txtFirstName').val() == "") {
                $('#MainContent_txtFirstName').css('border-color', 'red');

                err_flag = 1;
            }
            else {
                $('#MainContent_txtFirstName').css('border-color', '');
            }
            if ($('#MainContent_txtpwd').val() == "") {
                $('#MainContent_txtpwd').css('border-color', 'red');

                err_flag = 1;
            }
            else {
                $('#MainContent_txtpwd').css('border-color', '');
            }

            if ($('#MainContent_txtrepwd').val() == "") {
                $('#MainContent_txtrepwd').css('border-color', 'red');

                err_flag = 1;
            }
            else {
                $('#MainContent_txtrepwd').css('border-color', '');
            }

            if ($('#MainContent_txtEmailId').val() == "") {
                $('#MainContent_txtEmailId').css('border-color', 'red');

                err_flag = 1;
            }
            else {
                if (!validateEmail($('#MainContent_txtEmailId').val())) {
                    $('#MainContent_txtEmailId').css('border-color', 'red');

                    err_flag = 1;
                }
                else
                    $('#MainContent_txtEmailId').css('border-color', '');
            }
            if ($('#MainContent_txtStartDate').val() == "") {
                $('#MainContent_txtStartDate').css('border-color', 'red');

                err_flag = 1;
            }
            else {
                $('#MainContent_txtStartDate').css('border-color', '');
            }
            if ($('#MainContent_txtEndDate').val() == "") {
                $('#MainContent_txtEndDate').css('border-color', '');
            }
            else {
                //return validateDates();
                //$('#MainContent_txtEndDate').css('border-color', 'red');

            }

            if (err_flag == 0) {
                $('#MainContent_lblError').text('');
                return checkPasswordMatch();

            }
            else {
                $('#MainContent_lblError').text('Please enter all the mandatory fields/valid data.');
                return false;
            }
        }
        function checkPasswordMatch() {
            var pwd = $('#MainContent_txtpwd').val();
            var repwd = $('#MainContent_txtrepwd').val();
            if (pwd == repwd) {
                $('#MainContent_lblpwd').text('');
                return true;

            }
            else {
                $('#MainContent_lblpwd').text('Password does not match.');
                return false;
            }
        }

        function validateDates() {
            if (Date.parse($('#MainContent_txtEndDate').val()) <= Date.parse($('#MainContent_txtStartDate').val())) {
                //if ($.datepicker.parseDate('dd/mm/yy',$('#MainContent_txtEndDate').val()) < $.datepicker.parseDate('dd/mm/yy',$("#MainContent_txtStartDate").val())) {
                $('#MainContent_lblError').text('End date should be greater than start date.');
                return false;
            }
        }

    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div>
        <div class='block-web' style='float: left; width: 100%; height: 36px;'>
            <div class="header">
                <div class="crumbs">
                    <!-- Start : Breadcrumbs -->
                    <ul id="breadcrumbs" class="breadcrumb">
                        <li>
                            <a class="mn_breadcrumb">SetUp</a>
                        </li>
                        <li class="current">Users</li>

                    </ul>
                </div>
            </div>

        </div>
    </div>
    <div>
        <!-- main content start-->
        <div>
            <div class="main-page">
                <div class="row">
                    <div class="col-md-8">
                        <div class="col-md-12 nopad">

                            <div class="panel panel-default panel-table">
                                <div class="panel-group tool-tips widget-shadow" id="accordion" role="tablist" aria-multiselectable="true">
                                    <h4 class="title2">Users</h4>
                                    <%--<div style="float: right">

                                        <label class="switch">
                                            <asp:CheckBox ID="chkInActive" runat="server" OnCheckedChanged="chkInActive_CheckedChanged" AutoPostBack="true" />
                                            <span class="slider round"></span>
                                           
                                        </label>
                                    </div>--%>

                                    <div class="toggle-group" style="float: right;">
                                        <%--<input type="checkbox" name="on-off-switch" id="on-off-switch" checked="" tabindex="1">--%>
                                        <asp:CheckBox ID="chkInActive"  runat="server" AutoPostBack="true" OnCheckedChanged="chkInActive_CheckedChanged" />
                                        
                                        <label for="MainContent_chkInActive">
                                            <span class="aural"></span> 
                                        </label>
                                        <div class="onoffswitch pull-right" aria-hidden="true">
                                            <div class="onoffswitch-label">
                                                <div class="onoffswitch-inner"></div>
                                                <div class="onoffswitch-switch"></div>
                                            </div>
                                        </div>
                                    </div>

<%--                                    <div class="onoffswitch" style="float: right;">
                                        <asp:LinkButton ID="lnkCheckbox" runat="server" OnClick="lnkCheckbox_Click" CausesValidation="false">
                                        <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="chkInActive" />
                                        <asp:CheckBox ID="chkInActive" CssClass="onoffswitch-checkbox"  runat="server" OnCheckedChanged="chkInActive_CheckedChanged" AutoPostBack="true" />
                                        
                                         <label class="onoffswitch-label" for="chkInActive" >
                                            <span class="onoffswitch-inner"></span>
                                            <span class="onoffswitch-switch"></span>
                                        </label>
                                        </asp:LinkButton>
                                    </div>--%>
                                    <hr />
                                    <div class="col-md-12 nopad mn_table">
                                        <asp:GridView ID="grdUsers" OnRowDataBound="grdUsers_RowDataBound" runat="server" AllowPaging="true" AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="user_id" CssClass="dictionary" Width="100%" HeaderStyle-BackColor="#ff6c52" AlternatingRowStyle-BackColor="#cccccc" OnPageIndexChanging="grdUsers_PageIndexChanging">

                                            <Columns>
                                                <asp:TemplateField HeaderText="Default Location" HeaderStyle-ForeColor="#04522c" ControlStyle-Width="100px">
                                                    <HeaderTemplate>
                                                        Location
            <asp:DropDownList ID="ddlLocation_grid" Style="width: 150px;" runat="server" OnSelectedIndexChanged="ddlLocation_grid_SelectedIndexChanged" AutoPostBack="true" AppendDataBoundItems="true">
            </asp:DropDownList>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lblloc_nm" runat="server" Visible="true" Text='<%# Bind("location_name") %>' OnClick="Select" CommandArgument='<%# Bind("user_id") %>'></asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Last Name" HeaderStyle-ForeColor="#04522c" ControlStyle-Width="100px">
                                                    <HeaderTemplate>
                                                        Last Name
            <asp:DropDownList ID="ddlLastName_grid" Style="width: 150px;" runat="server" OnSelectedIndexChanged="ddlLastName_grid_SelectedIndexChanged" AutoPostBack="true" AppendDataBoundItems="true">
            </asp:DropDownList>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lbllastname" runat="server" Visible="true" Text='<%# Bind("emp_last_name") %>' OnClick="Select" CommandArgument='<%# Bind("user_id") %>'></asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="First Name" HeaderStyle-ForeColor="#04522c" ControlStyle-Width="100px">
                                                    <HeaderTemplate>
                                                        First Name
            <asp:DropDownList ID="ddlFirstName_grid" Style="width: 150px;" runat="server" OnSelectedIndexChanged="ddlFirstName_grid_SelectedIndexChanged" AutoPostBack="true" AppendDataBoundItems="true">
            </asp:DropDownList>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lblfirstname" runat="server" Visible="true" Text='<%# Bind("emp_first_name") %>' OnClick="Select" CommandArgument='<%# Bind("user_id") %>'></asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="User ID" HeaderStyle-ForeColor="#04522c" ControlStyle-Width="100px">
                                                    <HeaderTemplate>
                                                        User ID
            <asp:DropDownList ID="ddlUserName_grid" Style="width: 150px;" runat="server" OnSelectedIndexChanged="ddlUserName_grid_SelectedIndexChanged" AutoPostBack="true" AppendDataBoundItems="true">
            </asp:DropDownList>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lnkID" runat="server" Text='<%# Bind("username") %>' OnClick="Select" CommandArgument='<%# Bind("user_id") %>'></asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Admin Type" HeaderStyle-ForeColor="#04522c" ControlStyle-Width="100px">
                                                    <HeaderTemplate>
                                                        Admin Type
            <asp:DropDownList ID="ddlType_grid" Style="width: 150px;" runat="server" OnSelectedIndexChanged="ddlType_grid_SelectedIndexChanged" AutoPostBack="true" AppendDataBoundItems="true">
            </asp:DropDownList>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lnkAdmin" runat="server" Visible="true" Text='<%# Bind("admin_flag") %>' OnClick="Select" CommandArgument='<%# Bind("user_id") %>'></asp:LinkButton>
                                                    </ItemTemplate>

                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Role" HeaderStyle-ForeColor="#04522c" ControlStyle-Width="100px">
                                                    <HeaderTemplate>
                                                        Role
            <asp:DropDownList ID="ddlRole_grid" Style="width: 150px;" runat="server" OnSelectedIndexChanged="ddlRole_grid_SelectedIndexChanged" AutoPostBack="true" AppendDataBoundItems="true">
            </asp:DropDownList>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lnkRole" runat="server" Visible="true" Text='<%# Bind("role") %>' OnClick="Select" CommandArgument='<%# Bind("user_id") %>'></asp:LinkButton>
                                                    </ItemTemplate>

                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Privacy Settings" HeaderStyle-ForeColor="#04522c" ControlStyle-Width="100px">
                                                    <ItemTemplate>
                                                        <asp:CheckBox runat="server" Enabled="false" Checked='<%#Eval("display_name_flag").ToString() == "Y"  %>' />
                                                        <%--<asp:LinkButton ID="lblprvsetting" runat="server" Visible="true" Text='<%# Bind("display_name_flag") %>' OnClick="Select" CommandArgument='<%# Bind("user_id") %>'></asp:LinkButton>
                                                        --%>
                                                    </ItemTemplate>

                                                </asp:TemplateField>


                                                <asp:TemplateField HeaderText="Action" HeaderStyle-ForeColor="#04522c" ControlStyle-Width="50px">
                                                    <ItemTemplate>
                                                        <asp:ImageButton runat="server" ToolTip="Edit" Width="20px" ImageUrl="images/edit_icon.jpg" ID="imgAction" OnClick="Edit" CommandArgument='<%# Bind("user_id") %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>

                                        </asp:GridView>

                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="panel-group tool-tips widget-shadow">
                            <div class="col-md-12 nopad">
                                <asp:Button runat="server" CssClass="btn-add" ID="btnAdd" Text="Add" OnClick="btnAdd_Click" />
                            </div>
                            <hr />
                            <asp:Panel runat="server" ID="viewpanel" Visible="true">
                                <div class="col-md-12 nopad">
                                    <div class="row-info">
                                        <div class="col-md-5 nopad">
                                            <asp:Label runat="server" Text="User Name : "></asp:Label>
                                        </div>
                                        <div class="col-md-7 nopad">
                                            <asp:Label runat="server" ID="lblUserName"></asp:Label>
                                        </div>
                                    </div>

                                    <div class="clearfix"></div>
                                    <div class="row-info">
                                        <div class="col-md-5 nopad">
                                            <asp:Label runat="server" Text="Last Name : "></asp:Label>
                                        </div>
                                        <div class="col-md-7 nopad">
                                            <asp:Label runat="server" ID="lblLastName"></asp:Label>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="row-info">
                                        <div class="col-md-5 nopad">
                                            <asp:Label runat="server" Text="First Name : "></asp:Label>
                                        </div>
                                        <div class="col-md-7 nopad">
                                            <asp:Label runat="server" ID="lblFirstName"></asp:Label>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="row-info">
                                        <div class="col-md-5 nopad">
                                            <p>Email ID : </p>
                                        </div>
                                        <div class="col-md-7 nopad">
                                            <asp:Label runat="server" ID="lblEmail"></asp:Label>
                                        </div>
                                    </div>

                                    <div class="clearfix"></div>
                                    <div class="row-info">
                                        <div class="col-md-5 nopad">
                                            <label class="mnq_label" style="line-height: 18px!important;">Default Location :</label>
                                        </div>
                                        <div class="col-md-7 nopad">
                                            <asp:Label runat="server" ID="lblLocation"></asp:Label>

                                        </div>
                                    </div>

                                    <div class="clearfix"></div>
                                    <div class="row-info">
                                        <div class="col-md-5 nopad">
                                            <p>Role : </p>
                                        </div>
                                        <div class="col-md-7 nopad">
                                            <asp:Label runat="server" ID="lblRole"></asp:Label>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="row-info">
                                        <div class="col-md-5 nopad">
                                            <p>Start Date : </p>
                                        </div>
                                        <div class="col-md-7 nopad">
                                            <asp:Label runat="server" ID="lblStartDate"></asp:Label>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="row-info">
                                        <div class="col-md-5 nopad">
                                            <p>End Date : </p>
                                        </div>
                                        <div class="col-md-7 nopad">
                                            <asp:Label runat="server" ID="lblEndDate"></asp:Label>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="row-info">
                                        <div class="col-md-5 nopad">
                                            <label class="mnq_label" style="line-height: 18px!important;">Privacy Settings : </label>
                                        </div>
                                        <div class="col-md-7 nopad">
                                            <asp:CheckBox class="mn_inp control3" Enabled="false" runat="server" ID="chkPrivSett" />
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="row-info">
                                        <div class="col-md-5 nopad">
                                            <p style="line-height: 18px!important;">Admin Type :  </p>
                                        </div>
                                        <div class="col-md-7 nopad">
                                            <asp:RadioButtonList runat="server" ID="rdbAdminView" Enabled="false">
                                                <asp:ListItem>Location Admin</asp:ListItem>
                                                <asp:ListItem>Global Admin</asp:ListItem>
                                                <asp:ListItem>None</asp:ListItem>
                                            </asp:RadioButtonList>
                                            <%--<span style="margin-right: -1px; vertical-align: text-bottom;">Location Admin</span>
                                <asp:RadioButton ID="rdbvwLoc" Enabled="false"  GroupName="Admin" runat="server" />
                                <span style="margin-right: 0px; margin-left: 6px; vertical-align: text-bottom;">Super Admin</span>
                                <asp:RadioButton ID="rdbvwSuper" Enabled="false" GroupName="Admin" runat="server" />--%>
                                        </div>
                                    </div>
                                </div>
                            </asp:Panel>
                            <asp:Panel runat="server" ID="addpanl" Visible="false">
                                <span class="failureNotification">
                                    <asp:Literal ID="FailureText" runat="server"></asp:Literal>
                                </span>
                                <asp:ValidationSummary ID="AddUserValidationSummary" runat="server" CssClass="failureNotification" ValidationGroup="AddUserValidationGroup" />
                                <div class="col-md-12 nopad mn_mar_5">
                                    <div class="row-info">
                                        <div class="col-md-5 nopad">
                                            <p>User ID* : </p>
                                        </div>
                                        <div class="col-md-7 nopad">
                                            <asp:TextBox class="form-control1 mn_inp control3" runat="server" ID="txtUserID"></asp:TextBox>
                                        </div>
                                        <%--<asp:RequiredFieldValidator ControlToValidate="txtUserID" runat="server" ID="rfvUserID" SetFocusOnError="true" ErrorMessage="User Id is mandatory." ForeColor="Red"></asp:RequiredFieldValidator>--%>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="row-info">
                                        <div class="col-md-5 nopad">
                                            <p>Last Name* : </p>
                                        </div>
                                        <div class="col-md-7 nopad">
                                            <asp:TextBox class="form-control1 mn_inp control3" runat="server" ID="txtLastName"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="row-info">
                                        <div class="col-md-5 nopad">
                                            <p>First Name* : </p>
                                        </div>
                                        <div class="col-md-7 nopad">
                                            <asp:TextBox class="form-control1 mn_inp control3" runat="server" ID="txtFirstName"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="row-info">
                                        <div class="col-md-5 nopad">
                                            <p>Enter Password* : </p>
                                        </div>
                                        <div class="col-md-7 nopad">
                                            <asp:TextBox class="form-control1 mn_inp control3" runat="server" ID="txtpwd"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="row-info">
                                        <div class="col-md-5 nopad">
                                            <p style="line-height: 18px!important;">Re-enter Password* : </p>
                                        </div>
                                        <div class="col-md-7 nopad">
                                            <asp:TextBox class="form-control1 mn_inp control3" runat="server" ID="txtrepwd" onchange="return validateControls();"></asp:TextBox><asp:Label runat="server" ID="lblpwd" ForeColor="Red"></asp:Label>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="row-info">
                                            <div class="col-md-5 nopad">
                                                <p style="line-height: 18px!important;">Default Location* : </p>
                                            </div>
                                            <div class="col-md-7 nopad">
                                                <asp:DropDownList class="form-control1 mn_inp control3" runat="server" ID="ddlLocation"></asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="row-info">
                                            <div class="col-md-5 nopad">
                                                <label class="mnq_label">Email Id* :  </label>
                                            </div>
                                            <div class="col-md-7 nopad">
                                                <asp:TextBox CssClass="form-control1 mn_inp control3" runat="server" ID="txtEmailId"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="row-info">
                                            <div class="col-md-5 nopad">
                                                <p>Role* :  </p>
                                            </div>
                                            <div class="col-md-7 nopad">
                                                <asp:DropDownList class="form-control1 mn_inp control3" runat="server" ID="ddlRole"></asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="row-info">
                                            <div class="col-md-5 nopad">
                                                <p>Start Date* :  </p>
                                            </div>
                                            <div class="col-md-7 nopad">
                                                <asp:TextBox class="form-control1 mn_inp control3" runat="server" ID="txtStartDate"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="row-info">
                                            <div class="col-md-5 nopad">
                                                <p>End Date : </p>
                                            </div>
                                            <div class="col-md-7 nopad">
                                                <asp:TextBox class="form-control1 mn_inp control3" runat="server" ID="txtEndDate"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="row-info">
                                        <div class="col-md-5 nopad">
                                            <p style="line-height: 18px!important;">Privacy Settings : </p>
                                        </div>
                                        <div class="col-md-7 nopad">
                                            <asp:CheckBox CssClass="mn_inp control3" runat="server" ID="chkPrivacy" />
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="row-info">
                                        <div class="col-md-5 nopad">
                                            <p style="line-height: 18px!important;">Admin Type :  </p>
                                        </div>
                                        <div class="col-md-7 nopad">
                                            <asp:RadioButtonList runat="server" ID="rdbAdmin">
                                                <asp:ListItem>Location Admin</asp:ListItem>
                                                <asp:ListItem Enabled="false">Global Admin</asp:ListItem>
                                                <asp:ListItem Selected="True">None</asp:ListItem>
                                            </asp:RadioButtonList>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <asp:Label runat="server" ID="lblError" ForeColor="Red"></asp:Label>
                                <asp:Button runat="server" ID="btnSave" CssClass="btn-add" Text="Save" OnClick="btnSave_Click" OnClientClick="return validateControls();" />
                            </asp:Panel>


                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>

        </div>
        <div class="clearfix"></div>
    </div>
</asp:Content>
