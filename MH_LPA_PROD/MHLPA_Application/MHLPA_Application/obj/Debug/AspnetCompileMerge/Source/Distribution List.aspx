﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Distribution List.aspx.cs" Inherits="MHLPA_Application.Distribution_List"  MasterPageFile="~/Site.Master"%>

<asp:Content runat="server" ContentPlaceHolderID="HeadContent">
    <script type="text/javascript">
        function addEmail() {
            //debugger
            $("#MainContent_txtError").text("");
            var email = $("#MainContent_txtemail").val();
            if (validateEmail(email)) {
                $("#MainContent_lblError").text("");
                var mails = $("#MainContent_txtList").val();
                if (mails == '' || mails == null)
                    mails = email;
                else
                    mails = mails + "; " + email;
                $("#MainContent_txtList").val(mails);
                $("#MainContent_txtemail").val('');
                return true;
            } else {
                $("#MainContent_lblError").text(email + " is not valid.");
                return false;
            }
           
        }
        function validateEmail(email) {
            var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(email);
        }
        function validateControls() {
            var err_flag = 0;
            if ($("#MainContent_txtemail").val() == '' || $("#MainContent_txtemail").val() == null) {
                if ($("#MainContent_txtList").val() == '' || $("#MainContent_txtList").val() == null) {
                    $("#MainContent_lblError").text("Email is mandatory field.");
                    $("#MainContent_txtemail").css("border-color", "red");
                    return false
                }
                else {
                    $("#MainContent_lblError").text("");
                    $("#MainContent_txtemail").css("border-color", "");
                }
            }
            else {
                addEmail();
            }
            return true;
        }
    </script>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="MainContent">
     <div class="col-md-12">
                            <div class='block-web' style='float: left; width: 100%'>
                                <div class="header">
                                    <div class="crumbs">
                                        <!-- Start : Breadcrumbs -->
                                        <ul id="breadcrumbs" class="breadcrumb">
                                            <li>
                                                <a class="mn_breadcrumb">SetUp</a>
                                            </li>
                                            <li class="current">DistributionList</li>

                                        </ul>
                                    </div>
                                </div>

                            </div>
                        </div>
    <!-- main content start-->
		<div>
			<div>
                <div class="main-page">
                    
			<div class="row">
			<div class="col-md-8">
				<div class="col-md-12 nopad">
            <div class="panel panel-default panel-table">
                    <div class="panel-group tool-tips widget-shadow" id="accordion" role="tablist" aria-multiselectable="true">
					<h4 class="title2"> Distribution List</h4>
					<hr/>
				  <div class="col-md-12 nopad mn_table">
                       <asp:GridView ID="grdDistribution" runat="server" AllowPaging="true" AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="dist_list_id" CssClass="dictionary" Width="100%" HeaderStyle-BackColor="#ff6c52" AlternatingRowStyle-BackColor="#cccccc">

                                        <Columns>
                                           
                                            <asp:TemplateField HeaderText="Distribution Type" HeaderStyle-ForeColor="#04522c" ControlStyle-Width="100%">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnktype" runat="server" Text='<%# Bind("list_type") %>' OnClick="Select" CommandArgument='<%# Bind("dist_list_id") %>'  Width="100%"></asp:LinkButton>
                                                </ItemTemplate> 
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Employees Mail List" HeaderStyle-ForeColor="#04522c" ControlStyle-Width="100%">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnklist" runat="server" Visible="true" Text='<%# Bind("email_list") %>'  Width="100%" OnClick="Select" CommandArgument='<%# Bind("dist_list_id") %>'></asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Notification To Owner" HeaderStyle-ForeColor="#04522c" ControlStyle-Width="100px">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkOwner" runat="server" Enabled="false" Checked='<%#Eval("send_notification_to_owner").ToString() == "Y"  %>'/>
                                                </ItemTemplate>
                                                
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Action" HeaderStyle-ForeColor="#04522c" ControlStyle-Width="50px">
                                                <ItemTemplate>
                                                    <asp:ImageButton runat="server" Width="20px" ImageUrl="images/edit_icon.jpg" ID="imgAction" OnClick="Edit" CommandArgument='<%# Bind("dist_list_id") %>' />                                                
                                                </ItemTemplate> 
                                            </asp:TemplateField>
                                        </Columns>
                                        
                                    </asp:GridView>

				  </div>
				</div>

            </div>
        </div>
				</div>
				<div class="col-md-4">
				<div class="panel-group tool-tips widget-shadow">
				<div class="col-md-12 nopad">
   	</div>
<%--   	<hr/>--%>
        <asp:Panel runat="server" ID="viewpanel" Visible="true">
   	        <div class="col-md-12 nopad">
                                    	<div class="row-info"> 
											<div class="col-md-5 nopad"><p>Distribution Type : </p></div> 
											<div class="col-md-7 nopad"><asp:Label runat="server" ID="lbltype"></asp:Label></div> 
										</div>
                   
										<div class="clearfix"></div>
                   <div class="row-info"> 
											<div class="col-md-5 nopad"><p>Distribution List : </p></div> 
											<div class="col-md-7 nopad"><asp:Label runat="server" ID="lbllist"></asp:Label></div> 
										</div>
                
										<div class="clearfix"></div>
										<div class="row-info">
											<div class="col-md-5 nopad"><label class="mnq_label" style="line-height: 18px!important;">Notification To Owner : </label></div> 
											<div class="col-md-7 nopad"><asp:CheckBox class="mn_inp control3" Enabled="false" runat="server" ID="chkNot" /></div> 
										</div>
                   
                                            
                   
	</div>
</asp:Panel>
                     <asp:Panel runat="server" ID="addpanel" Visible="false">
                                <div class="col-md-12 nopad mn_mar_5">
                                                    <div class="row-info">
                                                        <div class="col-md-5 nopad">
                                                            <p style="line-height:18px!important;">Distribution Type: </p>
                                                        </div>
                                                        <div class="col-md-7 nopad">
                                                            <asp:TextBox runat="server" ID="txtdistType" Enabled="false" ></asp:TextBox>
                                                        </div>
                                                    </div>
                                                   
                                                    <div class="row-info">
                                                        <div class="col-md-5 nopad">
                                                            <p>Distribution List:</p>
                                                        </div>
                                                        <div class="col-md-7 nopad">
                                                            
                                                            <asp:TextBox ID="txtemail" style="margin-top: 10px;" runat="server">

                                                            </asp:TextBox>
                                                            <img width="30px" src="images/Next-icon.png" onclick="addEmail();" />
                                                           
                                                            <asp:TextBox Rows="4" style="width:100%;" TextMode="MultiLine" runat="server" ID="txtList"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="row-info">
                                                        <div class="col-md-5 nopad">
                                                            <p>Notification To Owner: </p>
                                                        </div>
                                                        <div class="col-md-7 nopad">
                                                            <asp:CheckBox runat="server" ID="chkNotOwner" />
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <asp:Label runat="server" ID="lblError" ForeColor="Red"></asp:Label>
                                                    <asp:Button runat="server" ID="btnSave" CssClass="btn-add" Text="Save" OnClick="btnSave_Click" OnClientClick="return validateControls();" />
                                                </div>
                                <div class="clearfix"></div>
                            </asp:Panel>
                    </div>
                    </div>
                </div>
                    </div>
                </div>
            </div>
    
</asp:Content>
