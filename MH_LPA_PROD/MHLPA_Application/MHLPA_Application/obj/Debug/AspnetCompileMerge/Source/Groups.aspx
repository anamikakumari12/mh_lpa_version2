﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Groups.aspx.cs" Inherits="MHLPA_Application.Groups" MasterPageFile="~/Site.Master" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1"  ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript">
        $(document).ready(function () {

        });
    </script>
    <style>
        .mn_margin_none
            {
                padding:0
            }
            .mn_margin_none option
            {
                font-size:13px;
                line-height:24px;
                padding:5px;
            }
    </style>
    </asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
        <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
   </ajaxToolkit:ToolkitScriptManager>
     <div class="col-md-12">
                            <div class='block-web' style='float: left; width: 100%; height:36px;'>
                                <div class="header">
                                    <div class="crumbs">
                                        <!-- Start : Breadcrumbs -->
                                        <ul id="breadcrumbs" class="breadcrumb">
                                            <li>
                                                <a class="mn_breadcrumb">SetUp</a>
                                            </li>
                                            <li class="current">Groups</li>

                                        </ul>
                                    </div>
                                </div>

                            </div>
                        </div>
<div>
<!-- main content start-->
		<div>
			<div>
                <div class="main-page">
                    <div class="clearfix" style="height:10px;"></div>
<div  class="row filter_panel" style="    width: 90%; margin-left: 5%!important;">
                <div>       <div> 
                    <div class="col-md-4 filter_label" style="width:13%"><p>Region</p></div> 
<div class="col-md-4" style="width:20%">
    <asp:DropDownList class="form-control1 mn_inp control3 filter_select_control" AutoPostBack="true" runat="server" ID="ddlRegion" OnSelectedIndexChanged="ddlRegion_SelectedIndexChanged"></asp:DropDownList>
</div> 
                    <div class="col-md-4 filter_label" style="width:13%"><p>Country</p></div> 
<div class="col-md-4" style="width:20%">
    <asp:DropDownList class="form-control1 mn_inp control3 filter_select_control" AutoPostBack="true" runat="server" ID="ddlCountry" OnSelectedIndexChanged="ddlCountry_SelectedIndexChanged"></asp:DropDownList>
</div> 
                                            <div class="col-md-4 filter_label" style="width:14%"><p>Location</p></div> 
<div class="col-md-4" style="width:20%">
    <asp:DropDownList class="form-control1 mn_inp control3 filter_select_control" AutoPostBack="true" runat="server" ID="ddlLocation" OnSelectedIndexChanged="ddlLocation_SelectedIndexChanged"></asp:DropDownList>
</div> </div>
            
                    </div>
</div>
                
    <div class="clearfix" style="height:5px;"></div>   
			<div class="row">
			<div class="col-md-8">
				<div class="col-md-12 nopad">
            <div class="panel panel-default panel-table">
                    <div class="panel-group tool-tips widget-shadow" id="accordion" role="tablist" aria-multiselectable="true">
					<h4 class="title2"> Groups</h4>
					<hr/>
				  <div class="col-md-12 nopad mn_table">
                       <asp:GridView ID="grdGroups" runat="server" AllowPaging="true" AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="group_id" CssClass="dictionary" Width="100%" HeaderStyle-BackColor="#ff6c52" AlternatingRowStyle-BackColor="#cccccc" OnPageIndexChanging="grdGroups_PageIndexChanging">

                                        <Columns>
                                           
                                            <asp:TemplateField HeaderText="Group name" HeaderStyle-ForeColor="#04522c" ControlStyle-Width="100%">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkrole" runat="server" Text='<%# Bind("group_name") %>' OnClick="Select" CommandArgument='<%# Bind("group_id") %>'  Width="100%"></asp:LinkButton>
                                                </ItemTemplate> 
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Employees" HeaderStyle-ForeColor="#04522c" ControlStyle-Width="100%">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkaudits" runat="server" Visible="true" Text='<%# Bind("emp_full_name") %>'  Width="100%" OnClick="Select" CommandArgument='<%# Bind("group_id") %>'></asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Action" HeaderStyle-ForeColor="#04522c" ControlStyle-Width="50px">
                                                <ItemTemplate>
                                                    <asp:ImageButton runat="server" Width="20px" ImageUrl="images/edit_icon.jpg" ID="imgAction" OnClick="Edit" CommandArgument='<%# Bind("group_id") %>' />                                                
                                                </ItemTemplate> 
                                            </asp:TemplateField>
                                        </Columns>
                                        
                                    </asp:GridView>

				  </div>
				</div>

            </div>
        </div>
				</div>
				<div class="col-md-4">
				<div class="panel-group tool-tips widget-shadow">
				<div class="col-md-12 nopad">
            <asp:Button runat="server" CssClass="btn-add" ID="btnAdd" Text="Add" OnClick="btnAdd_Click"/>
   	</div>
   	<hr/>
        <asp:Panel runat="server" ID="viewpanel" Visible="true">
   	        <div class="col-md-12 nopad">
                                    	<div class="row-info"> 
											<div class="col-md-4 nopad"><p>Group Name : </p></div> 
											<div class="col-md-8 nopad"><asp:Label runat="server" ID="lblgroup"></asp:Label></div> 
										</div>
                   
										<div class="clearfix"></div>
                   <div class="row-info"> 
											<div class="col-md-4 nopad"><p>Employees : </p></div> 
											<div class="col-md-8 nopad"><asp:Label runat="server" ID="lblemployee"></asp:Label></div> 
										</div>
                
										<div class="clearfix"></div>
                   
                                            
                   
	</div>
</asp:Panel>
    <asp:Panel runat="server" ID="addpanl" Visible="false">
          	<div class="col-md-12 nopad mn_mar_5">
   	<div class="row-info"> 
											<div class="col-md-4 nopad"><p>Group name* : </p></div> 
											<div class="col-md-8 nopad">
                                                <asp:TextBox runat="server" ID="txtGroup" class="form-control1 mn_inp control3"></asp:TextBox>
                                              
                  	
                                                <asp:Panel ID="pnlGroup" Style="display: none;" runat="server" class="form-control1 mn_inp control4 mn_margin_none">
       <asp:UpdatePanel ID="updGroup" runat="server">
                    <ContentTemplate>
                        <asp:ListBox ID="ddlGroup" runat="server" class="form-control1 mn_inp control4" AutoPostBack="true" OnSelectedIndexChanged="ddlGroup_SelectedIndexChanged">
                        </asp:ListBox>
                    </ContentTemplate>
                    </asp:UpdatePanel>
                    </asp:Panel>

                                       </div> 
										</div>

											
                   <ajaxToolkit:PopupControlExtender ID="PopupControlExtender2" PopupControlID="pnlGroup" TargetControlID="txtGroup"
            Position="Bottom" runat="server">
        </ajaxToolkit:PopupControlExtender>
                                            <div class="clearfix"></div>
                  <div class="row-info"> 
											<div class="col-md-4 nopad"><p>Employees* : </p></div> 
											
										</div>
        <div class="clearfix"></div>
               <div class="row-info"> 
                                            
                                                 <table align="center" style="width:100%;">

<tr>
<td>
<asp:ListBox runat="server" ID="lstSelectEmployees" Height="169px" Width="121px" SelectionMode="Multiple"></asp:ListBox>    

</td>
<td>
<table>
<tr>
<td>
<asp:Button ID="btnAddSelected" runat="server" Text=">" Width="45px" OnClick="btnAddSelected_Click"/>
</td>
</tr>
<tr>
<td>
<asp:Button ID="btnAddAll" runat="server" Text=">>" Width="45px" OnClick="btnAddAll_Click"/>
</td>
</tr>
<tr>
<td>
<asp:Button ID="btnRemoveSelected" runat="server" Text="<" Width="45px" OnClick="btnRemoveSelected_Click" />
</td>
</tr>
<tr>
<td>
<asp:Button ID="btnRemoveAll" runat="server" Text="<<" Width="45px" OnClick="btnRemoveAll_Click"/>
</td>
</tr>
</table>
</td>
<td>
<asp:ListBox ID="lstSelectedEmployees" runat="server" Height="169px" Width="121px"  SelectionMode="Multiple">
</asp:ListBox>
</td>
</tr>
<tr>
<td colspan="3">
<asp:Label ID="lbltxt" runat="server" ForeColor="Red"></asp:Label>
</td>
</tr>
</table>
                                            
                                    
                                           
                                           </div>
                    <asp:Button runat="server" ID="btnSave" CssClass="btn-add" Text="Save" OnClick="btnSave_Click"/>
</asp:Panel>
										
   		
   	</div>
				</div>
				</div>
				</div>
                </div>
				<div class="clearfix"> </div>

			</div>
			<div class="clearfix"> </div>
		
    </div>
    </asp:Content>



