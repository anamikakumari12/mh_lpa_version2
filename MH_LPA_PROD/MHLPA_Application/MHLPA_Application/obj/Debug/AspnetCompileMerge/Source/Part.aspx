﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Part.aspx.cs" Inherits="MHLPA_Application.Part" MasterPageFile="~/Site.Master" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1"  ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript">
        $(document).ready(function () {
            $("#MainContent_txtStartDate").datepicker({
                showOn: "both",
                buttonImageOnly: true,
                buttonText: "",
                changeYear: true,
                changeMonth: true,
                yearRange: "c-20:c+50",
                // minDate: new Date(),
                dateFormat: 'dd-mm-yy',
                //buttonImage: "images/calander_icon.png",
            });
            $("#MainContent_txtEndDate").datepicker({
                showOn: "both",
                buttonImageOnly: true,
                buttonText: "",
                changeYear: true,
                changeMonth: true,
                yearRange: "c-20:c+50",
                // minDate: new Date(),
                dateFormat: 'dd-mm-yy',
                //buttonImage: "images/calander_icon.png",
            });
        });

        function validateControls() {
            //debugger
            var err_flag = 0;
            if ($('#MainContent_ddlRegion').val() == "") {
                $('#MainContent_ddlRegion').css('border-color', 'red');
                err_flag = 1;
            }
            else {
                $('#MainContent_ddlRegion').css('border-color', '');
            }
            if ($('#MainContent_ddlCountry').val() == "") {
                $('#MainContent_ddlCountry').css('border-color', 'red');

                err_flag = 1;
            }
            else {
                $('#MainContent_ddlCountry').css('border-color', '');
            }

            if ($('#MainContent_ddlLocation').val() == "") {
                $('#MainContent_ddlLocation').css('border-color', 'red');

                err_flag = 1;
            }
            else {
                $('#MainContent_ddlLocation').css('border-color', '');
            }
            if ($('#MainContent_ddlLineName').val() == "") {
                $('#MainContent_ddlLineName').css('border-color', 'red');

                err_flag = 1;
            }
            else {
                $('#MainContent_ddlLineName').css('border-color', '');
            }
            if ($('#MainContent_txtPartNumber').val() == "") {
                $('#MainContent_txtPartNumber').css('border-color', 'red');

                err_flag = 1;
            }
            else {
                $('#MainContent_txtPartNumber').css('border-color', '');
            }

           

            if (err_flag == 0) {
                $('#MainContent_lblError').text('');
                return true;

            }
            else {
                $('#MainContent_lblError').text('Please enter all the mandatory fields.');
                return false;
            }
        }
        $(function () {
            $("#dialogwindow").dialog({
                autoOpen: false,
                height: 400,
                width: 500
            });
        });
        //COde to upload the image and show in img tag
        function showpreview(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#imgpreview').css('visibility', 'visible');
                    $('#imgpreview').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
        //Code to open the window on buton click
        function openwindow() {
            $("#dialogwindow").dialog("open");
        }

        function CheckExtension(sender) {
            var validExts = new Array(".xlsx", ".xls");
            var fileExt = sender.value;
            fileExt = fileExt.substring(fileExt.lastIndexOf('.'));
            if (validExts.indexOf(fileExt) < 0) {
                $('#MainContent_lblExtError').text('Invalid file selected, valid files are of ' +
                         validExts.toString() + ' types.');
                $('#MainContent_btnExcel').attr("style", "display:none");
                return false;
            }
            else {
                $('#MainContent_lblExtError').text('');
                $('#MainContent_btnExcel').attr("style", "display:block");
                return true;
            }
        }
    </script>
    <style>
        .mn_margin_none
            {
                padding:0
            }
            .mn_margin_none option
            {
                font-size:13px;
                line-height:24px;
                padding:5px;
            }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <ajaxToolkit:ToolkitScriptManager EnablePageMethods="true" ID="ToolkitScriptManager1" runat="server" EnablePartialRendering="true">
   </ajaxToolkit:ToolkitScriptManager>
    <%--<asp:ScriptManager ID="sc" runat="server" EnablePartialRendering="true"></asp:ScriptManager>--%>
     <div class="col-md-12">
                            <div class='block-web' style='float: left; width: 100%; height:36px;'>
                                <div class="header">
                                    <div class="crumbs">
                                        <!-- Start : Breadcrumbs -->
                                        <ul id="breadcrumbs" class="breadcrumb">
                                            <li>
                                                <a class="mn_breadcrumb">Setup</a>
                                            </li>
                                            <li class="current">Part</li>

                                        </ul>
                                    </div>
                                </div>

                            </div>
                        </div>
    <div>
<!-- main content start-->
		<div>
			<div class="main-page">
			<div class="row">
			<div class="col-md-8">
				<div class="col-md-12 nopad">
            <div class="panel panel-default panel-table">
                   <div class="panel-group tool-tips widget-shadow" id="accordion" role="tablist" aria-multiselectable="true">
					<div class="clearfix"></div>
                       <div class="col-md-12 nopad">
   	                        <div class="row-info"> 
											<div class="col-md-4 nopad"><h4 class="title2"> Part</h4></div> 
											<div class="col-md-8 nopad"></div> 
										</div>
                           </div>
                       
                      <div class="clearfix"></div>
					
					<hr/>
				  <div class="col-md-12 nopad mn_table">
				   <asp:GridView ID="grdLineProduct" OnRowDataBound="grdLineProduct_RowDataBound" runat="server" AllowPaging="true" AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="line_id" CssClass="dictionary" Width="100%" HeaderStyle-BackColor="#ff6c52" AlternatingRowStyle-BackColor="#cccccc" OnPageIndexChanging="grdLineProduct_PageIndexChanging">

                                        <Columns>                                           
                                            <asp:TemplateField HeaderText="Region" HeaderStyle-ForeColor="#04522c" ControlStyle-Width="50px">
                                                <HeaderTemplate>Region
            <asp:DropDownList ID="ddlRegion_grid" runat="server" OnSelectedIndexChanged="ddlRegion_grid_SelectedIndexChanged" AutoPostBack="true" AppendDataBoundItems = "true">
            </asp:DropDownList>
        </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lblRegion" runat="server" Visible="true" OnClick="Select" CommandArgument='<%# Bind("line_id") %>' Text='<%# Bind("region_name") %>'></asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Country" HeaderStyle-ForeColor="#04522c" ControlStyle-Width="50px">
                                               <HeaderTemplate>Country
            <asp:DropDownList ID="ddlCountry_grid"  runat="server" OnSelectedIndexChanged="ddlCountry_grid_SelectedIndexChanged" AutoPostBack="true" AppendDataBoundItems = "true">
            </asp:DropDownList>
        </HeaderTemplate>
                                                 <ItemTemplate>
                                                    <asp:LinkButton ID="lblCountry" runat="server" Visible="true" OnClick="Select" CommandArgument='<%# Bind("line_id") %>' Text='<%# Bind("country_name") %>'></asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Location" HeaderStyle-ForeColor="#04522c" ControlStyle-Width="100px">
                                                <HeaderTemplate>Location
            <asp:DropDownList ID="ddlLocation_grid" style="width: 100px;" runat="server" OnSelectedIndexChanged="ddlLocation_grid_SelectedIndexChanged" AutoPostBack="true" AppendDataBoundItems = "true">
            </asp:DropDownList>
        </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lblLocation" runat="server" Visible="true" OnClick="Select" CommandArgument='<%# Bind("line_id") %>' Text='<%# Bind("location_name") %>'></asp:LinkButton>
                                                </ItemTemplate>
                                                
                                            </asp:TemplateField>
                                            
                                            <asp:TemplateField HeaderText="Line Name/No" HeaderStyle-ForeColor="#04522c" ControlStyle-Width="150px">
                                                <HeaderTemplate>Line Name/No
            <asp:DropDownList ID="ddlLine_grid" style="width: 150px;" runat="server" OnSelectedIndexChanged="ddlLine_grid_SelectedIndexChanged" AutoPostBack="true" AppendDataBoundItems = "true">
            </asp:DropDownList>
        </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkLineName" runat="server" Visible="true" OnClick="Select" CommandArgument='<%# Bind("line_id") %>' Text='<%# Bind("line_name") %>'></asp:LinkButton>
                                                 </ItemTemplate>
                                            </asp:TemplateField>
                                            <%--<asp:TemplateField HeaderText="Line Number" HeaderStyle-ForeColor="#04522c" ControlStyle-Width="100px">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkLineNumber" runat="server" Visible="true" OnClick="Select" CommandArgument='<%# Bind("line_product_rel_id") %>' Text='<%# Bind("line_code") %>'></asp:LinkButton>
                                                 </ItemTemplate>
                                            </asp:TemplateField>--%>
                                            <%--<asp:TemplateField HeaderText="Part Name" HeaderStyle-ForeColor="#04522c" ControlStyle-Width="100px">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkProductName" runat="server" Visible="true" OnClick="Select" CommandArgument='<%# Bind("line_product_rel_id") %>' Text='<%# Bind("product_name") %>'></asp:LinkButton>
                                                 </ItemTemplate>
                                               
                                            </asp:TemplateField>--%>
                                            <asp:TemplateField HeaderText="Product/Part No" HeaderStyle-ForeColor="#04522c" ControlStyle-Width="100px">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkProductNumber" runat="server" Visible="true" OnClick="Select" CommandArgument='<%# Bind("line_id") %>' Text='<%# Bind("product_code") %>'></asp:LinkButton>
                                                 </ItemTemplate>
                                               
                                            </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Action" HeaderStyle-ForeColor="#04522c" ControlStyle-Width="50px">
                                                <ItemTemplate>
                                                    <asp:ImageButton runat="server" Width="20px" ToolTip="Delete" ImageUrl="images/delete_icon.png" ID="imgAction" OnClick="Delete" CommandArgument='<%# Bind("line_id") %>' />                                                
                                                    <asp:ImageButton runat="server" Width="20px" ToolTip="Edit" ImageUrl="images/edit_icon.jpg" ID="imgEdit" OnClick="Edit" CommandArgument='<%# Bind("line_id") %>' />                                                
                                                </ItemTemplate> 
                                            </asp:TemplateField>
                                        </Columns>
                                        
                                    </asp:GridView>

				</div>
                </div>
            </div>
        </div>
				</div>
                
				<div class="col-md-4">
				<div class="panel-group tool-tips widget-shadow">
				<div class="col-md-12 nopad">
            
                    <%-- <div id="dialogwindow" title="Excel Upload">
                         <div>
                    <asp:FileUpload ID="updFile" runat="server" style="width: 90%;" onchange="CheckExtension(this);"/>
                             </div>
                         <div>
                         <asp:Label ID="lblExtError" runat="server" ForeColor="Red"></asp:Label>
                         <asp:Button runat="server" style="display:none;" CssClass="btn-add" ID="btnExcel" Text="Upload" OnClick="btnExcel_Click"/>
                             </div>
                         <div>
                         <asp:Label ID="lblMessage" runat="server">
                            <p style="font-size:.8em;"> * Please upload only excel file like .xls, .xlsx.</p>
                             <p style="font-size:.8em;"> * Column name of the excel sheet will be as "Region", "Country", "Location", "LineName", "PartNumber".</p>
                             <p style="font-size:.8em;"> * All the column datatype should be "General".</p>
                             <p style="font-size:.8em;"> * Only PartNumber column can have multiple part numbers concatenated with ",".</p>
                             <p style="font-size:.8em;"> * The Sheet name should be renamed as "Part_Mass_Upload".</p>
                         </asp:Label>
                             </div>
        
               </div>    --%>
                    <asp:Button runat="server" CssClass="btn-add" ID="btnAdd" Text="Add" OnClick="btnAdd_Click"/>
                    <asp:Button ID="btnShow" CssClass="btn-add" runat="server" Text="Excel Upload" OnClientClick="$('#addDialog').modal(); return false;" />
                   
   	</div>
                 
   	<hr/>
        <asp:Panel runat="server" ID="viewpanel" Visible="true">
   	<div class="col-md-12 nopad">
   	<div class="row-info mn_line_he"> 
           
											<div class="col-md-4 nopad"><p>Region : </p></div> 
											<div class="col-md-8 nopad"><asp:Label ID="lblregion" runat="server"></asp:Label></div> 
										</div>
										<div class="clearfix"></div>
										<div class="row-info">
											<div class="col-md-4 nopad"><p> 	Country : </p></div> 
											<div class="col-md-8 nopad"><asp:Label ID="lblCountry" runat="server"></asp:Label></div> 
										</div>
										<div class="clearfix"></div>
										<div class="row-info">
											<div class="col-md-4 nopad"><p> 	Location : </p></div> 
											<div class="col-md-8 nopad"><asp:Label ID="lblLocation" runat="server"></asp:Label></div> 
										</div>
										
										<div class="clearfix"></div>
           <div class="row-info">
											<div class="col-md-4 nopad"><p> Line Name/No : </p></div> 
											<div class="col-md-8 nopad"><asp:Label ID="lblLineName" runat="server"></asp:Label></div> </div> 
										
                    <div class="clearfix"></div>
          
										<%--<div class="row-info">
											<div class="col-md-4 nopad"><p> 	Part Name : </p></div> 
											<div class="col-md-8 nopad"><asp:Label ID="lblPartName" runat="server"></asp:Label></div> </div> 
										
            
										<div class="clearfix"></div>--%>
										<div class="row-info">
											<div class="col-md-4 nopad"><p> 	Product/Part No : </p></div> 
											<div class="col-md-8 nopad"><asp:Label ID="lblPartNumber" runat="server"></asp:Label></div> </div> 
										
            
										<div class="clearfix"></div>
							</div>			
            </asp:Panel>
                    <asp:Panel runat="server" ID="editpanel" Visible="false">
   	<div class="col-md-12 nopad mn_mar_5">
   	<div class="row-info"> 
											<div class="col-md-4 nopad"><p>Region : </p></div> 
											<div class="col-md-8 nopad"><asp:DropDownList class="form-control1 mn_inp control4" runat="server" ID="ddlRegion" AutoPostBack="true" OnSelectedIndexChanged="ddlRegion_SelectedIndexChanged"></asp:DropDownList>											
											</div> 
										</div>
										<div class="clearfix"></div>
										<div class="row-info">
											<div class="col-md-4 nopad"><p> 	Country : </p></div> 
											<div class="col-md-8 nopad"><asp:DropDownList class="form-control1 mn_inp control4" runat="server" ID="ddlCountry" AutoPostBack="true" OnSelectedIndexChanged="ddlCountry_SelectedIndexChanged"></asp:DropDownList>											
                                             
											</div> 
										</div>
										<div class="clearfix"></div>

										<div class="row-info">
											<div class="col-md-4 nopad"><p> 	Location : </p></div> 
											<div class="col-md-8 nopad"><asp:DropDownList class="form-control1 mn_inp control4" runat="server" ID="ddlLocation" OnSelectedIndexChanged="ddlLocation_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>											
											</div> 
										</div>
										<div class="clearfix"></div>
           <div class="row-info">
											<div class="col-md-4 nopad"><p> Line Name/No: </p></div> 
											<div class="col-md-8 nopad">
                                                <asp:DropDownList class="form-control1 mn_inp control4" runat="server" ID="ddlLineName" AutoPostBack="true"></asp:DropDownList>											

											</div> 
										</div>
           <div class="clearfix"></div>
										<%--<div class="row-info">
											<div class="col-md-4 nopad"><p> 	Part Name : </p></div> 
											<div class="col-md-8 nopad"><asp:TextBox runat="server" ID="txtPartName" class="form-control1 mn_inp control3"></asp:TextBox>
                                               
                                                <asp:Panel ID="pnlPartName" Style="display: none;" runat="server" class="form-control1 mn_inp control4 mn_margin_none">
       <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                    <ContentTemplate>
                        <asp:ListBox ID="ddlPartName" runat="server" class="form-control1 mn_inp control4" AutoPostBack="true" OnSelectedIndexChanged="ddlPartName_SelectedIndexChanged">
                        </asp:ListBox>
                    </ContentTemplate>
                    </asp:UpdatePanel>
                    </asp:Panel>
											</div> 
										</div>
           <ajaxToolkit:PopupControlExtender ID="PopupControlExtender4" PopupControlID="pnlPartName" TargetControlID="txtPartName"
            Position="Bottom" runat="server">
        </ajaxToolkit:PopupControlExtender>
										<div class="clearfix"></div>--%>
										<div class="row-info">
											<div class="col-md-4 nopad"><p> 	Product/Part No : </p></div> 
											<div class="col-md-8 nopad"><asp:TextBox Rows="4" style="width:100%;" TextMode="MultiLine" runat="server" ID="txtPartNumber"></asp:TextBox>
                                               
                                                <%--<asp:Panel ID="pnlPartNumber" Style="display: none;" runat="server" class="form-control1 mn_inp control4 mn_margin_none">
       <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <asp:ListBox ID="ddlPartNumber" runat="server" class="form-control1 mn_inp control4" AutoPostBack="true" OnSelectedIndexChanged="ddlPartNumber_SelectedIndexChanged">
                        </asp:ListBox>
                    </ContentTemplate>
                    </asp:UpdatePanel>
                    </asp:Panel>--%>
											</div> 
										</div>
           <%--<ajaxToolkit:PopupControlExtender ID="PopupControlExtender1" PopupControlID="pnlPartNumber" TargetControlID="txtPartNumber"
            Position="Bottom" runat="server">
        </ajaxToolkit:PopupControlExtender>--%>
										<div class="clearfix"></div>

   	</div>
                        <div class="clearfix"></div>
                        <asp:Label runat="server" ID="lblError" ForeColor="Red"></asp:Label>
                        <div class="clearfix"></div>
                        <%--<asp:Button runat="server" ID="btnDelete" CssClass="btn-add" Text="Delete" OnClick="btnDelete_Click" />--%>
										<asp:Button runat="server" ID="btnSave" CssClass="btn-add" Text="Save" OnClick="btnSave_Click" OnClientClick="return validateControls();"/>
            </asp:Panel>
					
            <div style='clear: both'></div>
            <div id="addDialog" class="modal fade">
                <div class="mn_popup">
                  <%--  <asp:UpdatePanel ID="updModel" runat="server">
                        <ContentTemplate>--%>
                    <div class="modal-content">
                      
                                    <div id="Div1" class="modal-body">
                                        <div class="filter_panel">
                    <div>
                    <asp:FileUpload ID="updFile" runat="server" style="width: 50%;" onchange="CheckExtension(this);"/>
                             
                         <asp:Label ID="lblExtError" runat="server" ForeColor="Red"></asp:Label>
                         <asp:Button runat="server" style="display:none;" CssClass="btn-add" ID="btnExcel" Text="Upload" OnClick="btnExcel_Click"/>
                             </div>
                        


             </div>    
                                        </div>
                        <div class="clearfix"></div>
                        <div class="modal-footer" style="text-align:left;">
                         <asp:Label ID="lblMessage" runat="server">
                            <p style="font-size:.8em;"> * Please upload only excel file like .xls, .xlsx.</p>
                             <p style="font-size:.8em;"> * Column name of the excel sheet will be as "Region", "Country", "Location", "LineName", "PartNumber".</p>
                             <p style="font-size:.8em;"> * All the column datatype should be "General".</p>
                             <p style="font-size:.8em;"> * Only PartNumber column can have multiple part numbers concatenated with ",".</p>
                             <p style="font-size:.8em;"> * The Sheet name should be renamed as "Part_Mass_Upload".</p>
                         </asp:Label>
                             </div>
                        </div>
                           <%-- </ContentTemplate>
                        <Triggers>
                          
                        </Triggers>   
                        </asp:UpdatePanel>--%>
                    </div>
                </div>
   	</div>
				</div>
				</div>
				</div>
				<div class="clearfix"> </div>
            
			</div>
			<div class="clearfix"> </div>
		
</asp:Content>


