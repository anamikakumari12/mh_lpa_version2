﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Role.aspx.cs" Inherits="MHLPA_Application.Role" MasterPageFile="~/Site.Master" %>

<asp:Content ID="Content1"  ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript">
        function validateControls() {
            //debugger
            var err_flag = 0;
            if ($('#MainContent_ddlRole').val() == "") {
                $('#MainContent_ddlRole').css('border-color', 'red');
                err_flag = 1;
            }
            else {
                $('#MainContent_ddlRole').css('border-color', '');
            }
            if ($('#MainContent_txtaudits').val() == "") {
                $('#MainContent_txtaudits').css('border-color', 'red');

                err_flag = 1;
            }
            else {
                $('#MainContent_txtaudits').css('border-color', '');
            }
            if (err_flag == 0) {
                $('#MainContent_lblError').text('');
                return setmin();
               
            }
            else {
                $('#MainContent_lblError').text('Please enter all the mandatory fields.');
                return false;
            }
        }
        function setmin() {
            if ($('#MainContent_txtaudits').val() <= 0) {
                $('#MainContent_txtaudits').val = 1;
                $('#MainContent_lblError').text('Audit number shoul be greater than 0.');
                return false;
            }
            else {
                $('#MainContent_lblError').text('');
                return true;
            }
        }
    </script>
    </asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
     <div>
                            <div class='block-web' style='float: left; width: 100%; height:36px;'>
                                <div class="header">
                                    <div class="crumbs">
                                        <!-- Start : Breadcrumbs -->
                                        <ul id="breadcrumbs" class="breadcrumb">
                                            <li>
                                                <a class="mn_breadcrumb">SetUp</a>
                                            </li>
                                            <li class="current">Role</li>

                                        </ul>
                                    </div>
                                </div>

                            </div>
                        </div>
<div>
<!-- main content start-->
		<div>
			<div class="main-page">
			<div class="row">
			<div class="col-md-8">
				<div class="col-md-12 nopad">
            <div class="panel panel-default panel-table">
                    <div class="panel-group tool-tips widget-shadow" id="accordion" role="tablist" aria-multiselectable="true">
                        <div class="col-md-12 nopad">
					<h4 class="title2"> Role</h4></div>
					<hr/>
				  <div class="col-md-12 nopad mn_table">
                       <asp:GridView ID="grdRoles" runat="server" AllowPaging="true" AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="role_id" CssClass="dictionary" Width="100%" HeaderStyle-BackColor="#ff6c52" AlternatingRowStyle-BackColor="#cccccc" OnPageIndexChanging="grdRoles_PageIndexChanging">

                                        <Columns>
                                           
                                            <asp:TemplateField HeaderText="Role name" HeaderStyle-ForeColor="#04522c" ControlStyle-Width="50px">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkrole" runat="server" Text='<%# Bind("rolename") %>' OnClick="Select" CommandArgument='<%# Bind("role_id") %>'></asp:LinkButton>
                                                </ItemTemplate> 
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Number of audits" HeaderStyle-ForeColor="#04522c" ControlStyle-Width="100px">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkaudits" runat="server" Visible="true" Text='<%# Bind("no_of_audits_required") %>' OnClick="Select" CommandArgument='<%# Bind("role_id") %>'></asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Unit of Measurement" HeaderStyle-ForeColor="#04522c" ControlStyle-Width="100px">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkfrequency" runat="server" Visible="true" Text='<%# Bind("frequency") %>' OnClick="Select" CommandArgument='<%# Bind("role_id") %>'></asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Action" HeaderStyle-ForeColor="#04522c" ControlStyle-Width="50px">
                                                <ItemTemplate>
                                                    <asp:ImageButton runat="server" Width="20px" ToolTip="Edit" ImageUrl="images/edit_icon.jpg" ID="imgAction" OnClick="Edit" CommandArgument='<%# Bind("role_id") %>' />                                                
                                                    <asp:ImageButton runat="server" Width="20px" ToolTip="Delete" ImageUrl="images/delete_icon.png" ID="imgDelete" OnClick="Delete" CommandArgument='<%# Bind("role_id") %>' />                                                
                                                </ItemTemplate> 
                                            </asp:TemplateField>
                                        </Columns>
                                        
                                    </asp:GridView>

				  </div>
				</div>

            </div>
        </div>
				</div>
				<div class="col-md-4">
				<div class="panel-group tool-tips widget-shadow">
				<div class="col-md-12 nopad">
            <asp:Button runat="server" CssClass="btn-add" ID="btnAdd" Text="Add" OnClick="btnAdd_Click"/>
   	</div>
   	<hr/>
        <asp:Panel runat="server" ID="viewpanel" Visible="true">
   	        <div class="col-md-12 nopad">
                                    	<div class="row-info"> 
											<div class="col-md-6 nopad"><p>Role Name: </p></div> 
											<div class="col-md-6 nopad"><asp:Label runat="server" ID="lblrole"></asp:Label></div> 
										</div>
                   
										<div class="clearfix"></div>
                   <div class="row-info"> 
											<div class="col-md-6 nopad"><p>Number of audits: </p></div> 
											<div class="col-md-6 nopad"><asp:Label runat="server" ID="lblaudits"></asp:Label></div> 
										</div>
                
										<div class="clearfix"></div>
                   <div class="row-info"> 
											<div class="col-md-6 nopad"><p>Unit of Measurement: </p></div> 
											<div class="col-md-6 nopad"><asp:Label runat="server" ID="lblmeasurement"></asp:Label></div> 
										</div>
                                            
                   
	</div>
</asp:Panel>
    <asp:Panel runat="server" ID="addpanl" Visible="false">
          	<div class="col-md-12 nopad mn_mar_5">
   	<div class="row-info "> 
											<div class="col-md-5 nopad"><p>Role name* : </p></div> 
											<div class="col-md-7 nopad"><asp:TextBox class="form-control1 mn_inp control3" runat="server" ID="ddlRole"></asp:TextBox>
           <%--<asp:RequiredFieldValidator ControlToValidate="ddlRole" runat="server" ID="rfvRole" SetFocusOnError="true" ErrorMessage="Role is required." ForeColor="Red"></asp:RequiredFieldValidator>--%>
										</div> </div>
                                            <div class="clearfix"></div>
               <div class="row-info"> 
                                            <div class="col-md-5 nopad"><p style="line-height:18px!important;">Number of audits* : </p></div> 
											<div class="col-md-7 nopad"><asp:TextBox class="form-control1 mn_inp control3" runat="server" ID="txtaudits" TextMode="Number" onchange="return validateControls();"></asp:TextBox>
                  <%-- <asp:RequiredFieldValidator ControlToValidate="txtaudits" runat="server" ID="rfvAudits" SetFocusOnError="true" ErrorMessage="Audits is required." ForeColor="Red"></asp:RequiredFieldValidator>--%>
                   <%--<asp:RangeValidator ControlToValidate="txtaudits" runat="server" ID="rfvRangeAudits" SetFocusOnError="true" MinimumValue="1" ErrorMessage="Minimum number of audits should be 1." ForeColor="Red"></asp:RangeValidator>--%>
										</div> </div>
                                            <div class="clearfix"></div>
        <div class="row-info"> 
                                            <div class="col-md-5 nopad"><p style="line-height:18px!important;">Unit of Measurement* : </p></div> 
											<div class="col-md-7 nopad"><asp:DropDownList class="form-control1 mn_inp control3" runat="server" ID="ddlfrequency">
                                                <asp:ListItem Text="Per Shift" Value="Per Shift"></asp:ListItem>
                                                <asp:ListItem Text="Per Day" Value="Per Day"></asp:ListItem>
                                                <asp:ListItem Text="Per Week" Value="Per Week"></asp:ListItem>
                                                <asp:ListItem Text="Per Month" Value="Per Month"></asp:ListItem>
                                                <asp:ListItem Text="Per Year" Value="Per Year"></asp:ListItem>
											                            </asp:DropDownList>

											</div> 
										</div>
                                           </div>
        <asp:Label runat="server" ID="lblError" ForeColor="Red"></asp:Label>
                    <asp:Button runat="server" ID="btnSave" CssClass="btn-add" Text="Save" OnClick="btnSave_Click" OnClientClick="return validateControls();"/>
</asp:Panel>
										
   		
   	</div>
				</div>
				</div>
				</div>
				<div class="clearfix"> </div>

			</div>
			<div class="clearfix"> </div>
		</div>
    </asp:Content>
