﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Line.aspx.cs" Inherits="MHLPA_Application.Line" MasterPageFile="~/Site.Master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script src="js/chosen.jquery.js" type="text/javascript"></script>
    <link href="css/chosen.css" rel="stylesheet" />
    <script src="js/jquery-json-to-datalist.js"></script>
    <a href="js/sample.json">js/sample.json</a>
    <script type="text/javascript">
        $(document).ready(function () {

            BindControls();
        });
        function BindControls() {
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "Line.aspx/GetDistributionEmails",
                data: "{'Name':''}",
                dataType: "json",
                success: function (data) {
                    console.log(data);
                    console.log(data.d);
                    var Countries = data.d;
                //        ['ARGENTINA',
                //'AUSTRALIA',
                //'BRAZIL',
                //'BELARUS',
                //'BHUTAN',
                //'CHILE',
                //'CAMBODIA',
                //'CANADA',
                //'CHILE',
                //'DENMARK',
                //'DOMINICA'];

                    $('#txtdistemail').autocomplete({
                        source: function (request, response) {
                            var matches = $.map(Countries, function (acItem) {
                                if (acItem.toUpperCase().indexOf(request.term.toUpperCase()) === 0) {
                                    return acItem;
                                }
                            });
                            response(matches);
                        },
                        minLength: 0,
                        scroll: true,
                        autoFocus: true
                    }).focus(function () {
                        $(this).autocomplete("search", "");
                    });
                },
                error: function (result) {
                    console.log("No Match");
                }
            });

            
        }
       
        function PrintPanel() {
            //debugger
            var panel = document.getElementById("<%=pnlQRPrint.ClientID%>");
            panel.style.visibility = "visible";
            PageMethods.SetDownloadPath("yes");
            window.print();

        }
        function addEmail() {
            debugger;
            //if ($('#MainContent_DropDownListdataplaceholder1').val() == "" || $('#MainContent_DropDownListdataplaceholder1').val() == null || $('#MainContent_DropDownListdataplaceholder1').val() == "0") {
            //}
            //else {
            //    var mails1 = $("#MainContent_txtList").val();
            //    if (mails1 == '' || mails == null)
            //        mails1 = $('#MainContent_DropDownListdataplaceholder1').val();
            //    else
            //        mails1 = mails + "; " + email;
            //    $("#MainContent_txtList").val(mails1);
            //}
            //$("#MainContent_txtError").text("");
            //var email = $("#MainContent_txtemail").val();
            
            //if (validateEmail(email)) {
            //    $("#MainContent_lblError").text("");
            //    var mails = $("#MainContent_txtList").val();
            //    if (mails == '' || mails == null)
            //        mails = email;
            //    else
            //        mails = mails + "; " + email;
            //    $("#MainContent_txtList").val(mails);
            //    $("#MainContent_txtemail").val('');
            //    return true;
            //} else {
            //    $("#MainContent_lblError").text(email + " is not valid.");
            //    return false;
            //}

            $("#MainContent_txtError").text("");
            var email = document.getElementById('txtdistemail').value;

            if (validateEmail(email)) {
                $("#MainContent_lblError").text("");
                var mails = $("#MainContent_txtList").val();
                if (mails == '' || mails == null)
                    mails = email;
                else
                    mails = mails + "; " + email;
                $("#MainContent_txtList").val(mails);
                document.getElementById('txtdistemail').value = "";
                return true;
            } else {
                $("#MainContent_lblError").text(email + " is not valid.");
                return false;
            }
            
        }
        function validateEmail(email) {
            var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(email);
        }
        function validateControls() {
            debugger;
            var err_flag = 0;
            if ($('#MainContent_ddlRegion').val() == "" || $('#MainContent_ddlRegion').val() == null || $('#MainContent_ddlRegion').val() == "0") {
                $('#MainContent_ddlRegion').css('border-color', 'red');
                err_flag = 1;
            }
            else {
                $('#MainContent_ddlRegion').css('border-color', '');
            }
            if ($('#MainContent_ddlCountry').val() == "" || $('#MainContent_ddlCountry').val() == null || $('#MainContent_ddlCountry').val() == "0") {
                $('#MainContent_ddlCountry').css('border-color', 'red');

                err_flag = 1;
            }
            else {
                $('#MainContent_ddlCountry').css('border-color', '');
            }

            if ($('#MainContent_ddlLocation').val() == "" || $('#MainContent_ddlLocation').val() == null || $('#MainContent_ddlLocation').val() == "0") {
                $('#MainContent_ddlLocation').css('border-color', 'red');

                err_flag = 1;
            }
            else {
                $('#MainContent_ddlLocation').css('border-color', '');
            }

            if ($('#MainContent_txtLineName').val() == "") {
                $('#MainContent_txtLineName').css('border-color', 'red');

                err_flag = 1;
            }
            else {
                $('#MainContent_txtLineName').css('border-color', '');
            }
            if (document.getElementById('txtdistemail').value == '' || document.getElementById('txtdistemail').value == null ) {
                if ($("#MainContent_txtList").val() == '' || $("#MainContent_txtList").val() == null) {
                    document.getElementById('txtdistemail').style["border-color"]= "red";
                    err_flag = 1;
                }
                else {
                    $("#MainContent_lblError").text("");
                    document.getElementById('txtdistemail').style["border-color"]="";
                }
            }
            else {
                addEmail();
            }
            if (err_flag == 0) {
                $('#MainContent_lblError').text('');
                return true;

            }
            else {
                $('#MainContent_lblError').text('Please enter all the mandatory fields.');
                return false;
            }
        }

      
    </script>
    <style>
         .ui-autocomplete { 
            cursor:pointer; 
            height:120px; 
            overflow-y:scroll;
        } 
        
 .clearfix:after  
     {  
 content: "\0020";  
 display: block;  
 height: 0;  
 clear: both;  
 overflow: hidden;  
 visibility: hidden;  
     } 
        @media print
        {
            .col-md-8
            {
                visibility: hidden;
            }

            .col-md-4
            {
                visibility: hidden;
            }

            .btn-add
            {
                visibility: hidden;
            }

            .col-md-12
            {
                visibility: hidden;
            }

            hr
            {
                visibility: hidden;
            }

            divPrint
            {
                visibility: visible;
                height: 100%;
            }
        }

        .mn_margin_none
        {
            padding: 0;
        }

            .mn_margin_none option
            {
                font-size: 13px;
                line-height: 24px;
                padding: 5px;
            }

        .mn_vertical
        {
            float: left;
            width: 200px;
            border: solid 1px #333;
        }

           /*.feedback-title
        {
         -webkit-transform: rotate(-90deg);
            -moz-transform: rotate(-90deg);

            -ms-transform: rotate(-90deg);
            -o-transform: rotate(-90deg);
            position: absolute;
            top: 440px;

        }*/

        .mn_vertical .feedback-list
        {
            float: right;
        }

        .feedback-list p
        {
            text-align: center;
        }
        .mn_vertical {
            float: left;
            width: 100%;
            border: none;
        }

        .feedback-title {
            /*-webkit-transform: rotate(-90deg);
                -moz-transform: rotate(-90deg);
                -ms-transform: rotate(-90deg);
                -o-transform: rotate(-90deg);
                transform: rotate(-90deg);
                position: absolute;
                margin: 0;
            width: 146px;
            height: 100px;
            font-size: 10px;*/
            margin: 20px auto;
            width: 150px;
            height: 30px;
            display: block;
            font-size: 10px;
            text-align: center;
        }

        .mn_vertical .feedback-list {
            float: left;
        }

        .mn_feedback {
            width: 100%;
            float: left;
            padding: 25px 50px;
        }

        .feedback-list p {
            text-align: center;
        }

        .colQR1 {
            width: 50%;
            float: left;
            padding: 15px;
            border-style: groove;
        }

        .mn_location_span {
            display: block;
            text-align: center;
            margin: 0 0 10px 0;
        }

        .colQR1 img {
            margin: 10px auto;
            display: block;
        }

        .qr-title {
            margin: 0 auto;
            display: block;
            text-align: center;
            font-size: 16px;
            font-weight: bold;
        }

        .qr-title1 {
            margin: 0 auto;
            display: block;
            text-align: center;
            font-size: 16px;
            font-weight: bold;
        }


        .mn_vertical {
            float: left;
            width: 100%;
            border: none;
        }

        .feedback-title {
            /*-webkit-transform: rotate(-90deg);
            -moz-transform: rotate(-90deg);
            -ms-transform: rotate(-90deg);
            -o-transform: rotate(-90deg);
            transform: rotate(-90deg);
            position: absolute;
            margin: 0;
            width: 146px;
            height: 100px;
            font-size: 10px;*/
            margin: 20px auto 0;
            width: 150px;
            height: 30px;
            font-size: 10px;
            display: block;
            text-align: center;
        }

        .mn_vertical .feedback-list {
            float: left;
            width: 100%;
        }

        .mn_feedback {
            width: 100%;
            float: left;
            padding: 25px 50px;
        }

        .feedback-list p {
            text-align: center;
        }

        .colQR1 {
            width: 50%;
            float: left;
            padding: 15px;
            border-style: groove;
        }

        .mn_location_span {
            display: block;
            text-align: center;
            margin: 0 0 10px 0;
        }

        .colQR1 img {
            margin: 0px auto;
            display: block;
        }

        .qr-title {
            margin: 0 auto;
            display: block;
            text-align: center;
            font-size: 16px;
            font-weight: bold;
        }

        .qr-title1 {
            margin: 0 auto;
            display: block;
            text-align: center;
            font-size: 16px;
            font-weight: bold;
        }

    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <ajaxToolkit:ToolkitScriptManager EnablePageMethods="true" ID="ToolkitScriptManager1" runat="server" EnablePartialRendering="true">
    </ajaxToolkit:ToolkitScriptManager>
    <%--<asp:ScriptManager ID="sc" runat="server" EnablePartialRendering="true"></asp:ScriptManager>--%>
    <div class="col-md-12">
        <div class='block-web' style='float: left; width: 100%; height: 36px;'>
            <div class="header">
                <div class="crumbs">
                    <!-- Start : Breadcrumbs -->
                    <ul id="breadcrumbs" class="breadcrumb">
                        <li>
                            <a class="mn_breadcrumb">Setup</a>
                        </li>
                        <li class="current">Line</li>

                    </ul>
                </div>
            </div>

        </div>
    </div>
    <div>
        <!-- main content start-->
        <div>
            <div class="divPrint" style="height: 1px;">
                <asp:Panel ID="pnlQRPrint" Visible="true" runat="server" Style="visibility: hidden;">
                    <div class="divPrintBulkQR" id="divPrintBulkQR">
                <asp:Table runat="server" Style="width: 100%" ID="tblPrintBulkQR">
                    <asp:TableRow>
                        <asp:TableCell>
                            <div class="mn_vertical">
                                <div class="mn_feedback">
                                    <div class="feedback-list">
                                        <asp:Panel ID="pnlprintQRCode" runat="server">
                                        </asp:Panel>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                            </div>

                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>

            </div>
                  <%--  <table border="0" style="height: 100%; width: 100%; border: solid; border-width: thin;">
                        <tbody>
                            <tr>
                                <td style="width: 30%">
                                    <p style="margin-left: 50px; font-size: x-large; font-weight: 200">Region : </p>
                                </td>
                                <td>
                                    <asp:Label ID="lnlprintregion" runat="server" Style="font-size: x-large"></asp:Label></td>
                            </tr>
                            <tr>
                                <td style="width: 30%">
                                    <p style="margin-left: 50px; font-size: x-large; font-weight: 200">Country : </p>
                                </td>
                                <td>
                                    <asp:Label ID="lblprintCountry" runat="server" Style="font-size: x-large"></asp:Label></td>
                            </tr>

                            <tr>
                                <td style="width: 30%">
                                    <p style="margin-left: 50px; font-size: x-large; font-weight: 200">Location : </p>
                                </td>
                                <td>
                                    <asp:Label ID="lblprintLocation" runat="server" Style="font-size: x-large"></asp:Label></td>
                            </tr>
                            <tr>
                                <td style="width: 30%">
                                    <p style="margin-left: 50px; font-size: x-large; font-weight: 200">Line Name/No:</p>
                                </td>
                                <td>
                                    <asp:Label ID="lblPrintLineName" runat="server" Style="font-size: x-large"></asp:Label></td>
                            </tr>
                            <tr>
                                <td style="width: 30%">
                                    <p style="margin-left: 50px; font-size: x-large; font-weight: 200">Distribution List:</p>
                                </td>
                                <td>
                                    <asp:Label ID="lblPrintMailList" runat="server" Style="font-size: x-large"></asp:Label></td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <p style="margin-left: 50px; margin-top: 0; font-size: x-large; font-weight: 200">QR Code : </p>
                                </td>
                            </tr>

                            <tr>
                                <td colspan="2">
                                    <asp:Panel HorizontalAlign="Center" runat="server" ID="pnlprintQRCode" Style="width: 500px; height: 500px;">
                                    </asp:Panel>
                                </td>
                            </tr>
                        </tbody>
                    </table>--%>
                </asp:Panel>
            </div>
           
            <div class="main-page">
                <div class="row">
                    <div class="col-md-8">
                        <div class="col-md-12 nopad">
                            <div class="panel panel-default panel-table">
                                <div class="panel-group tool-tips widget-shadow" id="accordion" role="tablist" aria-multiselectable="true">
                                    <div class="clearfix"></div>
                                    <div class="col-md-12 nopad">
                                        <div class="row-info">
                                            <div class="col-md-4 nopad">
                                                <h4 class="title2">Line</h4>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="clearfix"></div>

                                    <hr />
                                    <div class="col-md-12 nopad mn_table">
                                        <asp:GridView OnRowDataBound="grdLineProduct_RowDataBound" ID="grdLineProduct" runat="server" AllowPaging="true" AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="line_id" CssClass="dictionary" Width="100%" HeaderStyle-BackColor="#ff6c52" AlternatingRowStyle-BackColor="#cccccc" OnPageIndexChanging="grdLineProduct_PageIndexChanging">

                                            <Columns>
                                                <asp:TemplateField HeaderText="Region" HeaderStyle-ForeColor="#04522c" ControlStyle-Width="100px">
                                                    <HeaderTemplate>
                                                        Region
            <asp:DropDownList ID="ddlRegion_grid" Style="width: 100px;" runat="server" OnSelectedIndexChanged="ddlRegion_grid_SelectedIndexChanged" AutoPostBack="true" AppendDataBoundItems="true">
            </asp:DropDownList>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lblRegion" runat="server" Visible="true" OnClick="Select" CommandArgument='<%# Bind("line_id") %>' Text='<%# Bind("region_name") %>'></asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Country" HeaderStyle-ForeColor="#04522c" ControlStyle-Width="100px">
                                                    <HeaderTemplate>
                                                        Country
            <asp:DropDownList ID="ddlCountry_grid" Style="width: 100px;" runat="server" OnSelectedIndexChanged="ddlCountry_grid_SelectedIndexChanged" AutoPostBack="true" AppendDataBoundItems="true">
            </asp:DropDownList>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lblCountry" runat="server" Visible="true" OnClick="Select" CommandArgument='<%# Bind("line_id") %>' Text='<%# Bind("country_name") %>'></asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Location" HeaderStyle-ForeColor="#04522c" ControlStyle-Width="100px">
                                                    <HeaderTemplate>
                                                        Location
            <asp:DropDownList ID="ddlLocation_grid" Style="width: 150px;" runat="server" OnSelectedIndexChanged="ddlLocation_grid_SelectedIndexChanged" AutoPostBack="true" AppendDataBoundItems="true">
            </asp:DropDownList>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lblLocation" runat="server" Visible="true" OnClick="Select" CommandArgument='<%# Bind("line_id") %>' Text='<%# Bind("location_name") %>'></asp:LinkButton>
                                                    </ItemTemplate>

                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Line Name/No" HeaderStyle-ForeColor="#04522c" ControlStyle-Width="100px">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lnkLineName" runat="server" Visible="true" OnClick="Select" CommandArgument='<%# Bind("line_id") %>' Text='<%# Bind("line_name") %>'></asp:LinkButton>
                                                    </ItemTemplate>

                                                </asp:TemplateField>
                                                <%--<asp:TemplateField HeaderText="Line Number" HeaderStyle-ForeColor="#04522c" ControlStyle-Width="100px">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkLineNumber" runat="server" Visible="true" OnClick="Select" CommandArgument='<%# Bind("line_id") %>' Text='<%# Bind("line_code") %>'></asp:LinkButton>
                                                 </ItemTemplate>
                                            </asp:TemplateField>--%>
                                                <asp:TemplateField HeaderText="Action" HeaderStyle-ForeColor="#04522c" ControlStyle-Width="50px">
                                                    <ItemTemplate>
                                                        <asp:ImageButton runat="server" ToolTip="Edit" Width="20px" ImageUrl="images/edit_icon.jpg" ID="imgAction" OnClick="Edit" CommandArgument='<%# Bind("line_id") %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>

                                        </asp:GridView>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="panel-group tool-tips widget-shadow">
                            <div class="col-md-12 nopad">
                                <asp:Button runat="server" CssClass="btn-add" ID="btnPrint" Text="Print QRCode" OnClientClick="PrintPanel();" />
                                <asp:Button runat="server" CssClass="btn-add" ID="btnAdd" Text="Add" OnClick="btnAdd_Click" />
                            </div>
                            <hr />
                            <asp:Panel runat="server" ID="viewpanel" Visible="true">
                                <div class="col-md-12 nopad">
                                    <div class="row-info mn_line_he">

                                        <div class="col-md-4 nopad">
                                            <p>Region : </p>
                                        </div>
                                        <div class="col-md-8 nopad">
                                            <asp:Label ID="lblregion" runat="server"></asp:Label></div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="row-info">
                                        <div class="col-md-4 nopad">
                                            <p>Country : </p>
                                        </div>
                                        <div class="col-md-8 nopad">
                                            <asp:Label ID="lblCountry" runat="server"></asp:Label></div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="row-info">
                                        <div class="col-md-4 nopad">
                                            <p>Location : </p>
                                        </div>
                                        <div class="col-md-8 nopad">
                                            <asp:Label ID="lblLocation" runat="server"></asp:Label></div>
                                    </div>

                                    <div class="clearfix"></div>
                                    <div class="row-info">
                                        <div class="col-md-4 nopad">
                                            <p>Line Name/No : </p>
                                        </div>
                                        <div class="col-md-8 nopad">
                                            <asp:Label ID="lblLineName" runat="server"></asp:Label></div>
                                    </div>


                                    <div class="clearfix"></div>
                                    <%--<div class="row-info">
											<div class="col-md-4 nopad"><p> Line Number : </p></div> 
											<div class="col-md-8 nopad"><asp:Label ID="lblLineNumber" runat="server"></asp:Label></div> </div> 
										
                    <div class="clearfix"></div>--%>
                                    <div class="row-info">
                                        <div class="col-md-4 nopad">
                                            <p>Distribution List : </p>
                                        </div>
                                        <div class="col-md-8 nopad">
                                            <asp:Label ID="lblMailList" runat="server"></asp:Label></div>
                                    </div>

                                    <div class="clearfix"></div>

                                    <div class="row-info">
                                        <p>QR Code :</p>
                                        <asp:Panel ID="pnlQRCode" runat="server">
                                        </asp:Panel>
                                    </div>
                                </div>
                            </asp:Panel>
                            <asp:Panel runat="server" ID="editpanel" Visible="false">
                                <div class="col-md-12 nopad mn_mar_5">
                                    <div class="row-info">
                                        <div class="col-md-4 nopad">
                                            <p>Region : </p>
                                        </div>
                                        <div class="col-md-8 nopad">
                                            <asp:DropDownList class="form-control1 mn_inp control4" runat="server" ID="ddlRegion" AutoPostBack="true" OnSelectedIndexChanged="ddlRegion_SelectedIndexChanged"></asp:DropDownList>

                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="row-info">
                                        <div class="col-md-4 nopad">
                                            <p>Country : </p>
                                        </div>
                                        <div class="col-md-8 nopad">
                                            <asp:DropDownList class="form-control1 mn_inp control4" runat="server" ID="ddlCountry" AutoPostBack="true" OnSelectedIndexChanged="ddlCountry_SelectedIndexChanged"></asp:DropDownList>

                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="row-info">
                                        <div class="col-md-4 nopad">
                                            <p>Location : </p>
                                        </div>
                                        <div class="col-md-8 nopad">
                                            <asp:DropDownList class="form-control1 mn_inp control4" runat="server" ID="ddlLocation" AutoPostBack="true" OnSelectedIndexChanged="ddlLocation_SelectedIndexChanged"></asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="row-info">
                                        <div class="col-md-4 nopad">
                                            <p>Line Name/No : </p>
                                        </div>
                                        <div class="col-md-8 nopad">
                                            <asp:TextBox runat="server" ID="txtLineName" class="form-control1 mn_inp control3"></asp:TextBox>

                                            <%--  <asp:Panel ID="pnlLineName" Style="display: none;" runat="server" class="form-control1 mn_inp control4 mn_margin_none">
       <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <asp:ListBox ID="ddlLineName" runat="server" class="form-control1 mn_inp control4" AutoPostBack="true" OnSelectedIndexChanged="ddlLineName_SelectedIndexChanged">
                        </asp:ListBox>
                    </ContentTemplate>
                    </asp:UpdatePanel>
                    </asp:Panel>--%>
                                        </div>
                                    </div>
                                    <%-- <ajaxToolkit:PopupControlExtender ID="PopupControlExtender5" PopupControlID="pnlLineName" TargetControlID="txtLineName"
            Position="Bottom" runat="server">
        </ajaxToolkit:PopupControlExtender>--%>
                                    <div class="clearfix"></div>
                                    <%--<div class="row-info">
											<div class="col-md-4 nopad"><p> Line Number : </p></div> 
											<div class="col-md-8 nopad"><asp:TextBox runat="server" ID="txtLineNumber" class="form-control1 mn_inp control3"></asp:TextBox>
                                               
                                                <asp:Panel ID="pnlLineNumber" Style="display: none;" runat="server" class="form-control1 mn_inp control4 mn_margin_none">
       <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                    <ContentTemplate>
                        <asp:ListBox ID="ddlLineNumber" runat="server" class="form-control1 mn_inp control4" AutoPostBack="true" OnSelectedIndexChanged="ddlLineNumber_SelectedIndexChanged">
                        </asp:ListBox>
                    </ContentTemplate>
                    </asp:UpdatePanel>
                    </asp:Panel>
                                               

											</div> 
										</div>
           <ajaxToolkit:PopupControlExtender ID="PopupControlExtender1" PopupControlID="pnlLineNumber" TargetControlID="txtLineNumber"
            Position="Bottom" runat="server">
        </ajaxToolkit:PopupControlExtender>--%>
                                    
                                    <div class="row-info">
                                        <div class="col-md-4 nopad">
                                            <p>Distribution List:</p>
                                        </div>
                                        <div class="col-md-8 nopad">
                                             <input type="text" id="txtdistemail" />
                                            <div > 
                                                <%--<span style="font-size: 12px;">Select an emails...</span>
                                          <asp:DropDownList style="margin:0!important;" runat="server" ID="DropDownListdataplaceholder1"  class="form-control1 mn_inp control3 chzn-select">  
 </asp:DropDownList>  
                                                <span style="font-size: 12px;">Add new email id here...</span>
                                            <asp:TextBox AutoCompleteType="Email" ToolTip="Add new email id here..." style="margin:0!important;" ID="txtemail" runat="server" >

                                            </asp:TextBox>--%>
                                               
                                      <%--      <asp:Panel Visible="false" ID="pnlEmail" Style="display: none;" runat="server" class="form-control1 mn_inp control4 mn_margin_none">
                                                <asp:UpdatePanel ID="updEmail" runat="server">
                                                    <ContentTemplate>
                                                        <asp:ListBox  ID="lstEmail" runat="server" class="form-control1 mn_inp control4" AutoPostBack="true" OnSelectedIndexChanged="lstEmail_SelectedIndexChanged"></asp:ListBox>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </asp:Panel>--%>
                                            <img width="30px" src="images/Next-icon.png" onclick="addEmail();" />

                                            <asp:TextBox Rows="4" Style="width: 100%;" TextMode="MultiLine" runat="server" ID="txtList" onkeydown="$this.keydown(function(e) {if(e.keyCode !== 8) {e.preventDefault();}});"></asp:TextBox>
                                      </div>  </div>
                                    </div>
                                    <%--<ajaxToolkit:PopupControlExtender ID="PopupControlExtender1" PopupControlID="pnlEmail" TargetControlID="txtemail" 
                                     
                                        Position="Bottom" runat="server">
                                    </ajaxToolkit:PopupControlExtender>--%>
                                    <div class="clearfix"></div>
                                    <asp:Label runat="server" ID="lblError" ForeColor="Red"></asp:Label>
                                    <asp:Button runat="server" ID="btnDelete" CssClass="btn-add" Text="Delete" Visible="false" OnClick="btnDelete_Click" />
                                    <asp:Button runat="server" ID="btnSave" CssClass="btn-add" Text="Save" OnClick="btnSave_Click" OnClientClick="return validateControls();" />
                            </asp:Panel>


                        </div>
                    </div>

                     
                </div>
               
            </div>
             
            <div class="clearfix"></div>

        </div>
        <div class="clearfix"></div>
    </div>
</asp:Content>


