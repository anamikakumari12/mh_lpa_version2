﻿USE [mhlpa_db]
GO

/****** Object:  Table [dbo].[mh_lpa_Question_language_master]    Script Date: 12/4/2019 2:10:25 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
DROP TABLE mh_lpa_how_to_check_language_master

CREATE TABLE [dbo].[mh_lpa_how_to_check_language_master](
	[Question_id] [int] NULL,
	[English] [nvarchar](MAX) NULL,
	[German] [nvarchar](MAX) NULL,
	[French] [nvarchar](MAX) NULL,
	[Spanish] [nvarchar](MAX) NULL,
	[czech] [nvarchar](MAX) NULL,
	[Chinese] [nvarchar](MAX) NULL,
	[Korean] [nvarchar](MAX) NULL,
	[Thai] [nvarchar](MAX) NULL,
	[Bosnian] [nvarchar](MAX) NULL,
	[Portuguese] [nvarchar](MAX) NULL
) ON [PRIMARY]

GO

INSERT INTO mh_lpa_how_to_check_language_master(Question_id)
VALUES(1),(2),
(3),
(4),
(5),
(6),
(7),
(8),
(9),
(10),
(11),
(12),
(13),
(14),
(15),
(16),
(17),
(18),
(19),
(20),
(21),
(22),
(23),
(24),
(25)
GO

Update mh_lpa_how_to_check_language_master SET English=N'Look at previous Audit form and check if findings are closed' WHERE question_id=1
Update mh_lpa_how_to_check_language_master SET English=N'Compare both documents. Sample check ' WHERE question_id=2
Update mh_lpa_how_to_check_language_master SET English=N'Ask for the operators names and check Qualification Matrix and Validation Sheet for this operators. Minium one O-Level operator should be in place for the training.' WHERE question_id=3
Update mh_lpa_how_to_check_language_master SET English=N'Look at visual work instruction. Check which quality checks are in place. Ask operators what they need to check and which critical topic need to be checked.' WHERE question_id=4
Update mh_lpa_how_to_check_language_master SET English=N'Inspect the area, the machines and equipment' WHERE question_id=5
Update mh_lpa_how_to_check_language_master SET English=N'Inspect the area' WHERE question_id=6
Update mh_lpa_how_to_check_language_master SET English=N'Inspect the material supply, how are parts and components provided to the line.' WHERE question_id=7
Update mh_lpa_how_to_check_language_master SET English=N'Look at the material supply  for each material. How is the replenishment of the parts?' WHERE question_id=8
Update mh_lpa_how_to_check_language_master SET English=N'Inspect the area' WHERE question_id=9
Update mh_lpa_how_to_check_language_master SET English=N'look at the visual work instruction, which safety equipment is necessary and ask which are the general safety rules for the site. Check if the operators are following this rules. Inspect the whole line for any safety risks. Look in the TPM plan if the safety equipment is checked on a regular bases. When was the last time?' WHERE question_id=10
Update mh_lpa_how_to_check_language_master SET English=N'Compare TPM instruction and look at the check sheet if it was done. Sample checks if the tasks are really done (e.g. cleaning)' WHERE question_id=11
Update mh_lpa_how_to_check_language_master SET English=N'Look in the startup instruction if the Poka Yokes were checked. Sample check with a master part.' WHERE question_id=12
Update mh_lpa_how_to_check_language_master SET English=N'Check Startup checklist posted at the line. Check topic which were not OK and how the reaction was on this.' WHERE question_id=13
Update mh_lpa_how_to_check_language_master SET English=N'Look at the startup instruction form and compare with first piece. Look at the label of the first piece.' WHERE question_id=14
Update mh_lpa_how_to_check_language_master SET English=N'Work chart posted at the line, visual work instruction posted in the view of the operator. Compare work chart, work instruction with the real situation' WHERE question_id=15
Update mh_lpa_how_to_check_language_master SET English=N'look at the line for waste (waiting, buffers etc.). Compare work chart and real situation. Measure the time and the out put of the line.' WHERE question_id=16
Update mh_lpa_how_to_check_language_master SET English=N'Check startup instruction, what are the reactions? Ask people what to do in case of bad parts or break down?' WHERE question_id=17
Update mh_lpa_how_to_check_language_master SET English=N'Compare the visual work instruction with the boundary samples.' WHERE question_id=18
Update mh_lpa_how_to_check_language_master SET English=N'Look at the area where the parts are packed look how parts are packed and how they should be packed according to the instruction.' WHERE question_id=19
Update mh_lpa_how_to_check_language_master SET English=N'Look at the workplaces, ask the people what they are doing in case of a bad part.' WHERE question_id=20
Update mh_lpa_how_to_check_language_master SET English=N'Look at rework area. Check work instruction and ask people how the material flow works.' WHERE question_id=21
Update mh_lpa_how_to_check_language_master SET English=N'Look at deviation permit and check if it is followed. Ask the people what and why they have to do this.' WHERE question_id=22
Update mh_lpa_how_to_check_language_master SET English=N'Look at RPS board, talk to the people how it is used' WHERE question_id=23
Update mh_lpa_how_to_check_language_master SET English=N'Talk to the people about the last complaint, do they know it? What has changed and what happens?' WHERE question_id=24
Update mh_lpa_how_to_check_language_master SET English=N'Look at the visualization of the line KPIs. What are the targets? What happens if the value was below the target? Compare with Line RPS' WHERE question_id=25
GO

Update mh_lpa_how_to_check_language_master SET czech=N'Podívejte se na formulář předchozího auditu a zkontrolujte, zda jsou zjištění uzavřena.' WHERE question_id=1
Update mh_lpa_how_to_check_language_master SET czech=N'Porovnejte oba dokumenty. Namátková kontrola.' WHERE question_id=2
Update mh_lpa_how_to_check_language_master SET czech=N'Vyžádejte si jména operátorů a zkontrolujte kvalifikační matici a formulář Hodnocení dělníka na pracovišti pro tyto operátory. Na místě by měl být minimálně jeden operátor úrovně O pro zaškolení.' WHERE question_id=3
Update mh_lpa_how_to_check_language_master SET czech=N'Podívejte se na vizuální pracovní návodku. Zkontrolujte, jaké kontroly kvality jsou na daném místě nasazeny. Zeptejte se operátorů, co pro kontrolu potřebují a jaké kritické položky je nutné kontrolovat.' WHERE question_id=4
Update mh_lpa_how_to_check_language_master SET czech=N'Zkontrolujte danou oblast, stroje a zařízení.' WHERE question_id=5
Update mh_lpa_how_to_check_language_master SET czech=N'Zkontrolujte danou oblast.' WHERE question_id=6
Update mh_lpa_how_to_check_language_master SET czech=N'Zkontrolujte přísun materiálu, způsob dodávání dílů a komponentů na linku.' WHERE question_id=7
Update mh_lpa_how_to_check_language_master SET czech=N'Podívejte se na přísun materiálu pro jednotlivé materiály. Jak funguje doplňování dílů?' WHERE question_id=8
Update mh_lpa_how_to_check_language_master SET czech=N'Zkontrolujte danou oblast.' WHERE question_id=9
Update mh_lpa_how_to_check_language_master SET czech=N'Podívejte se na vizuální pracovní návodku, které bezpečnostní zařízení jsou potřebné a zeptejte se, jaké jsou všeobecné bezpečnostní pravidla pro dané pracoviště. Zkontrolujte, zda operátoři tyto pravidla dodržují. Prozkoumejte celou linku z hlediska možných bezpečnostních rizik. Podívejte se na plán TPM, zda jsou bezpečnostní zařízení kontrolována v pravidelných intervalech. Kdy byly kontrolovány naposled?' WHERE question_id=10
Update mh_lpa_how_to_check_language_master SET czech=N'Porovnejte pokyny TPM a podívejte se na kontrolní seznam, zda byly vykonány. Námatkově zkotrolujte, zda se úkony skutečně vykonávají (kupř. čištění).' WHERE question_id=11
Update mh_lpa_how_to_check_language_master SET czech=N'Podívejte se v pokynech pro zahájení, zda byly zkontrolovány opatření Poka Yoke. Námatkově ukontrolujte pomocí vzorového dílu.' WHERE question_id=12
Update mh_lpa_how_to_check_language_master SET czech=N'Zkontrolujte kontrolní seznam pro zahájení výroby vystavený na lince. Zkontrolujte položky, které nebyly OK a jaká byla na to reakce.' WHERE question_id=13
Update mh_lpa_how_to_check_language_master SET czech=N'Podívejte se na formulář s pokyny pro zahájení výroby a porovnejte je s prvním kusem. Podívejte se na štítek (etiketu) prvního kusu.' WHERE question_id=14
Update mh_lpa_how_to_check_language_master SET czech=N'Pracovní schéma vystavené na lince, vizuální pracovní pokyny vystavené v dohledu operátora. Porovnejte schéma práce a pracovní pokyny se skutečnou situací.' WHERE question_id=15
Update mh_lpa_how_to_check_language_master SET czech=N'Podívejte se na linku z hlediska plýtvání (čekání, vyrovnávací prostory atd.). Porovnejte schéma práce se skutečnou situací. Změřte čas a propustnost (výkon) linky.' WHERE question_id=16
Update mh_lpa_how_to_check_language_master SET czech=N'Zkontrolujte pokyny pro zahájení výroby. Jaké jsou reakce? Zeptejte se pracovníků, co dělají v případě chybných dílů nebo prostojů.' WHERE question_id=17
Update mh_lpa_how_to_check_language_master SET czech=N'Porovnejte vizuální pracovní návodky s hraničními vzorky.' WHERE question_id=18
Update mh_lpa_how_to_check_language_master SET czech=N'Podívejte se na pracoviště, kde se díly balí. Podívejte se, jak by měly být baleny v souladu s pokyny.' WHERE question_id=19
Update mh_lpa_how_to_check_language_master SET czech=N'Podívejte se na pracovní místa, zeptejte se pracovníků, co dělají v případě chybných dílů.' WHERE question_id=20
Update mh_lpa_how_to_check_language_master SET czech=N'Podívejte se do oblasti pro opravy. Zkontrolujte pracovní pokyny a zeptejte se pracovníků, jak funguje tok materiálu.' WHERE question_id=21
Update mh_lpa_how_to_check_language_master SET czech=N'Podívejte se na odchylku a zkontrolujte, zda se dodržuje. Zeptejte se pracovníků, co musí kvůli ní dělat a proč.' WHERE question_id=22
Update mh_lpa_how_to_check_language_master SET czech=N'Podívejte se na tabuli RPS. Promluvte si s pracovníky, jak se používá.' WHERE question_id=23
Update mh_lpa_how_to_check_language_master SET czech=N'Promluvte si s pracovníky o poslední reklamaci, zda ji znají. Co se změnilo a co se bude dít?' WHERE question_id=24
Update mh_lpa_how_to_check_language_master SET czech=N'Podívejte se na vizualizaci KPI na lince. Jaké jsou cíle? Co se stane, když byla hodnota pod cílovou hodnotou? Porovnejte s RPS na lince.' WHERE question_id=25
GO

Update mh_lpa_how_to_check_language_master SET German=N'Schau den vorherigen Auditfragebogen an und prüfe ob alle Feststellungen bearbeitet und geschlossen sind.' WHERE question_id=1
Update mh_lpa_how_to_check_language_master SET German=N'Vergleiche beide Dokumente. Stichprobenprüfung' WHERE question_id=2
Update mh_lpa_how_to_check_language_master SET German=N'Frage nach dem Namen des Mitarbeiters und prüfe Qualifikationsmatrix und Validierungsblatt für diesen Mitarbeiter. Mindestens ein Werker mit Level O sollte für Trainings vorhanden sein.' WHERE question_id=3
Update mh_lpa_how_to_check_language_master SET German=N'Schau auf die bildliche Arbeitsanweisung. Prüfe, welche Qualitätsprüfungen vorhanden sind. Frag den Werker, was er prüfen muss.' WHERE question_id=4
Update mh_lpa_how_to_check_language_master SET German=N'Überprüfe den Bereich, die Maschinen und Einrichtungen.' WHERE question_id=5
Update mh_lpa_how_to_check_language_master SET German=N'Überprüfe den Bereich.' WHERE question_id=6
Update mh_lpa_how_to_check_language_master SET German=N'Überprüfe das bereitgestellte Material, wie werden die Teile und Komponenten in der Linie bereitgestellt.' WHERE question_id=7
Update mh_lpa_how_to_check_language_master SET German=N'Schaue die Materialversorgung für die Teile an. Wie wird nachgefüllt?' WHERE question_id=8
Update mh_lpa_how_to_check_language_master SET German=N'Überprüfe den Bereich' WHERE question_id=9
Update mh_lpa_how_to_check_language_master SET German=N'Saue auf die bildliche Arbeitsanweisung, welche Schutzausrüstung notwendig ist und frage welche generellen Sicherheitsregeln für das Werk gelten. Prüfe, ob der Werker die Regeln befolgt. Überprüfe die gesamte Line auf Sicherheitsrisiken. Prüfe den TPM Plan, ob Sicherheitsausrüstung regelmäßig überprüft wird. Wann war das letzte Mal?' WHERE question_id=10
Update mh_lpa_how_to_check_language_master SET German=N'Vergleiche die TPM Anweisung und prüfe das "Unterschriftenblatt", ob die Tätigkeiten durchgeführt wurden. Stichprobenprüfen, ob die Tätigkeiten wirklich durchgeführt wurden (z.B. Reinigen)' WHERE question_id=11
Update mh_lpa_how_to_check_language_master SET German=N'Schaue auf die Produktionsfreigabechecklist, ob die Poka Yoke Einrichtungen geprüft wurden. Führe eine Stichprobe mit einem Masterteildurch.' WHERE question_id=12
Update mh_lpa_how_to_check_language_master SET German=N'Prüfe die Produktionsfreigabecheckliste an der Linie.  Prüfe welche Themen nicht in Ordnung waren und welche Reaktionen danach stattgefunden haben.' WHERE question_id=13
Update mh_lpa_how_to_check_language_master SET German=N'Schaue auf die Produktionsfreigabechecklist und vergleiche diese mit dem Erststück. Schaue auf die Beschriftung des Erstteils.' WHERE question_id=14
Update mh_lpa_how_to_check_language_master SET German=N'Standarisierte Arbeitskarte soll an der Linie ausgehängt sein, bildliche Arbeitsanweisung sollte im Blickbereich des jeweiligen Mitarbeiters angebracht sein. Vergleiche Arbeitskarte und Arbeitsanweisung mit der Realität.' WHERE question_id=15
Update mh_lpa_how_to_check_language_master SET German=N'Schaue in der Linie nach Verschwendung (Wartezeit, Pufferlager usw.) Vergleich Arbeitskarte und Realität. Messe die Zeit und die Ausbringung der Linie.' WHERE question_id=16
Update mh_lpa_how_to_check_language_master SET German=N'Prüfe die Produktionsfreigabe, wie sind die Reaktionen hier definiert? Frage die Werker, was sie tun müssen, wenn sie ein fehlerhaftes Teil finden oder die Anlage auf Störung geht?' WHERE question_id=17
Update mh_lpa_how_to_check_language_master SET German=N'Vergleiche die bildliche Arbeitsanweisung mit den Grenzmustern.' WHERE question_id=18
Update mh_lpa_how_to_check_language_master SET German=N'Schaue den Verpackplatz an. Wie werden die Teile Verpackt und wie sollten sie laut Verpackungsanweisung verpackt werden.' WHERE question_id=19
Update mh_lpa_how_to_check_language_master SET German=N'Schaue den Arbeitsplatz an. Frage die Werker, was sie mit schlechten Teilen machen.' WHERE question_id=20
Update mh_lpa_how_to_check_language_master SET German=N'Schaue den Nacharbeitsbereich an. Prüfe die Nacharbeitsanweisung und sprich mit den Werkern, wie der Materialfluss funktioniert.' WHERE question_id=21
Update mh_lpa_how_to_check_language_master SET German=N'Schau dir eine Abweichungsgenehmigung an und prüfe, ob sie befolgt wird. Sprich mit den Werker was und warum sie etwas zu tun haben.' WHERE question_id=22
Update mh_lpa_how_to_check_language_master SET German=N'Schaue das Linie PRS Board an und sprich mit den Werkern, wie diese benutzt wird.' WHERE question_id=23
Update mh_lpa_how_to_check_language_master SET German=N'Sprich mit den Werkern über die letzte Reklamation, kennen sie diese? Was hat sich geändert und ist passiert?' WHERE question_id=24
Update mh_lpa_how_to_check_language_master SET German=N'Schaue auf die Visualisierung der KPIs. Was sind die Zielwerte? Was passiert, wenn die Werte unter Ziel sind? Vergleich mit dem Linie RPS Board.' WHERE question_id=25
GO

Update mh_lpa_how_to_check_language_master SET Spanish=N'Examinar el formulario de auditoría anterior y comprobar si los resultados están cerrados' WHERE Question_id=1
Update mh_lpa_how_to_check_language_master SET Spanish=N'Comparar ambos documentos. Comprobar ejemplo' WHERE Question_id=2
Update mh_lpa_how_to_check_language_master SET Spanish=N'Preguntar los nombres de los operarios y comprobar la matriz de calificación y la hoja de validación de estos operarios.Debe estar presente como mínimo un operario nivel O para la formación.' WHERE Question_id=3
Update mh_lpa_how_to_check_language_master SET Spanish=N'Examinar la instrucción de trabajo visual. Comprobar qué comprobaciones de calidad existen. Preguntar a los operarios qué necesitan comprobar y qué tema crítico necesita comprobarse.' WHERE Question_id=4
Update mh_lpa_how_to_check_language_master SET Spanish=N'Inspeccionar el área, las máquinas y el equipo.' WHERE Question_id=5
Update mh_lpa_how_to_check_language_master SET Spanish=N'Inspeccionar el área' WHERE Question_id=6
Update mh_lpa_how_to_check_language_master SET Spanish=N'Inspeccionar el suministro de material, cómo se suministran piezas y componentes a la línea.' WHERE Question_id=7
Update mh_lpa_how_to_check_language_master SET Spanish=N'Examinar el suministro de material de cada material. ¿Cómo es la reposición de las piezas?' WHERE Question_id=8
Update mh_lpa_how_to_check_language_master SET Spanish=N'Inspeccionar el área' WHERE Question_id=9
Update mh_lpa_how_to_check_language_master SET Spanish=N'Examinar la instrucción de trabajo visual, qué equipo de seguridad es necesario y preguntar cuáles son las normas de seguridad generales de la planta. Comprobar si los operarios están siguiendo estas normas. Inspeccionar los riesgos de seguridad en toda la línea. Buscar en el plan de TPM si el equipo de seguridad se comprueba de forma regular. ¿Cuándo fue la última vez?' WHERE Question_id=10
Update mh_lpa_how_to_check_language_master SET Spanish=N'Comparar la instrucción TPM y mirar en la hoja de comprobación si se hizo. Muestrear si se han hecho realmente las tareas (p. ej., la limpieza)' WHERE Question_id=11
Update mh_lpa_how_to_check_language_master SET Spanish=N'Mirar en la instrucción de puesta en marcha si se comprobaron los Poka Yokes. Muestrear con una pieza maestra.' WHERE Question_id=12
Update mh_lpa_how_to_check_language_master SET Spanish=N'Comprobar la lista de comprobación de puesta en marcha fijada en la línea. Comprobar el punto que no estaba OK y cómo se reaccionó a ello.' WHERE Question_id=13
Update mh_lpa_how_to_check_language_master SET Spanish=N'Examinar el formulario de la instrucción de trabajo y comparar con la primera pieza. Examinar la etiqueta en la primera pieza.' WHERE Question_id=14
Update mh_lpa_how_to_check_language_master SET Spanish=N'Gráfico de trabajo fijado en la línea, instrucción de trabajo visual fijada a la vista del operario. Comparar el gráfico de trabajo y la instrucción de trabajo con la situación real.' WHERE Question_id=15
Update mh_lpa_how_to_check_language_master SET Spanish=N'Examinar pérdidas en la línea (espera, reguladores, etc.). Comparar el gráfico de trabajo y la situación real. Medir el tiempo y la producción de la línea.' WHERE Question_id=16
Update mh_lpa_how_to_check_language_master SET Spanish=N'Comprobar la instrucción de puesta en marcha, ¿cuáles son las reacciones? ¿Preguntar al personal qué hacer en caso de piezas malas o averías?' WHERE Question_id=17
Update mh_lpa_how_to_check_language_master SET Spanish=N'Comparar la instrucción de trabajo visual con las muestras de límite.' WHERE Question_id=18
Update mh_lpa_how_to_check_language_master SET Spanish=N'Examinar el área donde se embalan las piezas Mirar cómo se embalan las piezas y cómo se deben embalar con arreglo a la instrucción.' WHERE Question_id=19
Update mh_lpa_how_to_check_language_master SET Spanish=N'Examinar los puestos de trabajo. preguntar al personal qué hacen en caso de una pieza mala.' WHERE Question_id=20
Update mh_lpa_how_to_check_language_master SET Spanish=N'Examinar el área de reprocesado. Comprobar la instrucción de trabajo y preguntar al personal cómo funciona el flujo de material.' WHERE Question_id=21
Update mh_lpa_how_to_check_language_master SET Spanish=N'Examinar el permiso de desviación y comprobar si se sigue. Preguntar al personal qué y por qué tienen que hacer esto.' WHERE Question_id=22
Update mh_lpa_how_to_check_language_master SET Spanish=N'Examinar el tablero de RPS, decir al personal cómo se usa.' WHERE Question_id=23
Update mh_lpa_how_to_check_language_master SET Spanish=N'Hablar con el personal sobre la última reclamación; ¿la conocen? ¿Qué ha cambiado y qué sucede?' WHERE Question_id=24
Update mh_lpa_how_to_check_language_master SET Spanish=N'Examinar la visualización de los KPI de la línea. ¿Cuáles son los objetivos? ¿Qué sucede si el valor era inferior a los objetivos? Comparar con la RPS de la línea.' WHERE Question_id=25
GO

Update mh_lpa_how_to_check_language_master SET Chinese=N'Look at previous Audit form and check if findings are closed查看之前的审核表并检查结果是否关闭。' WHERE Question_id=1
Update mh_lpa_how_to_check_language_master SET Chinese=N'Compare both documents. Sample check 对比两个文件，抽查。' WHERE Question_id=2
Update mh_lpa_how_to_check_language_master SET Chinese=N'Ask for the operators names and check Qualification Matrix and Validation Sheet for this operators. Minium one O-Level operator should be in place for the training.询问操作员姓名，在质量矩阵和确认表中查找该操作员。至少一个O-级能力的操作员。' WHERE Question_id=3
Update mh_lpa_how_to_check_language_master SET Chinese=N'Look at visual work instruction. Check which quality checks are in place. Ask operators what they need to check and which critical topic need to be checked.查看可视化作业指导书。检查质量检查清单在现场。询问操作员需要检查什么，那个关键点需要检查。' WHERE Question_id=4
Update mh_lpa_how_to_check_language_master SET Chinese=N'Inspect the area, the machines and equipment检查工作区域，机器和设备。' WHERE Question_id=5
Update mh_lpa_how_to_check_language_master SET Chinese=N'Inspect the area 检查区域' WHERE Question_id=6
Update mh_lpa_how_to_check_language_master SET Chinese=N'Inspect the material supply, how are parts and components provided to the line.检查材料的供给，零件是如何提供到产线上的？' WHERE Question_id=7
Update mh_lpa_how_to_check_language_master SET Chinese=N'Look at the material supply  for each material. How is the replenishment of the parts?检查各个材料的供给。该零件的补给如何？' WHERE Question_id=8
Update mh_lpa_how_to_check_language_master SET Chinese=N'Inspect the area 检查区域' WHERE Question_id=9
Update mh_lpa_how_to_check_language_master SET Chinese=N'look at the visual work instruction, which safety equipment is necessary and ask which are the general safety rules for the site. Check if the operators are following this rules. Inspect the whole line for any safety risks. Look in the TPM plan if the safety equipment is checked on a regular bases. When was the last time?查看可视化工作条例，哪个安全设备是必须的，询问该工作场所的一般安全守则。检查操作员是否遵循该守则。检查整个生产线是否有安全隐患。查看TPM计划，安全设备是否定期检查。最后一次检查是什么时候。' WHERE Question_id=10
Update mh_lpa_how_to_check_language_master SET Chinese=N'Compare TPM instruction and look at the check sheet if it was done. Sample checks if the tasks are really done (e.g. cleaning)对比TPM条例，查看检查表是否完成。如果任务都完成则（例如清洁）进行抽样检查。' WHERE Question_id=11
Update mh_lpa_how_to_check_language_master SET Chinese=N'Look in the startup instruction if the Poka Yokes were checked. Sample check with a master part.查看开始条例看是否对防错验证进行了检查。对主要部件进行抽样检查。' WHERE Question_id=12
Update mh_lpa_how_to_check_language_master SET Chinese=N'Check Startup checklist posted at the line. Check topic which were not OK and how the reaction was on this.检查生产线的生产启动检查表。检查是否有不合格以及如何采取相应措施 ' WHERE Question_id=13
Update mh_lpa_how_to_check_language_master SET Chinese=N'Look at the startup instruction form and compare with first piece. Look at the label of the first piece.查看生产启动检查表，和首件进行比较。查看首件的标签。' WHERE Question_id=14
Update mh_lpa_how_to_check_language_master SET Chinese=N'Work chart posted at the line, visual work instruction posted in the view of the operator. Compare work chart, work instruction with the real situation工作表贴在生产线上，可视化作业指导书贴在操作员可见的地方。将流程图，作业指导书和实际情况进行比较。' WHERE Question_id=15
Update mh_lpa_how_to_check_language_master SET Chinese=N'look at the line for waste (waiting, buffers etc.). Compare work chart and real situation. Measure the time and the out put of the line.查看浪费时间（等待，缓冲等）对比流程图和实际情况。测量时间和生产线输出。' WHERE Question_id=16
Update mh_lpa_how_to_check_language_master SET Chinese=N'Check startup instruction, what are the reactions? Ask people what to do in case of bad parts or break down?检查生产启动表，应急计划是什么？询问当出现不合格品或者停机时要怎么做？' WHERE Question_id=17
Update mh_lpa_how_to_check_language_master SET Chinese=N'Compare the visual work instruction with the boundary samples.对比可视化作业指导书和边界样本 ' WHERE Question_id=18
Update mh_lpa_how_to_check_language_master SET Chinese=N'Look at the area where the parts are packed look how parts are packed and how they should be packed according to the instruction.查看零件包装区域，查看零件是如何包装的，是否根据指导书进行的包装。' WHERE Question_id=19
Update mh_lpa_how_to_check_language_master SET Chinese=N'Look at the workplaces, ask the people what they are doing in case of a bad part.查看工作区域，询问员工当出现不好的零件时他们会怎么做。' WHERE Question_id=20
Update mh_lpa_how_to_check_language_master SET Chinese=N'Look at rework area. Check work instruction and ask people how the material flow works.查看返工区域。检查作业指导书，询问员工料质流如何运作的。' WHERE Question_id=21
Update mh_lpa_how_to_check_language_master SET Chinese=N'Look at deviation permit and check if it is followed. Ask the people what and why they have to do this.查看偏差申请表，检查其是否被遵循。询问员工他们做了什么，为什么要这样做' WHERE Question_id=22
Update mh_lpa_how_to_check_language_master SET Chinese=N'Look at RPS board, talk to the people how it is used查看RPS板，询问员工是如何使用它的。' WHERE Question_id=23
Update mh_lpa_how_to_check_language_master SET Chinese=N'Talk to the people about the last complaint, do they know it? What has changed and what happens?和员工讨论他们是否知道最新的抱怨。有什么变化？发生了什么？' WHERE Question_id=24
Update mh_lpa_how_to_check_language_master SET Chinese=N'Look at the visualization of the line KPIs. What are the targets? What happens if the value was below the target? Compare with Line RPS查看生产线可视化的KPIs。目标是什么？如果低于目标时要怎么做？对比在线RPS看有没有对策。' WHERE Question_id=25
GO

Update mh_lpa_how_to_check_language_master SET Korean=N'지난 심사 양식을 보고 지적사항에 대한 개선사항이 완료 되었는지 확인.' WHERE Question_id=1
Update mh_lpa_how_to_check_language_master SET Korean=N'두개 문서 비교 확인. 샘플지정하여 확인. ' WHERE Question_id=2
Update mh_lpa_how_to_check_language_master SET Korean=N'작업자의 이름을 물어본 뒤 그 작업자에 대한Qualification matrix 와 Validation sheet 매칭 확인.최소 한명의 O레벨 관리자가 교육을 위해 배치 되어야 함.' WHERE Question_id=3
Update mh_lpa_how_to_check_language_master SET Korean=N'시각적 지침서를 본 뒤 품질 확인 기준이 있는지 체크, 그들이 확인해야할 중요 항목 및 검사 항목이 무엇인지 작업자에게 직접 질문.' WHERE Question_id=4
Update mh_lpa_how_to_check_language_master SET Korean=N'바닥 및 정돈사항 확인, 기계와 설비 확인.' WHERE Question_id=5
Update mh_lpa_how_to_check_language_master SET Korean=N'현장 검사' WHERE Question_id=6
Update mh_lpa_how_to_check_language_master SET Korean=N'부품 공급확인, 부품들이 라인에 어떻게 제공되는지 확인..' WHERE Question_id=7
Update mh_lpa_how_to_check_language_master SET Korean=N'각 부품에 대한 부품 공급확인.어떻게 부품이 보충되는지 확인.' WHERE Question_id=8
Update mh_lpa_how_to_check_language_master SET Korean=N'현장 검사' WHERE Question_id=9
Update mh_lpa_how_to_check_language_master SET Korean=N'시각적 지침서 확인 하여 안전보호 장비가 요구되는지 확인 그리고 일반적인 안전 규칙이 있는지 질문. 작업자가 규칙을 준수하고 있는지 확인. 안전 리스크가 있는지 검사. TPM 계획을 보고 안전설비가 정기적으로 확인되고 최근의 검사일은 언제인지 확인.' WHERE Question_id=10
Update mh_lpa_how_to_check_language_master SET Korean=N'TPM 지침과 설비점검체크시트 비교확인. 체크시트가 올바르게 작성되었는지 확인.(예, 청소)' WHERE Question_id=11
Update mh_lpa_how_to_check_language_master SET Korean=N'스타트업체크시트 상 Eroor proofing 확인이 되어 있는지 확인. 마스터샘플부품으로 체크.' WHERE Question_id=12
Update mh_lpa_how_to_check_language_master SET Korean=N'라인에 게시되어 있는 스타트업 체크시트 확인. Not ok  되는 이슈가 발생 했을 시 어떤 조치를 취하는지 스타트업체크시트 뒷장의 reaction plan 확인 및 조반장에게 해당사항 인지 여부.' WHERE Question_id=13
Update mh_lpa_how_to_check_language_master SET Korean=N'스타트업체크시트 와 초물비교. 초물에 라벨 및 초물 식별 확인.' WHERE Question_id=14
Update mh_lpa_how_to_check_language_master SET Korean=N'Work chart 가 게시되었는지 확인, 시각적 작업 지침서가 작업자의 시각에 위치해 있는지. 시각적 지침서와 work chart 를 비교해서 실제의 작업 업무와 일치 한지 확인.' WHERE Question_id=15
Update mh_lpa_how_to_check_language_master SET Korean=N'임의의 공정의 시각적 지침서를 선택하여 실제 업무와 확인.' WHERE Question_id=16
Update mh_lpa_how_to_check_language_master SET Korean=N'스타트업 체크시트 확인하여 각 항목별 조치사항 확인, 작업자 및 관리자에게 불량품 혹은 설비 고장시 조치사항이 어떠한 것인지 질문.(작업자와 관리자가 실제 조치사항을 인지하고 있는지 확인)' WHERE Question_id=17
Update mh_lpa_how_to_check_language_master SET Korean=N'한도견본 샘플과 시각적 작업 지침서 비교.' WHERE Question_id=18
Update mh_lpa_how_to_check_language_master SET Korean=N'제품이 포장되고 있는 공정으로 가서 지침서에 따라 포장작업이 이루어 지는지 확인.' WHERE Question_id=19
Update mh_lpa_how_to_check_language_master SET Korean=N'해당 부적합품박스로 가서 부적합품 발견시 후속조치를 어떻게 하는지 작업자에게 질문.부적합품이 테크로 식별이 되어 보관되어 있는지 확인.' WHERE Question_id=20
Update mh_lpa_how_to_check_language_master SET Korean=N'리워크 장소확인. 지침서와 제품의 입/출고 흐름에 대해 작업자에게 질문.' WHERE Question_id=21
Update mh_lpa_how_to_check_language_master SET Korean=N'특채허가가 있을 시 지침에 따르는지 확인한다. 작업자에게 무엇을 , 왜 하는지 질문 할 수 있다.' WHERE Question_id=22
Update mh_lpa_how_to_check_language_master SET Korean=N'RPS 보드판을 보고 작업자 및 관리자에게 어떻게 사용되고 있는지 확인.' WHERE Question_id=23
Update mh_lpa_how_to_check_language_master SET Korean=N'작업자 혹은 관리자에게 최근의 고객불만에 대해 인지하고 있는지 질문. 어떤 변화와 개선이 있었는지 확인.' WHERE Question_id=24
Update mh_lpa_how_to_check_language_master SET Korean=N'라인 회전 게시판의 KPI 운영성과를 확인한다.목표가 얼마인지? 목표 미 달성일 경우 조치 사항은? Line RPS 기입 여부 ?' WHERE Question_id=25
GO

Update mh_lpa_how_to_check_language_master SET Thai=N'Look at previous Audit form and check if findings are closed เช็คดูผลการaudit ก่อนหน้าและดูว่าได้ปิดไปหรือยัง。' WHERE Question_id=1
Update mh_lpa_how_to_check_language_master SET Thai=N'Compare both documents. Sample check  เปรียบเทียบเอกสาร โดยการสุ่มเช็ค。' WHERE Question_id=2
Update mh_lpa_how_to_check_language_master SET Thai=N'Ask for the operators names and check Qualification Matrix and Validation Sheet for this operators. Minium one O-Level operator should be in place for the training. สอบถามพนักงานและเช็คตารางคุณสมบัติและใบประเมิน อย่างน้อยต้องมีระดับ O เพื่อที่จะสามารถสอนงานได้ 。' WHERE Question_id=3
Update mh_lpa_how_to_check_language_master SET Thai=N'Look at visual work instruction. Check which quality checks are in place. Ask operators what they need to check and which critical topic need to be checked. สอบถามพนักงานว่ามีการตรวจเช็คงานอย่างไร เปรียนเทียบกับ WI 。' WHERE Question_id=4
Update mh_lpa_how_to_check_language_master SET Thai=N'Inspect the area, the machines and equipment. ตรวจเช็คสภาพทั่วไปของเครื่องจักร。' WHERE Question_id=5
Update mh_lpa_how_to_check_language_master SET Thai=N'Inspect the area . ตรวจสอบพื้นที่ทำงาน' WHERE Question_id=6
Update mh_lpa_how_to_check_language_master SET Thai=N'Inspect the material supply, how are parts and components provided to the line. ตรวจเช็คกล่องใส่ชิ้นงาน？' WHERE Question_id=7
Update mh_lpa_how_to_check_language_master SET Thai=N'Look at the material supply  for each material. How is the replenishment of the parts? ตรวจสอบชิ้นงานที่มาส่งเข้าไลน์' WHERE Question_id=8
Update mh_lpa_how_to_check_language_master SET Thai=N'Inspect the area ตรวจสอบพื้นที่ทำงาน' WHERE Question_id=9
Update mh_lpa_how_to_check_language_master SET Thai=N'look at the visual work instruction, which safety equipment is necessary and ask which are the general safety rules for the site. Check if the operators are following this rules. Inspect the whole line for any safety risks. Look in the TPM plan if the safety equipment is checked on a regular bases. When was the last time? ตรวจเช็ค WI, TPM ว่าพนักงานได้ปฏิบัติตามหรือไม่ รวมถึงสุ่มเช็คว่ามีการตรวจเช็คล่าสุดเมื่อไหร่ 。' WHERE Question_id=10
Update mh_lpa_how_to_check_language_master SET Thai=N'Compare TPM instruction and look at the check sheet if it was done. Sample checks if the tasks are really done (e.g. cleaning) เปรียบเทียบ TPM กับการปฏิบัติจริง โดยการสุ่มเช็ค。' WHERE Question_id=11
Update mh_lpa_how_to_check_language_master SET Thai=N'Look in the startup instruction if the Poka Yokes were checked. Sample check with a master part. เช็ค start up ว่าได้รวมการตรวจเช็ค Pokayoke หรือไม่ ลองสุ่มเช็คกับมาสเตอร์。' WHERE Question_id=12
Update mh_lpa_how_to_check_language_master SET Thai=N'Check Startup checklist posted at the line. Check topic which were not OK and how the reaction was on this. เช็ค start up ที่อยู่ในไลน์ ในกรณี NOK เช็คดูว่ามีการแก้ไขอย่างไร' WHERE Question_id=13
Update mh_lpa_how_to_check_language_master SET Thai=N'Look at the startup instruction form and compare with first piece. Look at the label of the first piece. เช็ค start up sheet เปรียบเทียบกับงานตัวแรก เช็คป้ายระบุงานชิ้นแรก。' WHERE Question_id=14
Update mh_lpa_how_to_check_language_master SET Thai=N'Work chart posted at the line, visual work instruction posted in the view of the operator. Compare work chart, work instruction with the real situation. มาตรฐานงาน std. work chart ต้องติดอยู่ในพื้นที่ทำงาน WI ต้องติดอยู่ด้านหน้าพนักงานที่สามารถมองเห็นได้ชัด ให้ทำการเปรียบเทียบ WI กับ std. work chart。' WHERE Question_id=15
Update mh_lpa_how_to_check_language_master SET Thai=N'look at the line for waste (waiting, buffers etc.). Compare work chart and real situation. Measure the time and the out put of the line. มองหาการสูญเสียในไลน์ (การรอ สต็อกค้างไลน์) ให้เช็คเวลาทำงานเปรียบเทียบกับงานมาตรฐาน。' WHERE Question_id=16
Update mh_lpa_how_to_check_language_master SET Thai=N'Check startup instruction, what are the reactions? Ask people what to do in case of bad parts or break down? เช็ค start up sheet ถามพนักงานในกรณีมีปัญหา ว่าทำอย่างไร บันทึกปัญหาอย่างไร？' WHERE Question_id=17
Update mh_lpa_how_to_check_language_master SET Thai=N'Compare the visual work instruction with the boundary samples.เปรียบเทียบ WI กับตัวอย่างลิมิตงานดี กับไม่ดี' WHERE Question_id=18
Update mh_lpa_how_to_check_language_master SET Thai=N'Look at the area where the parts are packed look how parts are packed and how they should be packed according to the instruction.เช็คการแพ็คงาน ว่าเป็นไปตามมาตรฐานที่เขียนไว้หรือไม่。' WHERE Question_id=19
Update mh_lpa_how_to_check_language_master SET Thai=N'Look at the workplaces, ask the people what they are doing in case of a bad part. สอบถามพนักงาน ว่าทำอย่างไรในกรณีเจองานเสีย。' WHERE Question_id=20
Update mh_lpa_how_to_check_language_master SET Thai=N'Look at rework area. Check work instruction and ask people how the material flow works. ที่พื้นที่ซ่อมงาน สอบถามพนักงานว่า มีขั้นตอนจัดการกับงานที่จะซ่อมอย่างไร。' WHERE Question_id=21
Update mh_lpa_how_to_check_language_master SET Thai=N'Look at deviation permit and check if it is followed. Ask the people what and why they have to do this. เช็คดูใบอนุญาตขอใช้ งานที่ไม่เป็นไปตามข้อกำหนด ถามพนักงานว่าต้องทำอย่างอะไร ทำอย่างไรบ้าง' WHERE Question_id=22
Update mh_lpa_how_to_check_language_master SET Thai=N'Look at RPS board, talk to the people how it is used ที่บอร์ด RPS ถามพนักงานว่าใช้งานอย่างไร。' WHERE Question_id=23
Update mh_lpa_how_to_check_language_master SET Thai=N'Talk to the people about the last complaint, do they know it? What has changed and what happens? ถามพนักงานเกี่ยวกับข้อร้องเรียนจากลูกค้า ล่าสุด ว่าเกิดอะไรขึ้น และมีการแก้ไอย่างไร？' WHERE Question_id=24
Update mh_lpa_how_to_check_language_master SET Thai=N'Look at the visualization of the line KPIs. What are the targets? What happens if the value was below the target? Compare with Line RPS ที่บอร์ดแสดงผลการปฏิบัติงาน KPI เป้าหมายคืออะไร ทำอย่างไรเมื่องานไม่ได้ตามเป้าหมายที่กำหนด เปรียบเทียบกับ RPS ว่าได้มีการนำมาบันทึกเพื่อการติดตามหรือไม่。' WHERE Question_id=25
GO
Update mh_lpa_how_to_check_language_master SET Bosnian=N'Pogledaj upitnik predhodnog audita i provjeri da li su sva odstupanja obrađena i zatvorena' WHERE Question_id=1
Update mh_lpa_how_to_check_language_master SET Bosnian=N'Uporedite oba dokumenta. Metod slučajnog uzorka.' WHERE Question_id=2
Update mh_lpa_how_to_check_language_master SET Bosnian=N'Pitaj za ime operatera i provjeri matricu kvalifikacija i obrazac za validaciju tog ooperatera. Minimalno treba postojati  jedan operater sa  nivom O radi  vršenja obuke.' WHERE Question_id=3
Update mh_lpa_how_to_check_language_master SET Bosnian=N'Pogledaj na vizuelno radno uputstvo. Provjeri koje kontrole kvaliteta postoje. Pitaj operatera šta treba da provjerava.' WHERE Question_id=4
Update mh_lpa_how_to_check_language_master SET Bosnian=N'Pregledajte područje, mašine i opremu' WHERE Question_id=5
Update mh_lpa_how_to_check_language_master SET Bosnian=N'Pregledajte područje ' WHERE Question_id=6
Update mh_lpa_how_to_check_language_master SET Bosnian=N'Pregledajte opskrbu materijalom, kako se dijelovi i komponente dostavljaju na liniju.' WHERE Question_id=7
Update mh_lpa_how_to_check_language_master SET Bosnian=N'Pogledaj opsrkbu materijalom za dijelove. Kako se dopunjava?' WHERE Question_id=8
Update mh_lpa_how_to_check_language_master SET Bosnian=N'Pregledajte područje ' WHERE Question_id=9
Update mh_lpa_how_to_check_language_master SET Bosnian=N'Pogledajte vizualno radno uputstvo, koja zaštitna oprema je neophodna i pitajtekoja  generalna sigurnosna pravila važe za ovu lokaciju. Provjerite da li operateri slijede ova pravila. Pregledajte cijelu liniju po pitanju sigurnosnih rizika. Pogledajte TPM plan,da li se sigurnosna oprema redovno provjerava. Kada je bila posljednja provjera ?' WHERE Question_id=10
Update mh_lpa_how_to_check_language_master SET Bosnian=N'Uporedite TPM uputstvo i provjerite obrazac sa potpisom, da li su zadatci odrađeni. Metodom slučajnog uzorka provjerite da li su zadatci u stvarnosti odrađeni (npr. čišćenje). ' WHERE Question_id=11
Update mh_lpa_how_to_check_language_master SET Bosnian=N'Pogledaj na checklisti odobrenja procesa proizvodnje,  da li se Poka Yoke uređaji  provjeravaju. Provjerite metodom slučajnog uzorka uz pomoć kontrolnog etalona.' WHERE Question_id=12
Update mh_lpa_how_to_check_language_master SET Bosnian=N'Provjeri  checklistu odobrenja procesa proizvodnje   na liniji. Provjeri koje teme  nisu bile OK i koje su reakcije uslijedile nakon toga.' WHERE Question_id=13
Update mh_lpa_how_to_check_language_master SET Bosnian=N'Pogledaj na checklisti odobrenja procesa proizvodnje i uporedi iste  sa prvim komadom. Pogledajte kako je obilježen prvi komada.' WHERE Question_id=14
Update mh_lpa_how_to_check_language_master SET Bosnian=N'Standardizovana radna karta treba da se nalazi na liniji , vizuelno radno uputstvo treba da se nalazi u vidnom području svakog zaposlenika . Usporedi radnu kartu i radno uputstvo sa realnošću. ' WHERE Question_id=15
Update mh_lpa_how_to_check_language_master SET Bosnian=N'Pogledajte rasipanja  na liniji (čekanja, baferi, itd.). Uporedite radnu kartu i realnost . Izmjerite vrijeme i ostvarenje  linije.' WHERE Question_id=16
Update mh_lpa_how_to_check_language_master SET Bosnian=N'Provjerite odobrenje procesa proizvodnje,  kako su ovdje definisane reakcije? Pitajte radnike, šta moraju raditi ako nađu loš dio ili ako na mašini dođe do zastoja ?' WHERE Question_id=17
Update mh_lpa_how_to_check_language_master SET Bosnian=N'Uporedite vizualnu radna uputstva sa  graničnim uzorcima.' WHERE Question_id=18
Update mh_lpa_how_to_check_language_master SET Bosnian=N'Pogledajte mjesto za pakovanje. Kako se pakuju dijelovi  i  kako treba da se pakuju prema uputstvu za pakovanje . ' WHERE Question_id=19
Update mh_lpa_how_to_check_language_master SET Bosnian=N'Pogledajte radno mjesto.  Pitajte radnike, šta rade sa lošim dijelovima. ' WHERE Question_id=20
Update mh_lpa_how_to_check_language_master SET Bosnian=N'Pogledajte područje dorade. Provjerite radno uputstvo za doradu i popričajte sa radnicima,  šta rade sa lošim dijelovima.' WHERE Question_id=21
Update mh_lpa_how_to_check_language_master SET Bosnian=N'Pogledajte jednu dozvolu za odstupanje i provjerite, da li se poštuje. Porazgovarajte sa radnicima šta i zašto treba to da urade.   ' WHERE Question_id=22
Update mh_lpa_how_to_check_language_master SET Bosnian=N'Pogledajte linijski  RPS i popričajte sa radnicima, kako se isti koristi.   ' WHERE Question_id=23
Update mh_lpa_how_to_check_language_master SET Bosnian=N'Popričajte sa radnicima o zadnjoj reklamaciji, da li je poznaju? Šta se izmjenilo i šta se desilo?' WHERE Question_id=24
Update mh_lpa_how_to_check_language_master SET Bosnian=N'Pogledajte  vizualizaciju KPI-ova.  Šta su ciljane  vrijednosti ? Šta se desi, kada su vrijednosti ispod cilja? Uporedite sa linijskim RPS-om.' WHERE Question_id=25
GO

Update mh_lpa_how_to_check_language_master SET Portuguese=N'Obervar o formulário da auditoria anterior e verificar se os resultados estão fechados' WHERE Question_id=1
Update mh_lpa_how_to_check_language_master SET Portuguese=N'Comparar ambos os documentos. Verificar amostra ' WHERE Question_id=2
Update mh_lpa_how_to_check_language_master SET Portuguese=N'Perguntar os nomes dos operadores e verificar a Matriz de Qualificação  e a  Folha de Validação para esses operações. No mínimo um operador Nível O deve estar no local para o treinamento.' WHERE Question_id=3
Update mh_lpa_how_to_check_language_master SET Portuguese=N'Observar a instrução de trabalho visual. Verificar quais verificações da qualidade estão em vigor. Perguntar aos operadores o que eles precisam verificar e qual o tópico crítico que precisa ser verificado.' WHERE Question_id=4
Update mh_lpa_how_to_check_language_master SET Portuguese=N'Inspecionar a área, as máquinas e equipamentos' WHERE Question_id=5
Update mh_lpa_how_to_check_language_master SET Portuguese=N'Inspecionar a área.' WHERE Question_id=6
Update mh_lpa_how_to_check_language_master SET Portuguese=N' Inspecionar o fornecimento do material, como estão as peças e componentes fornecidos para a linha.' WHERE Question_id=7
Update mh_lpa_how_to_check_language_master SET Portuguese=N'Observar o fornecimento de material para cada material. Como está a reposição das peças' WHERE Question_id=8
Update mh_lpa_how_to_check_language_master SET Portuguese=N'Inspecionar a área.' WHERE Question_id=9
Update mh_lpa_how_to_check_language_master SET Portuguese=N'Observar a instrução de trabalho visual, quais equipamentos de segurança são necessários e perguntar quais são as normas gerais de segurança para a planta. Verificar se os operadores estão seguindo as normas. Inspecionar a linha toda quanto a quaisquer riscos de segurança. Observar no plano TPM se os equipamentos de segurança são verificados regularmente. Quando foi a última vez?' WHERE Question_id=10
Update mh_lpa_how_to_check_language_master SET Portuguese=N'Comparar instrução TPM e observar na folha de verificação se isto foi feito. Verificações de amostras se as tarefas realmente são feitas (por ex.: limpeza)' WHERE Question_id=11
Update mh_lpa_how_to_check_language_master SET Portuguese=N'Observar a instrução de startup se os Poka Yokes foram verificados. Verificação de amostra com uma peça mestre.' WHERE Question_id=12
Update mh_lpa_how_to_check_language_master SET Portuguese=N'Verificar Lista de Verificação de Startup postada na linha. Verificar tópico que estava NOK e como foi a reação a isto.' WHERE Question_id=13
Update mh_lpa_how_to_check_language_master SET Portuguese=N'Observar o formulário de instrução de startup e comparar com a primeira peça. Observar a etiqueta da primeira peça.' WHERE Question_id=14
Update mh_lpa_how_to_check_language_master SET Portuguese=N'Gráfico de trabalho postado na linha, instrução de trabalho visual postado na vista do operador. Comparar o gráfico de trabalho, instrução de trabalho com a situação real.' WHERE Question_id=15
Update mh_lpa_how_to_check_language_master SET Portuguese=N'Observar a linha quanto a desperdícios (espera, buffers etc.). Comparar gráfico de trabalho e situação real. Medir o tempo e o rendimento/produção da linha.' WHERE Question_id=16
Update mh_lpa_how_to_check_language_master SET Portuguese=N'Verificar instrução de startup, quais são as reações? Perguntar às pessoas o que fazer em caso de peças ruins ou avarias?' WHERE Question_id=17
Update mh_lpa_how_to_check_language_master SET Portuguese=N'Comparar  a instrução de trabalho visual   com as amostras limite.' WHERE Question_id=18
Update mh_lpa_how_to_check_language_master SET Portuguese=N'Observar a área onde as peças são embaladas, observar se as peças são embaladas e como elas devem ser embaladas de acordo com a instrução.' WHERE Question_id=19
Update mh_lpa_how_to_check_language_master SET Portuguese=N'Observar nos locais de trabalho, perguntar às pessoas o que elas fazem em caso de peça ruim.' WHERE Question_id=20
Update mh_lpa_how_to_check_language_master SET Portuguese=N'Observar a área de retrabalho. Verificar a instrução de trabalho e perguntar às pessoas  como funciona o fluxo de material.' WHERE Question_id=21
Update mh_lpa_how_to_check_language_master SET Portuguese=N'Observar a autorização de desvio e verificar se ela é seguida. Perguntar às pessoas o que e por que elas têm que fazer isto.' WHERE Question_id=22
Update mh_lpa_how_to_check_language_master SET Portuguese=N'Observar o quadro RPS, conversar com as pessoas como ele é usado.' WHERE Question_id=23
Update mh_lpa_how_to_check_language_master SET Portuguese=N'Conversar com as pessoas sobre a última reclamação, eles sabem isto? O que mudou e o que acontece?' WHERE Question_id=24
Update mh_lpa_how_to_check_language_master SET Portuguese=N'Observar a visualização dos KPIs da linha. Quais são as metas? O que acontece se o valor estiver abaixo da meta? Comparar com a RPS da Linha.' WHERE Question_id=25
GO